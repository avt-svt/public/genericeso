/**
* @file Jacobian.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* Member definitions of class Jacobian                                 \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 5.8.2011
*/

#include "Jacobian.hpp"
#include <vector>
#include <assert.h>

using namespace std;

EsoIndex Jacobian::getNumEntries() const
{
  return m_rowIndices.size();
}

vector<EsoIndex>& Jacobian::getRowIndices()
{
  return m_rowIndices;
}

void Jacobian::setRowIndices(const std::vector<EsoIndex> &rowIndices)
{
  m_rowIndices.assign(rowIndices.begin(), rowIndices.end());
}

vector<EsoIndex>& Jacobian::getColIndices()
{
  return m_colIndices;
}

void Jacobian::setColIndices(const std::vector<EsoIndex> &colIndices)
{
  m_colIndices.assign(colIndices.begin(), colIndices.end());
}

void Jacobian::getSparsityPattern(const EsoIndex* indices, const EsoIndex lenInd,
                                  EsoIndex* rowIndices, EsoIndex* colIndices) const
{
  for(int i=0; i<lenInd; i++){
    assert((0 <= indices[i]) && (indices[i] < EsoIndex(m_rowIndices.size())));
  }

  for(int i=0; i<lenInd; i++){
    rowIndices[i] = m_rowIndices[indices[i]];
    colIndices[i] = m_colIndices[indices[i]];
  }
}
