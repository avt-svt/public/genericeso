#include "JADE2GenericEso.hpp"

JADE2GenericEso::JADE2GenericEso(const std::string& libname) : jade2Loader(libname) {
    model = jade2Loader.allocModel();
    model_name = libname;

    m_numVariables = jade2Loader.getNumVariables(AllVars::value);
    m_numStates = jade2Loader.getNumVariables(StateVars::value);
    m_numDiffVariables = jade2Loader.getNumVariables(DiffVars::value);
    m_numAlgVariables = jade2Loader.getNumVariables(AlgVars::value);
    m_numParameters = jade2Loader.getNumVariables(ParamVars::value);

    m_numEquations = jade2Loader.getNumEquations(AllEqns::value);
    m_numDiffEquations = jade2Loader.getNumEquations(DiffEqns::value);
    m_numAlgEquations = jade2Loader.getNumEquations(AlgEqns::value);

    jade2Loader.evalJacobianPattern(model, AllEqns::value, StateTimeParamVars::value, m_f_all);
    jade2Loader.evalJacobianPattern(model, AllEqns::value, StateTimeParamVars::value, m_initJacobian);
    jade2Loader.evalJacobianPattern(model, AllEqns::value, StateVars::value, m_f_x);
    jade2Loader.evalJacobianPattern(model, AllEqns::value, ParamVars::value, m_f_p);
    jade2Loader.evalJacobianPattern(model, AllEqns::value, DerDiffVars::value, m_f_derDiff);

    jade2Loader.evalJacobianValues(model, AllEqns::value, StateTimeParamVars::value, m_initJacobian);

    m_numNonZeros = m_f_all.nonZeros();
    m_numDiffNonZeros = m_f_derDiff.nonZeros();

    jade2Loader.getVariableNames(model, m_variableNames);
    m_initialStates.resize(m_numStates);
    m_tangents.resize(m_numVariables);
    jade2Loader.getInitialValues(model, StateVars::value, m_initialStates);
  }

  JADE2GenericEso::~JADE2GenericEso() {
    jade2Loader.freeModel(model);
  }

  /**
    * @brief get the total number of variables
    *
    * @return total number of variables
    */
  EsoIndex JADE2GenericEso::JADE2GenericEso::getNumVariables() const {
    return m_numVariables;
  }

  /**
    * @brief get the number of differential variables
    *
    * @return number of differential variables
    */
  EsoIndex JADE2GenericEso::getNumDifferentialVariables() const {
    return m_numDiffVariables;
  }

  /**
    * @brief get the number of parameter variables
    *
    * @return number of parameter variables
    */
  EsoIndex JADE2GenericEso::getNumParameters() const {
    return m_numParameters;
  }

  /**
    * @brief get the number of algebraic variables
    *
    * @return number of algebraic variables
    */
  EsoIndex JADE2GenericEso::getNumAlgebraicVariables() const {
    return m_numAlgVariables;
  }

  /**
    * @brief get the number of state variables
    *
    * @return number of state variables
    */
  EsoIndex JADE2GenericEso::getNumStates() const {
    return m_numStates;
  }

  /**
    * @brief get the number of differential equations
    *
    * @return number of differential equations
    */
  EsoIndex JADE2GenericEso::getNumEquations() const {
    return m_numEquations;
  }

  /**
    * @brief get the total number of equations
    *
    * @return total number of equations
    */
  EsoIndex JADE2GenericEso::getNumDiffEquations() const {
    return m_numDiffEquations;
  }

  /**
    * @brief get the number of algebraic equations
    *
    * @return number of algebraic equations
    */
  EsoIndex JADE2GenericEso::getNumAlgEquations() const {
    return m_numAlgEquations;
  }

  /**
    * @brief get the current value of the independent variable(time)
    *
    * @return current value of the independent variable
    */
  EsoIndex JADE2GenericEso::getNumNonZeroes() const{
    return m_numNonZeros;
  }

  /**
    * @brief get the current value of the independent variable(time)
    *
    * @return current value of the independent variable
    */
  EsoIndex JADE2GenericEso::getNumInitialNonZeroes() const{
	  // Initial equations are not supported. Therefore, there are not non-zeros.
    return 0;
  }

  /**
    * @brief get the current value of the independent variable(time)
    *
    * @return current value of the independent variable
    */
  EsoIndex JADE2GenericEso::getNumDifferentialNonZeroes() const{
    return m_numDiffNonZeros;
  }

  /**
    * @brief get the current value of the independent variable(time)
    *
    * @return current value of the independent variable
    */
  EsoIndex JADE2GenericEso::getNumConditions() const {
    return 0;
  }

  /**
    * @brief set the independent variable (time) to a new value
    *
    * @param[in] var new value of the independent variable
    */
  void JADE2GenericEso::setIndependentVariable(const EsoDouble var) {
	if (jade2Loader.getNumVariables(TimeVar::value) == 1)
		jade2Loader.setVariable(model, TimeVar::value, 0, var);
  }

  /**
    * @brief get the current value of the independent variable(time)
    *
    * @return current value of the independent variable
    */

  EsoDouble JADE2GenericEso::getIndependentVariable() const{
	  if (jade2Loader.getNumVariables(TimeVar::value) == 1)
		  return jade2Loader.getVariable(model, TimeVar::value, 0);
	  else
		  return 0.;
  }

  /**
    * @brief get the names of all variables of the model
    *
    * @param[out] names vector receiving the variable names - must be of size getNumVariables
    */


  void JADE2GenericEso::getVariableNames(std::vector<std::string> &names){
    names = m_variableNames;
  }

  /**
    * @brief set the values of all variables
    *
    * @param[in] variables array containing the values of the variables to be set -
  //				   must be of size getNumVariables
    */

  void JADE2GenericEso::setAllVariableValues(const EsoIndex n_var, const EsoDouble *variables) {
    if (n_var != m_numVariables){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::setAllVariableValues:\n size of input array (n_var="
                     << n_var << ") doesn't fit to number of variables (m_numVariables="
                     << m_numVariables << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    jade2Loader.setVariables(model, AllVars::value, variables);
  }

  /**
    * @brief set the value of the parameters
    *
    * @param[in] parameter array containing the values of the variables to be set -
  //						   must be of size getNumParameters
    */

  void JADE2GenericEso::setParameterValues(const EsoIndex n_pars, const EsoDouble *parameters) {
    if(n_pars != m_numParameters){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::setParameterValues:\n size of input array (n_pars="
                     << n_pars << ") doesn't fit to number of variables (m_numParameters="
                     << m_numParameters << ").";
      throw GenericEso::Error(errorMessage.str());
  }

    jade2Loader.setVariables(model, ParamVars::value, parameters);
  }

  /**
    * @brief set the value of the algebraic variables
    *
    * @param[in] algebraicVariables array containing the values of the  variables to be set -
  //							must be of size getNumAlgebraicVariables
    */

  void JADE2GenericEso::setAlgebraicVariableValues(const EsoIndex n_alg_var, const EsoDouble *algebraicVariables) {
    if(n_alg_var != m_numAlgVariables){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::setAlgebraicVariableValues:\n size of input array (n_alg_var="
                     << n_alg_var << ") doesn't fit to number of variables (m_numAlgVariables="
                     << m_numAlgVariables << ").";
      throw GenericEso::Error(errorMessage.str());
  }

    jade2Loader.setVariables(model, AlgVars::value, algebraicVariables);
  }

  /**
    * @brief set the value of the differential variables
    *
    * @param[in] differentialVariables array containing the values of the variables to be set -
  //							   must be of size getNumDifferentialVariables
    */

  void JADE2GenericEso::setDifferentialVariableValues(const EsoIndex n_diff_var, const EsoDouble *differentialVariables) {
    if(n_diff_var != m_numDiffVariables){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::setDifferentialVariableValues:\n size of input array (n_diff_var="
                     << n_diff_var << ") doesn't fit to number of variables (m_numDiffVariables="
                     << m_numDiffVariables << ").";
      throw GenericEso::Error(errorMessage.str());
  }

    jade2Loader.setVariables(model, DiffVars::value, differentialVariables);
  }

  /**
    * @brief set the values of all states
    *
    * @param[in] states array containing the values of the states to be set -
    *               must be of size getNumStates
    */

  void JADE2GenericEso::setStateValues(const EsoIndex n_states, const EsoDouble *states) {
    if (n_states != m_numStates){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::setStateValues:\n size of input array (n_states="
                     << n_states << ") doesn't fit to number of variables (m_numStates="
                     << m_numStates << ").";
      throw GenericEso::Error(errorMessage.str());
  }

    jade2Loader.setVariables(model, StateVars::value, states);
  }

  /**
    * @brief set the values of a subset of variables
    *
    * @param[in] variables array containing the values of the variables to be set -
    *                  must be at least of the size of parameter indices
    * @param[in] indices vector containing the indices for the variables to be set
    */

  void JADE2GenericEso::setVariableValues(const EsoIndex n_idx, const EsoDouble *variables, const EsoIndex *indices) {
    //TODO if and throw but how to get size of parameter indices
    //similar to ACSAMMMGenericEso.cpp line 674?
    if (n_idx > m_numVariables){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::setVariableValues:\n n_idx (="
                     << n_idx << ") exceedes number of variables (m_numVariables="
                     << m_numVariables << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    else if (n_idx < 0){
            std::ostringstream errorMessage;
            errorMessage << "GenericEso::setVariableValues:\n n_idx (="
                         << n_idx << ") has to be zero or positive.";
          throw GenericEso::Error(errorMessage.str());
	}
    for (int i = 0; i < n_idx; i++) {
      if (indices[i] < 0 || indices[i] >= m_numVariables){
          std::ostringstream errorMessage;
          errorMessage << "GenericEso::setVariableValues:\n out of variable indices range at index="
                       << i << " indices[index]=" << indices[i] << ".";
        throw GenericEso::Error(errorMessage.str());
      }

      jade2Loader.setVariable(model, AllVars::value, indices[i], variables[i]);
    }
  }

  /**
    * @brief getter for the values of all variables
    *
    * @param[out] variables array receiving the values of the variables -
    *                  must be of size getNumVariables
    */

  void JADE2GenericEso::getAllVariableValues(const EsoIndex n_var, EsoDouble *variables) const {
    if (n_var != m_numVariables){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getAllVariableValues:\n size of output array (n_var="
                     << n_var << ") doesn't fit to number of variables (m_numVariables="
                     << m_numVariables << ").";
      throw GenericEso::Error(errorMessage.str());
  }

    jade2Loader.getVariables(model, AllVars::value, variables);
  }

  /**
    * @brief getter for the values of the parameters
    *
    * @param[out] parameter array receiving the values of the parameters -
    *                          must be of size getNumParameters
    */

  void JADE2GenericEso::getParameterValues(const EsoIndex n_params, EsoDouble *parameters) {
    if (n_params != m_numParameters){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getParameterValues:\n size of output array (n_params="
                     << n_params << ") doesn't fit to number of variables (m_numParameters="
                     << m_numParameters << ").";
      throw GenericEso::Error(errorMessage.str());
  }
    jade2Loader.getVariables(model, ParamVars::value, parameters);
  }

  /* @brief getter for the values of the algebraic variables
    *
    * @param[out] algebraicVariables array receiving the values of the algebraic variables -
    *                           must be of size getNumAlgebraicVariables
    */

  void JADE2GenericEso::getAlgebraicVariableValues(const EsoIndex n_alg_var, EsoDouble *algebraicVariables) {
    if (n_alg_var != m_numAlgVariables){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getAlgebraicVariableValues:\n size of output array (n_alg_var="
                     << n_alg_var << ") doesn't fit to number of variables (m_numAlgVariables="
                     << m_numAlgVariables << ").";
      throw GenericEso::Error(errorMessage.str());
  }

    jade2Loader.getVariables(model, AlgVars::value, algebraicVariables);
  }

  /**
    * @brief getter for the values of the differential variables
    *
    * @param[out] differentialVariables array receiving the values of the differential variables -
    *                              must be of size getNumDifferentialVariables
    */

  void JADE2GenericEso::getDifferentialVariableValues(const EsoIndex n_diff_var, EsoDouble *differentialVariables) {
    if (n_diff_var != m_numDiffVariables){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getDifferentialVariableValues:\n size of output array (n_diff_var="
                     << n_diff_var << ") doesn't fit to number of variables (m_numDiffVariables="
                     << m_numDiffVariables << ").";
      throw GenericEso::Error(errorMessage.str());
  }
    jade2Loader.getVariables(model, DiffVars::value, differentialVariables);
  }

  /**
    * @brief get the values of all states
    *
    * @param[out] states array receiving the values of the states to be set - must be of size getNumStates
    */

  void JADE2GenericEso::getStateValues(const EsoIndex n_states, EsoDouble *states) {
    if (n_states != m_numStates){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getStateValues:\n size of output array (n_states="
                     << n_states << ") doesn't fit to number of variables (m_numStates="
                     << m_numStates << ").";
      throw GenericEso::Error(errorMessage.str());
  }

    jade2Loader.getVariables(model, StateVars::value, states);
  }

  /**
    * @brief get the values of all states - use initial equations to reset states
    *
    * @param[out] states array receiving the values of the states to be set - must be of size getNumStates
    */

  void JADE2GenericEso::getInitialStateValues(const EsoIndex n_states, EsoDouble *states) {
    if (n_states != m_numStates){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getInitialStateValues:\n size of output array (n_states="
                     << n_states << ") doesn't fit to number of variables (m_numStates="
                     << m_numStates << ").";
      throw GenericEso::Error(errorMessage.str());
  }

	for (int i = 0; i < m_numStates; i++) {
// use getVariable as initial values are fixed in the model.
		states[i] = jade2Loader.getVariable(model, StateVars::value, i);
	}

  }

  /**
    * @brief getter for the values of a subset of all variables
    *
    * @param[out] variables array receiving the values of the variables -
    *                  must be at least of size of parameter indices
    * @param[in] indices vector containing the indices of the variables to be received
    */

  void JADE2GenericEso::getVariableValues(const EsoIndex n_idx, EsoDouble *variables, const EsoIndex *indices) {
    if (n_idx > m_numVariables){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getVariableValues:\n n_idx (="
                     << n_idx << ") exceedes number of variables (m_numVariables="
                     << m_numVariables << ").";
      throw GenericEso::Error(errorMessage.str());
    }
    else if (n_idx <= 0){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getVariableValues:\n n_idx (="
                     << n_idx << ") has to be positive and non-zero.";
        throw GenericEso::Error(errorMessage.str());
    }

    for (int i = 0; i < n_idx; i++) {
      if (indices[i] < 0 || indices[i] >= m_numVariables){
          std::ostringstream errorMessage;
          errorMessage << "GenericEso::getVariableValues:\n out of variable indices range at index="
                       << i << " indices[index]=" << indices[i] << ".";
            throw GenericEso::Error(errorMessage.str());
      }

      variables[i] = jade2Loader.getVariable(model, AllVars::value, indices[i]);
    }
  }

  /**
    * @brief set the values of all derivatives
    *
    * Only the derivatives of the differential variables are set.
    * @param[in] derivatives array containing the values of the derivatives to be set -
    *                    must be of size getNumDifferentialVariables
    */

  void JADE2GenericEso::setDerivativeValues(const EsoIndex n_diff_var, const EsoDouble *derivatives) {
    if (n_diff_var != m_numDiffVariables){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::setDerivativeValues:\n size of input array (n_diff_var="
                     << n_diff_var << ") doesn't fit to number of variables (m_numDiffVariables="
                     << m_numDiffVariables << ").";
      throw GenericEso::Error(errorMessage.str());
  }

    jade2Loader.setVariables(model, DerDiffVars::value, derivatives);

  }

  /**
    * @brief getter for the values of all derivatives
    *
    * Only the derivatives of the differential variables are returned
    * @param[out] derivatives array receiving the values of the derivatives to be set -
    *                    must be of size getNumDifferentialVariables
    */

  void JADE2GenericEso::getDerivativeValues(const EsoIndex n_diff_var, EsoDouble *derivatives) {
    if (n_diff_var != m_numDiffVariables){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getDerivativeValues:\n size of output array (n_diff_var="
                     << n_diff_var << ") doesn't fit to number of variables (m_numDiffVariables="
                     << m_numDiffVariables << ").";
      throw GenericEso::Error(errorMessage.str());
  }

    jade2Loader.getVariables(model, DerDiffVars::value, derivatives);
  }

  /**
    * @brief getter for all variable bounds
    *
    * @param[out] lowerBounds vector receiving the lower bounds of all variables -
    *                    must be of size getNumVariables
    * @param[out] upperBounds vector receiving the upper bounds of all variables -
    *                    must be of size getNumVariables
    */


  void JADE2GenericEso::getAllBounds(const EsoIndex n_var, EsoDouble *lowerBounds, EsoDouble *upperBounds) {

  }

  /**
    * @brief getter for the residuals of all equations
    *
    * @param[out] residuals array receiving the residuals - must be of size getNumEquations
    */

  GenericEso::RetFlag JADE2GenericEso::getAllResiduals(const EsoIndex n_eq, EsoDouble *residuals) {
    if (n_eq != m_numEquations){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getAllResiduals:\n size of output array (n_eq="
                     << n_eq << ") doesn't fit to number of equations (m_numEquations="
                     << m_numEquations << ").";
      throw GenericEso::Error(errorMessage.str());
  }

    jade2Loader.evalAll(model, AllEqns::value, residuals);
    return GenericEso::RetFlag::OK;
  }

  /**
    * @brief getter for the residuals of all differential equations
    *
    * @param[out] differentialResiduals array receiving the residuals -
    *                              must be of size getNumDifferentialEquations
    */

  GenericEso::RetFlag JADE2GenericEso::getDifferentialResiduals(const EsoIndex n_diff_eq, EsoDouble *differentialResiduals) {
    if (n_diff_eq != m_numDiffEquations){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getDifferentialResiduals:\n size of output array (n_diff_eq="
                     << n_diff_eq << ") doesn't fit to number of equations (m_numDiffEquations="
                     << m_numDiffEquations << ").";
      throw GenericEso::Error(errorMessage.str());
  }

    jade2Loader.evalAll(model, DiffEqns::value, differentialResiduals);
    return GenericEso::RetFlag::OK;
  }

  /**
    * @brief getter for the residuals of all algebraic equations
    *
    * @param[out] algebraicResiduals array receiving the residuals - must be of size getNumAlgebraicEquations
    */

  GenericEso::RetFlag JADE2GenericEso::getAlgebraicResiduals(const EsoIndex n_alg_eq, EsoDouble *algebraicResiduals) {
    if (n_alg_eq != m_numAlgEquations){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getAlgebraicResiduals:\n size of output array (n_alg_eq="
                     << n_alg_eq << ") doesn't fit to number of equations (m_numAlgEquations="
                     << m_numAlgEquations << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    jade2Loader.evalAll(model, AlgEqns::value, algebraicResiduals);
    return GenericEso::RetFlag::OK;
  }

  /**
    * @brief getter for the residuals of a subset of equations
    *
    * @param[out] residuals array receiving the residuals - must be at least of the size of equationIndex
    * @param[in] equationIndex vector containing the indices of the equations to be returned
    */

  GenericEso::RetFlag JADE2GenericEso::getResiduals(const EsoIndex n_eq, EsoDouble *residuals, const EsoIndex *equationIndex) {

    if (n_eq > m_numEquations){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getResiduals:\n n_eq (="
                     << n_eq << ") exceedes number of equations (m_numEquations="
                     << m_numEquations << ").";
      throw GenericEso::Error(errorMessage.str());
    }
    else if (n_eq <= 0){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getResiduals:\n n_eq (="
                     << n_eq << ") has to be positive and non-zero.";
        throw GenericEso::Error(errorMessage.str());
    }
    if (n_eq < m_numEquations) {
      //  evaluation of the residual equation by equation
      for (int i = 0; i < n_eq; i++) {
        if (equationIndex[i] < 0 || equationIndex[i] >= m_numEquations) {
          std::ostringstream errorMessage;
          errorMessage << "GenericEso::getResiduals:\n out of equationen indices range at index="
            << i << " indices[index]=" << equationIndex[i] << ".";
          throw GenericEso::Error(errorMessage.str());
        }

        residuals[i] = jade2Loader.eval(model, AllEqns::value, equationIndex[i]);
      }
    }
    else {
      // complete evaluation of the residual (less function call overhead)
      jade2Loader.evalAll(model, AllEqns::value, residuals);
    }

    return GenericEso::RetFlag::OK;
  }

  /**
  * @copydoc GenericEso::getInitialJacobian(EsoIndex, EsoIndex*, EsoIndex*, EsoDouble*)
  *
  * initial Eso not definded for Jade - use standard Jacobian as InitialJacobian
  */
  GenericEso::RetFlag JADE2GenericEso::getInitialJacobian(const EsoIndex n_nz, EsoIndex *rowIndices,
                                                 EsoIndex *colIndices, EsoDouble *values)
  {
    return GenericEso::RetFlag::OK;
  }

  /**
    * @brief initialize differential variables with user defined values
    *
    * override initial values of the differential variables in the model's initial equations
    * if a variable is initialized to a constant value in the initial sections there
    * will be no problem doing so. Variables initialized by a model parameter can only be overridden,
    * if the parameter is not used as a optimization variable and will remain unset.
    * The
    * @param numDiffVals number of differential variables being set
    * @param diffIndices eso indices of the differential variables being set (length of numDiffVals)
    * @param diffValues new initial values of the differential variables
    * @param numParameters number of used parameters
    * @param parameterIndices indices of used parameters
    * @return GenericEso::OK if setting of values is not problematic, GenericEso::FAIL if dependencies
    *         of parameters lead to possible inconsistencies
    */


  GenericEso::RetFlag JADE2GenericEso::setInitialValues(const EsoIndex numDiffVals, EsoIndex *diffIndices,
                                               EsoDouble * diffValues,const EsoIndex numParameters,
                                               EsoIndex *parameterIndices) {
      assert(numDiffVals <= m_numDiffVariables);
      //const int minimalAllowedIndex = m_Model->variableIndexOffset(DiffVars{});
      const int minimalAllowedIndex = jade2Loader.variableIndexOffset(model, DiffVars::value);
      const int maximalAllowedIndex = minimalAllowedIndex + jade2Loader.getNumVariables(DiffVars::value) - 1 ;

      for(EsoIndex i=0; i<numDiffVals; i++){
        //throw assert if index in diffIndices is no actual differential index
        assert(diffIndices[i] >= minimalAllowedIndex && diffIndices[i] <= maximalAllowedIndex);
        m_initialStates[diffIndices[i]] = diffValues[i];
      }
      for(EsoIndex i=0; i<m_numDiffVariables; i++){
        jade2Loader.setVariable(model, DiffVars::value,i, m_initialStates[i+minimalAllowedIndex]);
      }
      return GenericEso::RetFlag::OK;

    }

  void JADE2GenericEso::getJacobianStruct(const EsoIndex n_nz, EsoIndex *rowIndices, EsoIndex *colIndices) {

    if (n_nz != m_numNonZeros){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getJacobianStruct:\n size of output array n_nz="
                   << n_nz << " does not fit to number of non-zero entries(" << m_numNonZeros << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    int i = 0;
    for (int k = 0; k < m_f_all.outerSize(); ++k)
    {
      for (Eigen::SparseMatrix<double>::InnerIterator it(m_f_all, k); it; ++it)
      {
        rowIndices[i] = it.row(); // row index
        colIndices[i] = it.col(); // col index (here it is equal to k)
        i++;
      }
    }
  }

  /**
    * @brief getter for all Jacobian values
    *
    * @param[out] values array receiving the Jacobian values in COO sparse format
    * @sa getJacobianStruct
    */

  GenericEso::RetFlag JADE2GenericEso::getJacobianValues(const EsoIndex n_nz, EsoDouble *values) {

    if (n_nz != m_numNonZeros){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getJacobianValues:\n size of output array n_nz="
                   << n_nz << " does not fit to number of non-zero entries(" << m_numNonZeros << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    jade2Loader.evalJacobianValues(model, AllEqns::value, StateTimeParamVars::value, m_f_all);

    int i = 0;
    for (int k = 0; k < m_f_all.outerSize(); ++k)
    {
      for (Eigen::SparseMatrix<double>::InnerIterator it(m_f_all, k); it; ++it)
      {
        values[i] = it.value();
        i++;
      }
    }

    return GenericEso::RetFlag::OK;
  }

  /**
    * @brief getter for a subset of Jacobian values
    *
    * @param[out] values array receiving the Jacobian values
    * @param[in] indices vector containing the eso indices of the subset -
  //				 each etnry must be in range [0, numNonZeroes-1]
    */

  GenericEso::RetFlag JADE2GenericEso::getJacobianValues(const EsoIndex n_idx, EsoDouble *values,
                                                const EsoIndex *indices) {

    if (n_idx <= 0){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getJacobianValues:\n n_idx="
                   << n_idx << " has to be positive and non zero.";
      throw GenericEso::Error(errorMessage.str());
    }
    if (n_idx > m_numNonZeros){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getJacobianValues:\n n_idx="
                   << n_idx << " exceeds number of non-zero entries (" << m_numNonZeros << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    int nnz = m_numNonZeros;
    std::vector<EsoDouble> allValues(nnz);
    getJacobianValues(nnz, allValues.data());

    for (int i = 0; i < n_idx; i++) {
      if (indices[i] < 0 || indices[i] >= m_numNonZeros){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getJacobianValues:\n indice at position i="
                     << i << " indices[i]=" << indices[i] << " is out of range";
        throw GenericEso::Error(errorMessage.str());
      }

      values[i] = allValues[indices[i]];
    }


    return GenericEso::RetFlag::OK;
  }

  void JADE2GenericEso::getDiffJacobianStruct(const EsoIndex n_diff_nz, EsoIndex *rowIndices, EsoIndex *colIndices) {

    if (n_diff_nz != m_numDiffNonZeros){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getDiffJacobianStruct:\n size of output array n_diff_nz="
                   << n_diff_nz << " does not fit to number of non-zero entries(" << m_numDiffNonZeros << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    int i = 0;
    for (int k = 0; k < m_f_derDiff.outerSize(); ++k)
    {
      for (Eigen::SparseMatrix<double>::InnerIterator it(m_f_derDiff, k); it; ++it)
      {
        rowIndices[i] = it.row(); // row index
        colIndices[i] = it.col(); // col index (here it is equal to k)
        i++;
      }
    }
  }

  /**
    * @brief getter for all differential Jacobian data of the model
    *
    * @param[out] values array receiving the differential Jacobian values in COO sparse format
    * @sa getDiffJacobianStruct
    */
  GenericEso::RetFlag JADE2GenericEso::getDiffJacobianValues(const EsoIndex n_diff_nz, EsoDouble *values) {

    if (n_diff_nz != m_numDiffNonZeros){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getDiffJacobianValues:\n size of output array n_diff_nz="
                   << n_diff_nz << " does not fit to number of non-zero entries(" << m_numDiffNonZeros << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    jade2Loader.evalJacobianValues(model, AllEqns::value, DerDiffVars::value, m_f_derDiff);

    int i = 0;
    for (int k = 0; k < m_f_derDiff.outerSize(); ++k)
    {
      for (Eigen::SparseMatrix<double>::InnerIterator it(m_f_derDiff, k); it; ++it)
      {
        values[i] = it.value();
        i++;
      }
    }

    return GenericEso::RetFlag::OK;
  }

  GenericEso::RetFlag JADE2GenericEso::getJacobianMultVector(const bool transpose, const EsoIndex n_states, EsoDouble *seedStates,
                                                    const EsoIndex n_params, EsoDouble *seedParameters, const EsoIndex n_y,
                                                    EsoDouble *y)
  {

    if (seedStates == NULL){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getJacobianMultVector:\n seedStates is null and has to be preallocated with length n_states="
                   << n_states << " .";
      throw GenericEso::Error(errorMessage.str());
    }
    if (seedParameters == NULL){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getJacobianMultVector:\n seedParameters is null and has to be preallocated with length seedParameters="
              << seedParameters << " .";
      throw GenericEso::Error(errorMessage.str());
    }
    if (y == NULL){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getJacobianMultVector:\n y is null and has to be preallocated with length n_y="
                   << n_y << " .";
      throw GenericEso::Error(errorMessage.str());
    }

    if (n_states != m_numStates){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getJacobianMultVector:\n n_states="
                   << n_states << " has to be equal to the model's number of states(" << m_numStates << ").";
      throw GenericEso::Error(errorMessage.str());
    }
    if (n_params != m_numParameters){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getJacobianMultVector:\n n_params="
                   << n_params << " has to be equal to the model's number of parameters(" << m_numParameters << ").";
      throw GenericEso::Error(errorMessage.str());
    }
    if (n_y != m_numEquations){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getJacobianMultVector:\n n_y="
                   << n_y << " has to be equal to the model's number of equations(" << m_numEquations << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    jade2Loader.evalJacobianValues(model, AllEqns::value, StateVars::value, m_f_x);
    jade2Loader.evalJacobianValues(model, AllEqns::value, ParamVars::value, m_f_p);

    Eigen::Map<Eigen::VectorXd> t1_x(seedStates, n_states, 1);
    Eigen::Map<Eigen::VectorXd> t1_p(seedParameters, n_params, 1);
    Eigen::Map<Eigen::VectorXd> t1_y(y, n_y, 1);

    if (!transpose)
      t1_y = m_f_x * t1_x + m_f_p * t1_p;
    else {
      t1_x = m_f_x.transpose() * t1_y;
      t1_p = m_f_p.transpose() * t1_y;
    }

    return GenericEso::RetFlag::OK;
  }

  void JADE2GenericEso::getParameterIndex(const EsoIndex n_para, EsoIndex *parameterIndex){
    if(n_para != m_numParameters){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getParameterIndex:\n size of output array n_para="
                   << n_para << " does not fit to number of parameters(" << m_numParameters << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    int offset = jade2Loader.variableIndexOffset(model, ParamVars::value);
    for (int i = 0; i < n_para; i++)
      parameterIndex[i] = i+offset;

  }

  void JADE2GenericEso::getDifferentialIndex(const EsoIndex n_diff_var, EsoIndex *differentialIndex){
    if(n_diff_var != m_numDiffVariables){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getDifferentialIndex:\n size of output array n_diff_var="
                   << n_diff_var << " does not fit to number of differential variables(" << m_numDiffVariables << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    int offset = jade2Loader.variableIndexOffset(model, DiffVars::value);
    for (int i = 0; i < n_diff_var; i++)
      differentialIndex[i] = i+offset;

  }

  void JADE2GenericEso::getAlgebraicIndex(const EsoIndex n_alg_var, EsoIndex *algebraicIndex){
    if(n_alg_var != m_numAlgVariables){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getAlgebraicIndex:\n size of output array n_alg_var="
                   << n_alg_var << " does not fit to number of algebraic variables(" << m_numAlgVariables << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    int offset = jade2Loader.variableIndexOffset(model, AlgVars::value);
    for (int i = 0; i < n_alg_var; i++)
      algebraicIndex[i] = i+offset;

  }

  void JADE2GenericEso::getStateIndex(const EsoIndex n_states, EsoIndex *stateIndex){
    if(n_states != m_numStates){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getStateIndex:\n size of output array n_states="
                   << n_states << " does not fit to number of states(" << m_numStates << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    int offset = jade2Loader.variableIndexOffset(model, StateVars::value);
    for (int i = 0; i < n_states; i++)
      stateIndex[i] = i+offset;

  }

  void JADE2GenericEso::getDiffEquationIndex(const EsoIndex n_diff_eq, EsoIndex *diffEqIndex) {
    if(n_diff_eq != m_numDiffEquations){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getDiffEquationIndex:\n size of output array n_diff_eq="
                   << n_diff_eq << " does not fit to number of differential equations(" << m_numDiffEquations << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    int offset = jade2Loader.equationIndexOffset(model, DiffEqns::value);
    for (int i = 0; i < n_diff_eq; i++)
      diffEqIndex[i] = i+offset;

  }

  void JADE2GenericEso::getAlgEquationIndex(const EsoIndex n_alg_eq, EsoIndex *algEqIndex) {
    if (n_alg_eq != m_numAlgEquations){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getAlgEquationIndex:\n size of output array n_alg_eq="
                   << n_alg_eq << " does not fit to number of algebraic equations(" << m_numAlgEquations << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    int offset = jade2Loader.equationIndexOffset(model, AlgEqns::value);
    for (int i = 0; i < n_alg_eq; i++)
      algEqIndex[i] = i + offset;

  }

  ////utility functions
  EsoIndex JADE2GenericEso::getEsoIndexOfVariable(const std::string &varName) {
    for (int i = 0; i < m_numVariables; i++)
        if (m_variableNames[i].compare(varName) == 0) return i;

    std::ostringstream errorMessage;
    errorMessage << "GenericEso::getEsoIndexOfVariable:\n variable name ("
                 << varName << ") could not be found.";
    throw GenericEso::Error(errorMessage.str());

  }

  std::string JADE2GenericEso::getVariableNameOfIndex(const EsoIndex esoIndex) {
    if (esoIndex < 0){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getVariableNameOfIndex:\n esoIndex="
                     << esoIndex << " has to be non-negative.";
        throw GenericEso::Error(errorMessage.str());
    }
    if (esoIndex >= m_numVariables){
        std::ostringstream errorMessage;
        errorMessage << "GenericEso::getVariableNameOfIndex:\n esoIndex="
                     << esoIndex << " exceeds number of variables("<< m_numVariables << ").";
        throw GenericEso::Error(errorMessage.str());
    }

    return m_variableNames[esoIndex];
  }

  EsoIndex JADE2GenericEso::getStateIndexOfVariable(const EsoIndex esoIndex) {
    if (esoIndex < 0){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getStateIndexOfVariable:\n esoIndex="
                   << esoIndex << " has to be non-negative.";
      throw GenericEso::Error(errorMessage.str());
    }
    if (esoIndex >= m_numVariables){
      std::ostringstream errorMessage;
      errorMessage << "GenericEso::getStateIndexOfVariable:\n esoIndex="
                   << esoIndex << " exceeds number of variables("<< m_numVariables << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    if (esoIndex >= m_numStates)
      return m_numStates;
    return esoIndex;
  }

  EsoType JADE2GenericEso::getType() const {
      return EsoType::JADE2;
  }

  ModelName JADE2GenericEso::getModel() const {
      return model_name;
  }

  /**
   * @brief Calculates second-order tangent-linear over adjoint derivatives of the DA-System.
   *
   * This functions calls the code generated by dcc calculating second-order tangent-linear over
   * adjoint derivatives. The actual values of the states, the parameters and the derivated states are
   * taken from the members m_states, m_parametersACS and m_der_states.
   * For explanation: z = [states p der_states]^T,
   *                  all arrays with suffix '_states' or '_der_states' are of size (number of states)
   *                  all arrays with suffix '_p' are of size (number of parameters)
   *                  all arrays with suffix '_yy' or '_residuals' are of size (number of equations)
   *                  <.,.> represents the inner dot product with appropriate dimensions.
   *
   * output definition: a1_z = (dF(z)/dz)^T * a1Yy;
   *                    t2_a1_z = <d2F(z)/dzdz, a1Yy, t2_z> + <dF(z)/dz, t2_a1Yy>;
   *
   * @param[in] lenT2States length of t2States array. Must be equal to m_n_states
   * @param[in] t2States array that defines the first m_n_states entries of t2_z
   * @param[in] lenT2DerStates length of t2DerStates arry. Must be equal to m_n_states
   * @param[in] t2DerStates array that defines the last m_n_states entries of t2_z
   * @param[in] lenT2P length of t2P array. Must be equal to m_n_pars
   * @param[in] t2P array that defines the entries from index m_n_states til index
   *            (m_n_states+m_n_pars-1) of t2_z
   * @param[in] lenA1States length of a1States array. Must be equal to m_n_states
   * @param[out] a1States contains on output the product of the transposed Jacobian, relative to the
   *             states, with a1Yy
   * @param[in] lenA1DerStates length of a1DerStates array. Must be equal to m_n_states
   * @param[out] a1DerStates contains on output the product of the transposed Jacobian, relative to
   *             the derivated states, with a1Yy
   * @param[in] lenA1P length of a1P array. Must be equal to m_n_pars
   * @param[out] a1P contains on output the product of the transposed Jacobian, relative to the
   *             parameters, with a1Yy
   * @param[in] lenT2A1States length of t2A1States array. Must be equal to m_n_states
   * @param[out] t2A1States array that defines the first m_n_states entries of output array
   *             t2_a1_z
   * @param[in] lenT2A1DerStates length of t2A1DerStates array. Must be equal to m_n_states
   * @param[out] t2A1DerStates array that defines the last m_n_states entries of output array
   *             t2_a1_z
   * @param[in] lenT2A1P length of t2A1P array. Must be equal to m_n_pars
   * @param[out] t2A1P array that defines the entries from index m_n_states til index
   *            (m_n_states+m_n_pars-1) of output array t2_a1_z
   * @param[in] lenA1Yy length of a1Yy array. Must be equal to m_n_eq
   * @param[in] a1Yy seed vector multiplying the Jacobian matrix to calculate a1_z and the Hessian
   *            matrix to calculate t2_a1_z.
   * @param[in] lenT2A1Yy length of t2A1Yy array. Must be equal to m_n_eq
   * @param[in] t2A1Yy seed vector to define the jacobian part of t2_a1_z
   */
  void JADE2GenericEso::evaluate2ndOrderDerivatives(unsigned lenT2States, double *t2States,
      unsigned lenT2DerStates, double *t2DerStates,
      unsigned lenT2P, double *t2P,
      unsigned lenA1States, double *a1States,
      unsigned lenA1DerStates, double *a1DerStates,
      unsigned lenA1P, double *a1P,
      unsigned lenT2A1States, double *t2A1States,
      unsigned lenT2A1DerStates, double *t2A1DerStates,
      unsigned lenT2A1P, double *t2A1P,
      unsigned lenA1Yy, double *a1Yy,
      unsigned lenT2A1Yy, double *t2A1Yy) {

      Eigen::Map<Eigen::VectorXd> a1_x(a1States, lenA1States, 1);
      Eigen::Map<Eigen::VectorXd> a1_der(a1DerStates, lenA1DerStates, 1);
      Eigen::Map<Eigen::VectorXd> a1_p(a1P, lenA1P, 1);
      Eigen::VectorXd t2All = Eigen::VectorXd::Zero(m_numVariables);
      Eigen::VectorXd t2A1All = Eigen::VectorXd::Zero(m_numVariables);

      Eigen::VectorXd a1_yy(lenA1Yy);
      for (unsigned i = 0; i < lenA1Yy; i++)
          a1_yy[i] = a1Yy[i];

      jade2Loader.evalJacobianValues(model, AllEqns::value, StateVars::value, m_f_x);
      a1_x = m_f_x.transpose() * a1_yy;

      //jade2Loader.evalJacobianValues(model, AllEqns::value, DerDiffVars::value, m_f_derDiff);
      //a1_der = m_f_derDiff.transpose() * a1_yy;

      jade2Loader.evalJacobianValues(model, AllEqns::value, ParamVars::value, m_f_p);
      a1_p = m_f_p.transpose() * a1_yy;

      jade2Loader.setAdjoints(model, AllEqns::value, a1_yy);

      for (int i = 0; i < m_numStates; i++)
          t2All[i + jade2Loader.variableIndexOffset(model, StateVars::value)] = t2States[i];

      for(int i=0; i < m_numParameters; i++)
          t2All[i + jade2Loader.variableIndexOffset(model, ParamVars::value)] = t2P[i];

      for(int i=0; i<m_numDiffVariables; i++)
          t2All[i + jade2Loader.variableIndexOffset(model, DerDiffVars::value)] = t2DerStates[i];

      jade2Loader.evalAllSecondOrderAdjoints(model, AllVars::value, t2All.data(), t2A1Yy, t2A1All);

	  for (int i = 0; i < m_numStates; i++)
		  t2A1States[i] = t2A1All[i + jade2Loader.variableIndexOffset(model, StateVars::value)];

	  for (int i = 0; i < m_numParameters; i++)
		  t2A1P[i] = t2A1All[i + jade2Loader.variableIndexOffset(model, ParamVars::value)];

	  for (int i = 0; i < m_numDiffVariables; i++)
		  t2A1DerStates[i] = t2A1All[i + jade2Loader.variableIndexOffset(model, DerDiffVars::value)];
  }


/**
 * @brief Calculates first-order tangent-linear derivatives of the DA-System.
 *
 * This functions calls the code generated by dcc calculating first-order forward derivatives.
 * The actual values of the states, the parameters and the derivated states are taken from the
 * members m_states, m_parametersACS and m_der_states.
 * For explanation: z = [states p der_states]^T
 *                  all arrays with suffix '_states' or '_der_states' are of size (number of states)
 *                  all arrays with suffix '_p' are of size (number of parameters)
 *                  all arrays with suffix '_yy' or '_residuals' are of size (number of equations)
 *
 * output definition: t1_residuals = dF(z)/dz * t1_z
 *
 * @param[in] lenT1States length of t2States array. Must be equal to m_n_states
 * @param[in] t1States seed vector multiplying the Jacobian part relative to the states
 * @param[in] lenT1DerStates length of t2States array. Must be equal to m_n_states
 * @param[in] t1DerStates seed vector multiplying the Jacobian part relative to the derivated
 *            states
 * @param[in] lenT1P length of t2States array. Must be equal to m_n_pars
 * @param[in] t1P seed vector multiplying the Jacobian part relative to the parameters
 * @param[in] lenT1Res length of t2States array. Must be equal to m_n_eq
 * @param[out] t1Residuals product of the jacobian matrix and the seed vectors
 */
  void JADE2GenericEso::evaluate1stOrdForwDerivatives(unsigned lenT1States, double *t1States,
      unsigned lenT1DerStates, double *t1DerStates,
      unsigned lenT1P, double *t1P,
      unsigned lenT1Res, double *t1Residuals) {

    int stateVarsOffset = jade2Loader.variableIndexOffset(model, StateVars::value);
    int paramVarsOffset = jade2Loader.variableIndexOffset(model, ParamVars::value);
    int derDiffVarsOffset = jade2Loader.variableIndexOffset(model, DerDiffVars::value);

    m_tangents.setZero();
    for (int i = 0; i < m_numDiffVariables + m_numAlgVariables; ++i)
      m_tangents[stateVarsOffset + i] = t1States[i];
    for (int i = 0; i < m_numParameters; ++i)
      m_tangents[paramVarsOffset + i] = t1P[i];
    for (int i = 0; i < m_numDiffVariables; ++i)
      m_tangents[derDiffVarsOffset + i] = t1DerStates[i];

    jade2Loader.evalAllTangents(model, m_tangents.data(), t1Residuals);

}

