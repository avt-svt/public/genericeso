/**
 * @file JadeGenericEso.hpp
 *
 * =====================================================================\n
 * &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
 * =====================================================================\n
 * Generic ESO - Part of DyOS                                           \n
 * =====================================================================\n
 * Member definitions of class JadeGenericEso                        \n
 * =====================================================================\n
 * @author Moritz Schmitz
 * @date 15.9.2011
 */

#include "JadeGenericEso.hpp"
#include "ESOExceptions.hpp"
#include <sstream>
#include <assert.h>
#include <cmath>

#ifndef ESO_DBL_MAX
#define ESO_DBL_MAX 1e300
#endif

/**
 * @brief constructor
 *
 * The constructor takes as argument the name (without the suffix ".dll") of a dynamic library
 * created by Jade. The library contains all the necessary information about the dynamic model
 * originally coded in Modelica. Furthermore all first-order derivatives in tangent-linear and
 * adjoint mode as well as second-order derivatives in tangent over tangent and tangent over adjoint
 * mode are available within this dynamic library corresponding to the Jade standard.
 *
 * @param[in] model name (without the suffix ".dll") of a dynamic library created by Jade
 */
JadeGenericEso::JadeGenericEso(const std::string &model)
  : m_JadeLoader(model),
    m_n_states(m_JadeLoader.Get_num_vars()),
    m_n_eq(m_JadeLoader.Get_num_eqns()),
    m_n_cond(m_JadeLoader.Get_num_cond()),
    m_n_pars(m_JadeLoader.Get_num_pars()),
    m_states(m_n_states),
    m_initialStates(m_n_states),
    m_der_states(m_n_states),
    m_parametersACS(m_n_pars),
    m_res(m_n_eq),
    m_t1_states(m_n_states),
    m_t1_der_states(m_n_states),
    m_t1_p(m_n_pars),
  m_t1_res(m_n_eq),
  m_a1_states(m_n_states),
    m_a1_der_states(m_n_states),
  m_a1_p(m_n_pars),
  m_a1_res(m_n_eq),
  m_a1_states_2ndOrd(m_n_states),
  m_a1_der_states_2ndOrd(m_n_states),
  m_a1_p_2ndOrd(m_n_pars),
  m_a1_res_2ndOrd(m_n_eq),
  m_t2_states(m_n_states),
  m_t2_der_states(m_n_states),
  m_t2_p(m_n_pars),
  m_t2_res(m_n_eq, 0.0),
  m_t2_a1_states(m_n_states),
  m_t2_a1_der_states(m_n_states),
  m_t2_a1_p(m_n_pars),
  m_t2_a1_res(m_n_eq),
  m_conditions(m_n_cond,0),
  m_locks(m_n_cond, 0),
	m_model(model)
{
  // default index of the independent variable
  m_independentVar = 0;
  m_numEquations = m_n_eq;
  m_numVariables = m_n_states + m_n_pars;
  m_numSwitching = m_n_cond;
  std::vector<EsoIndex> parameterIndices(m_n_pars);

  // the indices of the variables are ordered as follows: 1. states, 2.parameters
  // algebraic and differential state indices are set later, because the indices have
  // to be detected first
  for(EsoIndex i=0; i<m_n_pars; i++){
    parameterIndices[i] = m_n_states + i;
  }
  m_parameters.setEsoIndex(parameterIndices);
  std::vector<std::string> parNames(0);
  parNames.reserve(m_n_pars);
  m_JadeLoader.GetParNames(&parNames);
  m_parameters.setName(parNames);

  for(EsoIndex i=0; i<m_n_states; i++){
    m_states[i] = 0.0;
    m_initialStates[i] = 0.0;
    m_der_states[i] = 0.0;
  }
  for(EsoIndex i=0; i<m_n_pars; i++){
    m_parametersACS[i] = 0.0;
  }
  for(EsoIndex i=0; i<m_numEquations; i++){
    m_res[i] = 0.0;
  }
  m_JadeLoader.Init(m_states.getData(), m_parametersACS.getData(), m_n_states, m_n_pars, m_n_cond);
  for(unsigned i=0; i<m_states.getSize(); i++){
    m_initialStates[i] = m_states[i];
  }
  
  m_sparseJacobian = NULL;
  m_sparseDiffJacobian = NULL;
  calculateJacobianStructure();
  
  getVariableNames(m_varNames);
}

/**
 * @brief destructor
 */
JadeGenericEso::~JadeGenericEso()
{
  for(EsoIndex i=0; i<m_n_states+m_n_pars; i++){
    delete[] m_buffer[i];
  }
  delete[] m_buffer;
  for(EsoIndex i=0; i<m_n_states; i++){
    delete[] m_bufferDiff[i];
  }
  delete[] m_bufferDiff;
  delete m_sparseJacobian;
  delete m_sparseDiffJacobian;
}

/**
 * @brief find the jacobian structure for the current condition/lock configuration
 */
void JadeGenericEso::calculateJacobianStructure()
{
  /***** calculation of the sparsity pattern *****/
  // in constructor because the sparsity
  // pattern has to be calculated once and
  // we want avoid operator "new" in other functions

  /*** using special type jsp to define the sparsity pattern ***/
  // jsp means "jacobian sparsity pattern" and is a special type to determine the latter
  jsp *sp_var, *sp_der_states, *sp_rhs, *sp_param;
  sp_var=new jsp[m_n_states]; sp_der_states=new jsp[m_n_states];
  sp_param=new jsp[m_n_pars]; sp_rhs=new jsp[m_numEquations];

  /*** the sparsity pattern will refer to each variable using its indep_counter value ***/
  int indep_counter = 0;
  for(EsoIndex i=0; i<m_n_states; i++){
    sp_var[i] = 1;
    sp_var[i].nz.insert(indep_counter++);
  }
  for(EsoIndex i=0; i<m_n_pars; i++){
    sp_param[i] = 1;
    sp_param[i].nz.insert(indep_counter++);
  }
  for(EsoIndex i=0; i<m_n_states; i++){
    sp_der_states[i] = 1;
    sp_der_states[i].nz.insert(indep_counter++);
  }


  utils::Array<int> conditions(m_n_cond, 0);
  m_JadeLoader.Jsp_res(sp_rhs, sp_der_states, sp_var, sp_param, conditions.getData(), m_locks.getData(), m_conditions.getData(), m_n_states, m_n_pars, m_n_cond);

  /***** defintion of Jacobian matrices *****/

  int firstVarIndex = 0;
  const int lastVarIndex = m_n_states + m_n_pars - 1;
  const int firstDiffVarIndex = m_n_states + m_n_pars;
  const int lastDiffVarIndex = 2*m_n_states + m_n_pars - 1;
  if(m_sparseJacobian){
    delete m_sparseJacobian;
  }
  m_sparseJacobian = new SparseJacobian(firstVarIndex, lastVarIndex, sp_rhs, m_numEquations);
  if(m_sparseDiffJacobian){
    delete m_sparseDiffJacobian;
  }
  m_sparseDiffJacobian = new SparseJacobian(firstDiffVarIndex, lastDiffVarIndex,
                                            sp_rhs, m_numEquations);

  m_jacobian.setRowIndices(m_sparseJacobian->getRowIndices());
  m_jacobian.setColIndices(m_sparseJacobian->getColIndices());
  m_diffJacobian.setRowIndices(m_sparseDiffJacobian->getRowIndices());
  m_diffJacobian.setColIndices(m_sparseDiffJacobian->getColIndices());


  /***** find algebraic and differential variables and equations *****/

  const int nonZerosDiffJac = m_diffJacobian.getNumEntries();
  const int nonZerosJac = m_jacobian.getNumEntries();
  m_numInitialNonZeroes = 0;
  vector<EsoIndex> varsAlgIndices, varsDiffIndices, eqnsAlgIndices, eqnsDiffIndices;
  varsAlgIndices.reserve(m_n_states-nonZerosDiffJac);
  varsDiffIndices.reserve(nonZerosDiffJac);
  eqnsAlgIndices.reserve(m_numEquations-nonZerosDiffJac);
  eqnsDiffIndices.reserve(nonZerosDiffJac);
  
  std::set<int> varsDiff;
  std::set<int>::iterator it;
  utils::Array<EsoIndex> eqIndices(1);
  utils::Array<EsoIndex> varIndices(m_n_states);
  for(EsoIndex i=0; i<m_n_states; i++){
    // we need the indices relative to the original states, not the diferentiated ones
    varIndices[i] = i;
  }
  utils::Array<EsoIndex> rowIndices(nonZerosDiffJac);
  utils::Array<EsoIndex> colIndices(nonZerosDiffJac);
  EsoIndex numEntries = 0;
  for(EsoIndex i=0; i<m_numEquations; i++){
    eqIndices[0] = i;
    m_sparseDiffJacobian->getSparsityPattern(eqIndices.getData(), eqIndices.getSize(), varIndices.getData(), m_n_states, rowIndices.getData(),
                                             colIndices.getData(), numEntries);
    if (numEntries == 0){
      eqnsAlgIndices.push_back(i);
    }
    else{
      eqnsDiffIndices.push_back(i);
      for(EsoIndex j=0; j<numEntries; j++){
        // we want the indices of the differential variables in non-differentiated state
        varsDiff.insert(colIndices[j]);
      }
    }
  }
  for(EsoIndex i=0; i<m_n_states; i++){
    it = varsDiff.find(i);
    if(it == varsDiff.end()){
      varsAlgIndices.push_back(i);
    }
    else{
      varsDiffIndices.push_back(i);
    }
  }
  m_algebraicEquations.setEquationIndex(eqnsAlgIndices);
  m_differentialEquations.setEquationIndex(eqnsDiffIndices);

  m_algebraicStateVariables.setEsoIndex(varsAlgIndices);
  m_differentialStateVariables.setEsoIndex(varsDiffIndices);


  /***** find number of state and parameter jacobian entries *****/

  m_n_stateJacEntries = 0;
  std::vector<EsoIndex> jacColIndices(nonZerosJac);
  jacColIndices = m_sparseJacobian->getColIndices();
  for(int i=0; i<nonZerosJac; i++){
    if(jacColIndices[i] < m_n_states){
      m_n_stateJacEntries++;
    }
  }
  m_n_paramJacEntries = nonZerosJac - m_n_stateJacEntries;


  /***** definition of matrices *****/
  // in order to not use operator "new" in other methods

  m_buffer = new EsoDouble*[m_n_states+m_n_pars];
  const EsoIndex numSeedColumn = m_sparseJacobian->getNumColsSeed();
  for(EsoIndex i=0; i<m_n_states+m_n_pars; i++){
    m_buffer[i] = new EsoDouble[numSeedColumn];
  }
  for(EsoIndex i=0; i<m_n_states+m_n_pars; i++){
    for(EsoIndex k=0; k<numSeedColumn; k++){
      m_buffer[i][k] = 0;
    }
  }
  m_bufferDiff = new EsoDouble*[m_n_states];
  const EsoIndex numSeedColumnDiff = m_sparseDiffJacobian->getNumColsSeed();
  for(EsoIndex i=0; i<m_n_states; i++){
    m_bufferDiff[i] = new EsoDouble[numSeedColumnDiff];
  }
  for(EsoIndex i=0; i<m_n_states; i++){
    for(EsoIndex k=0; k<numSeedColumnDiff; k++){
      m_bufferDiff[i][k] = 0;
    }
  }

  /***** set bounds to the minimum and maximum double value *****/
  std::vector<double> lowerBounds(m_parameters.getNumberOfVariables(), -ESO_DBL_MAX);
  std::vector<double> upperBounds(m_parameters.getNumberOfVariables(), ESO_DBL_MAX);
  m_parameters.setModelBounds(lowerBounds, upperBounds);

  lowerBounds.resize(m_algebraicStateVariables.getNumberOfVariables(), -ESO_DBL_MAX);
  upperBounds.resize(m_algebraicStateVariables.getNumberOfVariables(),  ESO_DBL_MAX);
  m_algebraicStateVariables.setModelBounds(lowerBounds, upperBounds);

  lowerBounds.resize(m_differentialStateVariables.getNumberOfVariables(), -ESO_DBL_MAX);
  upperBounds.resize(m_differentialStateVariables.getNumberOfVariables(),  ESO_DBL_MAX);
  m_differentialStateVariables.setModelBounds(lowerBounds, upperBounds);

  std::vector<EsoIndex> stateIndices(m_n_states);
  for(EsoIndex i=0; i<m_n_states; i++){
    stateIndices[i] = i;
  }
  m_stateVariables.setEsoIndex(stateIndices);
  lowerBounds.resize(m_stateVariables.getNumberOfVariables(), -ESO_DBL_MAX);
  upperBounds.resize(m_stateVariables.getNumberOfVariables(),  ESO_DBL_MAX);
  m_stateVariables.setModelBounds(lowerBounds, upperBounds);

  /***** set names of algebraic and differential states *****/
  std::vector<std::string> algStateNames(0);
  std::vector<std::string> diffStateNames(0);
  algStateNames.resize(varsAlgIndices.size());
  diffStateNames.resize(varsDiffIndices.size());

  std::vector<std::string> varNames(0);
  varNames.reserve(m_n_states);
  m_JadeLoader.GetVarNames(&varNames);
  if (varNames.size()!=m_n_states) {
    std::stringstream sstream;
    sstream << "Something went terribly wrong. The number of states was reported to be " << m_n_states;
    sstream << ", but only " << varNames.size() << " variable names wer provided.\n";
    throw ESOException(sstream.str());
  }

  int algNamesCounter = 0;
  int diffNamesCounter = 0;
  for(EsoIndex i=0; i<m_n_states; i++){
    it = varsDiff.find(i);
    if(it == varsDiff.end()){
      algStateNames[algNamesCounter] = varNames[i];
      algNamesCounter++;
    }
    else{
      diffStateNames[diffNamesCounter] = varNames[i];
      diffNamesCounter++;
    }
  }
  m_algebraicStateVariables.setName(algStateNames);
  m_differentialStateVariables.setName(diffStateNames);

  const int nAlg = varsAlgIndices.size();
  const int nDiff = varsDiffIndices.size();
  if(nAlg+nDiff != m_n_states){
    throw ESOException("ERROR: mismatch in number of algebraic and differential variables!!!\n");
  }
  if(nAlg != eqnsAlgIndices.size() || nDiff != eqnsDiffIndices.size()){
    throw ESOException("ERROR: mismatch in number of algebraic equations and variables!!!\n");
  }

  /***** deletion of intermediate dynamic arrays *****/
  delete[] sp_var;
  delete[] sp_der_states;
  delete[] sp_param;
  delete[] sp_rhs;
}

/**
 * @brief Calculates first-order tangent-linear derivatives of the DA-System.
 *
 * This functions calls the code generated by dcc calculating first-order forward derivatives.
 * The actual values of the states, the parameters and the derivated states are taken from the
 * members m_states, m_parametersACS and m_der_states.
 * For explanation: z = [states p der_states]^T
 *                  all arrays with suffix '_states' or '_der_states' are of size (number of states)
 *                  all arrays with suffix '_p' are of size (number of parameters)
 *                  all arrays with suffix '_yy' or '_residuals' are of size (number of equations)
 *
 * output definition: t1_residuals = dF(z)/dz * t1_z
 *
 * @param[in] lenT1States length of t2States array. Must be equal to m_n_states
 * @param[in] t1States seed vector multiplying the Jacobian part relative to the states
 * @param[in] lenT1DerStates length of t2States array. Must be equal to m_n_states
 * @param[in] t1DerStates seed vector multiplying the Jacobian part relative to the derivated
 *            states
 * @param[in] lenT1P length of t2States array. Must be equal to m_n_pars
 * @param[in] t1P seed vector multiplying the Jacobian part relative to the parameters
 * @param[in] lenT1Res length of t2States array. Must be equal to m_n_eq
 * @param[out] t1Residuals product of the jacobian matrix and the seed vectors
 */
void JadeGenericEso::evaluate1stOrdForwDerivatives(unsigned lenT1States, double *t1States,
                                                      unsigned lenT1DerStates, double *t1DerStates,
                                                      unsigned lenT1P, double *t1P,
                                                      unsigned lenT1Res, double *t1Residuals)
{
  assert(t1States != NULL);
  assert(t1DerStates != NULL);
  assert(t1P != NULL);
  assert(t1Residuals != NULL);

  assert(lenT1States == (unsigned)m_n_states);
  assert(lenT1DerStates == (unsigned)m_n_states);
  assert(lenT1P == (unsigned)m_n_pars);

  utils::Array<int> conditions(m_n_cond, 0);
  m_JadeLoader.T1_res(m_res.getData(), t1Residuals, m_der_states.getData(), t1DerStates,
                         m_states.getData(), t1States, m_parametersACS.getData(), t1P,
                         conditions.getData(), m_locks.getData(), m_conditions.getData(),
                         m_n_states, m_n_pars, m_n_cond);
}

/**
 * @brief Calculates first-order tangent-linear derivatives of parts of the DA-System.
 *
 * This functions calls the code generated by dcc calculating first-order forward derivatives.
 * The actual values of the states, the parameters and the derivated states are taken from the
 * members m_states, m_parametersACS and m_der_states.
 * For explanation: z = [states p der_states]^T
 *                  all arrays with suffix '_states' or '_der_states' are of size (number of states)
 *                  all arrays with suffix '_p' are of size (number of parameters)
 *                  all arrays with suffix '_yy' or '_residuals' are of size (number of equations)
 *
 * output definition: t1_residuals = dF(z)/dz * t1_z
 *
 * @param[in] n_yy_ind number of equations whose derivatives are calculated
 * @param[in] yy_ind indices of equations whose derivatives are calculated
 * @param[in] t1_states seed vector multiplying the Jacobian part relative to the states
 * @param[in] t1_der_states seed vector multiplying the Jacobian part relative to the derivated
 *            states
 * @param[in] t1_p seed vector multiplying the Jacobian part relative to the parameters
 * @param[out] residuals residuum values of selected equations
 * @param[out] t1_residuals product of the jacobian matrix according to the selected equations and
 *             the seed vectors
 */
void JadeGenericEso::eval_t1_block_residuals(int n_yy_ind, int *yy_ind, double *t1_states,
                                                double *t1_der_states, double *t1_p,
                                                double *residuals, double *t1_residuals)
{
  // we use explicitly the "block"-version of the t1_res-function, where we can evaluate the
  // jacobian row-wise according to yy_ind
  utils::Array<int> conditions(m_n_cond, 0);
  m_JadeLoader.T1_res_block(residuals, t1_residuals, m_der_states.getData(), t1_der_states,
                               m_states.getData(), t1_states, m_parametersACS.getData(), t1_p,
                               conditions.getData(), m_locks.getData(), m_conditions.getData(),
                               m_n_states, m_n_pars, m_n_cond, n_yy_ind, yy_ind);
  for(int i=0; i<n_yy_ind; i++){
    m_res[yy_ind[i]] = residuals[i];
  }
}

/**
 * @brief Calculates first-order adjoint derivatives of the DA-System.
 *
 * This functions calls the code generated by dcc calculating first-order adjoint derivatives.
 * The actual values of the states, the parameters and the derivated states are taken from the
 * members m_states, m_parametersACS and m_der_states.
 * For explanation: z = [states p der_states]^T
 *                  all arrays with suffix '_states' or '_der_states' are of size (number of states)
 *                  all arrays with suffix '_p' are of size (number of parameters)
 *                  all arrays with suffix '_yy' or '_residuals' are of size (number of equations)
 *
 * output definition: a1_z = (dF(z)/dz)^T * a1_yy
 *
 * @param[in] a1_yy seed vector multiplying the transposed Jacobian
 * @param[out] a1_states contains on output the product of the transposed Jacobian, relative to the
 *             states, with a1_yy
 * @param[out] a1_der_states contains on output the product of the transposed Jacobian, relative to
 *             the derivated states, with a1_yy
 * @param[out] a1_p contains on output the product of the transposed Jacobian, relative to the
 *             parameters, with a1_yy
 */
void JadeGenericEso::eval_a1_residuals(double *a1_yy, double *a1_states, double *a1_der_states,
                                          double *a1_p)
{
  assert(a1_yy != NULL);
  assert(a1_states != NULL);
  assert(a1_der_states != NULL);
  assert(a1_p != NULL);

  int bmode_1 = 1;// @TODO: what's the aim of bmode_1 ? -> not documented dcc input
  for(EsoIndex i=0; i<m_n_states; i++){ // important for correct values in adjoint mode
    a1_states[i] = 0;
    a1_der_states[i] = 0;
  }
  for(EsoIndex i=0; i<m_n_pars; i++){
    a1_p[i] = 0;
  }
  utils::Array<int> conditions(m_n_cond, 0);
  m_JadeLoader.A1_res(bmode_1, m_res.getData(), a1_yy, m_der_states.getData(), a1_der_states,
                         m_states.getData(), a1_states, m_parametersACS.getData(), a1_p,
                         conditions.getData(), m_locks.getData(), m_conditions.getData(),
                         m_n_states, m_n_pars, m_n_cond);
}

/**
 * @brief Calculates second-order tangent-linear over adjoint derivatives of the DA-System.
 *
 * This functions calls the code generated by dcc calculating second-order tangent-linear over
 * adjoint derivatives. The actual values of the states, the parameters and the derivated states are
 * taken from the members m_states, m_parametersACS and m_der_states.
 * For explanation: z = [states p der_states]^T,
 *                  all arrays with suffix '_states' or '_der_states' are of size (number of states)
 *                  all arrays with suffix '_p' are of size (number of parameters)
 *                  all arrays with suffix '_yy' or '_residuals' are of size (number of equations)
 *                  <.,.> represents the inner dot product with appropriate dimensions.
 *
 * output definition: a1_z = (dF(z)/dz)^T * a1Yy;
 *                    t2_a1_z = <d2F(z)/dzdz, a1Yy, t2_z> + <dF(z)/dz, t2_a1Yy>;
 *
 * @param[in] lenT2States length of t2States array. Must be equal to m_n_states
 * @param[in] t2States array that defines the first m_n_states entries of t2_z
 * @param[in] lenT2DerStates length of t2DerStates arry. Must be equal to m_n_states
 * @param[in] t2DerStates array that defines the last m_n_states entries of t2_z
 * @param[in] lenT2P length of t2P array. Must be equal to m_n_pars
 * @param[in] t2P array that defines the entries from index m_n_states til index
 *            (m_n_states+m_n_pars-1) of t2_z
 * @param[in] lenA1States length of a1States array. Must be equal to m_n_states
 * @param[out] a1States contains on output the product of the transposed Jacobian, relative to the
 *             states, with a1Yy
 * @param[in] lenA1DerStates length of a1DerStates array. Must be equal to m_n_states
 * @param[out] a1DerStates contains on output the product of the transposed Jacobian, relative to
 *             the derivated states, with a1Yy
 * @param[in] lenA1P length of a1P array. Must be equal to m_n_pars
 * @param[out] a1P contains on output the product of the transposed Jacobian, relative to the
 *             parameters, with a1Yy
 * @param[in] lenT2A1States length of t2A1States array. Must be equal to m_n_states
 * @param[out] t2A1States array that defines the first m_n_states entries of output array
 *             t2_a1_z
 * @param[in] lenT2A1DerStates length of t2A1DerStates array. Must be equal to m_n_states
 * @param[out] t2A1DerStates array that defines the last m_n_states entries of output array
 *             t2_a1_z
 * @param[in] lenT2A1P length of t2A1P array. Must be equal to m_n_pars
 * @param[out] t2A1P array that defines the entries from index m_n_states til index
 *            (m_n_states+m_n_pars-1) of output array t2_a1_z
 * @param[in] lenA1Yy length of a1Yy array. Must be equal to m_n_eq
 * @param[in] a1Yy seed vector multiplying the Jacobian matrix to calculate a1_z and the Hessian
 *            matrix to calculate t2_a1_z.
 * @param[in] lenT2A1Yy length of t2A1Yy array. Must be equal to m_n_eq
 * @param[in] t2A1Yy seed vector to define the jacobian part of t2_a1_z
 */
void JadeGenericEso::evaluate2ndOrderDerivatives(
                unsigned lenT2States, double *t2States,
                unsigned lenT2DerStates, double *t2DerStates,
                unsigned lenT2P, double *t2P,
                unsigned lenA1States, double *a1States,
                unsigned lenA1DerStates, double *a1DerStates,
                unsigned lenA1P, double *a1P,
                unsigned lenT2A1States, double *t2A1States,
                unsigned lenT2A1DerStates, double *t2A1DerStates,
                unsigned lenT2A1P, double *t2A1P,
                unsigned lenA1Yy, double *a1Yy,
                unsigned lenT2A1Yy, double *t2A1Yy)
{
  assert(t2States != NULL);
  assert(t2DerStates != NULL);
  assert(t2P != NULL);
  assert(a1States != NULL);
  assert(a1DerStates != NULL);
  assert(a1P != NULL);
  assert(t2A1States != NULL);
  assert(t2A1DerStates != NULL);
  assert(t2A1P != NULL);
  assert(a1Yy != NULL);
  assert(t2A1Yy != NULL);

  assert(lenT2States == (unsigned)m_n_states);
  assert(lenT2DerStates == (unsigned)m_n_states);
  assert(lenT2P == (unsigned)m_n_pars);
  assert(lenA1States == (unsigned)m_n_states);
  assert(lenA1DerStates == (unsigned)m_n_states);
  assert(lenA1P == (unsigned)m_n_pars);
  assert(lenT2A1States == (unsigned)m_n_states);
  assert(lenT2A1DerStates == (unsigned)m_n_states);
  assert(lenT2A1P == (unsigned)m_n_pars);
  assert(lenA1Yy == (unsigned)m_n_eq);
  assert(lenT2A1Yy == (unsigned)m_n_eq);

  int bmode_1 = 1;// @TODO: what's the aim of bmode_1 ? -> not documented dcc input
  for(EsoIndex i=0; i<m_n_states; i++){ // important for correct values in adjoint mode
    a1States[i] = 0;
    a1DerStates[i] = 0;
    t2A1States[i] = 0;
    t2A1DerStates[i] = 0;
  }
  for(EsoIndex i=0; i<m_n_pars; i++){
    a1P[i] = 0;
    t2A1P[i] = 0;
  }
  utils::Array<int> conditions(m_n_cond, 0);
  m_JadeLoader.T2_a1_res(bmode_1, m_res.getData(), m_t2_res.getData(), a1Yy, t2A1Yy,
                            m_der_states.getData(), t2DerStates, a1DerStates, t2A1DerStates,
                            m_states.getData(), t2States, a1States, t2A1States,
                            m_parametersACS.getData(), t2P, a1P, t2A1P,
                            conditions.getData(), m_locks.getData(), m_conditions.getData(),
                            m_n_states, m_n_pars, m_n_cond);
}

/** @copydoc GenericEso::setIndependentVariable(const EsoDouble)
 */
void JadeGenericEso::setIndependentVariable(const EsoDouble var)
{
  m_independentVar = var;
}

/** @copydoc GenericEso::getModel()
*/
ModelName JadeGenericEso::getModel() const {
	return m_model;
}

/** @copydoc GenericEso::getIndependentVariable()
 */
EsoDouble JadeGenericEso::getIndependentVariable() const
{
  return m_independentVar;
}

/** @copydoc GenericEso::getVariableNames(std::vector<std::string>&)
 */
void JadeGenericEso::getVariableNames(std::vector<std::string> &names)
{
  names.clear();
  m_JadeLoader.GetVarNames(&names);
  m_JadeLoader.GetParNames(&names);
}

/** @copydoc GenericEso::setAllVariableValues(EsoIndex,const EsoDouble*)
 */
void JadeGenericEso::setAllVariableValues(const EsoIndex n_var, const EsoDouble *variables)
{
  assert(n_var == (m_n_states + m_n_pars));

  for(EsoIndex i=0; i<m_n_states; i++){
    m_states[i] = variables[i];
  }
  for(EsoIndex i=0; i<m_n_pars; i++){
    m_parametersACS[i] = variables[m_n_states+i];
  }
}

/** @copydoc GenericEso::setParameterValues(EsoIndex,const EsoDouble*)
 */
void JadeGenericEso::setParameterValues(const EsoIndex n_pars, const EsoDouble *parameters)
{
  assert(n_pars == m_n_pars);

  for(EsoIndex i=0; i<m_n_pars; i++){
    m_parametersACS[i] = parameters[i];
  }
}

/** @copydoc GenericEso::setAlgebraicVariableValues(EsoIndex,const EsoDouble*)
 */
void JadeGenericEso::setAlgebraicVariableValues(const EsoIndex n_alg_var, const EsoDouble *algebraicVariables)
{
  assert(n_alg_var == m_algebraicStateVariables.getNumberOfVariables());

  const vector<EsoIndex> algStateIndices = m_algebraicStateVariables.getEsoIndices();
  for(EsoIndex i=0; i<m_algebraicStateVariables.getNumberOfVariables(); i++){
    m_states[algStateIndices[i]] = algebraicVariables[i];
  }
}

/** @copydoc GenericEso::setDifferentialVariableValues(EsoIndex,const EsoDouble*)
 */
void JadeGenericEso::setDifferentialVariableValues(const EsoIndex n_diff_var,
                                                      const EsoDouble *differentialVariables)
{
  assert(n_diff_var == m_differentialStateVariables.getNumberOfVariables());

  const vector<EsoIndex> diffStateIndices = m_differentialStateVariables.getEsoIndices();
  for(EsoIndex i=0; i<m_differentialStateVariables.getNumberOfVariables(); i++){
    m_initialStates[diffStateIndices[i]] = differentialVariables[i];
    m_states[diffStateIndices[i]] = differentialVariables[i];
  }
}

/** @copydoc GenericEso::setStateValues(EsoIndex,const EsoDouble*)
 */
void JadeGenericEso::setStateValues(const EsoIndex n_states, const EsoDouble *states)
{
  assert(n_states == m_stateVariables.getNumberOfVariables());

  const vector<EsoIndex> stateIndices = m_stateVariables.getEsoIndices();
  for(EsoIndex i=0; i<m_stateVariables.getNumberOfVariables(); i++){
    m_states[stateIndices[i]] = states[i];
  }
  //update residuals
  utils::Array<int> conditions(m_n_cond, 0);
  m_JadeLoader.Res(m_res.getData(), m_der_states.getData(), m_states.getData(),
                      m_parametersACS.getData(),
                      conditions.getData(), m_locks.getData(), m_conditions.getData(),
                      m_n_states, m_n_pars, m_n_cond);
}

/** @copydoc GenericEso::setVariableValues(EsoIndex,const EsoDouble*,const EsoIndex*)
 */
void JadeGenericEso::setVariableValues(const EsoIndex n_idx, const EsoDouble *variables,
                                          const EsoIndex *indices)
{
  // provide assert function to check index vector (range 0..m_n_states + m_n_pars)
  for(EsoIndex i=0; i<n_idx; i++){
    assert((0 <= indices[i]) && (indices[i] < m_n_states + m_n_pars));
  }

  for(EsoIndex i=0; i<n_idx; i++){
    if(indices[i] < m_n_states) m_states[indices[i]] = variables[i];
    else                        m_parametersACS[indices[i]-m_n_states] = variables[i];
  }
}

/** @copydoc GenericEso::getAllVariableValues(EsoIndex,EsoDouble*)
 */
void JadeGenericEso::getAllVariableValues(const EsoIndex n_var, EsoDouble *variables) const
{
  assert(n_var == (m_n_states + m_n_pars));

  for(EsoIndex i=0; i<m_n_states; i++){
    variables[i] = m_states[i];
  }
  for(EsoIndex i=0; i<m_n_pars; i++){
    variables[m_n_states+i] = m_parametersACS[i];
  }
}

/** @copydoc GenericEso::getParameterValues(EsoIndex,EsoDouble*)
 */
void JadeGenericEso::getParameterValues(const EsoIndex n_params, EsoDouble *parameters)
{
  assert(n_params == m_n_pars);

  for(EsoIndex i=0; i<m_n_pars; i++){
    parameters[i] = m_parametersACS[i];
  }
}

/** @copydoc GenericEso::getAlgebraicVariableValues(EsoIndex,EsoDouble*)
 */
void JadeGenericEso::getAlgebraicVariableValues(const EsoIndex n_alg_var, EsoDouble *algebraicVariables)
{
  assert(n_alg_var == m_algebraicStateVariables.getNumberOfVariables());

  const vector<EsoIndex> algStateIndices = m_algebraicStateVariables.getEsoIndices();
  for(EsoIndex i=0; i<m_algebraicStateVariables.getNumberOfVariables(); i++){
    algebraicVariables[i] = m_states[algStateIndices[i]];
  }
}

/** @copydoc GenericEso::getDifferentialVariableValues(EsoIndex,EsoDouble*)
 */
void JadeGenericEso::getDifferentialVariableValues(const EsoIndex n_diff_var,
                                                      EsoDouble *differentialVariables)
{
  assert(n_diff_var == m_differentialStateVariables.getNumberOfVariables());

  const vector<EsoIndex> diffStateIndices = m_differentialStateVariables.getEsoIndices();
  for(EsoIndex i=0; i<m_differentialStateVariables.getNumberOfVariables(); i++){
    differentialVariables[i] = m_states[diffStateIndices[i]];
  }
}

/** @copydoc GenericEso::getStateValues(EsoIndex,EsoDouble*)
 */
void JadeGenericEso::getStateValues(const EsoIndex n_states, EsoDouble *states)
{
  assert(n_states == m_stateVariables.getNumberOfVariables());

  const vector<EsoIndex> stateIndices = m_stateVariables.getEsoIndices();
  for(EsoIndex i=0; i<m_stateVariables.getNumberOfVariables();i++){
    states[i] = m_states[stateIndices[i]];
  }
}

/**
* @copydoc GenericEso::getInitialStateValues(EsoIndex, EsoDouble*)
*
* in JadeGenericEso this function is not yet implemented (as Jade does not cope with initial equation system)
*/ 
void JadeGenericEso::getInitialStateValues(const EsoIndex n_states, EsoDouble *states)
{
  getStateValues(n_states, states);
}

/** @copydoc GenericEso::getVariableValues(EsoIndex,EsoDouble*,const EsoIndex*)
 */
void JadeGenericEso::getVariableValues(const EsoIndex n_idx, EsoDouble *variables,
                                          const EsoIndex *indices)
{
  // provide assert function to check index vector (range 0..m_n_states + m_n_pars)
  for(EsoIndex i=0; i<n_idx; i++){
    assert((0 <= indices[i]) && (indices[i] < m_n_states + m_n_pars));
  }


  for(EsoIndex i=0; i<n_idx; i++){
    if(indices[i]<m_n_states) variables[i] = m_states[indices[i]];
    else                      variables[i] = m_parametersACS[indices[i] - m_n_states];
  }
}

/** @copydoc GenericEso::setDerivativeValues(EsoIndex,const EsoDouble*)
 */
void JadeGenericEso::setDerivativeValues(const EsoIndex n_diff_var, const EsoDouble *derivatives)
{

  const vector<EsoIndex> diffStateIndices = m_differentialStateVariables.getEsoIndices();
  const int numdiffStates = diffStateIndices.size();
  assert(n_diff_var == numdiffStates);

  for(int i=0; i<numdiffStates; i++){
    m_der_states[diffStateIndices[i]] = derivatives[i]; // m_der_states according to alg. vars. are
  }                                                     // set to zero in the constructor
}

/** @copydoc GenericEso::getDerivativeValues(EsoIndex,EsoDouble*)
 */
void JadeGenericEso::getDerivativeValues(const EsoIndex n_diff_var, EsoDouble *derivatives)
{
  assert(n_diff_var == getNumDifferentialVariables());

  // in Jade the derivatives to all states (also the algebraic) are defined
  // for the GenericEso interface only the derivatives of the differential variables are used.
  // so first map the m_der_states vector to all variables and then extract the differential
  // state derivatives

  std::vector<double> allDerivatives(m_n_states+m_n_pars);
  utils::Array<EsoIndex> stateIndices(m_n_states);
  getStateIndex(m_n_states, stateIndices.getData());
  for(EsoIndex i=0; i< m_n_states; i++){
    allDerivatives[stateIndices[i]] = m_der_states[i];
  }

  utils::Array<EsoIndex> diffStateIndices(n_diff_var);
  getDifferentialIndex(n_diff_var, diffStateIndices.getData());
  for(EsoIndex i=0; i<n_diff_var; i++){
    derivatives[i] = allDerivatives[diffStateIndices[i]];
  }
}

/** @copydoc GenericEso::getAllBounds(EsoIndex,EsoDouble*,EsoDouble*)
 */
void JadeGenericEso::getAllBounds(const EsoIndex n_var, EsoDouble *lowerBounds, EsoDouble *upperBounds)
{
  assert(n_var == (m_n_states + m_n_pars));
  /*** Determine all indices ***/

  const vector<EsoIndex> algStateIndices = m_algebraicStateVariables.getEsoIndices();
  const vector<EsoIndex> diffStateIndices = m_differentialStateVariables.getEsoIndices();
  const vector<EsoIndex> parameterIndices = m_parameters.getEsoIndices();

  /*** Get all bounds from individual elements ***/

  const vector<double> algLowerBounds = m_algebraicStateVariables.getLowerModelBound();
  const vector<double> algUpperBounds = m_algebraicStateVariables.getUpperModelBound();
  const vector<double> diffLowerBounds = m_differentialStateVariables.getLowerModelBound();
  const vector<double> diffUpperBounds = m_differentialStateVariables.getUpperModelBound();
  const vector<double> parameterLowerBounds = m_parameters.getLowerModelBound();
  const vector<double> parameterUpperBounds = m_parameters.getUpperModelBound();

  /*** Copy all bounds to the right output according to the variable indices ***/

  for(EsoIndex i=0; i<m_algebraicStateVariables.getNumberOfVariables(); i++){
    lowerBounds[algStateIndices[i]] = algLowerBounds[i];
    upperBounds[algStateIndices[i]] = algUpperBounds[i];
  }
  for(EsoIndex i=0; i<m_differentialStateVariables.getNumberOfVariables(); i++){
    lowerBounds[diffStateIndices[i]] = diffLowerBounds[i];
    upperBounds[diffStateIndices[i]] = diffUpperBounds[i];
  }
  for(EsoIndex i=0; i<m_parameters.getNumberOfVariables(); i++){
    lowerBounds[parameterIndices[i]] = parameterLowerBounds[i];
    upperBounds[parameterIndices[i]] = parameterUpperBounds[i];
  }
}

/** @copydoc GenericEso::getAllResiduals(EsoIndex,EsoDouble*)
 */
GenericEso::RetFlag JadeGenericEso::getAllResiduals(const EsoIndex n_eq, EsoDouble *residuals)
{
  assert(n_eq == m_numEquations);

  utils::Array<int> conditions(m_n_cond, 0);
  m_JadeLoader.Res(m_res.getData(), m_der_states.getData(), m_states.getData(),
                      m_parametersACS.getData(),
                      conditions.getData(), m_locks.getData(), m_conditions.getData(),
                      m_n_states, m_n_pars, m_n_cond);
  for(EsoIndex i=0; i<m_numEquations; i++){
    residuals[i] = m_res[i];
    //this is true nor NANs
    if(m_res[i] != m_res[i]){
        std::cout << "Residual " << i << " has a value of " << m_res[i] << std::endl;
      return GenericEso::FAIL;
    }
  }
  return GenericEso::OK;
}

/** @copydoc GenericEso::getDifferentialResiduals(EsoIndex,EsoDouble*)
 */
GenericEso::RetFlag JadeGenericEso::getDifferentialResiduals(const EsoIndex n_diff_eq, EsoDouble *differentialResiduals)
{
  assert(n_diff_eq == m_differentialEquations.getNumEquations());

  utils::Array<int> conditions(m_n_cond, 0);
  m_JadeLoader.Res(m_res.getData(), m_der_states.getData(), m_states.getData(),
                      m_parametersACS.getData(),
                      conditions.getData(), m_locks.getData(), m_conditions.getData(), m_n_states, m_n_pars, m_n_cond);
  const vector<EsoIndex> diffEqIndices = m_differentialEquations.getEquationIndex();
  for(EsoIndex i=0; i<m_differentialEquations.getNumEquations(); i++){
    differentialResiduals[i] = m_res[diffEqIndices[i]];
    //true for NANs
    if(differentialResiduals[i] != differentialResiduals[i]){
      return GenericEso::FAIL;
    }
  }
  return GenericEso::OK;
}

/** @copydoc GenericEso::getAlgebraicResiduals(EsoIndex,EsoDouble*)
 */
GenericEso::RetFlag JadeGenericEso::getAlgebraicResiduals(const EsoIndex n_alg_eq, EsoDouble *algebraicResiduals)
{
  assert(n_alg_eq == m_algebraicEquations.getNumEquations());

  utils::Array<int> conditions(m_n_cond, 0);
  m_JadeLoader.Res(m_res.getData(), m_der_states.getData(), m_states.getData(),
                      m_parametersACS.getData(),
                      conditions.getData(), m_locks.getData(), m_conditions.getData(),
                      m_n_states, m_n_pars, m_n_cond);
  const vector<EsoIndex> algEqIndices = m_algebraicEquations.getEquationIndex();
  for(EsoIndex i=0; i<m_algebraicEquations.getNumEquations(); i++){
    algebraicResiduals[i] = m_res[algEqIndices[i]];
    //true for NANs
    if(algebraicResiduals[i] != algebraicResiduals[i]){
      return GenericEso::FAIL;
    }
  }
  return GenericEso::OK;
}

/** @copydoc GenericEso::getResiduals(EsoIndex,EsoDouble*,const EsoIndex*)
 */
GenericEso::RetFlag JadeGenericEso::getResiduals(const EsoIndex n_eq, EsoDouble *residuals,
                                     const EsoIndex *equationIndex)
{
  for (EsoIndex i=0; i< n_eq; i++) {
    assert(equationIndex[i] < int(m_numEquations));
  }

  if(n_eq < m_numEquations){
    // block-wise evaluation of the residual
    utils::Array<int> equationIndexArray(n_eq);
    for(EsoIndex i=0; i<n_eq; i++){
      equationIndexArray[i] = equationIndex[i];
    }
    int numEqns = n_eq;
    utils::Array<int> conditions(m_n_cond, 0);
    m_JadeLoader.Res_block(residuals, m_der_states.getData(), m_states.getData(),
                              m_parametersACS.getData(),
                              conditions.getData(), m_locks.getData(), m_conditions.getData(),
                              m_n_states, m_n_pars, m_n_cond, numEqns,
                              equationIndexArray.getData());
    for(EsoIndex i=0; i<n_eq; i++){
      //true for NANs
      if(residuals[i] != residuals[i]){
        return GenericEso::FAIL;
      }
    }
  }
  else{
    // complete evaluation of the residual
    utils::Array<int> conditions(m_n_cond, 0);
    m_JadeLoader.Res(m_res.getData(), m_der_states.getData(), m_states.getData(),
                        m_parametersACS.getData(),
                        conditions.getData(), m_locks.getData(), m_conditions.getData(),
                        m_n_states, m_n_pars, m_n_cond);
    for(EsoIndex i=0; i<n_eq; i++){
      residuals[i] = m_res[equationIndex[i]];
      //true for NANs
      if(residuals[i] != residuals[i]){
        return GenericEso::FAIL;
      }
    }
  }
  return GenericEso::OK;
}

/**
 * @brief see description in GenericEso.hpp
 */
GenericEso::RetFlag JadeGenericEso::getConditionResiduals(const EsoIndex n_c, EsoDouble *residuals)
{
  assert(n_c==m_n_cond);
  m_JadeLoader.Res_cond(residuals, m_der_states.getData(), m_states.getData(),
                           m_parametersACS.getData(), m_n_states, m_n_pars, m_n_cond);
  for(EsoIndex i=0; i<n_c; i++){
    //true for NANs
    if(residuals[i] != residuals[i]){
      return GenericEso::FAIL;
    }
  }
  return GenericEso::OK;
}

/**
 * @brief evaluates the condition functions at the current point and returns them
 */
void JadeGenericEso::evalConditions(const EsoIndex n_c, EsoIndex *conditions)
{
  assert(n_c==m_n_cond);
  m_JadeLoader.Eval_cond(conditions, m_der_states.getData(), m_states.getData(),
                            m_parametersACS.getData(), m_n_states, m_n_pars, m_n_cond);
}

/**
 * @brief fix which conditions should be evaluated. (used in conjunction with setLocks)
 */
void JadeGenericEso::setConditions(const EsoIndex n_c, const EsoIndex* conditions)
{
  assert(n_c==m_n_cond);
  bool need_update = false;
  for (EsoIndex k=0; k<n_c; ++k)
    if (conditions[k] != m_conditions[k]) {
      need_update = true;
      break;
    }
  utils::copy(m_n_cond, conditions, m_conditions.getData());
  if (need_update)
    updateAfterSwitch();
}

void JadeGenericEso::getCurrentConditions(const EsoIndex n_c, EsoIndex* conditions)
{
  assert(n_c==m_n_cond);
  utils::copy(m_n_cond, m_conditions.getData(), conditions);
}

/**
 * @brief see description in GenericEso.hpp
 */
void JadeGenericEso::setLocks(const EsoIndex n_c, const EsoIndex* locks) {
  assert(n_c==m_n_cond);
  utils::copy(m_n_cond, locks, m_locks.getData());
}

void JadeGenericEso::getLocks(const EsoIndex n_c, EsoIndex* locks) {
  assert(n_c==m_n_cond);
  utils::copy(m_n_cond, m_locks.getData(), locks);
}

/**
 * @brief get all Jacobian values
 *
 * @param values array receiving the current values - must be of size numNonZeroes
 * @todo check this documentation for correctness
 */
GenericEso::RetFlag JadeGenericEso::getJacobianValues(const EsoIndex n_nz, EsoDouble *values)
{
  // we use the tangent-linear code
  double** seed_t = m_sparseJacobian->getTransposedSeed();
  for(EsoIndex i=0; i<m_n_states; i++){
    m_t1_der_states[i] = 0;
  }
  for(EsoIndex k=0; k<m_sparseJacobian->getNumColsSeed(); k++){
    evaluate1stOrdForwDerivatives(m_n_states, (seed_t)[k], m_n_states, m_t1_der_states.getData(),
                                  m_n_pars, (seed_t)[k]+m_n_states, m_n_eq, m_t1_res.getData());
    for(EsoIndex i=0; i<m_n_eq; i++) {
      m_buffer[i][k] = m_t1_res[i];
      //true for NANs
      if(m_t1_res[i] != m_t1_res[i]){
        return GenericEso::FAIL;
      }
    }
  }

  /********** Recovery using ColPack **********/

  utils::WrappingArray<EsoDouble> valuesArray(n_nz, values);
  m_sparseJacobian->getValuesFromBuffer(m_buffer, valuesArray.getData(), n_nz);
  return GenericEso::OK;
}

/**
 * @brief get a subset of Jacobian values
 *
 * @param values array receiving the current values - must be of the same size as indices
 * @param indices vector containing indices of the Jacobian values to be received - each index
 *        must be in range [0..numNonZeroes-1]
 */
GenericEso::RetFlag JadeGenericEso::getJacobianValues(const EsoIndex n_idx, EsoDouble *values,
                                          const EsoIndex *indices)
{
  //=====================================================================
  // in case where the state-jacobian or parameter-jacobian is wanted,
  // it is probably more efficient to calculate the jacobian in one call
  //=====================================================================
  if((n_idx==m_n_stateJacEntries) || (n_idx==m_n_paramJacEntries)){
    const EsoIndex numNonZeros = getNumNonZeroes();
    for (EsoIndex i=0; i<n_idx; i++) {
      assert((indices[i]>=0) && (indices[i]<int(numNonZeros)));
    }

    utils::Array<EsoDouble> allValues(numNonZeros);
    getJacobianValues(numNonZeros, allValues.getData());
    for(EsoIndex i=0; i<n_idx; i++){
      values[i] = allValues[indices[i]];
    }
    //=====================================================================
    // in case where only few jacobian values are wanted, it is
    // probably more efficient to calculate them separately
    //=====================================================================
  }
  else{
    utils::Array<EsoIndex> rowIndices(n_idx);
    utils::Array<EsoIndex> colIndices(n_idx);
    utils::Array<double> valuesArray(n_idx);
    utils::Array<EsoIndex> indicesNonConst(n_idx);
    for(EsoIndex i=0; i<n_idx; i++){
      indicesNonConst[i] = indices[i];
    }
    const utils::WrappingArray<EsoIndex> indicesArray(n_idx, indicesNonConst.getData());
    m_sparseJacobian->getSparsityPattern(indicesArray.getData(), n_idx, rowIndices.getData(),
                                         colIndices.getData());

    for(EsoIndex i=0; i<m_n_states; i++){
      m_t1_der_states[i] = 0.0;
    }
    for(EsoIndex i=0; i<m_n_pars; i++){
      m_t1_p[i] = 0.0;
    }
    for(EsoIndex i=0; i<m_n_states; i++){
      m_t1_states[i] = 0.0;
    }
    int numEqLoc = 1;
    double res[1];

    for(EsoIndex i=0; i<n_idx; i++){ // we calculate each entry of the Jacobian separately
      if(colIndices[i] < m_n_states){
        m_t1_states[colIndices[i]] = 1.0;
      }
      else{
        m_t1_p[colIndices[i]-m_n_states] = 1.0;
      }
      eval_t1_block_residuals(numEqLoc, (int*)rowIndices.getData()+i, m_t1_states.getData(),
                              m_t1_der_states.getData(), m_t1_p.getData(), res,
                              valuesArray.getData()+i);
      if(colIndices[i] < m_n_states){
        m_t1_states[colIndices[i]] = 0.0;
      }
      else{
        m_t1_p[colIndices[i]-m_n_states] = 0.0;
      }
    }

    for(unsigned i=0; i<valuesArray.getSize(); i++){
      values[i] = valuesArray[i];
      //true for NANs
      if(values[i] != values[i]){
        return GenericEso::FAIL;
      }
    }
  }
  return GenericEso::OK;
}

/** @copydoc GenericEso::getDiffJacobianValues(EsoIndex,EsoDouble*)
 */
GenericEso::RetFlag JadeGenericEso::getDiffJacobianValues(const EsoIndex n_diff_nz, EsoDouble *values)
{
  // we use the tangent-linear code
  double** seedDiff_t = m_sparseDiffJacobian->getTransposedSeed();
  for(EsoIndex i=0; i<m_n_states; i++){
    m_t1_states[i] = 0;
  }
  for(EsoIndex i=0; i<m_n_pars; i++){
    m_t1_p[i] = 0;
  }
  for(EsoIndex k=0; k<m_sparseDiffJacobian->getNumColsSeed(); k++){
    evaluate1stOrdForwDerivatives(m_n_states, m_t1_states.getData(), m_n_states, (seedDiff_t)[k],
                                  m_n_pars, m_t1_p.getData(), m_n_eq, m_t1_res.getData());
    for(EsoIndex i=0; i<m_n_states; i++){
      m_bufferDiff[i][k] = m_t1_res[i];
      //true for NANs
      if(m_t1_res[i] != m_t1_res[i]){
        return GenericEso::FAIL;
      }
    }
  }

  /********** Recovery using ColPack **********/

  utils::WrappingArray<EsoDouble> valuesArray(n_diff_nz, values);
  m_sparseDiffJacobian->getValuesFromBuffer(m_bufferDiff, valuesArray.getData(), n_diff_nz);
  return GenericEso::OK;
}

/**
* @copydoc GenericEso::getInitialJacobian(EsoIndex, EsoIndex*, EsoIndex*, EsoDouble*)
*
* initial Eso not definded for Jade - use standard Jacobian as InitialJacobian
*/
GenericEso::RetFlag JadeGenericEso::getInitialJacobian(const EsoIndex n_nz, EsoIndex *rowIndices,
                                                                EsoIndex *colIndices, EsoDouble *values)
{
  return GenericEso::OK;
}

GenericEso::RetFlag JadeGenericEso::setInitialValues(const EsoIndex numDiffVals,
                                                              EsoIndex *diffIndices,
                                                              EsoDouble * diffValues,
                                                        const EsoIndex numParameters,
                                                              EsoIndex *parameterIndices)
{
  assert(numDiffVals <= m_differentialStateVariables.getNumberOfVariables());

  std::vector<EsoIndex> diffIndex = m_differentialStateVariables.getEsoIndices();
  for(EsoIndex i=0; i<numDiffVals; i++){
    //throw assert if index in diffIndices is no actual differential index
    assert(std::find(diffIndex.begin(),
                     diffIndex.end(),
                     diffIndices[i])
           != diffIndex.end());
    m_initialStates[diffIndices[i]] = diffValues[i];
  }
  for(EsoIndex i=0; i<m_differentialStateVariables.getNumberOfVariables(); i++){
    m_states[diffIndex[i]] = m_initialStates[diffIndex[i]];
  }
  
  return GenericEso::OK;
}

/** @copydoc GenericEso::getJacobianMultVector(bool,EsoIndex,EsoDouble*,EsoIndex,EsoDouble*,
 *                                             EsoIndex,EsoDouble*)
 */
GenericEso::RetFlag JadeGenericEso::getJacobianMultVector(const bool transpose, const EsoIndex n_states, EsoDouble *seedStates,
                                              const EsoIndex n_params, EsoDouble *seedParameters, const EsoIndex n_y,
                                              EsoDouble *y)
{
  if(transpose==false){// tangent-linear jacobian vector product
    for(EsoIndex i=0; i<m_n_states; i++){
      m_t1_der_states[i] = 0;
    }
    evaluate1stOrdForwDerivatives(m_n_states, (double*)seedStates, m_n_states, m_t1_der_states.getData(),
                                  m_n_pars, (double*)seedParameters, m_n_eq, (double*)y);
  }
  else{// adjoint transposed jacobian vector product
    eval_a1_residuals((double*)y, (double*)seedStates, m_a1_der_states.getData(), (double*)seedParameters);
  }
  return GenericEso::OK;
}

/** @brief Calculates the product of the Jacobian and a multivector

    @param[in] multivector_gradient of size m_n_states x np, where np is the number of Jacobian vector
    products that are calculated. The storage order of the input is:
    [ds0/dp0, ds0/dp1, ... ds0/dp(np-1), ds1/dp0, ..., ds(m_n_states-1)/dp(np-1)]
    where s are the states and p are the parameters
    @param[in] np number of Jacobian vector products
    @param[out] directional_derivs directional derivative of size m_n_eq x np.
    directional_derivs := jacobian values x multivector. The storage order of the output is:
    [de0/dp0, de0/dp1, ... de0/dp(np-1), de1/dp0, ..., de(m_n_eq-1)/dp(np-1)]
    where e are the equations and p are the parameters
*/
//void JadeGenericEso::get_jac_mult_multivector(double *multivector_gradient, long np,
//                                                 double *directional_derivs)
//{
//  // directional_derivs := jacobian values x multivector_gradient
//  // de_k/dp_l = sum_i=1->n_var(dpartial e_k/dpartial states_i * dpartial states_i/dpartial p_l)
//  //             + dpartial e_k/dpartial p_l
//  for(EsoIndex i=0; i<m_n_states; i++){
//    m_t1_der_states[i] = 0;
//  }
//  for(EsoIndex i=0; i<m_n_pars; i++){
//    m_t1_p[i] = 0;
//  }
//  for(int ip=0; ip<np; ip++){
//    m_t1_p[ip] = 1;
//    for(int iv=0; iv<m_n_states; iv++){
//      m_t1_states[iv] = multivector_gradient[iv*np+ip];
//    }
//    eval_t1_residuals(m_t1_states.getData(), m_t1_der_states.getData(), m_t1_p.getData(),
//                      m_t1_res.getData());
//    for(EsoIndex ie=0; ie<m_n_eq; ie++){
//      directional_derivs[ie*np+ip] = m_t1_res[ie];
//    }
//    m_t1_p[ip] = 0;
//  }
//}

void JadeGenericEso::updateAfterSwitch()
{
  calculateJacobianStructure();
}


JadeGenericEsoWithInitialization::JadeGenericEsoWithInitialization(const std::string &model,
                                                                         const std::string &initialModel)
    : JadeGenericEso(model), m_initialEso(initialModel)
{
  m_numInitialNonZeroes = m_initialEso.getNumNonZeroes();
  
  std::vector<std::string> varNames, initialVarNames;
  m_initialEso.getVariableNames(initialVarNames);
  getVariableNames(varNames);
  
  //every used variable in the initial model must be found in the main model
  for(unsigned i=0; i<initialVarNames.size(); i++){
    std::vector<std::string>::iterator position;
    position = std::find(varNames.begin(), varNames.end(), initialVarNames[i]);
    if(position == varNames.end()){
      std::stringstream sstream;
      sstream<<"Main model and initial model are incompatible. Did not find "<<initialVarNames[i]<<" in main model."<<std::endl;
      throw ESOException(sstream.str());
    }
    else{
      int originalIndex = position - varNames.begin();
      m_varIndexMapToMain[i] = originalIndex;
      m_varIndexMapToInitial[originalIndex] = i;
    }
  }
}

void JadeGenericEsoWithInitialization::getInitialStateValues(const EsoIndex n_states, EsoDouble *states)
{
  
  getStateValues(n_states, states);
}

GenericEso::RetFlag JadeGenericEsoWithInitialization::getInitialJacobian(const EsoIndex n_nz, 
                                                                                   EsoIndex *rowIndices,
                                                                                   EsoIndex *colIndices,
                                                                                   EsoDouble *values)
{
  m_initialEso.getJacobianStruct(n_nz, rowIndices, colIndices);
  for(EsoIndex i=0; i<n_nz; i++){
    colIndices[i] = m_varIndexMapToMain[colIndices[i]];
  }
  return m_initialEso.getJacobianValues(n_nz, values);
}


GenericEso::RetFlag JadeGenericEsoWithInitialization::getFreeVariables(
                                     const EsoIndex numDiffVals,
                                           EsoIndex *diffIndices,
                                     const EsoIndex numParameters,
                                           EsoIndex *parameterIndices,
                                           std::vector<std::vector<EsoIndex> > &freeVarIndices,
                                     const EsoIndex numNonZeroes,
                                     const utils::Array<EsoIndex> &rowIndices,
                                     const utils::Array<EsoIndex> &colIndices)
{
  const EsoIndex numDiffValsMain = getNumDifferentialVariables();
  utils::Array<EsoIndex> diffIndicesMain(numDiffValsMain);
  getDifferentialIndex(numDiffValsMain, diffIndicesMain.getData());
  
  assert(numNonZeroes > 0);
  EsoIndex previousRow = rowIndices[0];
  std::vector<EsoIndex> doubleEntries(1, colIndices[0]);
  std::vector<EsoIndex> emptyVector;
  freeVarIndices.push_back(emptyVector);
  
  for(EsoIndex i=1; i<=numNonZeroes; i++){
    bool evaluateDoubleEntries = false;
    if(i<numNonZeroes){
      if(previousRow != rowIndices[i]){
        evaluateDoubleEntries = true;
      }
    }
    else{
      evaluateDoubleEntries = true;
    }
    if(evaluateDoubleEntries){
      freeVarIndices.push_back(emptyVector);
      freeVarIndices.front().push_back(previousRow);
      for(unsigned j=0; j<doubleEntries.size(); j++){
        bool found = false;
        found = std::find(diffIndices, diffIndices + numDiffVals, doubleEntries[j])
                != diffIndices + numDiffVals;
        found = found ||
                std::find(parameterIndices, parameterIndices + numParameters, doubleEntries[j])
                != parameterIndices + numParameters;
        if(!found){
          freeVarIndices.back().push_back(doubleEntries[j]);
        }
      }
      if(freeVarIndices.back().empty()){
        freeVarIndices.pop_back();
        freeVarIndices.front().pop_back();
        //error if equation has more than one entry, but no degrees of freedom
        if(doubleEntries.size() > 1){
          return GenericEso::FAIL;
        }
      }
      //ignore equations where all variables are degrees of freedom, only the differential index is used
      else if(freeVarIndices.back().size() == doubleEntries.size()){
        for(unsigned k=0; k<freeVarIndices.back().size(); k++){
          if(std::find(diffIndicesMain.getData(), diffIndicesMain.getData() + numDiffValsMain, doubleEntries[k])
                != diffIndicesMain.getData() + numDiffValsMain){
            freeVarIndices.back().clear();
            freeVarIndices.back().push_back(doubleEntries[k]);
            break;
          }
        }
      }
      doubleEntries.clear();
    }
    if(i < numNonZeroes){
      previousRow = rowIndices[i];
      doubleEntries.push_back(colIndices[i]);
    }
  }
  return GenericEso::OK;
}


void calculateAndSetFreeValue(const int rowIndex,
                              const int colIndex,
                              const int nonZeroPosition,
                              GenericEso *genEso)
{
  EsoDouble value, residual, derivative;
  genEso->getVariableValues(1, &value, &colIndex);
  genEso->getResiduals(1, &residual, &rowIndex);
  genEso->getJacobianValues(1, &derivative, &nonZeroPosition);
  
  const double THRESHOLD = 1e-12;
  while(fabs(residual) > THRESHOLD){
    value -= residual/derivative;
    genEso->setVariableValues(1, &value, &colIndex);
    genEso->getResiduals(1, &residual, &rowIndex);
    genEso->getJacobianValues(1, &derivative, &nonZeroPosition);
  }
}

GenericEso::RetFlag JadeGenericEsoWithInitialization::setInitialValues
                                                          (const EsoIndex numDiffVals,
                                                                 EsoIndex *diffIndices,
                                                                 EsoDouble * diffValues,
                                                           const EsoIndex numParameters,
                                                                 EsoIndex *parameterIndices)
{
  utils::Array<EsoIndex> initialDiffIndices(numDiffVals);
  for(EsoIndex i=0; i<numDiffVals; i++){
    initialDiffIndices[i] = m_varIndexMapToInitial[diffIndices[i]];
  }
  m_initialEso.setVariableValues(numDiffVals, diffValues, initialDiffIndices.getData());
  
 
  
  
 //set all parameters of the main model to the initial model
  const EsoIndex numInitialParameters = m_initialEso.getNumParameters();

  utils::Array<EsoIndex> initialParamIndices(numInitialParameters);
  utils::Array<EsoIndex> mainParamIndices(numInitialParameters);
  utils::Array<EsoDouble> initialParamValues(numInitialParameters);
  
  m_initialEso.getParameterIndex(numInitialParameters, initialParamIndices.getData());
  
  for(int i=0; i<numInitialParameters; i++){
    mainParamIndices[i] = m_varIndexMapToMain[initialParamIndices[i]];
  }
  
  getVariableValues(numInitialParameters,
                    initialParamValues.getData(),
                    mainParamIndices.getData());
  m_initialEso.setVariableValues(numInitialParameters,
                                 initialParamValues.getData(),
                                 initialParamIndices.getData());
  
  const EsoIndex numNonZeroes = m_initialEso.getNumNonZeroes();
  utils::Array<EsoIndex> rowIndices(numNonZeroes), colIndices(numNonZeroes);


  std::vector<std::vector<EsoIndex> > freeVarIndices;
  if(numInitialParameters != 0 && numNonZeroes !=0){
    m_initialEso.getJacobianStruct(numNonZeroes, rowIndices.getData(), colIndices.getData());
    for(EsoIndex i=0; i<numNonZeroes; i++){
      colIndices[i] = m_varIndexMapToMain[colIndices[i]];
    }
    GenericEso::RetFlag retFlag = getFreeVariables(numDiffVals,
                                                   diffIndices,
                                                   numParameters,
                                                   parameterIndices,
                                                   freeVarIndices,
                                                   numNonZeroes,
                                                   rowIndices,
                                                   colIndices);
    if(retFlag != GenericEso::OK){
      return retFlag;
    }
  }
  
  
  while(freeVarIndices.size() > 1){
    //there should be no empty elements in the vector freeVarIndices
    for(unsigned i=0; i<freeVarIndices.size(); i++){
      assert(!freeVarIndices[i].empty());
    }
    //search for equation with exactly one degree of freedom
    std::vector<std::vector<EsoIndex> >::iterator iter, singleElement;
    singleElement = freeVarIndices.end();
    for(iter = freeVarIndices.begin() + 1; iter != freeVarIndices.end(); iter++){
      if((*iter).size() == 1){
        singleElement = iter;
        break;
      }
    }
    
    if(singleElement == freeVarIndices.end()){
      //system cannot be solved directly (we would need a nonlinear solver)
      return GenericEso::FAIL;
    }
    
    int position = singleElement - (freeVarIndices.begin() + 1);
    int nonZeroPosition = -1;
    for(int i=0; i<numNonZeroes; i++){
      if((freeVarIndices.front()[position] == rowIndices[i])
         && ((*singleElement).front() == colIndices[i])){
        nonZeroPosition = i;
        break;
      }
    }
    assert(nonZeroPosition >= 0);
    calculateAndSetFreeValue(freeVarIndices.front()[position],
                             m_varIndexMapToInitial[(*singleElement).front()],
                             nonZeroPosition,
                             &m_initialEso);
    EsoIndex deletedElement = (*singleElement).front();
    freeVarIndices.erase(singleElement);
    freeVarIndices.front().erase(freeVarIndices.front().begin() + position);
    
    for(unsigned j=0; j<freeVarIndices.size(); j++){
      std::vector<EsoIndex>::iterator foundElement;
      foundElement = std::find(freeVarIndices[j].begin(),
                               freeVarIndices[j].end(),
                               deletedElement);
      if(foundElement != freeVarIndices[j].end()){
        freeVarIndices[j].erase(foundElement);
      }
    }
  }
  
  //set all (changed) states and parameters of the initial model to the main model
  const EsoIndex numInitialVariables = m_initialEso.getNumVariables();
  utils::Array<EsoIndex> initialIndicesInMain(numInitialVariables);
  for(EsoIndex i=0; i<numInitialVariables; i++){
    initialIndicesInMain[i] = m_varIndexMapToMain[i];
  }
  utils::Array<EsoDouble> allInitialValues(numInitialVariables);
  m_initialEso.getAllVariableValues(numInitialVariables, allInitialValues.getData());
  setVariableValues(numInitialVariables,
                    allInitialValues.getData(),
                    initialIndicesInMain.getData());
  return GenericEso::OK;
}

EsoType JadeGenericEso::getType() const {
  return EsoType::JADE;
}
