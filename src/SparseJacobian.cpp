/** 
* @file SparseJacobian.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* Member definitions of class SparseJacobian                           \n
* =====================================================================\n
* @author Moritz Schmitz
* @date 15.9.2011
*/

#include "SparseJacobian.hpp"
#include "ColPackHeaders.h"
#include <assert.h>

using namespace ColPack;

/**
* @brief constructor
*
* @param[in] firstVarIndex defines the index of the variable whose corresponding entries are in the
*            first column of the jacobian matrix
* @param[in] lastVarIndex defines the index of the variable whose corresponding entries are in the
*            last column of the jacobian matrix
* @param[in] sp_res defines the sparsity pattern of the whole underlying DAE system
* @param[in] len_sp_res length of the pointer sp_res (equals the number of equations of DAE system)
*/
SparseJacobian::SparseJacobian(const EsoIndex firstVarIndex, const EsoIndex lastVarIndex, jsp* sp_res, const EsoIndex len_sp_res)
{
  assert(lastVarIndex >= firstVarIndex);
  assert(sp_res != NULL);


  m_num_equations = len_sp_res;
  m_firstVarIndex = firstVarIndex;
  const EsoIndex numJacobianCols = lastVarIndex-firstVarIndex+1;


  /***** calculation of the sparsity pattern *****/

  m_sparsityPattern = new unsigned int**;
  (*m_sparsityPattern) = new unsigned int*[m_num_equations];
  for(EsoIndex i=0;i<m_num_equations;i++){
    (*m_sparsityPattern)[i] = new unsigned int[sp_res[i].nz.size()+1];
    // "+1" because the first entry of sp_res[i] is the size of sp_res.
    // We don't know how many df/dx are there in each equation
  }
  EsoIndex nonZerosJacCount = 0; // counts jacobian entries
  set<int>::iterator pp;
  unsigned int kk;
  for(EsoIndex i=0;i<m_num_equations;i++){
    kk = 0;
    for(pp=sp_res[i].nz.begin(); pp!=sp_res[i].nz.end(); pp++){
      if(firstVarIndex <= *pp && *pp <= lastVarIndex){
        kk = kk + 1; 
        // we want entries relative to firstVarIndex and no absolute indices.
        // Required by BuildBPGraphFromRowCompressedFormat()
        (*m_sparsityPattern)[i][kk] = (unsigned int)(*pp)-(unsigned int)firstVarIndex;
        nonZerosJacCount += 1;
      }
    }
    assert(kk <= sp_res[i].nz.size());
    (*m_sparsityPattern)[i][0] = kk;
  }


  /***** calculation of the seed matrices *****/ 

  int *ip1_seedColumnCount = new int; // a pointer to the number of columns of the seed-matrix
  int *ip1_seedRowCount = new int;


    /*** Calling ColPack routines ***/

  m_coloringInterface = new BipartiteGraphPartialColoringInterface(-1); // -1: only step 0 will be done by ColPack
  m_coloringInterface->BuildBPGraphFromRowCompressedFormat(*m_sparsityPattern, m_num_equations,
    numJacobianCols);
  m_coloringInterface->PartialDistanceTwoColoring("SMALLEST_LAST", "COLUMN_PARTIAL_DISTANCE_TWO");
  EsoDouble*** seed = new EsoDouble**;
  *seed = m_coloringInterface->GetSeedMatrix(ip1_seedRowCount, ip1_seedColumnCount);
  m_numColsSeed = *(ip1_seedColumnCount);


    /*** Store seed information into m_seedMatrix_t ***/

  m_seedMatrix_t = new EsoDouble*[m_numColsSeed];
  for(EsoIndex j=0; j<m_numColsSeed; j++){
    m_seedMatrix_t[j] = new EsoDouble[numJacobianCols];
  }
  for(EsoIndex i=0; i<numJacobianCols; i++){
    for(EsoIndex j=0; j<m_numColsSeed; j++){
      m_seedMatrix_t[j][i] = (*seed)[i][j];
    }
  }


  /***** convert information in m_sparsityPattern to coordinate format *****/

  m_rowIndices.resize(nonZerosJacCount);
  m_colIndices.resize(nonZerosJacCount);
  int counter=0;
  for(EsoIndex i=0; i<m_num_equations; i++){
    // (*m_sparsityPattern)[i][0] contains number of entries, therefore we begin with j=1.
    for(unsigned j=1; j<(*m_sparsityPattern)[i][0]+1; j++){
      m_rowIndices[counter] = i;
      m_colIndices[counter] = (*m_sparsityPattern)[i][j];
      counter++;
    }
  }


  /***** delete local arrays *****/

  delete seed;
  delete ip1_seedColumnCount;
  delete ip1_seedRowCount;
}

/**
* @brief destructor
*/
SparseJacobian::~SparseJacobian()
{
  delete m_coloringInterface;
  for(EsoIndex i=0; i<m_num_equations; i++){
    delete[] (*m_sparsityPattern)[i];
  }
  delete[] (*m_sparsityPattern);
  delete m_sparsityPattern;
  for(EsoIndex i=0; i<m_numColsSeed; i++){
    delete[] (m_seedMatrix_t)[i];
  }
  delete[] m_seedMatrix_t;
}

/** 
* @brief obtain the values of the sparse-jacobian matrix knowing the product jacobian*seed=buffer
*
* For more details we refer to 
* Jacobian::getValuesFromBuffer(double** buffer, utils::Array<double> &values)
*/
void SparseJacobian::getValuesFromBuffer(EsoDouble** buffer, EsoDouble* values, const EsoIndex lenValues) const
{
  assert(buffer != NULL);
  assert(lenValues == this->getNumEntries());

  /********** Recovery using ColPack **********/  

  JacobianRecovery1D jD;
  double** pValue = new double*;
  unsigned int** ip2_RowIndex = new unsigned int*; // for Jacobian recovery
  unsigned int** ip2_ColumnIndex = new unsigned int*; // for Jacobian recovery
  jD.RecoverD2Cln_CoordinateFormat(m_coloringInterface, (double**)buffer, *m_sparsityPattern,
    ip2_RowIndex, ip2_ColumnIndex, pValue);
  for(EsoIndex i=0; i<this->getNumEntries(); i++){
    values[i] = (*pValue)[i];
  }
  delete pValue;
  delete ip2_RowIndex;
  delete ip2_ColumnIndex;
}

/**
* @copydoc Jacobian::getSparsityPattern(const utils::Array<int>&,const int,const utils::Array<int>&,
*                                       const int, utils::Array<int>&,utils::Array<int>&,int&)
*/
void SparseJacobian::getSparsityPattern(const EsoIndex* eqIndices, const EsoIndex lenEI,
                                        const EsoIndex* varIndices, const EsoIndex lenVI,
                                        EsoIndex* rowIndices, EsoIndex* colIndices,
                                        EsoIndex& numEntries) const
{
  EsoIndex ientry = 0;
  EsoIndex locEqIndex, locVarIndex;
  for(EsoIndex i=0; i<lenEI; i++){ // for all eqIndices
    if(lenEI == 0)
      locEqIndex = i;
    else
      locEqIndex = eqIndices[i];

    // check if the number of entries in sparsity pattern according to eq 'locEqIndex' is bigger than 0
    if((*m_sparsityPattern)[locEqIndex][0]>0){
      // for all entries in sparsityPattern[locEqIndex][]
      for(int j=0; j<(int)(*m_sparsityPattern)[locEqIndex][0]; j++){
        for(int k=0; k<lenVI; k++){//test if entry [j+1] in m_sparsityPattern is in varIndices
          if(lenVI == 0)
            locVarIndex = k;
          else
            locVarIndex = varIndices[k];
          if((*m_sparsityPattern)[locEqIndex][j+1] == (unsigned)locVarIndex){
            rowIndices[ientry] = i;
            colIndices[ientry] = k;
            ientry++;
            break;
          }
        }
      }
    }
  }// end: for all eqIndices
  numEntries = ientry;
}

/**
* @copydoc Jacobian::getSparsityPattern(const std::vector<int>&,utils::Array<int>&,
*                                       utils::Array<int>&);
*/
void SparseJacobian::getSparsityPattern(const EsoIndex* indices, const EsoIndex lenInd,
                                        EsoIndex* rowIndices, EsoIndex* colIndices) const
{
  Jacobian::getSparsityPattern(indices, lenInd, rowIndices, colIndices);
}