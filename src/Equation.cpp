/**
* @file Equation.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                      \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                          \n
* =====================================================================\n
* This file contains the equation data structure of the generic eso   \n
* according to the UML Diagramm METAESO_2                             \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 14.6.2011
*/

#include "Equation.hpp"

/**
* @brief standard constructor
*/
EquationSet::EquationSet()
{
}

/**
* @brief setter for Equation::m_equationIndex
* @param equationIndex vector of indices to be set
*/
void EquationSet::setEquationIndex(const vector<EsoIndex> &equationIndex)
{
  m_equationIndex.assign(equationIndex.begin(), equationIndex.end());
}

/**
* @brief get the total number of the models equation
* @return size of Equation::m_equationIndex
*/
EsoIndex EquationSet::getNumEquations() const
{
  return m_equationIndex.size();
}

/**
* @brief getter for Equation::m_equationIndex
* @return reference to Equation::m_equationIndex
*/
std::vector<EsoIndex>& EquationSet::getEquationIndex()
{
  return m_equationIndex;
}

/**
* @brief standard constructor
*/
AlgebraicEquationSet::AlgebraicEquationSet()
{
}

/**
* @brief standard constructor
*/
DifferentialEquationSet::DifferentialEquationSet()
{
}
