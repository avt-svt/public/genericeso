/**
* @file GenericEso.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                      \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                          \n
* =====================================================================\n
* This file contains  the generic eso class                            \n
* according to the UML Diagramm METAESO_2                             \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 14.6.2011
*/
#include <algorithm>
#include "GenericEso.hpp"
#include <ostream>
#pragma warning(disable : 4996)


GenericEso::Error::~Error() = default;

/**
* @brief get number of all variables
*
* @return number of variables
*/
EsoIndex GenericEso::getNumVariables() const
{
  return m_numVariables;
}

GenericEso::~GenericEso(){}

/**
* @brief get number of differential variables
*
*  @return number of differential variables
*/
EsoIndex GenericEso::getNumDifferentialVariables() const
{
  return m_differentialStateVariables.getNumberOfVariables();
}

/**
* @brief get number of parameters
*
* @return number of parameters
*/
EsoIndex GenericEso::getNumParameters() const
{
  return m_parameters.getNumberOfVariables();
}

/**
* @brief get number of algebraic variables
*
* @return number of algebraic variables
*/
EsoIndex GenericEso::getNumAlgebraicVariables() const
{
  return m_algebraicStateVariables.getNumberOfVariables();
}

/**
* @brief get number of states
*
* @return number of states
*/
EsoIndex GenericEso::getNumStates() const
{
  return m_stateVariables.getNumberOfVariables();
}

/**
* @brief get number of all equations
*
* @return number of equations
*/
EsoIndex GenericEso::getNumEquations() const
{
  return m_numEquations;
}

/**
* @brief get number of the differential equations
*
* @return number of equations
*/
EsoIndex GenericEso::getNumDiffEquations() const
{
  return m_differentialEquations.getNumEquations();
}

/**
* @brief get number of algebraic equations
*
* @return number of equations
*/
EsoIndex GenericEso::getNumAlgEquations() const
{
  return m_algebraicEquations.getNumEquations();
}


/**
* @brief get number of Jacobian entries
*
* @return number of Jacobian entries
*/
EsoIndex GenericEso::getNumNonZeroes() const
{
  return m_jacobian.getNumEntries();
}

EsoIndex GenericEso::getNumInitialNonZeroes() const
{
  return m_numInitialNonZeroes;
}

/**
* @brief get number of differential Jacobian entries
*
* @return number of differential Jacobian entries
*/
EsoIndex GenericEso::getNumDifferentialNonZeroes() const
{
  return m_diffJacobian.getNumEntries();
}

/**
* @brief get number of switching conditions
*
* @return number of switching conditions
*/
EsoIndex GenericEso::getNumConditions() const
{
  return m_numSwitching;
}

/**
* @brief getter for the algebraic indices
*
* Every variable has an own index (position in the variables vector).
* This function extracts the indices of the algebraic variables
* @param[out] algebraicIndex vector receiving the algebraic indices -
*                            vector may change its size
*/
void GenericEso::getAlgebraicIndex(const EsoIndex n_alg_var, EsoIndex *algebraicIndex)
{
  std::vector<EsoIndex> tmp;
  tmp = m_algebraicStateVariables.getEsoIndices();
  assert(static_cast<EsoIndex>(tmp.size()) == n_alg_var);
  if(!tmp.empty()){
    utils::copy(n_alg_var, &tmp[0], algebraicIndex);
  }
}

/**
* @brief getter for all parameter indices
*
* Every variable has an own index (position in the variables vector).
* This function extracts the indices of the parameters
* @param[out] parameterIndex vector receiving the indices of the parameters -
*                      vector may change its size
*/
void GenericEso::getParameterIndex(const EsoIndex n_parameters, EsoIndex *parameterIndex)
{
  std::vector<EsoIndex> tmp;
  tmp = m_parameters.getEsoIndices();
  assert(n_parameters == static_cast<EsoIndex>(tmp.size()));
  if(!tmp.empty()){
    utils::copy(n_parameters, &tmp[0], parameterIndex);
  }
}


/**
* @brief getter for all differential indices
*
* @param[out] differentialIndex vector receiving the indices of the differential variables -
*                          vector may change its size
* @sa GenericEso::getParameterIndex
*/
void GenericEso::getDifferentialIndex(const EsoIndex n_diff_var, EsoIndex *differentialIndex)
{
  std::vector<EsoIndex> tmp;
  tmp = m_differentialStateVariables.getEsoIndices();
  assert(static_cast<EsoIndex>(tmp.size()) == n_diff_var);
  if(!tmp.empty()){
    utils::copy(n_diff_var, &tmp[0], differentialIndex);
  }
}

/**
* @brief getter for all state indices
*
* @param[out] stateIndex vector receiving the indices of the state variables
* @sa GenericEso::getParameterIndex
*/
void GenericEso::getStateIndex(const EsoIndex n_states, EsoIndex *stateIndex)
{
  std::vector<EsoIndex> tmp;
  tmp = m_stateVariables.getEsoIndices();
  assert(static_cast<EsoIndex>(tmp.size()) == n_states);
  if(!tmp.empty()){
    utils::copy(n_states, &tmp[0], stateIndex);
  }
}

/**
* @brief getter for differential equations indices
*
* @param[out] diffEqIndices vector receiving the indices of the differential equations
*/
void GenericEso::getDiffEquationIndex(const EsoIndex n_diff_eq, EsoIndex *diffEqIndices)
{
  std::vector<EsoIndex> tmp;
  tmp = m_differentialEquations.getEquationIndex();
  assert(static_cast<EsoIndex>(tmp.size()) == n_diff_eq);
  if(!tmp.empty()){
    utils::copy(n_diff_eq, &tmp[0], diffEqIndices);
  }
}

/**
* @brief getter for algebraic equations indices
*
* @param[out] algEqIndices vector receiving the indices of the algebraic equations
*/
void GenericEso::getAlgEquationIndex(const EsoIndex n_alg_eq, EsoIndex *algEqIndices)
{
  std::vector<EsoIndex> tmp;
  tmp = m_algebraicEquations.getEquationIndex();
  assert(static_cast<EsoIndex>(tmp.size()) == n_alg_eq);
  if(!tmp.empty()){
    utils::copy(n_alg_eq, &tmp[0], algEqIndices);
  }
}

/**
* @brief getter for all Jacobian data of the model
*
* @param[out] rowIndices vector receiving the row-index-vector of the Jacobian in COO sparse format
* @param[out] colIndices vector receiving the col-index-vector of the Jacobian in COO sparse format
*/
void GenericEso::getJacobianStruct(const EsoIndex n_nz_jac, EsoIndex *rowIndices, EsoIndex *colIndices)
{
  std::vector<EsoIndex> tmp_row;
  std::vector<EsoIndex> tmp_col;
  tmp_row = m_jacobian.getRowIndices();
  tmp_col = m_jacobian.getColIndices();
  assert(static_cast<EsoIndex>(tmp_row.size()) == n_nz_jac);
  assert(static_cast<EsoIndex>(tmp_col.size()) == n_nz_jac);
  if(!tmp_row.empty()){
    utils::copy(n_nz_jac, &tmp_row[0], rowIndices);
  }
  if(!tmp_col.empty()){
    utils::copy(n_nz_jac, &tmp_col[0], colIndices);
  }
}

/**
* @brief getter for all differential Jacobian data of the model
*
* @param[out] rowIndices vector receiving the row-index-vector of the differential Jacobian in COO sparse format
* @param[out] colIndices vector receiving the col-index-vector of the differential Jacobian in COO sparse format
*/
void GenericEso::getDiffJacobianStruct(const EsoIndex n_nz_diff_jac, EsoIndex *rowIndices, EsoIndex *colIndices)
{
  std::vector<EsoIndex> tmp_row;
  std::vector<EsoIndex> tmp_col;
  tmp_row = m_diffJacobian.getRowIndices();
  tmp_col = m_diffJacobian.getColIndices();
  assert(static_cast<EsoIndex>(tmp_row.size()) == n_nz_diff_jac);
  assert(static_cast<EsoIndex>(tmp_col.size()) == n_nz_diff_jac);
  if(!tmp_row.empty()){
    utils::copy(n_nz_diff_jac, &tmp_row[0], rowIndices);
  }
  if(!tmp_col.empty()){
    utils::copy(n_nz_diff_jac, &tmp_col[0], colIndices);
  }
}

/**
* @brief get the eso index of a given variable name
* @param[in] varName name of the variable
* @return eso index of the variable, or -1 if not found in the models variable name list
*/
EsoIndex GenericEso::getEsoIndexOfVariable(const std::string &varName)
{

  for(EsoIndex i=0; i<m_numVariables; i++){
    if(varName == m_varNames[static_cast<size_t>(i)]){
      return i;
    }
  }
  return -1;
}

/**
* @brief convert an eso index into a variable name
*
* @param[in] esoIndex index to be converted
* @return variable name that corresponds to the index
*/
std::string GenericEso::getVariableNameOfIndex(const EsoIndex esoIndex)
{
  assert(esoIndex < m_numVariables);
  assert(static_cast<EsoIndex>(m_varNames.size()) == m_numVariables);
  return m_varNames[static_cast<size_t>(esoIndex)];
}

/**
* @brief convert an exo index into a state index
*
* State index here means the position of the variable in a state vector.
* So the index of the first state is 0, although the eso index might be different
* @param[in] esoIndex index to be converted
* @return converted index, if the given index was not found, the function returns
*         a value equal to the number of states
*/
EsoIndex GenericEso::getStateIndexOfVariable(const EsoIndex esoIndex)
{
  //determine the state index
  const EsoIndex nStates = getNumStates();

  if(nStates > 0){
    std::vector<EsoIndex> stateIndices(static_cast<size_t>(nStates));
    getStateIndex(nStates, &stateIndices[0]);
    const std::vector<EsoIndex>::iterator result = std::find(stateIndices.begin(), stateIndices.end(), esoIndex);
    return static_cast<EsoIndex>(result - stateIndices.begin());
  }
  else {
    return 0;
  }
}

