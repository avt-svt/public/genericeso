/**
* @file EsoUtilities.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* This file contains utility functions for GenericEso objects          \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 2.10.2012
*/

#include "EsoUtilities.hpp"

#include "cs.h"

/**
* @brief get the index of the corresponding equation for a differential variable
*
* @param[in] diffEsoIndex eso index of the differential variable
* @param[in] eso input struct used to create a GenericEso object (to retrieve the equation indices)
* @return the corresponding equation index
*/
EsoIndex getDifferentialEquationIndex(const unsigned diffEsoIndex, const GenericEso::Ptr esoPtr)
{

  const EsoIndex numDiffNonZeroes = esoPtr->getNumDifferentialNonZeroes();
  std::vector<EsoIndex> rows(numDiffNonZeroes), cols(numDiffNonZeroes);
  if(numDiffNonZeroes > 0){
    esoPtr->getDiffJacobianStruct(numDiffNonZeroes, &rows[0], &cols[0]);
  }

  const std::vector<EsoIndex>::iterator position = std::find(cols.begin(), cols.end(), diffEsoIndex);

  const unsigned rowIndex = position - cols.begin();
  //a differential variable must have at least one entry in the diff Jacobian struct
  assert(rowIndex < rows.size());

  return rows[rowIndex];
}

/**
* @brief get the index of the corresponding equation for an algebraic variable
*
* In order to determine the right equation a block decomposition is being made.
* It is expected that the given variable index points to a block of size 1.
* Otherwise an exception is thrown. Columns corresponding to a defined parameter are ignored
* in the block decomposition.
* @param[in] esoIndex eso index of the algebraic variable
* @param[in] imdInput input struct containing information about all parameters
* @param[in] eso input struct used to create a GenericEso object (to retrieve the equation indices)
* @return the corresponding equation index
* @throw NoConstraintEquationFoundException, AmbiguousEquationForConstraintException
*/
EsoIndex getAlgebraicEquationIndex(const EsoIndex esoIndex, const GenericEso::Ptr esoPtr)
{
    //first create the Jacobian struct
  const EsoIndex numEquations = esoPtr->getNumEquations();
  const EsoIndex numVars = esoPtr->getNumVariables();
  const EsoIndex numNonZeroes = esoPtr->getNumNonZeroes();
  const EsoIndex numAlgebraicVariables = esoPtr->getNumAlgebraicVariables();

  utils::Array<EsoIndex> rows(numNonZeroes), cols(numNonZeroes);
  utils::Array<EsoDouble> dummyValues(numNonZeroes);
  esoPtr->getJacobianStruct(numNonZeroes, rows.getData(), cols.getData());


  const CsTripletMatrix::Ptr jacobian(new CsTripletMatrix(numEquations,
                                                    numVars,
                                                    numNonZeroes,
                                                    rows.getData(),
                                                    cols.getData(),
                                                    dummyValues.getData()));

  //now get the vector containing non-parameter indices to be left in the matrix
  utils::Array<EsoIndex> algebraicVars(numAlgebraicVariables);
  esoPtr->getAlgebraicIndex(algebraicVars.getSize(), algebraicVars.getData());

  //keep only algebraic equations
  const unsigned numAlgebraicEquations = esoPtr->getNumAlgEquations();
  utils::Array<EsoIndex> algebraicRows(numAlgebraicEquations);
  esoPtr->getAlgEquationIndex(algebraicRows.getSize(), algebraicRows.getData());

  const CsTripletMatrix::Ptr algebraicJacobian = jacobian->get_matrix_slices(algebraicRows.getSize(),
                                                                       algebraicRows.getData(),
                                                                       algebraicVars.getSize(),
                                                                       algebraicVars.getData());

  const int seed = 0;

  //perform the block decomposition

  csd *result = algebraicJacobian->compress()->get_dulmange_mendelsohn_permutation(seed);

  //get the algebraicIndex of the eso index
  const EsoIndex *positionAlgIndex = std::find(algebraicVars.getData(),
                                         algebraicVars.getData() + algebraicVars.getSize(),
                                         esoIndex);
  assert(positionAlgIndex != algebraicVars.getData() + algebraicVars.getSize());
  int algebraicIndex = positionAlgIndex - algebraicVars.getData();

  //find the position of the variable in the colum after permutation
  int* res = std::find(result->q, result->q + numVars, algebraicIndex);
  assert(res != result->q + algebraicVars.getSize());
  const int permutatedColumnIndex = res - result->q;

  //find the block containing the column
  //since we expect it to be in an 1x1 block, s should contain the indexy
  res = std::find(result->s, result->s + result->nb + 1, permutatedColumnIndex);
  //block must exist
  if(res == result->s + result->nb+1){
    return -1; //throw NoConstraintEquationFoundException();
  }
  const int blockIndex = res - result->s;
  //block must be of size 1
  if(result->s[blockIndex] != result->s[blockIndex+1]-1){
    return -1; //throw AmbiguousEquationForConstraintException();
  }

  //find the original equation index of the equation found in the block
  const int permutatedRowIndex = result->r[blockIndex];
  const int algebraicEquationIndex = result->p[permutatedRowIndex];

  //free csd struct
  cs_free(result);

  return algebraicRows[algebraicEquationIndex];
}

/**
* @brief calculate the equation index for a given variable
*
* @param[in] esoIndex index of the given variable
* @return the index of the corresponding equation, -1 if no equation index could be calculated
*/
EsoIndex getEquationIndexOfVariable(const EsoIndex esoIndex, const GenericEso::Ptr esoPtr)
{

  const EsoIndex numDiffVariables = esoPtr->getNumDifferentialVariables();
  utils::Array<EsoIndex> diffIndices(numDiffVariables);
  esoPtr->getDifferentialIndex(numDiffVariables, diffIndices.getData());

  //if esoIndex is index of a differential variable
  const EsoIndex *position = std::find(diffIndices.getData(),
                                 diffIndices.getData() + numDiffVariables,
                                 esoIndex);
  if(position != diffIndices.getData() + numDiffVariables){
    return getDifferentialEquationIndex(esoIndex, esoPtr);
  }
  else{
    return getAlgebraicEquationIndex(esoIndex, esoPtr);
  }
  return -1;
}

CsTripletMatrix::Ptr getBMatrix(const GenericEso::Ptr esoPtr)
{
  // initialize general data
  const EsoIndex numEqns = esoPtr->getNumEquations();
  const EsoIndex numVars = esoPtr->getNumVariables();
  const EsoIndex numDiffEqns = esoPtr->getNumDiffEquations();
  const EsoIndex numDiffVars = esoPtr->getNumDifferentialVariables();
  const EsoIndex numDiffNonZeroes = esoPtr->getNumDifferentialNonZeroes();

  // get complete Jacobian dF/[dx, dx_dot, dy, dp]
  utils::Array<EsoIndex> cols(numDiffNonZeroes);
  utils::Array<EsoIndex> rows(numDiffNonZeroes);
  utils::Array<EsoDouble> values(numDiffNonZeroes);

  esoPtr->getDiffJacobianStruct(numDiffNonZeroes,
                                rows.getData(),
                                cols.getData());
  esoPtr->getDiffJacobianValues(numDiffNonZeroes,
                                values.getData());

  const CsTripletMatrix::Ptr difJac(new CsTripletMatrix(numEqns,
                                                  numVars,
                                                  numDiffNonZeroes,
                                                  rows.getData(),
                                                  cols.getData(),
                                                  values.getData()));

  // extract dF/dx_dot (as slices of complete Jacobian dF/[dx, dx_dot, dy, dp])

  utils::Array<EsoIndex> diffEqnIndices(numDiffEqns);
  utils::Array<EsoIndex> diffVarIndices(numDiffVars);
  esoPtr->getDiffEquationIndex(diffEqnIndices.getSize(), diffEqnIndices.getData());
  esoPtr->getDifferentialIndex(diffVarIndices.getSize(), diffVarIndices.getData());

  const CsTripletMatrix::Ptr bMatrix = difJac->get_matrix_slices(numDiffEqns, diffEqnIndices.getData(),
                                                           numDiffVars, diffVarIndices.getData());

  return bMatrix;
}

void getFullDerivativeValues(const GenericEso::Ptr eso, utils::Array<double>& ydfull)
{
  const int n_diffstates = eso->getNumDifferentialVariables();
  const int n_states = eso->getNumStates();
  assert(n_states == ydfull.getSize());
  utils::Array<double> ydeso(n_diffstates);
  eso->getDerivativeValues(ydeso.getSize(), ydeso.getData());
  std::vector<EsoIndex> diff_idx(n_diffstates);
  eso->getDifferentialIndex(n_diffstates, &diff_idx[0]);
  for (int k=0; k<n_states; ++k) {
    ydfull[k] = 0.0;
  }
  for (int k=0; k<n_diffstates; ++k) {
    ydfull[diff_idx[k]] = ydeso[k];
  }
}

void setFullDerivativeValues(const GenericEso::Ptr eso, const utils::Array<double>& ydfull)
{
  const int n_diffstates = eso->getNumDifferentialVariables();
  const int n_states = eso->getNumStates();
  assert(n_states == ydfull.getSize());
  utils::Array<double> ydeso(n_diffstates);
  std::vector<EsoIndex> diff_idx(n_diffstates);
  eso->getDifferentialIndex(n_diffstates, &diff_idx[0]);
  for (int k=0; k<n_diffstates; ++k) {
    ydeso[k] = ydfull[diff_idx[k]];
  }
  eso->setDerivativeValues(ydeso.getSize(), ydeso.getData());
}


bool BMatrixIsConstant(const GenericEso::Ptr esoPtr)
{
  EsoIndex numVariables = esoPtr->getNumVariables();
  utils::Array<EsoDouble> variables(numVariables), originalVariables(numVariables);
  esoPtr->getAllVariableValues(numVariables, originalVariables.getData());
  
  EsoIndex numDiffVariables = esoPtr->getNumDifferentialVariables();
  utils::Array<EsoDouble> derivatives(numDiffVariables), originalDerivatives(numDiffVariables);
  esoPtr->getDerivativeValues(numDiffVariables, originalDerivatives.getData());
  
  EsoIndex numDiffJacobians = esoPtr->getNumDifferentialNonZeroes();
  utils::Array<EsoDouble> diffJacobians(numDiffJacobians), originalDiffJacobians(numDiffJacobians);
  esoPtr->getDiffJacobianValues(numDiffJacobians, originalDiffJacobians.getData());
  
  // disturb all variables and check the differential Jacobians
  // question: do I need to disturb all variables, or just the derivatives?
  EsoDouble disturbance = 1e-6;
  
  for(EsoIndex i=0; i<numVariables; i++){
    variables[i] = originalVariables[i] +disturbance;
  }
  for(EsoIndex i=0; i<numDiffVariables; i++){
    derivatives[i] = originalDerivatives[i] + disturbance;
  }
  
  esoPtr->setAllVariableValues(numVariables, variables.getData());
  esoPtr->setDerivativeValues(numDiffVariables, derivatives.getData());
  
  esoPtr->getDiffJacobianValues(numDiffJacobians, diffJacobians.getData());
  
  for(EsoIndex i=0; i<numDiffJacobians; i++){
    //since Jacobian is supposed to be constant, we can make an equal check 
    if(diffJacobians[i] != originalDiffJacobians[i]){
      //reset original state
      esoPtr->setAllVariableValues(numVariables, originalVariables.getData());
      esoPtr->setDerivativeValues(numDiffVariables, originalDerivatives.getData());
      return false;
    }
  }
  
  //now disturb every variable once and check
  for(EsoIndex i=0; i<numVariables; i++){
    variables[i] = originalVariables[i];
  }
  for(EsoIndex i=0; i<numDiffVariables; i++){
    derivatives[i] = originalDerivatives[i];
  }
  esoPtr->setDerivativeValues(numDiffVariables, derivatives.getData());
  
  for(EsoIndex i=0; i<numVariables; i++){
    variables[i] -= disturbance;
    esoPtr->setAllVariableValues(numVariables, variables.getData());
    esoPtr->getDiffJacobianValues(numDiffJacobians, diffJacobians.getData());
    for(EsoIndex j=0; j<numDiffJacobians; j++){
      if(diffJacobians[j] != originalDiffJacobians[j]){
        esoPtr->setAllVariableValues(numVariables, originalVariables.getData());
        return false;
      }
    }
    variables[i] = originalVariables[i];
  }
  
  esoPtr->setAllVariableValues(numVariables, originalVariables.getData());
  for(EsoIndex i=0; i<numDiffVariables; i++){
    derivatives[i] -= disturbance;
    esoPtr->setDerivativeValues(numDiffVariables, derivatives.getData());
    esoPtr->getDiffJacobianValues(numDiffJacobians, diffJacobians.getData());
    for(EsoIndex j=0; j<numDiffJacobians; j++){
      if(diffJacobians[j] != originalDiffJacobians[j]){
        esoPtr->setDerivativeValues(numDiffVariables, originalDerivatives.getData());
        return false;
      }
    }
    derivatives[i] = originalDerivatives[i];
  }
  esoPtr->setDerivativeValues(numDiffVariables, originalDerivatives.getData());

 return true;
}