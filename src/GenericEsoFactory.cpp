/** 
* @file GenericEsoFactory.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericESO                                                           \n
* =====================================================================\n
* This file contains the definitions of GenericEsoFactory classes      \n
* =====================================================================\n
* @author Tjalf Hoffmann 
* @date 10.02.2012
*/

#include "GenericEsoFactory.hpp"
#include <cassert>

using namespace FactoryInput;

/**
* @brief create a GenericEso object
*
* @param input EsoInput struct containing all information to create a GenericEso object
* @return pointer to the created GenericEso object
*/
GenericEso::Ptr GenericEsoFactory::create(const struct EsoInput &input)const
{

	if (input.model == "")
		throw MissingModelNameException();

		GenericEso::Ptr genEso;
		switch (input.type) {
    case EsoType::JADE:
			genEso.reset(createJadeGenericEso(input.model, input.initialModel));
			break;
#ifdef BUILD_JADE2
    case EsoType::JADE2:
			genEso.reset(createJADE2GenericEso(input.model));
			break;
#endif
#ifdef BUILD_WITH_MADO
	case EsoType::MADO:
		genEso.reset(createMADOGenericEso(input.model));
		break;
#endif
#ifdef BUILD_WITH_FMU
    case EsoType::FMI:
			genEso.reset(createFMIGenericEso(input.model, input.relFmuTolerance));
			break;
#endif
	default:
		assert(false);
		}
		return genEso;
	
}

GenericEso2ndOrder::Ptr GenericEsoFactory::create2ndOrder(const struct EsoInput &input)const
{

	if (input.model == "") 
		throw MissingModelNameException();

		GenericEso2ndOrder::Ptr genEso;

		switch (input.type) {
    case EsoType::JADE:
			genEso.reset(createJadeGenericEso(input.model, input.initialModel));
			break;
#ifdef BUILD_JADE2
    case EsoType::JADE2:
			genEso.reset(createJADE2GenericEso(input.model));
			break;
#endif
#ifdef BUILD_WITH_MADO
	case EsoType::MADO:
		genEso.reset(createMADOGenericEso(input.model));
		break;
#endif
#ifdef BUILD_WITH_FMU
    case EsoType::FMI:
			genEso.reset(createFMIGenericEso(input.model, input.relFmuTolerance));
			break;	
#endif
	default:
		assert(false);
		}
		return genEso;
	
}

/**
* @brief create an JadeGenericEso object
*
* @param model name of the model dll to be loaded
* @return pointer to the created JadeGenericEso object
*/
JadeGenericEso *GenericEsoFactory::createJadeGenericEso(const std::string &model,
                                                              const std::string &initialModel)const
{
  if(initialModel.empty()){
    return new JadeGenericEso(model);
  }
  else{
    return new JadeGenericEsoWithInitialization(model, initialModel);
  }
}

#ifdef BUILD_JADE2
/**
* @brief create an JADE2GenericEso object
*
* @param model name of the model dll to be loaded
* @return pointer to the created JADE2GenericEso object
*/
JADE2GenericEso *GenericEsoFactory::createJADE2GenericEso(const std::string &model)const {
	return new JADE2GenericEso(model);
}
#endif

#ifdef BUILD_WITH_FMU
/**
* @brief create an FMIGenericEso object
*
* @param model name of the model
* @return pointer to the created FMIGenericEso object
*/

FMIGenericEso *GenericEsoFactory::createFMIGenericEso(const std::string &FmiName, double relativeFmuTolerance)const
{
		return new FMIGenericEso(FmiName, relativeFmuTolerance);
}
#endif

#ifdef BUILD_WITH_MADO
/**
* @brief create an MADOGenericEso object
*
* @param model name of the model dll to be loaded
* @return pointer to the created MADOGenericEso object
*/
MADOGenericEso *GenericEsoFactory::createMADOGenericEso(const std::string &model)const {
	return new MADOGenericEso(model);
}
#endif
