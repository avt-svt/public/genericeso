/**
* @file MADOGenericEso.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrennstechnik, RWTH Aachen          \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* In this testsuite the extended WO batch model is tested              \n
* =====================================================================\n
*
* @author Lars Henrichfreise
* @date 05.02.2020
*/


#include "MADOGenericEso.hpp"


/**
* @brief MADO constructor
*/
MADOGenericEso::MADOGenericEso(const std::string& libname): madoLoader(libname) {
	
	m_myMADO = madoLoader.getMyMADO();
    model_name = libname;

    m_numVariables = m_myMADO->getNumVariables();
	m_numAllVariables = m_myMADO->getNumAllVariables();
    m_numStates = m_myMADO->getNumDiffStates() + m_myMADO->getNumAlgStates(); // = m_myMADO->getNumEquations()
    m_numDiffVariables = m_myMADO->getNumDiffStates();
    m_numAlgVariables = m_myMADO->getNumAlgStates();
    m_numParameters = m_myMADO->getNumParams();
    m_numEquations = m_myMADO->getNumEquations();
	
	assert(m_numStates == m_numEquations);

	/**********************************************/
	// GET m_numAlgEquations AND m_numDiffEquations
	/**********************************************/
	// Index-1 systems:
	m_numDiffEquations = m_myMADO->getNumDiffStates();
	m_numAlgEquations = m_myMADO->getNumAlgStates();



	/**********************************************/
	// GET list of equation indices of diff + alg eqn
	/**********************************************/

	m_numDiffNonZeros = 0;


	m_indexAlgEqn.resize(m_numAlgEquations);
	m_indexDiffEqn.resize(m_numDiffEquations);
	
	unsigned jA = 0;
	unsigned jD = 0;

	unsigned int ** JP_diff;
	JP_diff = (unsigned int**)malloc(m_numEquations * sizeof(unsigned int*));
	m_myMADO->getDiffJacobianStruct(JP_diff);
	for (int i = 0; i < m_numEquations; i++) {
		if (JP_diff[i][0] == 0) {
			m_indexAlgEqn[jA] = i;
			jA++;
		}
		else {
			m_numDiffNonZeros += JP_diff[i][0];
			m_indexDiffEqn[jD] = i;
			jD++;
		}
	}
	free(JP_diff);
	JP_diff = NULL;



	/**********************************************/
	// Initilize all variables
	/**********************************************/
	m_x.resize(m_numAllVariables);
	double* m_xVar = new double[m_numDiffVariables + m_numAlgVariables];

	for (EsoIndex i = 0; i < m_numAllVariables; i++) {
		m_x[i] = 0.0;
	}

	m_myMADO->getInitialValues(m_xVar, m_numDiffVariables + m_numAlgVariables);

	for (EsoIndex i = 0; i < m_numDiffVariables + m_numAlgVariables ; i++) {
		m_x[i] = m_xVar[i];
	}
	delete[] m_xVar;

	/**********************************************/
	////Dense Jacobian
	/**********************************************/
	//m_J_dense = new double*[m_numEquations];
	//for (unsigned int i = 0; i < m_numEquations; i++) {
	//	m_J_dense[i] = new double[m_numVariables];
	//}
	//m_myMADO->getDenseJacobianValues(m_x, m_J_dense);
	


	/**********************************************/
	// GET m_numDiffNonZeros FROM Diff. Jacobian
	// allocate memory for m_rind_diff, m_cind_diff
	/**********************************************/
	m_rind_diff.resize(m_numDiffNonZeros);
	m_cind_diff.resize(m_numDiffNonZeros);
	m_J_derDiff.resize(m_numDiffNonZeros);
	m_myMADO->getDiffJacobianValues(m_x.data(), &m_numDiffNonZeros, m_rind_diff.data(), m_cind_diff.data(), m_J_derDiff.data());



	/**********************************************/
	// GET m_numNonZeros FROM Jacobian
	// allocate memory for m_rind, m_cind
	/**********************************************/
	unsigned int ** JP;
	m_numNonZeros = 0;
	JP = (unsigned int**)malloc(m_numEquations * sizeof(unsigned int*));
	m_myMADO->getJacobianStruct(JP);
	for (int i = 0; i < m_numEquations; i++) {
			m_numNonZeros += JP[i][0];
	}
	free(JP);
	JP = NULL;

	
	m_rind.resize(m_numNonZeros);
	m_cind.resize(m_numNonZeros);
	m_J.resize(m_numNonZeros);
	m_myMADO->getJacobianValues(m_x.data(), &m_numNonZeros, m_rind.data(), m_cind.data(), m_J.data()); //m_numDiffNonZeros not included




	/**********************************************/
	// MEMORY ALLOCATION
	/**********************************************/
	m_rowIndicesJacStruct.resize(m_numNonZeros);
	m_colIndicesJacStruct.resize(m_numNonZeros);

	m_rowIndicesDiffJacStruct.resize(m_numDiffNonZeros);
	m_colIndicesDiffJacStruct.resize(m_numDiffNonZeros);



	/**********************************************/
	// GET VARIABLE NAMES
	/**********************************************/
	//convert mado's variable names [char**] to  [std::vector<std::string>] for generic eso
	const unsigned maxNameLength = m_myMADO->getMaxVarNameLength();
	char ** temp_names = (char **)malloc(23 * sizeof(char*));
	for (int i = 0; i < m_numAllVariables; ++i) {
		temp_names[i] = (char *)malloc(maxNameLength * sizeof(char));
	}
	m_myMADO->getVariableNames(temp_names);

	// Eso's getVatiableNames NOT for der_x!!!
	std::vector<std::string> temp_names_str(temp_names, temp_names + m_numVariables);

	// Free memory
	for (unsigned int i = 0; i < m_numAllVariables; i++) {
		free(temp_names[i]);
	}
	free(temp_names);

	m_variableNames = temp_names_str;



  }



/**
* @brief MADO destructor
*/
  MADOGenericEso::~MADOGenericEso() {

	
	  m_myMADO->destroy();
	  m_myMADO = nullptr;


	  

	  //for (unsigned int i = 0; i < m_numEquations; i++) {
		 // delete[] m_J_dense[i];
	  //}
	  //delete[] m_J_dense;
  }

  /**
    * @brief get the total number of variables
    *
    * @return total number of variables
    */
  EsoIndex MADOGenericEso::getNumVariables() const {
    return m_numVariables;
  }

  /**
    * @brief get the number of differential variables
    *
    * @return number of differential variables
    */
  EsoIndex MADOGenericEso::getNumDifferentialVariables() const {
    return m_numDiffVariables;
  }

  /**
    * @brief get the number of parameter variables
    *
    * @return number of parameter variables
    */
  EsoIndex MADOGenericEso::getNumParameters() const {
    return m_numParameters;
  }

  /**
    * @brief get the number of algebraic variables
    *
    * @return number of algebraic variables
    */
  EsoIndex MADOGenericEso::getNumAlgebraicVariables() const {
    return m_numAlgVariables;
  }

  /**
    * @brief get the number of state variables
    *
    * @return number of state variables
    */
  EsoIndex MADOGenericEso::getNumStates() const {
    return m_numStates;
  }

  /**
    * @brief get the number of differential equations
    *
    * @return number of differential equations
    */
  EsoIndex MADOGenericEso::getNumEquations() const {
    return m_numEquations;
  }

  /**
    * @brief get the total number of equations
    *
    * @return total number of equations
    */
  EsoIndex MADOGenericEso::getNumDiffEquations() const {
    return m_numDiffEquations;
  }

  /**
    * @brief get the number of algebraic equations
    *
    * @return number of algebraic equations
    */
  EsoIndex MADOGenericEso::getNumAlgEquations() const {
    return m_numAlgEquations;
  }

  /**
    * @brief get the current value of the independent variable(time)
    *
    * @return current value of the independent variable
    */
  EsoIndex MADOGenericEso::getNumNonZeroes() const{
    return m_numNonZeros;
  }


  /**
    * @brief get the current value of the independent variable(time)
    *
    * @return current value of the independent variable
    */
  EsoIndex MADOGenericEso::getNumDifferentialNonZeroes() const{
    return m_numDiffNonZeros;
  }

  /**
    * @brief get the current value of the independent variable(time)
    *
    * @return current value of the independent variable
    */
  EsoIndex MADOGenericEso::getNumConditions() const {
    return 0;
  }



  /**
    * @brief get the names of all variables of the model
    *
    * @param[out] names vector receiving the variable names - must be of size getNumVariables
    */


  void MADOGenericEso::getVariableNames(std::vector<std::string> &names){
	  names = m_variableNames; 
  }

  /**
    * @brief set the values of all variables
    *
    * @param[in] variables array containing the values of the variables to be set -
  //				   must be of size getNumVariables
    */

  void MADOGenericEso::setAllVariableValues(const EsoIndex n_var, const EsoDouble *variables) {
    if (n_var != m_numVariables){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::setAllVariableValues:\n size of input array (n_var="
                     << n_var << ") doesn't fit to number of variables (m_numVariables="
                     << m_numVariables << ").";
      throw GenericEso::Error(errorMessage.str());
    }
	for (EsoIndex i = 0; i < m_numVariables; i++) {
		m_x[i] = variables[i];
	}
  }

  /**
    * @brief set the value of the parameters
    *
    * @param[in] parameter array containing the values of the variables to be set -
  //						   must be of size getNumParameters
    */

  void MADOGenericEso::setParameterValues(const EsoIndex n_pars, const EsoDouble *parameters) {
    if(n_pars != m_numParameters){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::setParameterValues:\n size of input array (n_pars="
                     << n_pars << ") doesn't fit to number of variables (m_numParameters="
                     << m_numParameters << ").";
      throw GenericEso::Error(errorMessage.str());
  }

	int j = 0;
	for (EsoIndex i = m_numDiffVariables+ m_numAlgVariables; i < m_numVariables; i++) {
		m_x[i] = parameters[j];
		j++;
	}
  }

  /**
    * @brief set the value of the algebraic variables
    *
    * @param[in] algebraicVariables array containing the values of the  variables to be set -
  //							must be of size getNumAlgebraicVariables
    */

  void MADOGenericEso::setAlgebraicVariableValues(const EsoIndex n_alg_var, const EsoDouble *algebraicVariables) {
    if(n_alg_var != m_numAlgVariables){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::setAlgebraicVariableValues:\n size of input array (n_alg_var="
                     << n_alg_var << ") doesn't fit to number of variables (m_numAlgVariables="
                     << m_numAlgVariables << ").";
      throw GenericEso::Error(errorMessage.str());
  }

	int j = 0;
	for (EsoIndex i = m_numDiffVariables; i < m_numDiffVariables + m_numAlgVariables; i++) {
		m_x[i] = algebraicVariables[j];
		j++;
	}
  }

  /**
    * @brief set the value of the differential variables
    *
    * @param[in] differentialVariables array containing the values of the variables to be set -
  //							   must be of size getNumDifferentialVariables
    */

  void MADOGenericEso::setDifferentialVariableValues(const EsoIndex n_diff_var, const EsoDouble *differentialVariables) {
    if(n_diff_var != m_numDiffVariables){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::setDifferentialVariableValues:\n size of input array (n_diff_var="
                     << n_diff_var << ") doesn't fit to number of variables (m_numDiffVariables="
                     << m_numDiffVariables << ").";
      throw GenericEso::Error(errorMessage.str());
  }

	// Set values for x (not der_x)
	for (EsoIndex i = 0; i < m_numDiffVariables; i++) {
		m_x[i] = differentialVariables[i];
	}
  }

  /**
    * @brief set the values of all states
    *
    * @param[in] states array containing the values of the states to be set -
    *               must be of size getNumStates
    */

  void MADOGenericEso::setStateValues(const EsoIndex n_states, const EsoDouble *states) {
    if (n_states != m_numStates){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::setStateValues:\n size of input array (n_states="
                     << n_states << ") doesn't fit to number of variables (m_numStates="
                     << m_numStates << ").";
      throw GenericEso::Error(errorMessage.str());
  }

	// Set values for x and y (diff. + alg. variables)
	for (EsoIndex i = 0; i < m_numStates; i++) {
		m_x[i] = states[i];
	}
  }

  /**
    * @brief set the values of a subset of variables
    *
    * @param[in] variables array containing the values of the variables to be set -
    *                  must be at least of the size of parameter indices
    * @param[in] indices vector containing the indices for the variables to be set
    */

  void MADOGenericEso::setVariableValues(const EsoIndex n_idx, const EsoDouble *variables, const EsoIndex *indices) {
    // Check n_idx
	if (n_idx > m_numVariables){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::setVariableValues:\n n_idx (="
                     << n_idx << ") exceedes number of variables (m_numVariables="
                     << m_numVariables << ").";
      throw GenericEso::Error(errorMessage.str());
    }
    else if (n_idx <= 0){
            std::ostringstream errorMessage;
            errorMessage << "MADOGenericEso::setVariableValues:\n n_idx (="
                         << n_idx << ") has to be positive and non-zero.";
          throw GenericEso::Error(errorMessage.str());
	}

	// Check indices
	for (int i = 0; i < n_idx; i++) {
		if (indices[i] < 0 || indices[i] >= m_numAllVariables) {
			std::ostringstream errorMessage;
			errorMessage << "MADOGenericEso::setVariableValues:\n out of variable indices range at index="
				<< i << " indices[index]=" << indices[i] << ".";
			throw GenericEso::Error(errorMessage.str());
		}
	}

	// Set values
	  for (EsoIndex iX = 0; iX < n_idx; iX++) {
		  m_x[indices[iX]] = variables[iX];
	  }
    
  }

  /**
    * @brief getter for the values of all variables
    *
    * @param[out] variables array receiving the values of the variables -
    *                  must be of size getNumVariables
    */

  void MADOGenericEso::getAllVariableValues(const EsoIndex n_var, EsoDouble *variables) const {
    if (n_var != m_numVariables){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getAllVariableValues:\n size of output array (n_var="
                     << n_var << ") doesn't fit to number of variables (m_numVariables="
                     << m_numVariables << ").";
      throw GenericEso::Error(errorMessage.str());
  }
	for (EsoIndex i = 0; i < m_numVariables; i++) {
		variables[i] = m_x[i];
	}
  }

  /**
    * @brief getter for the values of the parameters
    *
    * @param[out] parameter array receiving the values of the parameters -
    *                          must be of size getNumParameters
    */

  void MADOGenericEso::getParameterValues(const EsoIndex n_params, EsoDouble *parameters) {
    if (n_params != m_numParameters){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getParameterValues:\n size of output array (n_params="
                     << n_params << ") doesn't fit to number of variables (m_numParameters="
                     << m_numParameters << ").";
      throw GenericEso::Error(errorMessage.str());
  }
	int j = 0;
	for (EsoIndex i = m_numDiffVariables + m_numAlgVariables; i < m_numVariables; i++) {
		parameters[j] = m_x[i];
		j++;
	}
  }

  /* @brief getter for the values of the algebraic variables
    *
    * @param[out] algebraicVariables array receiving the values of the algebraic variables -
    *                           must be of size getNumAlgebraicVariables
    */

  void MADOGenericEso::getAlgebraicVariableValues(const EsoIndex n_alg_var, EsoDouble *algebraicVariables) {
    if (n_alg_var != m_numAlgVariables){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getAlgebraicVariableValues:\n size of output array (n_alg_var="
                     << n_alg_var << ") doesn't fit to number of variables (m_numAlgVariables="
                     << m_numAlgVariables << ").";
      throw GenericEso::Error(errorMessage.str());
  }

	int j = 0;
	for (EsoIndex i = m_numDiffVariables; i < m_numDiffVariables + m_numAlgVariables; i++) {
		algebraicVariables[j] = m_x[i];
		j++;
	}
  }

  /**
    * @brief getter for the values of the differential variables
    *
    * @param[out] differentialVariables array receiving the values of the differential variables -
    *                              must be of size getNumDifferentialVariables
    */

  void MADOGenericEso::getDifferentialVariableValues(const EsoIndex n_diff_var, EsoDouble *differentialVariables) {
    if (n_diff_var != m_numDiffVariables){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getDifferentialVariableValues:\n size of output array (n_diff_var="
                     << n_diff_var << ") doesn't fit to number of variables (m_numDiffVariables="
                     << m_numDiffVariables << ").";
      throw GenericEso::Error(errorMessage.str());
  }
	for (EsoIndex i =0; i < m_numDiffVariables; i++) {
		differentialVariables[i] = m_x[i];
	}
  }

  /**
    * @brief get the values of all states
    *
    * @param[out] states array receiving the values of the states to be set - must be of size getNumStates
    */

  void MADOGenericEso::getStateValues(const EsoIndex n_states, EsoDouble *states) {
    if (n_states != m_numStates){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getStateValues:\n size of output array (n_states="
                     << n_states << ") doesn't fit to number of variables (m_numStates="
                     << m_numStates << ").";
      throw GenericEso::Error(errorMessage.str());
  }

	for (EsoIndex i = 0; i < m_numStates; i++) {
		states[i] = m_x[i];
	}
  }


  /**
    * @brief getter for the values of a subset of all variables
    *
    * @param[out] variables array receiving the values of the variables -
    *                  must be at least of size of parameter indices
    * @param[in] indices vector containing the indices of the variables to be received
    */

  void MADOGenericEso::getVariableValues(const EsoIndex n_idx, EsoDouble *variables, const EsoIndex *indices) {
    if (n_idx > m_numVariables){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getVariableValues:\n n_idx (="
                     << n_idx << ") exceedes number of variables (m_numVariables="
                     << m_numVariables << ").";
      throw GenericEso::Error(errorMessage.str());
    }
    else if (n_idx <= 0){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getVariableValues:\n n_idx (="
                     << n_idx << ") has to be positive and non-zero.";
        throw GenericEso::Error(errorMessage.str());
    }

	for (int i = 0; i < n_idx; i++) {
		if (indices[i] < 0 || indices[i] >= m_numAllVariables) {
			std::ostringstream errorMessage;
			errorMessage << "MADOGenericEso::getVariableValues:\n out of variable indices range at index="
				<< i << " indices[index]=" << indices[i] << ".";
			throw GenericEso::Error(errorMessage.str());
		}
	}
	for (EsoIndex i = 0; i < n_idx; i++) {
		  variables[i] = m_x[indices[i]];
	 }

    
  }

  /**
    * @brief set the values of all derivatives
    *
    * Only the derivatives of the differential variables are set.
    * @param[in] derivatives array containing the values of the derivatives to be set -
    *                    must be of size getNumDifferentialVariables
    */

  void MADOGenericEso::setDerivativeValues(const EsoIndex n_diff_var, const EsoDouble *derivatives) {
    if (n_diff_var != m_numDiffVariables){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::setDerivativeValues:\n size of input array (n_diff_var="
                     << n_diff_var << ") doesn't fit to number of variables (m_numDiffVariables="
                     << m_numDiffVariables << ").";
      throw GenericEso::Error(errorMessage.str());
  }

	// Set values for der_x in m_y
	for (EsoIndex i = m_numVariables; i < m_numAllVariables; i++) {
		m_x[i] = derivatives[i - m_numVariables];
	}
  }

  /**
    * @brief getter for the values of all derivatives
    *
    * Only the derivatives of the differential variables are returned
    * @param[out] derivatives array receiving the values of the derivatives to be set -
    *                    must be of size getNumDifferentialVariables
    */

  void MADOGenericEso::getDerivativeValues(const EsoIndex n_diff_var, EsoDouble *derivatives) {
    if (n_diff_var != m_numDiffVariables){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getDerivativeValues:\n size of output array (n_diff_var="
                     << n_diff_var << ") doesn't fit to number of variables (m_numDiffVariables="
                     << m_numDiffVariables << ").";
      throw GenericEso::Error(errorMessage.str());
  }

	int j = 0;
    	for (EsoIndex i = m_numVariables; i < m_numAllVariables; i++) {
			derivatives[j] = m_x[i];
			j++;
	}
  }



  /**
    * @brief getter for the residuals of all equations
    *
    * @param[out] residuals array receiving the residuals - must be of size getNumEquations
    */

  GenericEso::RetFlag MADOGenericEso::getAllResiduals(const EsoIndex n_eq, EsoDouble *residuals) {
    if (n_eq != m_numEquations){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getAllResiduals:\n size of output array (n_eq="
                     << n_eq << ") doesn't fit to number of equations (m_numEquations="
                     << m_numEquations << ").";
      throw GenericEso::Error(errorMessage.str());
  }

    m_myMADO->getAllResiduals(m_x.data(), residuals, m_numAllVariables, m_numEquations);
    return GenericEso::RetFlag::OK;
  }

  /**
    * @brief getter for the residuals of all differential equations
    *
    * @param[out] differentialResiduals array receiving the residuals -
    *                              must be of size getNumDifferentialEquations
    */

  GenericEso::RetFlag MADOGenericEso::getDifferentialResiduals(const EsoIndex n_diff_eq, EsoDouble *differentialResiduals) {
    if (n_diff_eq != m_numDiffEquations){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getDifferentialResiduals:\n size of output array (n_diff_eq="
                     << n_diff_eq << ") doesn't fit to number of equations (m_numDiffEquations="
                     << m_numDiffEquations << ").";
      throw GenericEso::Error(errorMessage.str());
  }


	

	m_myMADO->getDifferentialResiduals(m_x.data(), differentialResiduals, m_numAllVariables, m_numDiffEquations);
	

    return GenericEso::RetFlag::OK;
  }

  /**
    * @brief getter for the residuals of all algebraic equations
    *
    * @param[out] algebraicResiduals array receiving the residuals - must be of size getNumAlgebraicEquations
    */

  GenericEso::RetFlag MADOGenericEso::getAlgebraicResiduals(const EsoIndex n_alg_eq, EsoDouble *algebraicResiduals) {
    if (n_alg_eq != m_numAlgEquations){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getAlgebraicResiduals:\n size of output array (n_alg_eq="
                     << n_alg_eq << ") doesn't fit to number of equations (m_numAlgEquations="
                     << m_numAlgEquations << ").";
      throw GenericEso::Error(errorMessage.str());
    }



	m_myMADO->getAlgebraicResiduals(m_x.data(), algebraicResiduals, m_numAllVariables, m_numAlgEquations);

    return GenericEso::RetFlag::OK;
  }

  /**
    * @brief getter for the residuals of a subset of equations
    *
    * @param[out] residuals array receiving the residuals - must be at least of the size of equationIndex
    * @param[in] equationIndex vector containing the indices of the equations to be returned
    */

  GenericEso::RetFlag MADOGenericEso::getResiduals(const EsoIndex n_eq, EsoDouble *residuals, const EsoIndex *equationIndex) {

    if (n_eq > m_numEquations){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getResiduals:\n n_eq (="
                     << n_eq << ") exceedes number of equations (m_numEquations="
                     << m_numEquations << ").";
      throw GenericEso::Error(errorMessage.str());
    }
    else if (n_eq <= 0){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getResiduals:\n n_eq (="
                     << n_eq << ") has to be positive and non-zero.";
        throw GenericEso::Error(errorMessage.str());
    }

    if (n_eq < m_numEquations) {
		double*res = new double[m_numEquations];
		m_myMADO->getAllResiduals(m_x.data(), res, m_numAllVariables, m_numEquations);
      for (int i = 0; i < n_eq; i++) {
        if (equationIndex[i] < 0 || equationIndex[i] >= m_numEquations) {
          std::ostringstream errorMessage;
          errorMessage << "MADOGenericEso::getResiduals:\n out of equationen indices range at index="
            << i << " indices[index]=" << equationIndex[i] << ".";
          throw GenericEso::Error(errorMessage.str());
        }
		residuals[i] = res[equationIndex[i]];

      }
	  delete[] res;
    }
    else {
      // complete evaluation of the residual (less function call overhead)
		m_myMADO->getAllResiduals(m_x.data(), residuals, m_numAllVariables, m_numEquations);
    }

    return GenericEso::RetFlag::OK;
  }


  void MADOGenericEso::getJacobianStruct(const EsoIndex n_nz, EsoIndex *rowIndices, EsoIndex *colIndices) {

    if (n_nz != m_numNonZeros){
      std::ostringstream errorMessage;
      errorMessage << "MADOGenericEso::getJacobianStruct:\n size of output array n_nz="
                   << n_nz << " does not fit to number of non-zero entries(" << m_numNonZeros << ").";
      throw GenericEso::Error(errorMessage.str());
    }

	if (!m_jacobianStructKnown) {

		unsigned int** JP = NULL;
		JP = (unsigned int**)malloc(m_numEquations * sizeof(unsigned int*));

		m_myMADO->getJacobianStruct(JP);

		int idx = 0;
		for (int i = 0; i < m_numEquations; i++)
		{
			if (JP[i][0] != 0) {
				for (int k = 0; k < JP[i][0]; k++)
				{
					m_rowIndicesJacStruct[idx] = i;
					m_colIndicesJacStruct[idx] = JP[i][k + 1];
					idx++;
				}
			}
		}

		free(JP); JP = NULL;

		m_jacobianStructKnown = true;
	}
	
		
		for (int k = 0; k < m_numNonZeros; k++)
		{
			rowIndices[k] =  m_rowIndicesJacStruct[k];
			colIndices[k] =  m_colIndicesJacStruct[k];

		}

	

  }

  /**
    * @brief getter for all Jacobian values
    *
    * @param[out] values array receiving the Jacobian values in COO sparse format
    * @sa getJacobianStruct
    */

  GenericEso::RetFlag MADOGenericEso::getJacobianValues(const EsoIndex n_nz, EsoDouble *values) {

    if (n_nz != m_numNonZeros){
      std::ostringstream errorMessage;
      errorMessage << "MADOGenericEso::getJacobianValues:\n size of output array n_nz="
                   << n_nz << " does not fit to number of non-zero entries(" << m_numNonZeros << ").";
      throw GenericEso::Error(errorMessage.str());
    }

	
	m_myMADO->getJacobianValues(m_x.data(), &m_numNonZeros, m_rind.data(), m_cind.data(), values);
	


    return GenericEso::RetFlag::OK;
  }

  /**
    * @brief getter for a subset of Jacobian values
    *
    * @param[out] values array receiving the Jacobian values
    * @param[in] indices vector containing the eso indices of the subset -
  //				 each etnry must be in range [0, numNonZeroes-1]
    */

  GenericEso::RetFlag MADOGenericEso::getJacobianValues(const EsoIndex n_idx, EsoDouble *values,
                                                const EsoIndex *indices) {

    if (n_idx <= 0){
      std::ostringstream errorMessage;
      errorMessage << "MADOGenericEso::getJacobianValues:\n n_idx="
                   << n_idx << " has to be positive and non zero.";
      throw GenericEso::Error(errorMessage.str());
    }
    if (n_idx > m_numNonZeros){
      std::ostringstream errorMessage;
      errorMessage << "MADOGenericEso::getJacobianValues:\n n_idx="
                   << n_idx << " exceeds number of non-zero entries (" << m_numNonZeros << ").";
      throw GenericEso::Error(errorMessage.str());
    }


    int nnz = m_numNonZeros;
    std::vector<EsoDouble> allValues(nnz);
    getJacobianValues(nnz, allValues.data());

    for (int i = 0; i < n_idx; i++) {
      if (indices[i] < 0 || indices[i] >= m_numNonZeros){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getJacobianValues:\n indice at position i="
                     << i << " indices[i]=" << indices[i] << " is out of range";
        throw GenericEso::Error(errorMessage.str());
      }

      values[i] = allValues[indices[i]];
    }


    return GenericEso::RetFlag::OK;
  }

  void MADOGenericEso::getDiffJacobianStruct(const EsoIndex n_diff_nz, EsoIndex *rowIndices, EsoIndex *colIndices) {

	  if (n_diff_nz != m_numDiffNonZeros) {
		  std::ostringstream errorMessage;
		  errorMessage << "MADOGenericEso::getDiffJacobianStruct:\n size of output array n_diff_nz="
			  << n_diff_nz << " does not fit to number of non-zero entries(" << m_numDiffNonZeros << ").";
		  throw GenericEso::Error(errorMessage.str());
	  }

	  if (!m_jacobianDiffStructKnown) {

	  unsigned int** JP_diff = NULL;
	  JP_diff = (unsigned int**)malloc(m_numEquations * sizeof(unsigned int*));

	  m_myMADO->getDiffJacobianStruct(JP_diff);

	  int idx = 0;
	  for (int i = 0; i < m_numEquations; i++)
	  {

		  if (JP_diff[i][0] != 0) {
			  for (int k = 0; k < JP_diff[i][0]; k++)
			  {
				  m_rowIndicesDiffJacStruct[idx] = i;
				  m_colIndicesDiffJacStruct[idx] = JP_diff[i][k + 1];
				  idx++;
			  }
		  }
	  }

	  free(JP_diff); JP_diff = NULL;

	  m_jacobianDiffStructKnown = true;
  }

	for (int k = 0; k < m_numDiffNonZeros; k++)
	{
		rowIndices[k] = m_rowIndicesDiffJacStruct[k];
		colIndices[k] = m_colIndicesDiffJacStruct[k];

	}

  }

  /**
    * @brief getter for all differential Jacobian data of the model
    *
    * @param[out] values array receiving the differential Jacobian values in COO sparse format
    * @sa getDiffJacobianStruct
    */
  GenericEso::RetFlag MADOGenericEso::getDiffJacobianValues(const EsoIndex n_diff_nz, EsoDouble *values) {

    if (n_diff_nz != m_numDiffNonZeros){
      std::ostringstream errorMessage;
      errorMessage << "MADOGenericEso::getDiffJacobianValues:\n size of output array n_diff_nz="
                   << n_diff_nz << " does not fit to number of non-zero entries(" << m_numDiffNonZeros << ").";
      throw GenericEso::Error(errorMessage.str());
    }

	
	m_myMADO->getDiffJacobianValues(m_x.data(), &m_numDiffNonZeros, m_rind_diff.data(), m_cind_diff.data(), values);

	

    return GenericEso::RetFlag::OK;
  }

  /**
  * @brief Specifies index set of parameters p
  */

  void MADOGenericEso::getParameterIndex(const EsoIndex n_para, EsoIndex *parameterIndex){
    if(n_para != m_numParameters){
      std::ostringstream errorMessage;
      errorMessage << "MADOGenericEso::getParameterIndex:\n size of output array n_para="
                   << n_para << " does not fit to number of parameters(" << m_numParameters << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    int offset = m_numDiffVariables + m_numAlgVariables;
    for (int i = 0; i < n_para; i++)
      parameterIndex[i] = i+offset;

  }

  /**
* @brief Specifies index set of differential variables x
*/
  void MADOGenericEso::getDifferentialIndex(const EsoIndex n_diff_var, EsoIndex *differentialIndex){
    if(n_diff_var != m_numDiffVariables){
      std::ostringstream errorMessage;
      errorMessage << "MADOGenericEso::getDifferentialIndex:\n size of output array n_diff_var="
                   << n_diff_var << " does not fit to number of differential variables(" << m_numDiffVariables << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    int offset = 0;
    for (int i = 0; i < n_diff_var; i++)
      differentialIndex[i] = i+offset;

  }

/**
* @brief Specifies index set of algebraic variables y
*/
  void MADOGenericEso::getAlgebraicIndex(const EsoIndex n_alg_var, EsoIndex *algebraicIndex){
    if(n_alg_var != m_numAlgVariables){
      std::ostringstream errorMessage;
      errorMessage << "MADOGenericEso::getAlgebraicIndex:\n size of output array n_alg_var="
                   << n_alg_var << " does not fit to number of algebraic variables(" << m_numAlgVariables << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    int offset = m_numDiffVariables;
    for (int i = 0; i < n_alg_var; i++)
      algebraicIndex[i] = i+offset;

  }

  /**
  * @brief Specifies index set of differential & algebraic variables, x & y
  */
  void MADOGenericEso::getStateIndex(const EsoIndex n_states, EsoIndex *stateIndex){
    if(n_states != m_numStates){
      std::ostringstream errorMessage;
      errorMessage << "MADOGenericEso::getStateIndex:\n size of output array n_states="
                   << n_states << " does not fit to number of states(" << m_numStates << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    int offset = 0;
    for (int i = 0; i < n_states; i++)
      stateIndex[i] = i+offset;

  }

  /**
* @brief Specifies index set of differential equations in the system of residual equations
*/
  void MADOGenericEso::getDiffEquationIndex(const EsoIndex n_diff_eq, EsoIndex *diffEqIndex) {
    if(n_diff_eq != m_numDiffEquations){
      std::ostringstream errorMessage;
      errorMessage << "MADOGenericEso::getDiffEquationIndex:\n size of output array n_diff_eq="
                   << n_diff_eq << " does not fit to number of differential equations(" << m_numDiffEquations << ").";
      throw GenericEso::Error(errorMessage.str());
    }

	for (int i = 0; i < m_numDiffEquations; i++) {
		diffEqIndex[i] = m_indexDiffEqn[i];
	}
  }

  /**
* @brief Specifies index set of algebraic equations in the system of residual equations
*/
  void MADOGenericEso::getAlgEquationIndex(const EsoIndex n_alg_eq, EsoIndex *algEqIndex) {
    if (n_alg_eq != m_numAlgEquations){
      std::ostringstream errorMessage;
      errorMessage << "MADOGenericEso::getAlgEquationIndex:\n size of output array n_alg_eq="
                   << n_alg_eq << " does not fit to number of algebraic equations(" << m_numAlgEquations << ").";
      throw GenericEso::Error(errorMessage.str());
    }

	//use differential structure to get index algebraic equations
	for (int i = 0; i < m_numAlgEquations; i++) {
		algEqIndex[i] = m_indexAlgEqn[i];
	}


  }

  ////utility functions
  EsoIndex MADOGenericEso::getEsoIndexOfVariable(const std::string &varName) {
    for (int i = 0; i < m_numVariables; i++)
        if (m_variableNames[i].compare(varName) == 0) return i;

    std::ostringstream errorMessage;
    errorMessage << "MADOGenericEso::getEsoIndexOfVariable:\n variable name ("
                 << varName << ") could not be found.";
    throw GenericEso::Error(errorMessage.str());

  }

  std::string MADOGenericEso::getVariableNameOfIndex(const EsoIndex esoIndex) {
    if (esoIndex < 0){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getVariableNameOfIndex:\n esoIndex="
                     << esoIndex << " has to be non-negative.";
        throw GenericEso::Error(errorMessage.str());
    }
    if (esoIndex >= m_numVariables){
        std::ostringstream errorMessage;
        errorMessage << "MADOGenericEso::getVariableNameOfIndex:\n esoIndex="
                     << esoIndex << " exceeds number of variables("<< m_numVariables << ").";
        throw GenericEso::Error(errorMessage.str());
    }

    return m_variableNames[esoIndex];
  }



  void MADOGenericEso::getInitialStateValues(const EsoIndex n_states, EsoDouble *states) {
	  if (n_states != m_numStates) {
		  std::ostringstream errorMessage;
		  errorMessage << "GenericEso::getInitialStateValues:\n size of output array (n_states="
			  << n_states << ") doesn't fit to number of variables (m_numStates="
			  << m_numStates << ").";
		  throw GenericEso::Error(errorMessage.str());
	  }
      for (int i = 0; i < n_states; i++) {
          states[i] = m_x[i]; 
      }
      
	  //m_myMADO->getInitialValues(states, n_states); //this is only true for the very first integration!


  }


  //todo: What's the point of this function? and does it what it should?
  EsoIndex MADOGenericEso::getStateIndexOfVariable(const EsoIndex esoIndex) {
    if (esoIndex < 0){
      std::ostringstream errorMessage;
      errorMessage << "MADOGenericEso::getStateIndexOfVariable:\n esoIndex="
                   << esoIndex << " has to be non-negative.";
      throw GenericEso::Error(errorMessage.str());
    }
    if (esoIndex >= m_numVariables){
      std::ostringstream errorMessage;
      errorMessage << "MADOGenericEso::getStateIndexOfVariable:\n esoIndex="
                   << esoIndex << " exceeds number of variables("<< m_numVariables << ").";
      throw GenericEso::Error(errorMessage.str());
    }

    if (esoIndex >= m_numStates)
      return m_numStates;
    return esoIndex;
  }

  EsoType MADOGenericEso::getType() const {
      return EsoType::MADO;
  }

  ModelName MADOGenericEso::getModel() const {
      return model_name;
  }























  ///The following to functions are not supported or needed in MADOGenricEso. ------------
  //Todo: confirm and remove (also in header)



  /**
  * @brief get the current value of the independent variable(time)
  *
  * @return current value of the independent variable
  */
  EsoIndex MADOGenericEso::getNumInitialNonZeroes() const {
	  // Initial equations are not supported. Therefore, there are not non-zeros.
	  return 0;
  }

  /**
  * no independet variable for mado
  * @brief set the independent variable (time) to a new value
  *
  * @param[in] var new value of the independent variable
  */
  void MADOGenericEso::setIndependentVariable(const EsoDouble var) {
  }

  /**
	* no independet variable for mado
	* @brief get the current value of the independent variable(time)
	*
	* @return current value of the independent variable
	*/
  EsoDouble MADOGenericEso::getIndependentVariable() const {
	  return 0;
  }

  /** @copydoc GenericEso::getAllBounds(EsoIndex,EsoDouble*,EsoDouble*)
  * @brief getter for all variable bounds
  *
  * @param[out] lowerBounds vector receiving the lower bounds of all variables -
  *                    must be of size getNumVariables
  * @param[out] upperBounds vector receiving the upper bounds of all variables -
  *                    must be of size getNumVariables
  */

  void MADOGenericEso::getAllBounds(const EsoIndex n_var, EsoDouble *lowerBounds, EsoDouble *upperBounds)
  {
	  assert(n_var == (m_numStates + m_numParameters));
	  /*** Determine all indices ***/

	  const vector<EsoIndex> algStateIndices = m_algebraicStateVariables.getEsoIndices();
	  const vector<EsoIndex> diffStateIndices = m_differentialStateVariables.getEsoIndices();
	  const vector<EsoIndex> parameterIndices = m_parameters.getEsoIndices();

	  /*** Get all bounds from individual elements ***/

	  const vector<double> algLowerBounds = m_algebraicStateVariables.getLowerModelBound();
	  const vector<double> algUpperBounds = m_algebraicStateVariables.getUpperModelBound();
	  const vector<double> diffLowerBounds = m_differentialStateVariables.getLowerModelBound();
	  const vector<double> diffUpperBounds = m_differentialStateVariables.getUpperModelBound();
	  const vector<double> parameterLowerBounds = m_parameters.getLowerModelBound();
	  const vector<double> parameterUpperBounds = m_parameters.getUpperModelBound();

	  /*** Copy all bounds to the right output according to the variable indices ***/

	  for (EsoIndex i = 0; i < m_algebraicStateVariables.getNumberOfVariables(); i++) {
		  lowerBounds[algStateIndices[i]] = algLowerBounds[i];
		  upperBounds[algStateIndices[i]] = algUpperBounds[i];
	  }
	  for (EsoIndex i = 0; i < m_differentialStateVariables.getNumberOfVariables(); i++) {
		  lowerBounds[diffStateIndices[i]] = diffLowerBounds[i];
		  upperBounds[diffStateIndices[i]] = diffUpperBounds[i];
	  }
	  for (EsoIndex i = 0; i < m_parameters.getNumberOfVariables(); i++) {
		  lowerBounds[parameterIndices[i]] = parameterLowerBounds[i];
		  upperBounds[parameterIndices[i]] = parameterUpperBounds[i];
	  }
  }

  /**
  * @brief get the values of all states - use initial equations to reset states
  *
  * @param[out] states array receiving the values of the states to be set - must be of size getNumStates
  */


  /**
  * @copydoc GenericEso::getInitialJacobian(EsoIndex, EsoIndex*, EsoIndex*, EsoDouble*)
  *
  * initial Eso not definded for MADO - use standard Jacobian as InitialJacobian
  */
  GenericEso::RetFlag MADOGenericEso::getInitialJacobian(const EsoIndex n_nz, EsoIndex *rowIndices,
	  EsoIndex *colIndices, EsoDouble *values)
  {
	  return GenericEso::RetFlag::OK;
  }

  /**
	* @brief initialize differential variables with user defined values
	*
	* override initial values of the differential variables in the model's initial equations
	* if a variable is initialized to a constant value in the initial sections there
	* will be no problem doing so. Variables initialized by a model parameter can only be overridden,
	* if the parameter is not used as a optimization variable and will remain unset.
	* The
	* @param numDiffVals number of differential variables being set
	* @param diffIndices eso indices of the differential variables being set (length of numDiffVals)
	* @param diffValues new initial values of the differential variables
	* @param numParameters number of used parameters
	* @param parameterIndices indices of used parameters
	* @return GenericEso::OK if setting of values is not problematic, GenericEso::FAIL if dependencies
	*         of parameters lead to possible inconsistencies
	*/


  GenericEso::RetFlag MADOGenericEso::setInitialValues(const EsoIndex numDiffVals, EsoIndex *diffIndices,
	  EsoDouble * diffValues, const EsoIndex numParameters,
	  EsoIndex *parameterIndices) {
	  // get initial values from model, which might be overwritten
	  
	  m_myMADO->getInitialValues(m_x.data(), m_numDiffVariables + m_numAlgVariables);


	  
	 // FMI
	  assert(numDiffVals <= m_differentialStateVariables.getNumberOfVariables());

	  std::vector<EsoIndex> diffIndex = m_differentialStateVariables.getEsoIndices();
	  for (EsoIndex i = 0; i < numDiffVals; i++) {
		  //throw assert if index in diffIndices is no actual differential index
		  assert(std::find(diffIndex.begin(),
			  diffIndex.end(),
			  diffIndices[i])
			  != diffIndex.end());
		  m_x[diffIndices[i]] = diffValues[i];
	  }
	  //for (EsoIndex i = 0; i < m_differentialStateVariables.getNumberOfVariables(); i++) {
		 // m_x[diffIndex[i]] = m_initialStates[diffIndex[i]];
	  //}


	  // JAde2
	  //assert(numDiffVals <= m_numDiffVariables);
	  ////const int minimalAllowedIndex = m_Model->variableIndexOffset(DiffVars{});
	  //const int minimalAllowedIndex = jade2Loader.variableIndexOffset(model, DiffVars::value);
	  //const int maximalAllowedIndex = minimalAllowedIndex + jade2Loader.getNumVariables(DiffVars::value) - 1 ;

	  //for(EsoIndex i=0; i<numDiffVals; i++){
	  //  //throw assert if index in diffIndices is no actual differential index
	  //  assert(diffIndices[i] >= minimalAllowedIndex && diffIndices[i] <= maximalAllowedIndex);
	  //  m_initialStates[diffIndices[i]] = diffValues[i];
	  //}
	  //for(EsoIndex i=0; i<m_numDiffVariables; i++){
	  //  jade2Loader.setVariable(model, DiffVars::value,i, m_initialStates[i+minimalAllowedIndex]);
	  //}


	  return GenericEso::RetFlag::OK;

  }

  GenericEso::RetFlag MADOGenericEso::getJacobianMultVector(const bool transpose, const EsoIndex n_states, EsoDouble *seedStates,
	  const EsoIndex n_params, EsoDouble *seedParameters, const EsoIndex n_y,
	  EsoDouble *y)
  {

	  /*if (seedStates == NULL){
		std::ostringstream errorMessage;
		errorMessage << "GenericEso::getJacobianMultVector:\n seedStates is null and has to be preallocated with length n_states="
					 << n_states << " .";
		throw GenericEso::Error(errorMessage.str());
	  }
	  if (seedParameters == NULL){
		std::ostringstream errorMessage;
		errorMessage << "GenericEso::getJacobianMultVector:\n seedParameters is null and has to be preallocated with length seedParameters="
				<< seedParameters << " .";
		throw GenericEso::Error(errorMessage.str());
	  }
	  if (y == NULL){
		std::ostringstream errorMessage;
		errorMessage << "GenericEso::getJacobianMultVector:\n y is null and has to be preallocated with length n_y="
					 << n_y << " .";
		throw GenericEso::Error(errorMessage.str());
	  }

	  if (n_states != m_numStates){
		std::ostringstream errorMessage;
		errorMessage << "GenericEso::getJacobianMultVector:\n n_states="
					 << n_states << " has to be equal to the model's number of states(" << m_numStates << ").";
		throw GenericEso::Error(errorMessage.str());
	  }
	  if (n_params != m_numParameters){
		std::ostringstream errorMessage;
		errorMessage << "GenericEso::getJacobianMultVector:\n n_params="
					 << n_params << " has to be equal to the model's number of parameters(" << m_numParameters << ").";
		throw GenericEso::Error(errorMessage.str());
	  }
	  if (n_y != m_numEquations){
		std::ostringstream errorMessage;
		errorMessage << "GenericEso::getJacobianMultVector:\n n_y="
					 << n_y << " has to be equal to the model's number of equations(" << m_numEquations << ").";
		throw GenericEso::Error(errorMessage.str());
	  }

	  jade2Loader.evalJacobianValues(model, AllEqns::value, StateVars::value, m_f_x);
	  jade2Loader.evalJacobianValues(model, AllEqns::value, ParamVars::value, m_f_p);

	  Eigen::Map<Eigen::VectorXd> t1_x(seedStates, n_states, 1);
	  Eigen::Map<Eigen::VectorXd> t1_p(seedParameters, n_params, 1);
	  Eigen::Map<Eigen::VectorXd> t1_y(y, n_y, 1);

	  if (!transpose)
		t1_y = m_f_x * t1_x + m_f_p * t1_p;
	  else {
		t1_x = m_f_x.transpose() * t1_y;
		t1_p = m_f_p.transpose() * t1_y;
	  }
	  */
	  return GenericEso::RetFlag::OK;
  }

  /**
   * @brief Calculates second-order tangent-linear over adjoint derivatives of the DA-System.
   *
   * This functions calls the code generated by dcc calculating second-order tangent-linear over
   * adjoint derivatives. The actual values of the states, the parameters and the derivated states are
   * taken from the members m_states, m_parametersACS and m_der_states.
   * For explanation: z = [states p der_states]^T,
   *                  all arrays with suffix '_states' or '_der_states' are of size (number of states)
   *                  all arrays with suffix '_p' are of size (number of parameters)
   *                  all arrays with suffix '_yy' or '_residuals' are of size (number of equations)
   *                  <.,.> represents the inner dot product with appropriate dimensions.
   *
   * output definition: a1_z = (dF(z)/dz)^T * a1Yy;
   *                    t2_a1_z = <d2F(z)/dzdz, a1Yy, t2_z> + <dF(z)/dz, t2_a1Yy>;
   *
   * @param[in] lenT2States length of t2States array. Must be equal to m_n_states
   * @param[in] t2States array that defines the first m_n_states entries of t2_z
   * @param[in] lenT2DerStates length of t2DerStates arry. Must be equal to m_n_states
   * @param[in] t2DerStates array that defines the last m_n_states entries of t2_z
   * @param[in] lenT2P length of t2P array. Must be equal to m_n_pars
   * @param[in] t2P array that defines the entries from index m_n_states til index
   *            (m_n_states+m_n_pars-1) of t2_z
   * @param[in] lenA1States length of a1States array. Must be equal to m_n_states
   * @param[out] a1States contains on output the product of the transposed Jacobian, relative to the
   *             states, with a1Yy
   * @param[in] lenA1DerStates length of a1DerStates array. Must be equal to m_n_states
   * @param[out] a1DerStates contains on output the product of the transposed Jacobian, relative to
   *             the derivated states, with a1Yy
   * @param[in] lenA1P length of a1P array. Must be equal to m_n_pars
   * @param[out] a1P contains on output the product of the transposed Jacobian, relative to the
   *             parameters, with a1Yy
   * @param[in] lenT2A1States length of t2A1States array. Must be equal to m_n_states
   * @param[out] t2A1States array that defines the first m_n_states entries of output array
   *             t2_a1_z
   * @param[in] lenT2A1DerStates length of t2A1DerStates array. Must be equal to m_n_states
   * @param[out] t2A1DerStates array that defines the last m_n_states entries of output array
   *             t2_a1_z
   * @param[in] lenT2A1P length of t2A1P array. Must be equal to m_n_pars
   * @param[out] t2A1P array that defines the entries from index m_n_states til index
   *            (m_n_states+m_n_pars-1) of output array t2_a1_z
   * @param[in] lenA1Yy length of a1Yy array. Must be equal to m_n_eq
   * @param[in] a1Yy seed vector multiplying the Jacobian matrix to calculate a1_z and the Hessian
   *            matrix to calculate t2_a1_z.
   * @param[in] lenT2A1Yy length of t2A1Yy array. Must be equal to m_n_eq
   * @param[in] t2A1Yy seed vector to define the jacobian part of t2_a1_z
   */
  void MADOGenericEso::evaluate2ndOrderDerivatives(unsigned lenT2States, double *t2States,
      unsigned lenT2DerStates, double *t2DerStates,
      unsigned lenT2P, double *t2P,
      unsigned lenA1States, double *a1States,
      unsigned lenA1DerStates, double *a1DerStates,
      unsigned lenA1P, double *a1P,
      unsigned lenT2A1States, double *t2A1States,
      unsigned lenT2A1DerStates, double *t2A1DerStates,
      unsigned lenT2A1P, double *t2A1P,
      unsigned lenA1Yy, double *a1Yy,
      unsigned lenT2A1Yy, double *t2A1Yy) {

   //   Eigen::Map<Eigen::VectorXd> a1_x(a1States, lenA1States, 1);
   //   Eigen::Map<Eigen::VectorXd> a1_der(a1DerStates, lenA1DerStates, 1);
   //   Eigen::Map<Eigen::VectorXd> a1_p(a1P, lenA1P, 1);
   //   Eigen::VectorXd t2All = Eigen::VectorXd::Zero(m_numVariables);
   //   Eigen::VectorXd t2A1All = Eigen::VectorXd::Zero(m_numVariables);

   //   Eigen::VectorXd a1_yy(lenA1Yy);
   //   for (unsigned i = 0; i < lenA1Yy; i++)
   //       a1_yy[i] = a1Yy[i];

   //   jade2Loader.evalJacobianValues(model, AllEqns::value, StateVars::value, m_f_x);
   //   a1_x = m_f_x.transpose() * a1_yy;

   //   //jade2Loader.evalJacobianValues(model, AllEqns::value, DerDiffVars::value, m_f_derDiff);
   //   //a1_der = m_f_derDiff.transpose() * a1_yy;

   //   jade2Loader.evalJacobianValues(model, AllEqns::value, ParamVars::value, m_f_p);
   //   a1_p = m_f_p.transpose() * a1_yy;

   //   jade2Loader.setAdjoints(model, AllEqns::value, a1_yy);

   //   for (int i = 0; i < m_numStates; i++)
   //       t2All[i + jade2Loader.variableIndexOffset(model, StateVars::value)] = t2States[i];

   //   for(int i=0; i < m_numParameters; i++)
   //       t2All[i + jade2Loader.variableIndexOffset(model, ParamVars::value)] = t2P[i];

   //   for(int i=0; i<m_numDiffVariables; i++)
   //       t2All[i + jade2Loader.variableIndexOffset(model, DerDiffVars::value)] = t2DerStates[i];

   //   jade2Loader.evalAllSecondOrderAdjoints(model, AllVars::value, t2All.data(), t2A1Yy, t2A1All);

	  //for (int i = 0; i < m_numStates; i++)
		 // t2A1States[i] = t2A1All[i + jade2Loader.variableIndexOffset(model, StateVars::value)];

	  //for (int i = 0; i < m_numParameters; i++)
		 // t2A1P[i] = t2A1All[i + jade2Loader.variableIndexOffset(model, ParamVars::value)];

	  //for (int i = 0; i < m_numDiffVariables; i++)
		 // t2A1DerStates[i] = t2A1All[i + jade2Loader.variableIndexOffset(model, DerDiffVars::value)];
  }


/**
 * @brief Calculates first-order tangent-linear derivatives of the DA-System.
 *
 * This functions calls the code generated by dcc calculating first-order forward derivatives.
 * The actual values of the states, the parameters and the derivated states are taken from the
 * members m_states, m_parametersACS and m_der_states.
 * For explanation: z = [states p der_states]^T
 *                  all arrays with suffix '_states' or '_der_states' are of size (number of states)
 *                  all arrays with suffix '_p' are of size (number of parameters)
 *                  all arrays with suffix '_yy' or '_residuals' are of size (number of equations)
 *
 * output definition: t1_residuals = dF(z)/dz * t1_z
 *
 * @param[in] lenT1States length of t2States array. Must be equal to m_n_states
 * @param[in] t1States seed vector multiplying the Jacobian part relative to the states
 * @param[in] lenT1DerStates length of t2States array. Must be equal to m_n_states
 * @param[in] t1DerStates seed vector multiplying the Jacobian part relative to the derivated
 *            states
 * @param[in] lenT1P length of t2States array. Must be equal to m_n_pars
 * @param[in] t1P seed vector multiplying the Jacobian part relative to the parameters
 * @param[in] lenT1Res length of t2States array. Must be equal to m_n_eq
 * @param[out] t1Residuals product of the jacobian matrix and the seed vectors
 */
  void MADOGenericEso::evaluate1stOrdForwDerivatives(unsigned lenT1States, double *t1States,
      unsigned lenT1DerStates, double *t1DerStates,
      unsigned lenT1P, double *t1P,
      unsigned lenT1Res, double *t1Residuals) {

    /*int stateVarsOffset = jade2Loader.variableIndexOffset(model, StateVars::value);
    int paramVarsOffset = jade2Loader.variableIndexOffset(model, ParamVars::value);
    int derDiffVarsOffset = jade2Loader.variableIndexOffset(model, DerDiffVars::value);

    m_tangents.setZero();
    for (int i = 0; i < m_numDiffVariables + m_numAlgVariables; ++i)
      m_tangents[stateVarsOffset + i] = t1States[i];
    for (int i = 0; i < m_numParameters; ++i)
      m_tangents[paramVarsOffset + i] = t1P[i];
    for (int i = 0; i < m_numDiffVariables; ++i)
      m_tangents[derDiffVarsOffset + i] = t1DerStates[i];

    jade2Loader.evalAllTangents(model, m_tangents.data(), t1Residuals);*/

}

///-----------------------