/**
* @file ModelVariable.cpp
*
* =====================================================================\n
* (C) Lehrstuhl fuer Prozesstechnik, RWTH Aachen                       \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* This file contains the data structure of the generic eso             \n
* according to the UML Diagramm METAESO_2                              \n
* =====================================================================\n
* @author Fady Assassa, Tjalf Hoffmann                                 \n
* @date 19.07.2011
*/

#include "ModelVariable.hpp"

/**
* @brief setter of ModelVariable::m_name
* @param name vector of names to be set
*/
void ModelVariableSet::setName(const std::vector<std::string> &name)
{
  m_name.assign(name.begin(),name.end());
}

/**
* @brief setter of ModelVariable::m_esoIndex
* @param esoIndex vector of Eso indices to be set
*/
void ModelVariableSet::setEsoIndex(const std::vector<EsoIndex> &esoIndex)
{
  m_esoIndices.assign(esoIndex.begin(), esoIndex.end());
}

/**
* @brief setter of ModelVariable::m_lowerModelBound
* @param lowerModelBound vector of bounds to be set
*/
void ModelVariableSet::setLowerModelBound(const std::vector<EsoDouble> &lowerModelBound)
{
  m_lowerModelBound.assign(lowerModelBound.begin(), lowerModelBound.end());
}
/**
* @brief setter of ModelVariable::m_upperModelBound
* @param upperModelBound vector of bounds to be set
*/
void ModelVariableSet::setUpperModelBound(const std::vector<EsoDouble> &upperModelBound)
{
    m_upperModelBound.assign(upperModelBound.begin(), upperModelBound.end());
}
/**
* @brief setter of ModelVariable::m_lowerModelBound and ModelVariable::m_upperModelBounds
* @param lowerModelBound vector of lower bounds to be set
* @param upperModelBound vector of upper bounds to be set
*/
void ModelVariableSet::setModelBounds(const std::vector<EsoDouble> &lowerModelBound,
                                      const std::vector<EsoDouble> &upperModelBound)
{
  setLowerModelBound(lowerModelBound);
  setUpperModelBound(upperModelBound);
}
