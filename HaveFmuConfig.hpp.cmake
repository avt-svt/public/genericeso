/**
* @file HaveFmuConfig.hpp.cmake
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Configuration Header for Dyos                                          \n
* =====================================================================\n
* configure header         \n
* =====================================================================\n
* @author Adrian Caspari  
* @date 14.12.2017
*/

#pragma once


// Path to fmi.fmu for FmiGenericEsoTest
#cmakedefine BUILD_WITH_FMU "@BUILD_WITH_FMU@"


