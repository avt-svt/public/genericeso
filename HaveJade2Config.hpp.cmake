/**
* @file HaveJade2Config.hpp.cmake
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Configuration Header for Dyos                                          \n
* =====================================================================\n
* configure header         \n
* =====================================================================\n
* @author Adrian Caspari, Jan Schulze
* @date 27.01.2020
*/

#pragma once

#cmakedefine BUILD_JADE2 "@BUILD_JADE2@"


