/**
* @file MADOLoader.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic MADOLoader - Part of DyOS                                           \n
* =====================================================================\n
* testsuite     for the MADOLoader module	                             \n
* =====================================================================\n
* @author Lars Henrichfreise
* @date 03.01.2020
*/


#define BOOST_TEST_MODULE example

#include <boost/test/included/unit_test.hpp>
#include "MADOGenericEsoTestConfig.hpp"
#include "DyosMADOLoader.hpp"

BOOST_AUTO_TEST_SUITE(TestMADOLoader);

BOOST_AUTO_TEST_CASE(TestMADOLoaderConstructor) {

	//path to mado model 
	std::string path = PATH_MADO_LOADER_TEST_MODEL;

	BOOST_CHECK_NO_THROW(MADOLoader MADOLoaderObj(path));

}

BOOST_AUTO_TEST_CASE(TestMADOLoaderGetFunctions) {

	std::string path = PATH_MADO_LOADER_TEST_MODEL;

	MADOLoader MADOLoaderObj(path);
	MADO* myMADO = MADOLoaderObj.getMyMADO();
	BOOST_CHECK_EQUAL(myMADO->getNumEquations(), 12);
	BOOST_CHECK_EQUAL(myMADO->getNumSwitching(), 0);
	BOOST_CHECK_EQUAL(myMADO->getNumParams(), 2);
	BOOST_CHECK_EQUAL(myMADO->getNumDiffStates(), 9);
	BOOST_CHECK_EQUAL(myMADO->getNumAlgStates(), 3);
	BOOST_CHECK_EQUAL(myMADO->getNumVariables(), 14); // m_numDiffStates + m_numAlgStates + m_numParams
	BOOST_CHECK_EQUAL(myMADO->getNumAllVariables(), 23); // 2 * m_numDiffStates + m_numAlgStates + m_numParams
	
	myMADO->destroy();

}

BOOST_AUTO_TEST_CASE(TestMADOLoadergetJacobianStruct) {

	std::string path = PATH_MADO_LOADER_TEST_MODEL;
	MADOLoader MADOLoaderObj(path);
	MADO* myMADO = MADOLoaderObj.getMyMADO();

	const unsigned Nres = myMADO->getNumEquations();

	std::vector<int> JP_0_correct{ 0,1,6,7,10 };
	std::vector<int> JP_1_correct{ 0, 1, 2, 6, 7, 10 };
	std::vector<int> JP_2_correct{ 0,1,2,3,6,7,10 };
	std::vector<int> JP_3_correct{ 1,2,3,6,7,10 };
	std::vector<int> JP_4_correct{ 1,2,4,6,7,10 };
	std::vector<int> JP_5_correct{ 2,3,5,6,7,10 };
	std::vector<int> JP_6_correct{ 10 };
	std::vector<int> JP_7_correct{ 1,2,3,6,7 };
	std::vector<int> JP_8_correct{ 9 };
	std::vector<int> JP_9_correct{ 0,1,2,3,6,7,9,10,11 };
	std::vector<int> JP_10_correct{ 10,13 };
	std::vector<int> JP_11_correct{ 11,12 };
	


	unsigned int** JP = NULL;
	JP = (unsigned int**)malloc(Nres * sizeof(unsigned int*));
	myMADO->getJacobianStruct(JP);

	for (unsigned i = 0; i < JP_0_correct.size(); i++) {
		BOOST_CHECK_EQUAL(JP_0_correct[i], JP[0][i + 1]);
	}

	for (unsigned i = 0; i < JP_1_correct.size(); i++) {
		BOOST_CHECK_EQUAL(JP_1_correct[i], JP[1][i + 1]);
	}

	for (unsigned i = 0; i < JP_2_correct.size(); i++) {
		BOOST_CHECK_EQUAL(JP_2_correct[i], JP[2][i + 1]);
	}

	for (unsigned i = 0; i < JP_3_correct.size(); i++) {
		BOOST_CHECK_EQUAL(JP_3_correct[i], JP[3][i + 1]);
	}

	for (unsigned i = 0; i < JP_4_correct.size(); i++) {
		BOOST_CHECK_EQUAL(JP_4_correct[i], JP[4][i + 1]);
	}

	for (unsigned i = 0; i < JP_5_correct.size(); i++) {
		BOOST_CHECK_EQUAL(JP_5_correct[i], JP[5][i + 1]);
	}

	for (unsigned i = 0; i < JP_6_correct.size(); i++) {
		BOOST_CHECK_EQUAL(JP_6_correct[i], JP[6][i + 1]);
	}

	for (unsigned i = 0; i < JP_7_correct.size(); i++) {
		BOOST_CHECK_EQUAL(JP_7_correct[i], JP[7][i + 1]);
	}

	for (unsigned i = 0; i < JP_8_correct.size(); i++) {
		BOOST_CHECK_EQUAL(JP_8_correct[i], JP[8][i + 1]);
	}

	for (unsigned i = 0; i < JP_9_correct.size(); i++) {
		BOOST_CHECK_EQUAL(JP_9_correct[i], JP[9][i + 1]);
	}

	for (unsigned i = 0; i < JP_10_correct.size(); i++) {
		BOOST_CHECK_EQUAL(JP_10_correct[i], JP[10][i + 1]);
	}

	for (unsigned i = 0; i < JP_11_correct.size(); i++) {
		BOOST_CHECK_EQUAL(JP_11_correct[i], JP[11][i + 1]);
	}


	free(JP); JP = NULL;


	myMADO->destroy();
}

BOOST_AUTO_TEST_CASE(TestMADOLoadergetDiffJacobianStruct) {

	std::string path = PATH_MADO_LOADER_TEST_MODEL;
	MADOLoader MADOLoaderObj(path);
	MADO* myMADO = MADOLoaderObj.getMyMADO();

	const unsigned Nres = myMADO->getNumEquations();

	unsigned int** JPdiff = NULL;
	JPdiff = (unsigned int**)malloc(Nres * sizeof(unsigned int*));
	myMADO->getDiffJacobianStruct(JPdiff);

	std::vector<int> dep_correct{ 1,1,1,1,1,1,1,1,1,0,0,0 };
	std::vector<int> dep_var_correct{ 0,1,2,3,4,5,7,8,6 };

	for (int i = 0; i < Nres; i++) {
		BOOST_CHECK_EQUAL(dep_correct[i], JPdiff[i][0]);
	}

	BOOST_CHECK_EQUAL(dep_var_correct[0], JPdiff[0][1]);
	BOOST_CHECK_EQUAL(dep_var_correct[1], JPdiff[1][1]);
	BOOST_CHECK_EQUAL(dep_var_correct[2], JPdiff[2][1]);
	BOOST_CHECK_EQUAL(dep_var_correct[3], JPdiff[3][1]);
	BOOST_CHECK_EQUAL(dep_var_correct[4], JPdiff[4][1]);
	BOOST_CHECK_EQUAL(dep_var_correct[5], JPdiff[5][1]);
	BOOST_CHECK_EQUAL(dep_var_correct[6], JPdiff[6][1]);
	BOOST_CHECK_EQUAL(dep_var_correct[7], JPdiff[7][1]);
	BOOST_CHECK_EQUAL(dep_var_correct[8], JPdiff[8][1]);

	free(JPdiff);
	JPdiff = NULL;
	myMADO->destroy();
}

BOOST_AUTO_TEST_CASE(TestMADOLoadergetResiduals) {

	std::string path = PATH_MADO_LOADER_TEST_MODEL;
	MADOLoader MADOLoaderObj(path);
	MADO* myMADO = MADOLoaderObj.getMyMADO();

	const unsigned Nres = myMADO->getNumEquations();
	const unsigned NX = myMADO->getNumAllVariables();


	double x1, x2, x3, x4, x5, x6, x7, x8, x9;
	double Fbin;
	double Tw;
	double derX7;

	x1 = 1;
	x2 = 1;
	x3 = 1;
	x4 = 1;
	x5 = 1;
	x6 = 1;
	x7 = 1;
	x8 = 1;
	x9 = 1;
	derX7 = 1;
	Fbin = 1;
	Tw = 1;
	double TwCur = 0.02;
	double FbinCur = 5.784;

	double* x = new double[NX];
	x[0] = x1;
	x[1] = x2;
	x[2] = x3;
	x[3] = x4;
	x[4] = x5;
	x[5] = x6;
	x[6] = x7;
	x[7] = x8;
	x[8] = x9;
	x[9] = derX7;
	x[10] = Fbin;
	x[11] = Tw;
	x[12] = TwCur;
	x[13] = FbinCur;
	x[14] = 0;
	x[15] = 0;
	x[16] = 0;
	x[17] = 0;
	x[18] = 0;
	x[19] = 0;
	x[20] = 0;
	x[21] = 0;
	x[22] = 0;

	double*res = new double[Nres];

	myMADO->getAllResiduals(x, res, NX, Nres);

	double* res_correct = new double[Nres];
	double res1, res2, res3, res4, res5, res6, res7, res8, res9, res10, res11, res12;

	//correct residual values, based on MATLAB calculation
	res1 = -0.0010456073638891456620380184551751; //-1659900 * exp(-133334 / 5483)-1 / 1000;
	res2 = -0.000090985616725424579443895323416514; //-1659900 * exp(-133334 / 5483) - 721170000 * exp(-166666 / 5483);
	res3 = -0.0010062369493511677618429996543714; //3319800 * exp(-133334 / 5483) - 1442340000 * exp(-166666 / 5483) - 2674500000000 * exp(-222220 / 5483) - 1 / 1000;
	res4 = -0.00095796933289217170814776454587803; //721170000 * exp(-166666 / 5483) - 1337250000000 * exp(-222220 / 5483) - 1 / 1000;
	res5 = -0.00090924349432744216518824626351722;//1442340000 * exp(-166666 / 5483) - 1 / 1000;
	res6 = -0.00098995724281464812333907575764173;//4011750000000 * exp(-222220 / 5483) - 1 / 1000;
	res7 = 0.001;//1 / 1000;
	res8 = -0.24486967981282087275764884869936; //7427220225000000 * exp(-222220 / 5483) - 4187055326400 * exp(-166666 / 5483);
	res9 = 1;
	res10 = -0.71783424756321446530822086145625; // (54735202500 * exp(-133334 / 5483)) / 523 + (14270151375000 * exp(-166666 / 5483)) / 523 + (75654918750000000 * exp(-222220 / 5483)) / 523 - 1666637429931326838507 / 2305843009213693952000;
	res11 = 4.784; //598 / 125;
	res12 = -0.98;//-49 / 50;


	res_correct[0] = res1;
	res_correct[1] = res2;
	res_correct[2] = res3;
	res_correct[3] = res4;
	res_correct[4] = res5;
	res_correct[5] = res6;
	res_correct[6] = res7;
	res_correct[7] = res8;
	res_correct[8] = res9;
	res_correct[9] = res10;
	res_correct[10] = res11;
	res_correct[11] = res12;

	// tolerance for the comparison in percentage
	double eps_res = 0.00000000001;


	for (unsigned i = 0; i < Nres; i++) {
		BOOST_CHECK_CLOSE(res_correct[i], res[i], eps_res);
	}




	delete[] res;
	delete[] x;
	delete[] res_correct;
	myMADO->destroy();
}

BOOST_AUTO_TEST_CASE(TestMADOLoadergetJacobianValues) {

	std::string path = PATH_MADO_LOADER_TEST_MODEL;
	MADOLoader MADOLoaderObj(path);
	MADO* myMADO = MADOLoaderObj.getMyMADO();

	const unsigned Nres = myMADO->getNumEquations();
	const unsigned Np = myMADO->getNumParams();
	const unsigned NX = myMADO->getNumAllVariables();
	const unsigned NDerx = myMADO->getNumDiffStates();




	double x1, x2, x3, x4, x5, x6, x7, x8, x9;
	double Fbin;
	double Tw;
	double derX7;

	x1 = 1;
	x2 = 1;
	x3 = 1;
	x4 = 1;
	x5 = 1;
	x6 = 1;
	x7 = 1;
	x8 = 1;
	x9 = 1;
	derX7 = 1;
	Fbin = 1;
	Tw = 1;
	double TwCur = 0.02;
	double FbinCur = 5.784;

	double* x = new double[NX];
	x[0] = x1;
	x[1] = x2;
	x[2] = x3;
	x[3] = x4;
	x[4] = x5;
	x[5] = x6;
	x[6] = x7;
	x[7] = x8;
	x[8] = x9;
	x[9] = derX7;
	x[10] = Fbin;
	x[11] = Tw;
	x[12] = TwCur;
	x[13] = FbinCur;
	x[14] = 0;
	x[15] = 0;
	x[16] = 0;
	x[17] = 0;
	x[18] = 0;
	x[19] = 0;
	x[20] = 0;
	x[21] = 0;
	x[22] = 0;



	int nnz = 56;
	std::vector<unsigned int>rind(nnz);
	std::vector<unsigned int> cind(nnz);
	std::vector<double> values(nnz);


	int options[4];

	options[0] = 0;          /* sparsity pattern by index domains (default) */
	options[1] = 0;          /*                         safe mode (default) */
	options[2] = 0;          /*              not required if options[0] = 0 */
	options[3] = 0;          /*                column compression (default) */



	myMADO->getJacobianValues(x, &nnz, rind.data(), cind.data(), values.data());

	//correct jacobian values for existing variables in each model equation, based on MATLAB calculation
	std::vector<double> values_0_correct{ -0.0010456073638891456620380184551751,  -0.000045607363889145662038018455175125,  -0.0000040454737050196654811635231942321, 0.001, -0.001, -1.0 };
	std::vector<double> values_1_correct{ -0.000045607363889145662038018455175125, -0.0010909856167254245794438953234165, -0.00004537825283627891740587686824139, -0.0000090768672343246341080944449056386, 0, 0, -1.0 };
	std::vector<double> values_2_correct{ 0.000091214727778291324076036910350249, 0.00000045822210573348926428317386747023,  -0.0010974516771294590859190365647216, -0.0000066951714569012511072828282388476, -0.0000029616189113469419220510656244135, 0.001, -0.001, -1.0 };
	std::vector<double> values_3_correct{ 0.00004537825283627891740587686824139, 0.000042030667107828291852235454121966, -0.0010033475857284506255536414141194, 0.0000045365038979168008116727874163742, 0.001, -0.001, -1.0 };
	std::vector<double> values_4_correct{ 0.000090756505672557834811753736482779, 0.000090756505672557834811753736482779, -0.001, 0.000010062787058609937253861843422813, 0.001, -0.001, -1.0 };
	std::vector<double> values_5_correct{ 0.000010042757185351876660924242358271, 0.000010042757185351876660924242358271, -0.001, 0.0000014846688941645034457744028850969, 0.001, -0.001, -1.0 };
	std::vector<double> values_6_correct{ 0.001, -1.0 };
	std::vector<double> values_7_correct{ -0.26346250570720849214512862686005, -0.24486967981282087275764884869936, 0.018592825894387619387479778160692, -0.02646320181796928058774557329465, -0.24486967981282087275764884869936, -1.0 };
	std::vector<double> values_8_correct{ 1.0, -1.0 };
	std::vector<double> values_9_correct{ 0.0028755312127047384430280278382404, 0.0045923996218784842903870881256716, 0.0020789901349616887550020105815161, 0.0003621217257879429076429502940849, -0.00074449373782091478525243031304263, -0.034, -1.0, 0.034, 0.24345468577489400149524434446846 };
	std::vector<double> values_10_correct{ -1.0, 1.0 };
	std::vector<double> values_11_correct{ -1.0, 1.0 };


	// tolerance for the comparison in percentage
	double eps_jac = 1e-10;

	//boost check for each model equation
	for (unsigned i = 0; i < 5; i++) {
		BOOST_CHECK_CLOSE(values_0_correct[i], values[i], eps_jac);
	}

	for (unsigned i = 5; i < 11; i++) {
		BOOST_CHECK_CLOSE(values_1_correct[i - 5], values[i], eps_jac);
	}

	for (unsigned i = 11; i < 18; i++) {
		BOOST_CHECK_CLOSE(values_2_correct[i - 11], values[i], eps_jac);
	}

	for (unsigned i = 18; i < 24; i++) {
		BOOST_CHECK_CLOSE(values_3_correct[i - 18], values[i], eps_jac);
	}

	for (unsigned i = 24; i < 30; i++) {
		BOOST_CHECK_CLOSE(values_4_correct[i - 24], values[i], eps_jac);
	}

	for (unsigned i = 30; i < 36; i++) {
		BOOST_CHECK_CLOSE(values_5_correct[i - 30], values[i], eps_jac);
	}

	for (unsigned i = 36; i < 37; i++) {
		BOOST_CHECK_CLOSE(values_6_correct[i - 36], values[i], eps_jac);
	}

	for (unsigned i = 37; i < 42; i++) {
		BOOST_CHECK_CLOSE(values_7_correct[i - 37], values[i], eps_jac);
	}

	for (unsigned i = 42; i < 43; i++) {
		BOOST_CHECK_CLOSE(values_8_correct[i - 42], values[i], eps_jac);
	}

	for (unsigned i = 43; i < 52; i++) {
		BOOST_CHECK_CLOSE(values_9_correct[i - 43], values[i], eps_jac);
	}

	for (unsigned i = 52; i < 53; i++) {
		BOOST_CHECK_CLOSE(values_10_correct[i - 52], values[i], eps_jac);
	}

	for (unsigned i = 54; i < 56; i++) {
		BOOST_CHECK_CLOSE(values_11_correct[i - 54], values[i], eps_jac);
	}

	delete[] x;

	myMADO->destroy();
}

BOOST_AUTO_TEST_CASE(TestMADOLoadergetDiffJacobianValues) {

	std::string path = PATH_MADO_LOADER_TEST_MODEL;
	MADOLoader MADOLoaderObj(path);
	MADO* myMADO = MADOLoaderObj.getMyMADO();

	const unsigned Nres = myMADO->getNumEquations();
	const unsigned Np = myMADO->getNumParams();
	const unsigned NX = myMADO->getNumAllVariables();
	const unsigned NDerx = myMADO->getNumDiffStates();

	double x1, x2, x3, x4, x5, x6, x7, x8, x9;
	double Fbin;
	double Tw;
	double derX7;

	x1 = 1;
	x2 = 1;
	x3 = 1;
	x4 = 1;
	x5 = 1;
	x6 = 1;
	x7 = 1;
	x8 = 1;
	x9 = 1;
	derX7 = 1;
	Fbin = 1;
	Tw = 1;
	double TwCur = 0.02;
	double FbinCur = 5.784;

	double* x = new double[NX];
	x[0] = x1;
	x[1] = x2;
	x[2] = x3;
	x[3] = x4;
	x[4] = x5;
	x[5] = x6;
	x[6] = x7;
	x[7] = x8;
	x[8] = x9;
	x[9] = derX7;
	x[10] = Fbin;
	x[11] = Tw;
	x[12] = FbinCur;
	x[13] = TwCur;
	x[14] = 0;
	x[15] = 0;
	x[16] = 0;
	x[17] = 0;
	x[18] = 0;
	x[19] = 0;
	x[20] = 0;
	x[21] = 0;
	x[22] = 0;



	int nnz = 9;
	std::vector<unsigned int>rind(nnz);
	std::vector<unsigned int> cind(nnz);
	std::vector<double> values(nnz);


	int options[4];

	options[0] = 0;          /* sparsity pattern by index domains (default) */
	options[1] = 0;          /*                         safe mode (default) */
	options[2] = 0;          /*              not required if options[0] = 0 */
	options[3] = 0;          /*                column compression (default) */




	myMADO->getDiffJacobianValues(x, &nnz, rind.data(), cind.data(), values.data());

	std::vector<double> value_correct{ -1,-1,-1,-1,-1,-1,-1,-1,-1 };

	// tolerance for the comparison in percentage
	double eps_diff_jac = 0.00000000001;

	for (int i = 0; i < 9; i++) {
		BOOST_CHECK_CLOSE(value_correct[i], values[i], eps_diff_jac);
	}



	delete[] x;
	
	myMADO->destroy();
}

BOOST_AUTO_TEST_CASE(TestMADOLoadergetVariableNames) {

	std::string path = PATH_MADO_LOADER_TEST_MODEL;
	MADOLoader MADOLoaderObj(path);
	MADO* myMADO = MADOLoaderObj.getMyMADO();

	const unsigned numVars = myMADO->getNumAllVariables();
	const unsigned maxNameLength = myMADO->getMaxVarNameLength();

	// This here is necessary and cannot be done inside MADO. 
	// Otherwise problems with freeing the allocated memory:
	char ** names = (char **)malloc(23 * sizeof(char*));
	for (int i = 0; i < numVars; ++i) {
		names[i] = (char *)malloc(maxNameLength * sizeof(char));
	}	
	

	myMADO->getVariableNames(names);

	char* varnames_correct[23];
	varnames_correct[0] = "X1";
	varnames_correct[1] = "X2";
	varnames_correct[2] = "X3";
	varnames_correct[3] = "X4";
	varnames_correct[4] = "X5";
	varnames_correct[5] = "X6";
	varnames_correct[6] = "X7";
	varnames_correct[7] = "X8";
	varnames_correct[8] = "X9";
	varnames_correct[9] = "derX7";
	varnames_correct[10] = "Fbin";
	varnames_correct[11] = "Tw";
	varnames_correct[12] = "TwCur";
	varnames_correct[13] = "FbinCur";
	varnames_correct[14] = "der_X1";
	varnames_correct[15] = "der_X2";
	varnames_correct[16] = "der_X3";
	varnames_correct[17] = "der_X4";
	varnames_correct[18] = "der_X5";
	varnames_correct[19] = "der_X6";
	varnames_correct[20] = "der_X7";
	varnames_correct[21] = "der_X8";
	varnames_correct[22] = "der_X9";


	for (unsigned i = 0; i < 23; i++) {
		BOOST_CHECK_EQUAL(varnames_correct[i], names[i]);
	}


	for (unsigned int i = 0; i < 23; i++) {
		free(names[i]);
	}
	free(names);

	myMADO->destroy();
}

BOOST_AUTO_TEST_SUITE_END(TestMADOLoader);