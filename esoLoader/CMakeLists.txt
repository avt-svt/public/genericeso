#find_package(Boost COMPONENTS filesystem system REQUIRED)

if(WIN32)
    set(DLL_SUFFIX .dll)
else(WIN32)
    if (APPLE)
        set(DLL_SUFFIX .dylib)
    else (APPLE)
        set(DLL_SUFFIX .so)
    endif (APPLE)
endif(WIN32)

##### Jade-EsoLoader

add_library(JadeLoader src/DyosJadeLoader.cpp inc/DyosJadeLoader.hpp)


target_link_libraries(JadeLoader EsoCommon ${CMAKE_DL_LIBS} CppUtils)
#Linking to debug versions can cause problems if running with mixed runtime libraries (debug and release).
#For that reason usage of the release version is recommended (especially debugging is not possible without
#source code). For that reason the debug version is deactivated if not found on the given relative path.
target_include_directories(JadeLoader PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/inc)

set_target_properties(JadeLoader
    PROPERTIES
    FOLDER  EsoLoader
)




##### MADO-EsoLoader
if(BUILD_WITH_MADO)
set (MADO_LOADER_SRC
    ${CMAKE_CURRENT_SOURCE_DIR}/src/DyosMADOLoader.cpp
    )
set (MADO_LOADER_INC
    ${CMAKE_CURRENT_SOURCE_DIR}/inc/DyosMADOLoader.hpp
    )

add_library(MADOLoader ${MADO_LOADER_SRC} ${MADO_LOADER_INC})

add_dependencies(MADOLoader EsoCommon)
target_link_libraries(MADOLoader EsoCommon CppUtils)
target_include_directories(MADOLoader PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/inc)
set_target_properties(MADOLoader
    PROPERTIES
    FOLDER  EsoLoader
)


#####Test MADOLoader  #lahe

#    add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/testMADO/mado_model")
#    set(PATH_MADO_LOADER_TEST_MODEL "${CMAKE_BINARY_DIR}/input/MADO")


    set (TEST_MADOLOADER_SOURCE
        ${CMAKE_CURRENT_SOURCE_DIR}/testMADO/MADOLoaderTest.cpp
        )

    add_executable(TestMADOLoader ${TEST_MADOLOADER_SOURCE})
    #add_dependencies(TestMADOLoader Model)
    #    add_dependencies(TestMADOLoader MADOModel)
	
	
	target_link_libraries(TestMADOLoader
         Boost::unit_test_framework
	       MADOLoader
    )
    set_target_properties(TestMADOLoader
        PROPERTIES
        FOLDER  Tests
    )
   
    add_test(NAME TestMADOLoader COMMAND TestMADOLoader)

# For dynamic linking:
#copy adolc.dll to solution
#add_custom_command(TARGET TestMADOLoader POST_BUILD
#		COMMAND ${CMAKE_COMMAND} -E copy_if_different  # which executes "cmake - E copy_if_different..."
#        "${CMAKE_CURRENT_SOURCE_DIR}/precompiled/win64/bin/adolc.dll"      # <--this is in-file
#        "${CMAKE_CURRENT_BINARY_DIR}")                 # <--this is out-file path




endif(BUILD_WITH_MADO)

##### Test JadeLoader
# @todo: Needs to be updated.
#add_subdirectory(testJade)
##### FMU-EsoLoader
if(BUILD_WITH_FMU)
    add_library(FmiLoader STATIC  src/DyosFmiLoader.cpp  inc/DyosFmiLoader.hpp)
    
    target_link_libraries(FmiLoader
        PUBLIC
        fmilib::fmilib
        Boost::filesystem
        Boost::system
        EsoCommon::EsoCommon
     )

    target_include_directories(FmiLoader PUBLIC 
        ${CMAKE_CURRENT_SOURCE_DIR}/inc
              ${CMAKE_BINARY_DIR}/configureHeaders
	  )

    set_target_properties(FmiLoader
        PROPERTIES
        FOLDER  EsoLoader
    )

    ###### test FMI loader
    # add subdirectory to include fmu model generation
    add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/testFMILib/TrivialModelFmu")
    set(PATH_FMI_LOADER_TEST_FMI "${CMAKE_BINARY_DIR}/input/FMI/TrivialModel")
    configure_file("${CMAKE_CURRENT_SOURCE_DIR}/testFMILib/FMILoaderTestConfig.hpp.cmake"
        "${CMAKE_BINARY_DIR}/configureHeaders/FMILoaderTestConfig.hpp"
    )

    set (TEST_FMILOADER_SOURCE
        ${CMAKE_CURRENT_SOURCE_DIR}/testFMILib/DyosFmiLoaderTest.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/testFMILib/FMILoaderTestsuite.hpp
        )

    add_executable(TestFMILoader ${TEST_FMILOADER_SOURCE})
    add_dependencies(TestFMILoader TrivialModel)
    target_include_directories(TestFMILoader PRIVATE ${CMAKE_BINARY_DIR}/configureHeaders)
    target_link_libraries(TestFMILoader
         Boost::unit_test_framework
	       FmiLoader
    )
    set_target_properties(TestFMILoader
        PROPERTIES
        FOLDER  Tests
    )
   
    add_test(NAME TestFMILoader COMMAND TestFMILoader)
endif(BUILD_WITH_FMU)

########################

if(BUILD_JADE2)
add_library(Jade2Loader src/Jade2Loader.cpp inc/Jade2Loader.hpp)
add_library(Jade2Loader::Jade2Loader ALIAS Jade2Loader)


target_include_directories(Jade2Loader PUBLIC
	${CMAKE_CURRENT_SOURCE_DIR}/inc
    )

target_link_libraries(Jade2Loader 
	PUBLIC
	EsoCommon 
	${CMAKE_DL_LIBS} 
	CppUtils
	Eigen3)

set_target_properties(Jade2Loader
    PROPERTIES
    FOLDER  EsoLoader
)
endif(BUILD_JADE2)

###### DyosUser Version 

install(TARGETS JadeLoader 
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
)

if(BUILD_JADE2)
install(TARGETS Jade2Loader 
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
)
endif(BUILD_JADE2)

if(BUILD_WITH_FMU)
  install(TARGETS  FmiLoader
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
  )
endif(BUILD_WITH_FMU)

if(BUILD_WITH_MADO)
install(TARGETS MADOLoader 
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
)
endif(BUILD_WITH_MADO)
