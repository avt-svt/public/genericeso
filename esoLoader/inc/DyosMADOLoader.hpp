/**
* @file MADOLoader.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* Header with declaration of class MADOLoader                          \n
* =====================================================================\n
* @author Lars Henrichfreise
* @date 15.12.2019
*/
#pragma once

#include <string>
#include <vector>
#include "jsp.hpp"
#include "Loader.hpp"
#include "mado.h"

//! class that has to be defined differently whether the operating system is Windows or Linux
//class LibraryWrapper;

/**
* @class MADOLoader
*
* @brief Class that encapsulates all commands from MADOGenericEso to the model library
*
* The class MADOLoader links at runtime an MADO-model library and redirects the calls to its
* member functions to the functions defined in the 'model'.dll.
* This class treats DA-systems given by M*der_x = f(x,p). All member functions treat the residual
* system given by
*
* Res(x,p,der_x) = M*der_x - f(x,p),
*
* where x are the states and p the parameters.
* In the following equations we refer to z, defined as z = [x p der_x]^T.
*/
class MADOLoader : public utils::Loader {
public:
	/**
  * @brief definition of function pointer used to load function getMADO from MADO model
  */
	using t_allocModel = MADO * (void);

	t_allocModel getMyMADO;

	MADOLoader(const std::string& libname);
	virtual ~MADOLoader();
private:

	//! Handle for shared object
	std::string m_Libname; // name of Library


	t_allocModel *m_allocModel = nullptr;

};

//  /**
//  * @brief definition of function pointer used to load Get_num functions from MADO model
//  */
//  using t_Get_num = void(int& num);
//  /**
//* @brief definition of function pointer used to load Get_num functions from MADO model
//*/
//  using t_getVariableNames = void(char** names);
//  /**
//  * @brief definition of function pointer used to load overloaded function getAllResiduals with 4 inputs from MADO model
//  */
//  using t_evalAll_4in = void(double* x, double* res, unsigned numX, unsigned numRes);
//
//  /**
// * @brief definition of function pointer used to load overloaded function getAllResiduals with 2 inputs from MADO model
// */
//  //using t_getAllResiduals = void (double* x, double* res);
//  using t_evalAll = void(double* x, double* res);
//
//  /**
//* @brief definition of function pointer used to load function getJacobianStruct from MADO model
//*/
// using t_evalJacobianPattern = void(unsigned int **& JP);
//
//  /**
//* @brief definition of function pointer used to load function getJacobianValues from MADO model
//*/
//  using t_evalJacobianValues = void(double* x, int& num_nonZeros, unsigned int*& rind, unsigned int*& cind, double*& values);
//
//
//  /**
//* @brief definition of function pointer used to load function getDenseJacobianValues from MADO model
//*/
//  using t_evalAllTangents = void(double* x, double** J);
//
//  /**
//* @brief definition of function pointer used to load function destroy from MADO model
//*/
//  using t_freeModel = void();
//
//  /**
//* @brief definition of function pointer used to load function getMADO from MADO model
//*/
//  using t_allocModel = MADO*(void);
//
//
//
//
//
//  int getNumEquations() const;
//  int getNumVariables() const;
//  char** getVariableNames() const;
//
//  void getAllResiduals(double* x, double* res, unsigned numX, unsigned numRes) const;
//  void getAllResiduals(double* x, double* res) const;
//
//  t_allocModel getMyMADO;
//  t_freeModel destroy;
//  t_evalJacobianPattern getJacobianStruct;
//  t_evalJacobianValues getJacobianValues;
//  t_evalAllTangents getDenseJacobianValues;
//
//
//  int Get_num_pars() const;
//  int getNumDiffStates() const;
//  int getNumAlgStates() const;
//  int getNumSwitching() const;
//  void getDiffJacobianStruct(unsigned int **& JP);
//  //void getJacobianStruct(unsigned int **& JP);
//  void getDiffJacobianValues(double* x, int &num_nonZeros, unsigned int *& rind, unsigned int * & cind, double* &values);
//  //void getJacobianValues(double* x, int& num_nonZeros, unsigned int*& rind, unsigned int*& cind, double*& values);
//  //void getDenseJacobianValues(double* x, double** J);
//
//
//  MADOLoader(const std::string& libname);
//  virtual ~MADOLoader();
//
//private:
//	//! Handle for shared object
//	std::string m_Libname; // name of Library
//
//	 //! function pointer to the function destroy() form the MADO model
//	t_allocModel *m_allocModel = nullptr;
//   //! function pointer to the function destroy() form the MADO model
//	t_freeModel *m_freeModel = nullptr;
//
//
//  //! function pointer to the function get_num_eqns() form the MADO model
//  t_Get_num *m_getNumEquations = nullptr;
//  //! function pointer to the function get_num_vars() form the MADO model
//  t_Get_num *m_getNumVariables = nullptr;
//  //! function pointer to the function getVariableNames() form the MADO model
//  t_getVariableNames* m_getVariableNames = nullptr;
//
//  //! function pointer to the function getJacobianStruct() form the MADO model
//  t_evalJacobianPattern  *m_evalJacobianPattern = nullptr;
//  //! function pointer to the function getDiffJacobianValues() form the MADO model
//  t_evalJacobianValues *m_evalJacobianValues = nullptr;
//
//  //! function pointer to the function getAllResiduals() form the MADO model
//  t_evalAll_4in *m_evalAll_with_input_check = nullptr;
//  t_evalAll *m_evalAll = nullptr;
//
//  //! function pointer to the function getDenseJacobianValues() form the MADO model
//  t_evalAllTangents	  *m_evalAllTangents = nullptr;
//
//
//
//
//  //! function pointer to the function get_num_pars() form the MADO model
//  t_Get_num *m_Get_num_pars = nullptr;
//	//! function pointer to the function getNumDiffStates() form the MADO model
//  t_Get_num *m_Get_num_DiffStates = nullptr;
//  //! function pointer to the function getNumAlgStates() form the MADO model
//  t_Get_num *m_Get_num_AlgStates = nullptr;
//  //! function pointer to the function getNumSwitching() form the MADO model
//  t_Get_num *m_Get_num_Switching = nullptr;
//
//
//  //! function pointer to the function getDiffJacobianStruct() form the MADO model
//  t_evalJacobianPattern  *m_get_Diff_Jacobian_Struct = nullptr;
//  //! function pointer to the function getJacobianStruct() form the MADO model
//  t_evalJacobianValues *m_get_Diff_Jacobian_Values = nullptr;
//  //! function pointer to the function getJacobianValues() form the MADO model
//
//
//
//
//};
