/**
	model.h

	@author Adrian Caspari, Jan C. Schulze, Johannes M. M. Faust
	@version 1.0 21.11.2019
*/
#pragma once


#ifdef MODELLIB_EXPORTS
#define MODELLIB_EXPORTS __declspec(dllexport)
#else
#define MODELLIB_EXPORTS __declspec(dllimport)
#endif


/* class MADO;

Interface class to model.
Abstract interface in order to make library compiler/binary independent.

*/
class  MADO {

public:

	virtual void getAllResiduals(double* x, double* res, unsigned numX, unsigned numRes) = 0;
	virtual void getDifferentialResiduals(double* x, double* res, unsigned numX, unsigned numRes) = 0;
	virtual void getAlgebraicResiduals(double* x, double* res, unsigned numX, unsigned numRes) = 0;
	virtual void getDiffJacobianValues(double* x, int* num_nonZeros, unsigned int* rind, unsigned int* cind, double* values) = 0;
	virtual void getJacobianValues(double* x, int* num_nonZeros, unsigned int* rind, unsigned int* cind, double* values) = 0;
	virtual void getJacobianStruct(unsigned int** JP) = 0;
	virtual void getDiffJacobianStruct(unsigned int** JP) = 0;
	virtual void getInitialValues(double* x, unsigned numX) = 0;
	virtual void destroy() = 0;

	virtual unsigned int getNumVariables() = 0;
	virtual unsigned int getNumAllVariables() = 0;
	virtual unsigned int getNumEquations() = 0;
	virtual unsigned int getNumSwitching() = 0;
	virtual unsigned int getNumDiffStates() = 0;
	virtual unsigned int getNumAlgStates() = 0;
	virtual unsigned int getNumParams() = 0;
	virtual unsigned int getMaxVarNameLength() = 0;

	virtual void getVariableNames(char** varnames) = 0;

};


/*MADO* getMADO();

This function returns a pointer to a new created model object.
The memory has to be deallocated afterwars by delete.

*/
extern "C" MODELLIB_EXPORTS MADO* getMADO();