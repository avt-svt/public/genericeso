/**
* @file JadeLoader.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* Header with declaration of class ACSAMMMLoader                       \n
* =====================================================================\n
* @author Moritz Schmitz
* @date 15.9.2011
*/
#pragma once

#include <string>
#include <vector>
#include "jsp.hpp"
#include "Loader.hpp"

//! class that has to be defined differently whether the operating system is Windows or Linux
//class LibraryWrapper;

/**
* @class JadeLoader
*
* @brief Class that encapsulates all commands from JadeGenericEso to the model library
*
* The class JadeLoader links at runtime an Jade-model library and redirects the calls to its
* member functions to the functions defined in the 'model'.dll.
* This class treats DA-systems given by M*der_x = f(x,p). All member functions treat the residual
* system given by
*
* Res(x,p,der_x) = M*der_x - f(x,p),
*
* where x are the states and p the parameters.
* In the following equations we refer to z, defined as z = [x p der_x]^T.
*/
class JadeLoader : public utils::Loader {
public:
  /**
  * @typedef t_Get_num
  * @brief definition of function pointer used to load Get_num functions from Jade model
  */
  typedef void t_Get_num(int& num);

  /**
  * @typedef t_GetNames
  * @brief definition of function pointer used to load get_var_names and get_par_names from Jade model
  */
  typedef void t_GetNames(std::vector<std::string> * names);

  /**
  * @typedef t_Init
  * @brief definition of function pointer used to load function init from Jade model
  */
  typedef void t_Init(double * x, double * p, int &n_x, int& n_p, int& n_c);

  /**
  * @typedef t_Res
  * @brief definition of function pointer used to load function res from Jade model
  */
  typedef void t_Res(double* yy, double* der_x, double* x, double* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);

  typedef void t_Res_cond(double* yy, double* der_x, double* x, double* p, int& n_x, int& n_p, int& n_c);

  typedef void t_Eval_cond(int* yy, double* der_x, double* x, double* p, int& n_x, int& n_p, int& n_c);

  /**
  * @typedef t_Res_block
  * @brief definition of function pointer used to load function res_block from Jade model
  */
  typedef void t_Res_block(double * yy, double * der_x, double * x, double * p,
			   int* condit, int* lock, int* prev, int &n_x, int &n_p, int &n_c,
			   int &n_yy_ind, int* yy_ind);

  /**
  * @typedef t_T1_res_block
  * @brief definition of function pointer used to load function t1_res_block from Jade model
  */
  typedef void t_T1_res_block(double* yy, double* t1_yy, double* der_x, double* t1_der_x, double* x,
			      double* t1_x, double* p, double* t1_p, int* condit, int* lock, int* prev,
			      int& n_x, int& n_p, int& n_c, int& n_yy_ind, int* yy_ind);

  /**
  * @typedef t_A1_res
  * @brief definition of function pointer used to load function a1_res from Jade model
  */
  typedef void t_A1_res(int bmode_1, double* yy, double* a1_yy, double* der_x, double* a1_der_x,
			double* x, double* a1_x, double* p, double* a1_p,
			int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);

  /**
  * @typedef t_T1_res
  * @brief definition of function pointer used to load function t1_res from Jade model
  */
  typedef void t_T1_res(double* yy, double* t1_yy, double* der_x, double* t1_der_x, double* x,
			double* t1_x, double* p, double* t1_p,
			int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);

  /**
  * @typedef t_T2_a1_res
  * @brief definition of function pointer used to load function t2_a1_res from Jade model
  */
  typedef void t_T2_a1_res(int bmode_1, double* yy, double* t2_yy, double* a1_yy, double* t2_a1_yy,
			   double* der_x, double* t2_der_x, double* a1_der_x, double* t2_a1_der_x, double* x, double* t2_x,
			   double* a1_x, double* t2_a1_x, double* p, double* t2_p, double* a1_p, double* t2_a1_p,
			   int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);

  /**
  * @typedef t_Jsp_res
  * @brief definition of function pointer used to load function jsp_res from Jade model
  */
  typedef void t_Jsp_res(jsp* yy, jsp* der_x, jsp* x, jsp* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);

  int Get_num_eqns();
  int Get_num_vars();
  int Get_num_pars();
  int Get_num_cond();
  void GetVarNames(std::vector<std::string> * names);
  void GetParNames(std::vector<std::string> * names);
  void Init(double * x, double * p, int &n_x, int& n_p, int& n_c);
  void Res(double* yy, double* der_x, double* x, double* p, int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);
  void Res_block(double * yy, double * der_x, double * x, double * p,
		 int* condit, int* lock, int* prev, int &n_x, int &n_p, int &n_c,
                 int &n_yy_ind, int* yy_ind);
  void Res_cond(double* yy, double* der_x, double* x, double* p,
		int &n_x, int &n_p, int &n_c);
  void Eval_cond(int* yy, double* der_x, double* x, double* p,
		 int &n_x, int &n_p, int &n_c);
  void T1_res_block(double* yy, double* t1_yy, double* der_x, double* t1_der_x, double* x,
                    double* t1_x, double* p, double* t1_p,
		    int* condit, int* lock, int* prev, int& n_x, int& n_p, int &n_c,
                    int& n_yy_ind, int* yy_ind);
  void A1_res(int bmode_1, double* yy, double* a1_yy, double* der_x, double* a1_der_x,
              double* x, double* a1_x, double* p, double* a1_p,
	      int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);
  void T1_res(double* yy, double* t1_yy, double* der_x, double* t1_der_x, double* x,
              double* t1_x, double* p, double* t1_p,
	      int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);
  void T2_a1_res(int bmode_1, double* yy, double* t2_yy, double* a1_yy, double* t2_a1_yy,
                 double* der_x, double* t2_der_x, double* a1_der_x, double* t2_a1_der_x,
                 double* x, double* t2_x, double* a1_x, double* t2_a1_x, double* p,
                 double* t2_p, double* a1_p, double* t2_a1_p,
		 int* condit, int* lock, int* prev, int& n_x, int& n_p, int& n_c);
  void Jsp_res(jsp* yy, jsp* der_x, jsp* x, jsp* p, int* condit, int* lock, int* prev,
	       int& n_x, int& n_p, int& n_c);
  JadeLoader(const std::string& libname);
  virtual ~JadeLoader();

protected:
  //! function pointer to the function get_num_eqns() form the Jade model
  t_Get_num *m_Get_num_eqns;
  //! function pointer to the function get_num_vars() form the Jade model
  t_Get_num *m_Get_num_vars;
  //! function pointer to the function get_num_pars() form the Jade model
  t_Get_num *m_Get_num_pars;
  //! function pointer to the function get_num_cond() from the Jade model
  t_Get_num *m_Get_num_cond;
  //! function pointer to the function get_var_names() form the Jade model
  t_GetNames *m_GetVarNames;
  //! function pointer to the function get_par_names() form the Jade model
  t_GetNames *m_GetParNames;
  //! function pointer to the function init() form the Jade model
  t_Init *m_Init;
  //! function pointer to the function res() form the Jade model
  t_Res *m_Res;
  //! function pointer to the function res_cond() from the Jade model
  t_Res_cond *m_Res_cond;
  //! function pointer to the function eval_cond() from the Jade model
  t_Eval_cond *m_Eval_cond;
  //! function pointer to the function res_block() form the Jade model
  t_Res_block *m_Res_block;
  //! function pointer to the function t1_res_block() form the Jade model
  t_T1_res_block *m_T1_res_block;
  //! function pointer to the function a1_res() form the Jade model
  t_A1_res *m_A1_res;
  //! function pointer to the function t1_res() form the Jade model
  t_T1_res *m_T1_res;
  //! function pointer to the function t2_a1_res() form the Jade model
  t_T2_a1_res *m_T2_a1_res;
  //! function pointer to the function jsp_res() form the Jade model
  t_Jsp_res *m_Jsp_res;

  //! Handle for shared object
  std::string m_Libname; // name of Library
};
