#ifndef JADE2_LOADER_HPP
#define JADE2_LOADER_HPP


#include "Loader.hpp"
#include "Eigen/Sparse"
#include <string>


/**
* @class Jade2Loader
*
* @brief Class that encapsulates all commands from ACSAMMMGenericEso to the model library
*
* The class Jade2Loader links at runtime an JADE2-model library and redirects the calls to its
* member functions to the functions defined in the 'model'.dll.
* This class treats DA-systems given by M*der_x = f(x,p). All member functions treat the residual
* system given by
*
* Res(x,p,der_x) = M*der_x - f(x,p),
*
* where x are the states and p the parameters.
* In the following equations we refer to z, defined as z = [x p der_x]^T.
*/
class Jade2Loader : public utils::Loader {
public:

  using t_getNumVariables = void(int, int&);
  using t_getNumEquations = void(int, int&);
  using t_allocModel = void*();
  using t_freeModel = void(void*);
  using t_evalJacobianPattern = void(void*, int, int, Eigen::SparseMatrix<double>&);
  using t_evalJacobianValues = void(void*, int, int, Eigen::SparseMatrix<double>&);
  using t_getVariableNames = void(void*, std::vector<std::string>&);
  using t_getInitialValues = void(void*, int, Eigen::VectorXd&);
  using t_getInitialValue = void(void*, int, int, double&);
  using t_setVariable = void(void*, int, int, const double);
  using t_getVariable = void(void*, int, int, double&);
  using t_setVariables = void(void*, int, const double*);
  using t_getVariables = void(void*, int, double*);
  using t_evalAll = void(void*, int, double*);
  using t_eval = void(void*, int, int, double&);
  using t_variableIndexOffset = void(void*, int, int&);
  using t_equationIndexOffset = void(void*, int, int&);
  using t_setAdjoints = void(void*, const int, Eigen::VectorXd&);
  using t_evalAllSecondOrderAdjoints = void(void*, const int, double*, double*, Eigen::VectorXd&);
  using t_evalAllTangents = void(void*, const double*, double*);

  int getNumVariables(int) const;
  int getNumEquations(int) const;
  void getVariableNames(void*, std::vector<std::string>&) const;
  double getInitialValue(void*, int, int) const;
  void getInitialValues(void*, int, Eigen::VectorXd&) const;
  double getVariable(void*, int, int) const;
  void getVariables(void*, int, double*) const;
  double eval(void*, int, int) const;
  void evalAll(void*, int, double*) const;
  int variableIndexOffset(void*, int) const;  
  int equationIndexOffset(void*, int) const;
 
  t_allocModel allocModel;
  t_freeModel freeModel;
  t_evalJacobianPattern evalJacobianPattern;
  t_evalJacobianPattern evalJacobianValues;
  t_setVariable setVariable;
  t_setVariables setVariables;
  t_setAdjoints setAdjoints;
  t_evalAllSecondOrderAdjoints evalAllSecondOrderAdjoints;
  t_evalAllTangents evalAllTangents;


  Jade2Loader(const std::string& libname);
private:
  std::string m_Libname;

  t_allocModel *m_allocModel = nullptr;
  t_freeModel *m_freeModel = nullptr;
  t_evalJacobianPattern *m_evalJacobianPattern = nullptr;
  t_evalJacobianValues *m_evalJacobianValues = nullptr;
  t_getInitialValues *m_getInitialValues = nullptr;
  t_getInitialValue *m_getInitialValue = nullptr;
  t_getVariable *m_getVariable = nullptr;
  t_setVariable *m_setVariable = nullptr;
  t_getVariables *m_getVariables = nullptr;
  t_setVariables *m_setVariables = nullptr;
  t_evalAll *m_evalAll = nullptr;
  t_eval *m_eval = nullptr;
  t_setAdjoints *m_setAdjoints = nullptr;
  t_evalAllSecondOrderAdjoints *m_evalAllSecondOrderAdjoints = nullptr;
  t_evalAllTangents* m_evalAllTangents = nullptr;

  //! function pointer to the function get_num_vars() form the JADE2 model
  t_getNumVariables* m_getNumVariables = nullptr;

  t_getNumEquations* m_getNumEquations = nullptr;

  t_getVariableNames* m_getVariableNames = nullptr;

  t_variableIndexOffset* m_variableIndexOffset = nullptr;
  t_equationIndexOffset* m_equationIndexOffset = nullptr;
};

#endif
