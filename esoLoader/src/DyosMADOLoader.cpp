/**
* @file MADOLoader.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen           \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* Member definitions of class MADOLoader                               \n
* =====================================================================\n
* @author Lars Henrichfreise
* @date 15.12.2019
*/

#include "DyosMADOLoader.hpp"
#include <iostream>
#include <cstdlib>

/**
* @brief loads a function from a dll
*
* @param[out] ptPtFunction pointer of a pointer to the function whose call is redirected to the
*            function of same type with name 'functionName' in the library 'library'
* @param[in] library is a library where the address of a function with name 'functionName' can be
*            retrieved
* @param[in] functionName of the function who should be addressed when the function where 'ptPtFunction'
*            points to is called
*/
/*
template<typename t_function> void myLoadFunction(t_function **ptPtFunction, HINSTANCE& library,
                                                  const char* functionName)
{
  *(void**) (ptPtFunction) = (GetProcAddress(library, functionName));
  const DWORD myError = GetLastError();
  if(myError){
    std::cerr << "Could not load function " << functionName << ", error code is: " << myError
      << std::endl;
    exit(1);
  }
  }*/

/**
* @brief constructor
*
* The constructor takes as argument the name (without the suffix ".dll") of a dynamic library
* created by Jade. The library contains all the necessary information about the dynamic model
* originally coded in Modelica. Furthermore all first-order derivatives in tangent-linear and
* adjoint mode as well as second-order derivatives in tangent over tangent and tangent over adjoint
* mode are available within this dynamic library corresponding to the Jade standard.
*
* @param[in] libname name of the dynamic model library to be loaded
*/
MADOLoader::MADOLoader(const std::string& libname) :

	Loader(libname),
	m_Libname(libname)
{
	if (!isLoaded()) {
		std::cerr << "Could not find " << m_Libname.c_str() << std::endl;
		std::cerr << "Opening dynamic link library failed.\n";
		exit(1);
	}


	loadFcn(m_allocModel, "getMADO");
	//MADO* myMADO = m_allocModel();
	//int test = myMADO->getNumEquations();


	// not needed from here on!

 //loadFcn(m_freeModel, "destroy");
 //
 // loadFcn(m_getNumEquations, "getNumEquations");
 // loadFcn(m_getNumVariables, "getNumVariables");
 // loadFcn(m_getVariableNames, "getVariableNames");
 // 
 // loadFcn(m_evalJacobianPattern, "getJacobianStruct");
 // loadFcn(m_evalJacobianValues, "getJacobianValues");
 //
 // //no method for get initial values, get/set variable in mado
 //
 // loadFcn(m_evalAll, "getAllResiduals");
 // loadFcn(m_evalAll_with_input_check, "getAllResiduals");
 // //no method for single res in mado 
 // loadFcn(m_evalAllTangents, "getDenseJacobianValues");
 //
 // //not in jade 2
 // loadFcn(m_Get_num_pars, "getNumParams");
 // loadFcn(m_Get_num_DiffStates, "getNumDiffStates");
 // loadFcn(m_Get_num_AlgStates, "getNumAlgStates");
 // loadFcn(m_Get_num_Switching, "getNumSwitching");
 // loadFcn(m_get_Diff_Jacobian_Struct, "getDiffJacobianStruct");
 // loadFcn(m_get_Diff_Jacobian_Values, "getDiffJacobianValues");



}


  /**
* @brief destructor
*/
  MADOLoader::~MADOLoader()
  {
  }
//  /**
//* @brief Getter for the number of equations (differential and algebraic) of the model
//*
//* @return number of equations
//*/
//  int MADOLoader::getNumEquations() const
//  {
//	  int ne;
//	  m_getNumEquations(ne);
//	  return ne;
//  }
//
//  /**
//  * @brief Getter for the number of states (differential and algebraic) of the model
//  *
//  * @return number of states
//  */
//  int MADOLoader::getNumVariables() const
//  {
//	  int nv;
//	  m_getNumVariables(nv);
//	  return nv;
//  }

  MADO* MADOLoader::getMyMADO() {
	  return m_allocModel();
  }

//  /**
//* @brief * The function deletes the model
//*/
//  void MADOLoader::destroy()
//  {
//	  return m_freeModel();
//  }
//
//  /**
//* @brief Evaluates the structure of the jacobian matrix
//*
//* @param[out] JP contains the structure of the jacobian matrix
//*/
//  void MADOLoader::getJacobianStruct(unsigned int **& JP)
//  {
//	  return m_evalJacobianPattern(JP);
//  }
//
//  /**
//* @brief Evaluates the jacobian matrix
//*
//* The function evaluates d(Res(x))/d(x)
//*
//* @param[out] values contains the values of the jacobian matrix
//* @param[in] x contains the values of the states (including der_x and p)
//* @param[in] num_nonZeros
//* @param[in] rind
//* @param[in] cind
//*/
//void MADOLoader::getJacobianValues(double* x, int& num_nonZeros, unsigned int*& rind, unsigned int*& cind, double*& values)
//{
//	return m_evalJacobianValues(x, num_nonZeros, rind, cind, values);
//}
//
//
////get variable names...
//char** MADOLoader::getVariableNames() const
//{
//	char** names;
//	 m_getVariableNames(names);
//	return names;
//}
//
//
//**
//* @brief Evaluates the residual of the whole equation system
//*
//* The function evaluates Res(x) = f(x) - M*der_x .
//* with check if muX and numRes equals getNumVariables and getNumEquations respectively
//*
//* @param[out] res of size numRes contains the residuals of the equation system
//* @param[in] x of size numX contains the values of the states (including der_x and p)
//* @param[in] numX size of the states x
//* @param[in] numRes size of the parameters p
//*/
//void MADOLoader::getAllResiduals(double* x, double* res, unsigned numX, unsigned numRes) const
//{
//	return m_evalAll_with_input_check(x, res, numX, numRes);
//}
//
//**
//* @brief Evaluates the residual of the whole equation system
//*
//* The function evaluates Res(x,der_x) =  f(x) - M*der_x .
//*
//* @param[out] res of size numRes contains the residuals of the equation system
//* @param[in] x of size numX contains the values of the states (including der_x and p)
//* @param[in] numX size of the states x
//* @param[in] numRes size of the parameters p
//*/
//void MADOLoader::getAllResiduals(double* x, double* res) const 
//{
//	return m_evalAll(x, res);
//}
//
//
//
//**
//* @brief Calculates the jacobian matrix
//* The function evaluates d(Res(x))/d(x)
//*
//* @param[out] J contains the jacobian matrix
//* @param[in] x contains the values of the states (including der_x and p)
//*/
//void MADOLoader::getDenseJacobianValues(double* x, double** J)
//{
//	return m_evalAllTangents(x, J);
//}
//
//
//
//
//
//
//
//
//
//**
//* @brief Getter for the number of (time-invariant) parameters (not constants) of the model
//*
//* @return number of parameters
//*/
//int MADOLoader::Get_num_pars() const
//{
//  int np;
//  m_Get_num_pars(np);
//  return np;
//}
//
//**
//* @brief Getter for the number of equations differential states of the model
//*
//* @return number of differntial states
//*/
//int MADOLoader::getNumDiffStates() const
//{
//	int nd;
//	m_Get_num_DiffStates(nd);
//	return nd;
//}
//
//**
//* @brief Getter for the number of equations algebraic states of the model
//*
//* @return number of algebraic states
//*/
//int MADOLoader::getNumAlgStates() const
//{
//	int na;
//	m_Get_num_AlgStates(na);
//	return na;
//}
//
//
//**
//* @brief Getter for the number switching of the model
//*
//* @return number of switching
//*/
//int MADOLoader::getNumSwitching() const
//{
//	int ns;
//	m_Get_num_Switching(ns);
//	return ns;
//}
//
//
//
//**
//* @brief Evaluates the structure of the differential jacobian matrix
//*
//* @param[out] JP contains the structure of the differential jacobian matrix
//*/
//void MADOLoader::getDiffJacobianStruct(unsigned int **& JP)
//{
//	return m_get_Diff_Jacobian_Struct(JP);
//}
//
//
//
//
//**
//* @brief Evaluates the differential jacobian matrix
//*
//* The function evaluates d(Res(x))/d(der_x)
//*
//* @param[out] values contains the values of the differntial jacobian matrix
//* @param[in] x contains the values of the states (including der_x and p)
//* @param[in] num_nonZeros
//* @param[in] rind
//* @param[in] cind
//*/
//void MADOLoader::getDiffJacobianValues(double* x, int &num_nonZeros, unsigned int *& rind, unsigned int * & cind, double* &values)
//{
//	return m_get_Diff_Jacobian_Values(x, num_nonZeros, rind, cind, values);
//}








