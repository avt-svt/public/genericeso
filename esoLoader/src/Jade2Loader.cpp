#include "Jade2Loader.hpp"


Jade2Loader::Jade2Loader(const std::string& libname)
	:
	Loader(libname),
	m_Libname(libname)
{
	if (!isLoaded()) {
		std::cerr << "Could not find " << m_Libname.c_str() << std::endl;
		std::cerr << "Opening dynamic link library failed.\n";
		exit(1);
	}


	loadFcn(m_allocModel, "allocModel");
	loadFcn(m_freeModel, "freeModel");
	loadFcn(m_getNumVariables, "getNumVariables");
	loadFcn(m_getNumEquations, "getNumEquations");
    loadFcn(m_getVariableNames, "getVariableNames");
	loadFcn(m_evalJacobianPattern, "evalJacobianPattern");
	loadFcn(m_evalJacobianValues, "evalJacobianValues");
	loadFcn(m_getInitialValues, "getInitialValues");
	loadFcn(m_getInitialValue, "getInitialValue");
	loadFcn(m_getVariable, "getVariable");
	loadFcn(m_setVariable, "setVariable");
	loadFcn(m_getVariables, "getVariables");
	loadFcn(m_setVariables, "setVariables");
	loadFcn(m_evalAll, "evalAll");
	loadFcn(m_eval, "eval");
	loadFcn(m_setAdjoints, "setAdjoints");
	loadFcn(m_evalAllSecondOrderAdjoints, "evalAllSecondOrderAdjoints");
  loadFcn(m_evalAllTangents, "evalAllTangents");
	loadFcn(m_variableIndexOffset, "variableIndexOffset");
	loadFcn(m_equationIndexOffset, "equationIndexOffset");

}

/**
* @brief Getter for the number of all variables of the model
*
* @return number of variables
*/
int Jade2Loader::getNumVariables(int varGroup) const
{
	int nv;
	m_getNumVariables(varGroup, nv);
	return nv;
}

int Jade2Loader::getNumEquations(int eqGroup) const
{
	int ne;
	m_getNumEquations(eqGroup, ne);
	return ne;
}

void *Jade2Loader::allocModel() {
	return m_allocModel();
}

void Jade2Loader::freeModel(void* model) {
	return m_freeModel(model);
}

void Jade2Loader::evalJacobianPattern(void *model, int eqGroup, int varGroup, Eigen::SparseMatrix<double> &jac) {
	return m_evalJacobianPattern(model, eqGroup, varGroup, jac);
}

void Jade2Loader::evalJacobianValues(void *model, int eqGroup, int varGroup, Eigen::SparseMatrix<double> &jac) {
	return m_evalJacobianValues(model, eqGroup, varGroup, jac);
}

void Jade2Loader::getInitialValues(void *model, int varGroup, Eigen::VectorXd &initial_values) const{
	return m_getInitialValues(model, varGroup, initial_values);
}

double Jade2Loader::getInitialValue(void *model, int varGroup, int i) const{
	double out;
	m_getInitialValue(model, varGroup, i, out);
	return out;
}

double Jade2Loader::getVariable(void *model, int varGroup, int i) const {
	double out;
	m_getVariable(model, varGroup, i, out);
	return out;
}

void Jade2Loader::getVariableNames(void *model, std::vector<std::string> &names) const {
    return m_getVariableNames(model, names);
}

void Jade2Loader::setVariable(void *model, int varGroup, int i, const double val) {
	return m_setVariable(model, varGroup, i, val);
}

void Jade2Loader::getVariables(void *model, int varGroup, double* variables) const{
	return m_getVariables(model, varGroup, variables);
}

void Jade2Loader::setVariables(void *model, int varGroup, const double* variables) {
	return m_setVariables(model, varGroup, variables);
}

void Jade2Loader::evalAll(void *model, int eqGroup, double* residuals) const {
	return m_evalAll(model, eqGroup, residuals);
}

double Jade2Loader::eval(void *model, int eqGroup, int i) const {
	double out;
	m_eval(model, eqGroup, i, out);
	return out;
}

int Jade2Loader::variableIndexOffset(void *model, int variableGroup) const{
    int offset;
    m_variableIndexOffset(model, variableGroup, offset);
    return offset;
}

int Jade2Loader::equationIndexOffset(void *model, int eqGroup) const {
	int offset;
	m_equationIndexOffset(model, eqGroup, offset);
	return offset;
}

void Jade2Loader::setAdjoints(void* model, const int eqGroup, Eigen::VectorXd &values) {
	return m_setAdjoints(model, eqGroup, values);
}

void Jade2Loader::evalAllSecondOrderAdjoints(void* model, const int varGroup, double* tangents, double* tangentAdjoints, Eigen::VectorXd &rhs) {
	return m_evalAllSecondOrderAdjoints(model, varGroup, tangents, tangentAdjoints, rhs);
}

void Jade2Loader::evalAllTangents(void* model, const double* tangents, double* tangentResiduals) {
  return m_evalAllTangents(model, tangents, tangentResiduals);
}