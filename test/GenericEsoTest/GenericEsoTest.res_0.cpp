#include "EsoAcsammm_Header.hpp"

void res(double * yy, 
double* der_x, double * x, 
double* p,
int* condit, int* lock, int* prev,
int &n_x, int &n_p, int &n_c)
  #pragma ad indep x p
  #pragma ad dep yy
{

	int i_E=0;
	int i_C=0;
	int i__=0;
	int j__=0;
	int k__=0;
	int m__=0;
	int r_i=0; // for vector assignments
	int r_j=0; // for matrix assignments
	int i_a=0;
	int i_b=0;
	int i_loop=0;
	int i__switch=0;
	int i__start=0;
	int i__end=0;
	int i__step=0;
	int while_end=0; // for vector statements
	int i_cont=0; // for container assignments
	int j_cont=0; // for container assignments
	int k_cont=0; // for container assignments
	double var_tim=0;
	double der_var_tim=0;
	double var_x=0;
	double der_var_x=0;
	double var_y=0;
	double der_var_y=0;
	double var_z=0;
	double der_var_z=0;
	double var_testvar1=0;
	double der_var_testvar1=0;
	double var_testvar2=0;
	double der_var_testvar2=0;
	double par_Para=0;
	double par_Cont=0;
	double var_pow0=0; // procedure var
	double var_i_expr=0; // assign var
	double var_pow1=0; // procedure var
	double var_i_expr2=0; // assign var
	int i_i=0; //loop
	int i_j=0; //loop
	int i_z=0; //loop
	var_tim = x[0];
	der_var_tim = der_x[0];
	var_x = x[1];
	der_var_x = der_x[1];
	var_y = x[2];
	der_var_y = der_x[2];
	var_z = x[3];
	der_var_z = der_x[3];
	var_testvar1 = x[4];
	der_var_testvar1 = der_x[4];
	var_testvar2 = x[5];
	der_var_testvar2 = der_x[5];
	par_Para = p[0];
	par_Cont = p[1];
// procedure assignment pow0 not here
// temporary assignment i_expr not here
// procedure assignment pow1 not here
// temporary assignment i_expr2 not here

// #pragma dcc equation begin
// scalar equation 0
yy[i_E] = 2 * der_var_tim - (1 * 2);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 1
var_i_expr=2;
acs_pow(par_Para, var_i_expr, var_pow0);
yy[i_E] = der_var_x - (-2 * var_tim+4+var_pow0);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 2
var_i_expr2=3;
acs_pow(var_testvar1, var_i_expr2, var_pow1);
yy[i_E] = 3 * der_var_y - ( ( 2 * var_y+var_z+var_pow1 )  * 3);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 3
yy[i_E] = 4 * der_var_z - (var_z * var_tim * 4);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 4
yy[i_E] = var_testvar1 - (var_x+var_y+par_Cont);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 5
yy[i_E] = var_testvar2 - (var_x-var_y);
i_E = i_E+1;
// #pragma dcc equation end

n_c=i_C;
}
