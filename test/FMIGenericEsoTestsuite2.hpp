/**
* @file FMIGenericEso.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen           \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* In this testsuite the FMU model TrivialModel is tested               \n
* =====================================================================\n
*
* @author Adrian Caspari, Johannes M.M. Faust
* @date 03.11.2017
*/



/* // this is the used model in the FMU
model TrivialModel
Real x1,x2;
output Real y( start = 6);
input Real p(start=1);

initial equation
0 = x1 - (  1 + x2);
0 = x2-4;

equation
0 = der(x1) -( -p*y + x2 - x1);
0 = der(x2)-(-x1);
0 = y - x1^2;
end TrivialModel;
*/
#include "floatingPointComparisonThreshold.hpp"
#include "GenericEsoFactory.hpp"
#include <vector>

#include "Array.hpp"
#include "FMIGenericEsoTestConfig.hpp"
using namespace std;

#include "boost/test/unit_test.hpp"
#include "boost/test/tools/floating_point_comparison.hpp"

BOOST_AUTO_TEST_SUITE(TestFMIGenericEso2)

/**
* @test FMIGenericEsoTest - test initialization and deletion of an FMIGenericEso object
*
* @author Adrian Caspari, Johannes M.M. Faust
* @date 03.11.2017
*/
BOOST_AUTO_TEST_CASE(TestInitializationACSAMMMGenericEso)
{
	printf("Boost Test TestInitializationFMIGenericEso. \n");
	GenericEsoFactory genEsoFac;
	GenericEso *genericEso;
	//BOOST_CHECK_NO_THROW(genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI));
	genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_CarFmu,1e-8);
	//BOOST_CHECK_NO_THROW(delete genericEso);
	delete genericEso;
}

/**
* @test ACSAMMMGenericEsoTest - test all residual funcions
*/
BOOST_AUTO_TEST_CASE(TestGetResiduals)
{
	GenericEsoFactory genEsoFac;
	GenericEso *genericEso;

	genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_CarFmu,1e-8);

	const EsoIndex numAlgebraicEquations = 1;
	const EsoIndex numDifferentialEquations = 4;
	const EsoIndex numEquations = numAlgebraicEquations + numDifferentialEquations;

	BOOST_REQUIRE_EQUAL(numAlgebraicEquations, genericEso->getNumAlgEquations());


	utils::Array<EsoDouble> residuals(numEquations);
	std::vector<EsoDouble> compare(numEquations);

	compare[0] = -3;
	compare[1] = -1;
	compare[2] = -0.99676;
	compare[3] = -16;
	compare[4] = 0.0;
	BOOST_CHECK_NO_THROW(genericEso->getAllResiduals(numEquations, residuals.getData()));

	BOOST_REQUIRE_EQUAL(residuals.getSize(), numEquations);
	for (unsigned i = 0; i<residuals.getSize(); i++) {
    BOOST_CHECK_CLOSE(residuals[i], compare[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> differentialResiduals(numDifferentialEquations);
	BOOST_CHECK_NO_THROW(genericEso->getDifferentialResiduals(numDifferentialEquations, differentialResiduals.getData()));
	BOOST_REQUIRE_EQUAL(differentialResiduals.getSize(), numDifferentialEquations);
	for (unsigned i = 0; i<differentialResiduals.getSize(); i++) {
    BOOST_CHECK_CLOSE(differentialResiduals[i], compare[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> algebraicResiduals(numAlgebraicEquations);
	BOOST_CHECK_NO_THROW(genericEso->getAlgebraicResiduals(numAlgebraicEquations, algebraicResiduals.getData()));
	compare.erase(compare.begin(), compare.begin() + numDifferentialEquations);
	BOOST_REQUIRE_EQUAL(algebraicResiduals.getSize(), numAlgebraicEquations);
	for (unsigned i = 0; i<algebraicResiduals.getSize(); i++) {
    BOOST_CHECK_CLOSE(algebraicResiduals[i], compare[i], floatingPointComparisonThreshold());
	}

	delete genericEso;
}

/**
* @test ACSAMMMGenericEsoTest - test all variable functions
*/
BOOST_AUTO_TEST_CASE(TestVariablesESO)
{
	GenericEsoFactory genEsoFac;
	GenericEso *genericEso;

	genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_CarFmu,1e-8);

	const EsoIndex numAlgebraicVariables = 1;
	const EsoIndex numDifferentialVariables = 4;
	const EsoIndex numParameters = 1;
	const EsoIndex numVariables = numParameters + numDifferentialVariables + numAlgebraicVariables;
	const double defaultValue = 0.0;

	BOOST_REQUIRE_EQUAL(numVariables, genericEso->getNumVariables());
	BOOST_REQUIRE_EQUAL(numAlgebraicVariables, genericEso->getNumAlgebraicVariables());
	BOOST_REQUIRE_EQUAL(numDifferentialVariables, genericEso->getNumDifferentialVariables());
	BOOST_REQUIRE_EQUAL(numParameters, genericEso->getNumParameters());

	utils::Array<EsoDouble> differentialVals(numDifferentialVariables);
	vector<EsoDouble> compareDiff(numDifferentialVariables);

	compareDiff[0] = 2.0;
	compareDiff[1] = 4.0;
	compareDiff[2] = 3.0;
	compareDiff[3] = 5.0;

	BOOST_CHECK_NO_THROW(genericEso->getDifferentialVariableValues(numDifferentialVariables, differentialVals.getData()));

	BOOST_REQUIRE_EQUAL(differentialVals.getSize(), numDifferentialVariables);
	for (unsigned i = 0; i<differentialVals.getSize(); i++) {
    BOOST_CHECK_CLOSE(differentialVals[i], compareDiff[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> parameters(numParameters);
	vector<EsoDouble> compareParams(numParameters);
	compareParams[0] = 1.0;

	BOOST_CHECK_NO_THROW(genericEso->getParameterValues(numParameters, parameters.getData()));

	BOOST_REQUIRE_EQUAL(parameters.getSize(), numParameters);
	for (unsigned i = 0; i<parameters.getSize(); i++) {
    BOOST_CHECK_CLOSE(parameters[i], compareParams[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> algebraicVals(numAlgebraicVariables);
	vector<EsoDouble> compareAlgeb(numAlgebraicVariables);
	compareAlgeb[0] = 16;

	BOOST_CHECK_NO_THROW(genericEso->getAlgebraicVariableValues(numAlgebraicVariables, algebraicVals.getData()));

	BOOST_REQUIRE_EQUAL(algebraicVals.getSize(), numAlgebraicVariables);
	for (unsigned i = 0; i<algebraicVals.getSize(); i++) {
    BOOST_CHECK_CLOSE(algebraicVals[i], compareAlgeb[i], floatingPointComparisonThreshold());
	}


	utils::Array<EsoDouble> allVariables(numVariables);
	vector<EsoDouble> compareVariables(numVariables);

	compareVariables[0] = 2.0;
	compareVariables[1] = 4.0;
	compareVariables[2] = 3.0;
	compareVariables[3] = 5.0;
	compareVariables[4] = 16;
	compareVariables[5] = 1.0;

	BOOST_CHECK_NO_THROW(genericEso->getAllVariableValues(numVariables, allVariables.getData()));

	BOOST_REQUIRE_EQUAL(allVariables.getSize(), numVariables);
	for (EsoIndex i = 0; i<numVariables; i++) {
    BOOST_CHECK_CLOSE(allVariables[i], compareVariables[i], floatingPointComparisonThreshold());
	}

	const EsoIndex numIndices = 4;
	utils::Array<EsoIndex> indices(numIndices);
	indices[0] = 2;
	indices[1] = 4;
	indices[2] = 1;
	indices[3] = 3;
	utils::Array<EsoDouble> variableSubset(numIndices);

	BOOST_CHECK_NO_THROW(genericEso->getVariableValues(numIndices, variableSubset.getData(), indices.getData()));

	for (EsoIndex i = 0; i<numIndices; i++) {
    BOOST_CHECK_CLOSE(variableSubset[i], compareVariables[indices[i]], floatingPointComparisonThreshold());
	}

	delete genericEso;
}


//
///**
//* @test ACSAMMMGenericEsoTest - set variables and test all variable functions and check for correct residuals
//*/
//BOOST_AUTO_TEST_CASE(TestSetVariablesESO)
//{
//	GenericEsoFactory genEsoFac;
//	GenericEso *genericEso;
//
//	genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_CarFmu);
//
//	const EsoIndex numAlgebraicVariables = 1;
//	const EsoIndex numDifferentialVariables = 4;
//	const EsoIndex numParameters = 1;
//	const EsoIndex numVariables = numParameters + numDifferentialVariables + numAlgebraicVariables;
//	const EsoDouble defaultValue = 1.0;
//
//	BOOST_REQUIRE_EQUAL(numVariables, genericEso->getNumVariables());
//	BOOST_REQUIRE_EQUAL(numAlgebraicVariables, genericEso->getNumAlgebraicVariables());
//	BOOST_REQUIRE_EQUAL(numDifferentialVariables, genericEso->getNumDifferentialVariables());
//	BOOST_REQUIRE_EQUAL(numParameters, genericEso->getNumParameters());
//
//	utils::Array<EsoDouble> differentialVals(numDifferentialVariables);
//	utils::Array<EsoDouble> compareDiff(numDifferentialVariables);
//
//	compareDiff[0] = 2 * defaultValue;
//	compareDiff[1] = 3 * defaultValue;
//	compareDiff[2] = 4 * defaultValue;
//	compareDiff[3] = 0.5*defaultValue;
//
//	genericEso->setDifferentialVariableValues(numDifferentialVariables, compareDiff.getData());
//	genericEso->getDifferentialVariableValues(numDifferentialVariables, differentialVals.getData());
//
//	BOOST_REQUIRE_EQUAL(differentialVals.getSize(), numDifferentialVariables);
//	for (unsigned i = 0; i<differentialVals.getSize(); i++) {
//		BOOST_CHECK_CLOSE(differentialVals[i], compareDiff[i], floatingPointComparisonThreshold());
//	}
//
//	utils::Array<EsoDouble> parameters(numParameters);
//	utils::Array<EsoDouble> compareParams(numParameters);
//	compareParams[0] = 3.9;
//	genericEso->setParameterValues(numParameters, compareParams.getData());
//	genericEso->getParameterValues(numParameters, parameters.getData());
//
//	BOOST_REQUIRE_EQUAL(parameters.getSize(), numParameters);
//	for (unsigned i = 0; i<parameters.getSize(); i++) {
//		BOOST_CHECK_CLOSE(parameters[i], compareParams[i], floatingPointComparisonThreshold());
//	}
//
//	utils::Array<EsoDouble> algebraicVals(numAlgebraicVariables);
//	utils::Array<EsoDouble> compareAlgeb(numAlgebraicVariables);
//	compareAlgeb[0] = defaultValue + 15;
//	genericEso->setAlgebraicVariableValues(numAlgebraicVariables, compareAlgeb.getData());
//	genericEso->getAlgebraicVariableValues(numAlgebraicVariables, algebraicVals.getData());
//
//	for (unsigned i = 0; i<algebraicVals.getSize(); i++) {
//		BOOST_CHECK_CLOSE(algebraicVals[i], compareAlgeb[i], floatingPointComparisonThreshold());
//	}
//
//	const EsoIndex numStates = genericEso->getNumStates();
//	BOOST_CHECK_EQUAL(numStates, numAlgebraicVariables + numDifferentialVariables);
//	utils::Array<EsoDouble> stateVals(numStates);
//	utils::Array<EsoDouble> compareStates(numStates);
//	compareStates[0] = 12.45;
//	compareStates[1] = 34.98;
//	compareStates[2] = -17.6;
//	compareStates[3] = 1.17;
//	compareStates[4] = 85.42;
//	genericEso->setStateValues(numStates, compareStates.getData());
//	genericEso->getStateValues(numStates, stateVals.getData());
//
//	for (unsigned i = 0; i<stateVals.getSize(); i++) {
//		BOOST_CHECK_CLOSE(stateVals[i], compareStates[i], floatingPointComparisonThreshold());
//	}
//
//	utils::Array<EsoDouble> derivativeVals(numDifferentialVariables);
//	utils::Array<EsoDouble> compareDerivs(numDifferentialVariables);
//	compareDerivs[0] = 23.41;
//	compareDerivs[1] = 1.09;
//	compareDerivs[2] = -23.43;
//	compareDerivs[3] = 4.19;
//	genericEso->setDerivativeValues(numDifferentialVariables, compareDerivs.getData());
//	genericEso->getDerivativeValues(numDifferentialVariables, derivativeVals.getData());
//
//	for (unsigned i = 0; i<derivativeVals.getSize(); i++) {
//		BOOST_CHECK_CLOSE(derivativeVals[i], compareDerivs[i], floatingPointComparisonThreshold());
//	}
//
//	const EsoIndex n_idx = 4;
//	utils::Array<EsoIndex> indices(n_idx);
//	utils::Array<EsoDouble> varVals(n_idx);
//	utils::Array<EsoDouble> compVarVals(n_idx);
//	indices[0] = 0;
//	indices[1] = 3;
//	indices[2] = 4;
//	indices[3] = 5;
//	compVarVals[0] = 13.4;
//	compVarVals[1] = -7.41;
//	compVarVals[2] = 6.234;
//	compVarVals[3] = 71.0;
//	genericEso->setVariableValues(n_idx, compVarVals.getData(), indices.getData());
//	genericEso->getVariableValues(n_idx, varVals.getData(), indices.getData());
//
//	for (EsoIndex i = 0; i<n_idx; i++) {
//		BOOST_CHECK_CLOSE(varVals[i], compVarVals[i], floatingPointComparisonThreshold());
//	}
//
//	utils::Array<EsoDouble> allVariables(numVariables);
//	utils::Array<EsoDouble> compareVariables(numVariables);
//
//	compareVariables[0] = 2.2;
//	compareVariables[1] = 4.3;
//	compareVariables[2] = 8.4;
//	compareVariables[3] = 6.5;
//	compareVariables[3] = 9.75;
//	compareVariables[4] = -3.6;
//	compareVariables[5] = -5.0;
//
//	genericEso->setAllVariableValues(numVariables, compareVariables.getData());
//	genericEso->getAllVariableValues(numVariables, allVariables.getData());
//
//	vector<string> variableNames(numVariables);
//	genericEso->getVariableNames(variableNames);
//
//	BOOST_REQUIRE_EQUAL(allVariables.getSize(), numVariables);
//	for (EsoIndex i = 0; i<numVariables; i++) {
//		BOOST_CHECK_CLOSE(allVariables[i], compareVariables[i], floatingPointComparisonThreshold());
//	}
//	delete genericEso;
//}

/**
* @test ACSAMMMGenericEsoTest - check if Jacobian struct information is correct
*/
BOOST_AUTO_TEST_CASE(testJacobian)
{
	GenericEsoFactory genEsoFac;
	GenericEso *genericEso;

	genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_CarFmu,1e-8);

	const EsoIndex numJacobianEntries = 6;
	const EsoIndex numDiffJacobianEntries = 4;

	BOOST_REQUIRE_EQUAL(numJacobianEntries, genericEso->getNumNonZeroes());
	BOOST_REQUIRE_EQUAL(numDiffJacobianEntries, genericEso->getNumDifferentialNonZeroes());

	utils::Array<EsoIndex> compareRowIndices(numJacobianEntries);
	compareRowIndices[0] = 0;
	compareRowIndices[1] = 2;
	compareRowIndices[2] = 2;
	compareRowIndices[3] = 3;
	compareRowIndices[4] = 4;
	compareRowIndices[5] = 4;

	utils::Array<EsoIndex> compareColIndices(numJacobianEntries);
	compareColIndices[0] = 2;
	compareColIndices[1] = 2;
	compareColIndices[2] = 5;
	compareColIndices[3] = 5; // results from ODE respresentation
	compareColIndices[4] = 4;
	compareColIndices[5] = 5;

	utils::Array<EsoIndex> rowIndices(numJacobianEntries);
	utils::Array<EsoIndex> colIndices(numJacobianEntries);
	genericEso->getJacobianStruct(numJacobianEntries, rowIndices.getData(),
		colIndices.getData());

	BOOST_CHECK_EQUAL_COLLECTIONS(rowIndices.getData(),
		rowIndices.getData() + numJacobianEntries,
		compareRowIndices.getData(),
		compareRowIndices.getData() + numJacobianEntries);
	BOOST_CHECK_EQUAL_COLLECTIONS(colIndices.getData(),
		colIndices.getData() + numJacobianEntries,
		compareColIndices.getData(),
		compareColIndices.getData() + numJacobianEntries);
	delete genericEso;

}

/**
* @test ACSAMMMGenericEsoTest - check correctness of differential Jacobian struct information
*/
BOOST_AUTO_TEST_CASE(testDiffJacobianStruct)
{
	GenericEsoFactory genEsoFac;
	GenericEso *genericEso;

	genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_CarFmu,1e-8);

	const EsoIndex numDiffJacobianEntries = genericEso->getNumDifferentialNonZeroes();
	utils::Array<EsoIndex> rowIndices(numDiffJacobianEntries);
	utils::Array<EsoIndex> colIndices(numDiffJacobianEntries);
	genericEso->getDiffJacobianStruct(numDiffJacobianEntries, rowIndices.getData(),
		colIndices.getData());

	utils::Array<EsoIndex> compareRowIndices(numDiffJacobianEntries);
	compareRowIndices[0] = 0;
	compareRowIndices[1] = 1;
	compareRowIndices[2] = 2;
	compareRowIndices[3] = 3;

	utils::Array<EsoIndex> compareColIndices(numDiffJacobianEntries);
	compareColIndices[0] = 0;
	compareColIndices[1] = 1;
	compareColIndices[2] = 2;
	compareColIndices[3] = 3;

	BOOST_CHECK_EQUAL_COLLECTIONS(rowIndices.getData(),
		rowIndices.getData() + numDiffJacobianEntries,
		compareRowIndices.getData(),
		compareRowIndices.getData() + numDiffJacobianEntries);
	BOOST_CHECK_EQUAL_COLLECTIONS(colIndices.getData(),
		colIndices.getData() + numDiffJacobianEntries,
		compareColIndices.getData(),
		compareColIndices.getData() + numDiffJacobianEntries);
	delete genericEso;
}



/**
* @test ACSAMMMGenericEsoTest - check function getJacobianMultVector
*/
BOOST_AUTO_TEST_CASE(testJacobianMultVector)
{
	GenericEsoFactory genEsoFac;
	GenericEso *genericEso;

	genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_CarFmu,1e-8);

	const EsoIndex numStates = genericEso->getNumStates();
	const EsoIndex numParams = genericEso->getNumParameters();
	const EsoIndex numEqns = genericEso->getNumEquations();
	utils::Array<EsoDouble> seedStates(numStates, 1.0);
	utils::Array<EsoDouble> seedParams(numStates, 1.0);
	utils::Array<EsoDouble> y(numEqns, 0.0);

	// check tangent-linear calculation
	bool transpose = false;
	utils::Array<EsoDouble> compY(numEqns, 0.0);

	compY[0] = -1.0;
	compY[1] = 0.0;
	compY[2] = -0.99784;
	compY[3] = -15.0;
	compY[4] = -14.0;
	genericEso->getJacobianMultVector(transpose, numStates, seedStates.getData(),
		numParams, seedParams.getData(), numEqns, y.getData());

	for (EsoIndex i = 0; i<numEqns; i++) {
    BOOST_CHECK_CLOSE(y[i], compY[i], floatingPointComparisonThreshold());
	}

	
	delete genericEso;
}

BOOST_AUTO_TEST_SUITE_END()
