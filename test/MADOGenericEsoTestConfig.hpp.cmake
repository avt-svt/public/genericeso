/**
* @file MADOGenericEsoTestConfig.hpp.cmake.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* configure header for generic eso factory                             \n
* =====================================================================\n
* @author Adrian Caspari, Jan Schulze 
* @date 13.02.2020
*/

#pragma once

// Path to MADO test dll for FmiGenericEsoTest
#cmakedefine PATH_MADO_GENERICESO_TEST_MODEL "@PATH_MADO_GENERICESO_TEST_MODEL@"
#cmakedefine PATH_MADO_LOADER_TEST_MODEL "@PATH_MADO_LOADER_TEST_MODEL@"
