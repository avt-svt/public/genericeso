/**
* @file FMIGenericEsoTestConfig.hpp.cmake.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* configure header for generic eso factory                             \n
* =====================================================================\n
* @author Adrian Caspari  
* @date 14.12.2017
*/

#ifndef _FMIGENERICESOTESTCONFIG_H_
#define _FMIGENERICESOTESTCONFIG_H_


// Path to fmi.fmu for FmiGenericEsoTest
#cmakedefine PATH_FMI_GENERICESO_TEST_FMI "@PATH_FMI_GENERICESO_TEST_FMI@"
#cmakedefine PATH_FMI_GENERICESO_TEST_CarFmu "@PATH_FMI_GENERICESO_TEST_CarFmu@"


#endif