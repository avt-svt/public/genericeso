/**
* @file GenericEsoTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* testsuite     for the Generic Eso module	                             \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 15.8.2011
*/
#include "GenericEsoFactory.hpp"

/**
* @def BOOST_TEST_MODULE
* @brief name of the test unit (needed to be set for BOOST_TEST)
*/
#define BOOST_TEST_MODULE FmiGenericEso


#include "boost/test/unit_test.hpp"
#include "boost/test/tools/floating_point_comparison.hpp"


#include "FMIGenericEsoTestsuite.hpp"   // trivial model test
#include "FMIGenericEsoTestsuite2.hpp"	// car example test

