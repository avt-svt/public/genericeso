/**
* @file FMIGenericEso.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen           \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* In this testsuite the FMU model TrivialModel is tested               \n
* =====================================================================\n
*
* @author Adrian Caspari, Johannes M.M. Faust
* @date 03.11.2017
*/

#include "floatingPointComparisonThreshold.hpp"

/* // this is the used model in the FMU
model TrivialModel
Real x1,x2;
output Real y( start = 6);
input Real p(start=1);

initial equation
0 = x1 - (  1 + x2);
0 = x2-4;

equation
0 = der(x1) -( -p*y + x2 - x1);
0 = der(x2)-(-x1);
0 = y - x1^2;
end TrivialModel;
*/
#include "GenericEsoFactory.hpp"
#include <vector>

#include "Array.hpp"
#include "FMIGenericEsoTestConfig.hpp"
using namespace std;

#include "boost/test/unit_test.hpp"
#include "boost/test/tools/floating_point_comparison.hpp"


BOOST_AUTO_TEST_SUITE(TestFMIGenericEso)

/**
* @test FMIGenericEsoTest - test initialization and deletion of an FMIGenericEso object
*
* @author Adrian Caspari, Johannes M.M. Faust
* @date 03.11.2017
*/
BOOST_AUTO_TEST_CASE(TestInitializationFMIGenericEso)
{
	printf("Boost Test TestInitializationFMIGenericEso. \n");
	GenericEsoFactory genEsoFac;
	GenericEso *genericEso;
	//BOOST_CHECK_NO_THROW(genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI));
	genericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI,1e-8);
	//BOOST_CHECK_NO_THROW(delete genericEso);
	delete genericEso;

}



/**
* @test FMIGenericEsoTest - test all residual funcions
*/
BOOST_AUTO_TEST_CASE(TestGetResiduals)
{

	printf("Boost Test TestGetResiduals. \n");


	GenericEso *FMIGenericEso;
	GenericEsoFactory genEsoFac;
	BOOST_REQUIRE_NO_THROW(FMIGenericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI,1e-8));
	const EsoIndex numAlgebraicEquations = 1;
	const EsoIndex numDifferentialEquations = 2;
	const EsoIndex numEquations = numAlgebraicEquations + numDifferentialEquations;

	BOOST_REQUIRE_EQUAL(numAlgebraicEquations, FMIGenericEso->getNumAlgEquations());


	utils::Array<EsoDouble> residuals(numEquations);
	std::vector<EsoDouble> compare(numEquations);

	compare[0] = 26;
	compare[1] = 5;
	compare[2] = 0;
	BOOST_CHECK_NO_THROW(FMIGenericEso->getAllResiduals(numEquations, residuals.getData()));

	BOOST_REQUIRE_EQUAL(residuals.getSize(), numEquations);
	for (unsigned i = 0; i<residuals.getSize(); i++) {
    BOOST_CHECK_CLOSE(residuals[i], compare[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> differentialResiduals(numDifferentialEquations);
	BOOST_CHECK_NO_THROW(FMIGenericEso->getDifferentialResiduals(numDifferentialEquations, differentialResiduals.getData()));
	BOOST_REQUIRE_EQUAL(differentialResiduals.getSize(), numDifferentialEquations);
	for (unsigned i = 0;i<differentialResiduals.getSize(); i++) {
    BOOST_CHECK_CLOSE(differentialResiduals[i], compare[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> algebraicResiduals(numAlgebraicEquations);
	BOOST_CHECK_NO_THROW(FMIGenericEso->getAlgebraicResiduals(numAlgebraicEquations, algebraicResiduals.getData()));
	compare.erase(compare.begin(), compare.begin() + numDifferentialEquations);
	BOOST_REQUIRE_EQUAL(algebraicResiduals.getSize(), numAlgebraicEquations);
	for (unsigned i = 0;i<algebraicResiduals.getSize(); i++) {
    BOOST_CHECK_CLOSE(algebraicResiduals[i], compare[i], floatingPointComparisonThreshold());
	}

	delete FMIGenericEso;
}




/**
* @test FMIGenericEsoTest - test all residual funcions
*/
BOOST_AUTO_TEST_CASE(TestSolveAlgebraicEquations)
{

	printf("Boost Test TestSolveAlgebraicEquations. \n");


	GenericEso *FMIGenericEso;
	GenericEsoFactory genEsoFac;
	BOOST_REQUIRE_NO_THROW(FMIGenericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI,1e-8));
	const EsoIndex numAlgebraicEquations = 1;
	const EsoIndex numDifferentialEquations = 2;
	const EsoIndex numEquations = numAlgebraicEquations + numDifferentialEquations;

	BOOST_REQUIRE_EQUAL(numAlgebraicEquations, FMIGenericEso->getNumAlgEquations());
	BOOST_REQUIRE_EQUAL(numAlgebraicEquations, FMIGenericEso->getNumAlgebraicVariables());


	utils::Array<EsoDouble> residuals(numEquations);
	std::vector<EsoDouble> compare(numEquations);

	std::vector<EsoDouble> states(numEquations);
	states[0] = 5;
	states[1] = 5;
	states[2] = 5;

	FMIGenericEso->setStateValues(numEquations, states.data());


	compare[0] = 25;
	compare[1] = 5;
	compare[2] = -20;



	BOOST_CHECK_NO_THROW(FMIGenericEso->getAllResiduals(numEquations, residuals.getData()));

	BOOST_REQUIRE_EQUAL(residuals.getSize(), numEquations);
	for (unsigned i = 0; i<residuals.getSize(); i++) {
    BOOST_CHECK_CLOSE(residuals[i], compare[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> differentialResiduals(numDifferentialEquations);
	BOOST_CHECK_NO_THROW(FMIGenericEso->getDifferentialResiduals(numDifferentialEquations, differentialResiduals.getData()));
	BOOST_REQUIRE_EQUAL(differentialResiduals.getSize(), numDifferentialEquations);
	for (unsigned i = 0;i<differentialResiduals.getSize(); i++) {
    BOOST_CHECK_CLOSE(differentialResiduals[i], compare[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> algebraicResiduals(numAlgebraicEquations);
	BOOST_CHECK_NO_THROW(FMIGenericEso->getAlgebraicResiduals(numAlgebraicEquations, algebraicResiduals.getData()));
	compare.erase(compare.begin(), compare.begin() + numDifferentialEquations);
	BOOST_REQUIRE_EQUAL(algebraicResiduals.getSize(), numAlgebraicEquations);
	for (unsigned i = 0;i<algebraicResiduals.getSize(); i++) {
    BOOST_CHECK_CLOSE(algebraicResiduals[i], compare[i], floatingPointComparisonThreshold());
	}

	
	FMIGenericEso->solveAlgebraicEquations();



	BOOST_CHECK_NO_THROW(FMIGenericEso->getAlgebraicResiduals(numAlgebraicEquations, algebraicResiduals.getData()));
	for (unsigned i = 0;i<algebraicResiduals.getSize(); i++) {
    BOOST_CHECK_CLOSE(algebraicResiduals[i], 0, floatingPointComparisonThreshold());
	}

	FMIGenericEso->solveDifferentialEquations();
	BOOST_CHECK_NO_THROW(FMIGenericEso->getDifferentialResiduals(numDifferentialEquations, differentialResiduals.getData()));
	BOOST_REQUIRE_EQUAL(differentialResiduals.getSize(), numDifferentialEquations);
	for (unsigned i = 0;i<differentialResiduals.getSize(); i++) {
    BOOST_CHECK_CLOSE(differentialResiduals[i], 0, floatingPointComparisonThreshold());
	}

	delete FMIGenericEso;
}



/**
* @test FMIGenericEsoTest - test all residual funcions
*/
BOOST_AUTO_TEST_CASE(TestSolveAlgebraicEquations_2)
{

	printf("Boost Test TestSolveAlgebraicEquations. \n");


	GenericEso *FMIGenericEso;
	GenericEsoFactory genEsoFac;
	BOOST_REQUIRE_NO_THROW(FMIGenericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI, 1e-8));
	const EsoIndex numAlgebraicEquations = 1;
	const EsoIndex numDifferentialEquations = 2;
	const EsoIndex numEquations = numAlgebraicEquations + numDifferentialEquations;

	BOOST_REQUIRE_EQUAL(numAlgebraicEquations, FMIGenericEso->getNumAlgEquations());
	BOOST_REQUIRE_EQUAL(numAlgebraicEquations, FMIGenericEso->getNumAlgebraicVariables());


	utils::Array<EsoDouble> residuals(numEquations);
	std::vector<EsoDouble> compare(numEquations);

	std::vector<EsoDouble> states(numEquations);
	states[0] = 3;
	states[1] = 4;
	states[2] = 5;

	FMIGenericEso->setStateValues(numEquations, states.data());


	compare[0] = 8;
	compare[1] = 3;
	compare[2] = -4;



	BOOST_CHECK_NO_THROW(FMIGenericEso->getAllResiduals(numEquations, residuals.getData()));

	BOOST_REQUIRE_EQUAL(residuals.getSize(), numEquations);
	for (unsigned i = 0; i<residuals.getSize(); i++) {
    BOOST_CHECK_CLOSE(residuals[i], compare[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> differentialResiduals(numDifferentialEquations);
	BOOST_CHECK_NO_THROW(FMIGenericEso->getDifferentialResiduals(numDifferentialEquations, differentialResiduals.getData()));
	BOOST_REQUIRE_EQUAL(differentialResiduals.getSize(), numDifferentialEquations);
	for (unsigned i = 0;i<differentialResiduals.getSize(); i++) {
    BOOST_CHECK_CLOSE(differentialResiduals[i], compare[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> algebraicResiduals(numAlgebraicEquations);
	BOOST_CHECK_NO_THROW(FMIGenericEso->getAlgebraicResiduals(numAlgebraicEquations, algebraicResiduals.getData()));
	compare.erase(compare.begin(), compare.begin() + numDifferentialEquations);
	BOOST_REQUIRE_EQUAL(algebraicResiduals.getSize(), numAlgebraicEquations);
	for (unsigned i = 0;i<algebraicResiduals.getSize(); i++) {
    BOOST_CHECK_CLOSE(algebraicResiduals[i], compare[i], floatingPointComparisonThreshold());
	}


	FMIGenericEso->solveAlgebraicEquations();



	BOOST_CHECK_NO_THROW(FMIGenericEso->getAlgebraicResiduals(numAlgebraicEquations, algebraicResiduals.getData()));
	for (unsigned i = 0;i<algebraicResiduals.getSize(); i++) {
    BOOST_CHECK_CLOSE(algebraicResiduals[i], 0, floatingPointComparisonThreshold());
	}

	FMIGenericEso->solveDifferentialEquations();
	BOOST_CHECK_NO_THROW(FMIGenericEso->getDifferentialResiduals(numDifferentialEquations, differentialResiduals.getData()));
	BOOST_REQUIRE_EQUAL(differentialResiduals.getSize(), numDifferentialEquations);
	for (unsigned i = 0;i<differentialResiduals.getSize(); i++) {
    BOOST_CHECK_CLOSE(differentialResiduals[i], 0, floatingPointComparisonThreshold());
	}

	delete FMIGenericEso;
}


/**
* @test FMIGenericEsoTest - test all variable functions
*/
BOOST_AUTO_TEST_CASE(TestVariablesESO)
{

	printf("Boost Test TestVariablesESO. \n");

	GenericEso *FMIGenericEso;
	GenericEsoFactory genEsoFac;
	BOOST_REQUIRE_NO_THROW(FMIGenericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI,1e-8));
	const EsoIndex numAlgebraicVariables = 1;
	const EsoIndex numDifferentialVariables = 2;
	const EsoIndex numParameters = 1;
	const EsoIndex numVariables = numParameters + numDifferentialVariables + numAlgebraicVariables;
	const double defaultValue = 0.0;

	BOOST_REQUIRE_EQUAL(numVariables, FMIGenericEso->getNumVariables());
	BOOST_REQUIRE_EQUAL(numAlgebraicVariables, FMIGenericEso->getNumAlgebraicVariables());
	BOOST_REQUIRE_EQUAL(numDifferentialVariables, FMIGenericEso->getNumDifferentialVariables());
	BOOST_REQUIRE_EQUAL(numParameters, FMIGenericEso->getNumParameters());

	utils::Array<EsoDouble> differentialVals(numDifferentialVariables);
	vector<EsoDouble> compareDiff(numDifferentialVariables);

	compareDiff[0] = 5.0;
	compareDiff[1] = 4.0;
	
	BOOST_CHECK_NO_THROW(FMIGenericEso->getDifferentialVariableValues(numDifferentialVariables, differentialVals.getData()));

	BOOST_REQUIRE_EQUAL(differentialVals.getSize(), numDifferentialVariables);
	for (unsigned i = 0;i<differentialVals.getSize(); i++) {
    BOOST_CHECK_CLOSE(differentialVals[i], compareDiff[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> parameters(numParameters);
	vector<EsoDouble> compareParams(numParameters);
	compareParams[0] = 1.0;

	BOOST_CHECK_NO_THROW(FMIGenericEso->getParameterValues(numParameters, parameters.getData()));

	BOOST_REQUIRE_EQUAL(parameters.getSize(), numParameters);
	for (unsigned i = 0;i<parameters.getSize(); i++) {
    BOOST_CHECK_CLOSE(parameters[i], compareParams[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> algebraicVals(numAlgebraicVariables);
	vector<EsoDouble> compareAlgeb(numAlgebraicVariables);
	compareAlgeb[0] = 25;

	BOOST_CHECK_NO_THROW(FMIGenericEso->getAlgebraicVariableValues(numAlgebraicVariables, algebraicVals.getData()));

	BOOST_REQUIRE_EQUAL(algebraicVals.getSize(), numAlgebraicVariables);
	for (unsigned i = 0;i<algebraicVals.getSize(); i++) {
    BOOST_CHECK_CLOSE(algebraicVals[i], compareAlgeb[i], floatingPointComparisonThreshold());
	}


	utils::Array<EsoDouble> allVariables(numVariables);
	vector<EsoDouble> compareVariables(numVariables);

	compareVariables[0] = 5.0;
	compareVariables[1] = 4.0;
	compareVariables[2] = 25.0;
	compareVariables[3] = 1.0;

	BOOST_CHECK_NO_THROW(FMIGenericEso->getAllVariableValues(numVariables, allVariables.getData()));

	BOOST_REQUIRE_EQUAL(allVariables.getSize(), numVariables);
	for (EsoIndex i = 0; i<numVariables; i++) {
    BOOST_CHECK_CLOSE(allVariables[i], compareVariables[i], floatingPointComparisonThreshold());
	}

	const EsoIndex numIndices = 3;
	utils::Array<EsoIndex> indices(numIndices);
	indices[0] = 2;
	indices[1] = 3;
	indices[2] = 1;

	utils::Array<EsoDouble> variableSubset(numIndices);

	BOOST_CHECK_NO_THROW(FMIGenericEso->getVariableValues(numIndices, variableSubset.getData(), indices.getData()));

	for (EsoIndex i = 0; i<numIndices; i++) {
    BOOST_CHECK_CLOSE(variableSubset[i], compareVariables[indices[i]], floatingPointComparisonThreshold());
	}

	delete FMIGenericEso;
}

/**
* @test FMIGenericEsoTest - set variables and test all variable functions and check for correct residuals
*/
BOOST_AUTO_TEST_CASE(TestSetVariablesESO)
{
	printf("Boost Test TestSetVariablesESO. \n");

	GenericEso *FMIGenericEso;
	GenericEsoFactory genEsoFac;
	BOOST_REQUIRE_NO_THROW(FMIGenericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI,1e-8));
	const EsoIndex numAlgebraicVariables = 1;
	const EsoIndex numDifferentialVariables = 2;
	const EsoIndex numParameters = 1;
	const EsoIndex numVariables = numParameters + numDifferentialVariables + numAlgebraicVariables;
	const EsoDouble defaultValue = 1.0;

	BOOST_REQUIRE_EQUAL(numVariables, FMIGenericEso->getNumVariables());
	BOOST_REQUIRE_EQUAL(numAlgebraicVariables, FMIGenericEso->getNumAlgebraicVariables());
	BOOST_REQUIRE_EQUAL(numDifferentialVariables, FMIGenericEso->getNumDifferentialVariables());
	BOOST_REQUIRE_EQUAL(numParameters, FMIGenericEso->getNumParameters());

	utils::Array<EsoDouble> differentialVals(numDifferentialVariables);
	utils::Array<EsoDouble> compareDiff(numDifferentialVariables);

	compareDiff[0] = 6 * defaultValue;
	compareDiff[1] = 7 * defaultValue;

	FMIGenericEso->setDifferentialVariableValues(numDifferentialVariables, compareDiff.getData());
	FMIGenericEso->getDifferentialVariableValues(numDifferentialVariables, differentialVals.getData());

	BOOST_REQUIRE_EQUAL(differentialVals.getSize(), numDifferentialVariables);
	for (unsigned i = 0; i<differentialVals.getSize(); i++) {
    BOOST_CHECK_CLOSE(differentialVals[i], compareDiff[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> parameters(numParameters);
	utils::Array<EsoDouble> compareParams(numParameters);
	compareParams[0] = 3.9;
	FMIGenericEso->setParameterValues(numParameters, compareParams.getData());
	FMIGenericEso->getParameterValues(numParameters, parameters.getData());

	BOOST_REQUIRE_EQUAL(parameters.getSize(), numParameters);
	for (unsigned i = 0; i<parameters.getSize(); i++) {
    BOOST_CHECK_CLOSE(parameters[i], compareParams[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> algebraicVals(numAlgebraicVariables);
	utils::Array<EsoDouble> compareAlgeb(numAlgebraicVariables);
	compareAlgeb[0] = defaultValue + 15;
	FMIGenericEso->setAlgebraicVariableValues(numAlgebraicVariables, compareAlgeb.getData());
	FMIGenericEso->getAlgebraicVariableValues(numAlgebraicVariables, algebraicVals.getData());

	for (unsigned i = 0; i<algebraicVals.getSize(); i++) {
    BOOST_CHECK_CLOSE(algebraicVals[i], compareAlgeb[i], floatingPointComparisonThreshold());
	}

	const EsoIndex numStates = FMIGenericEso->getNumStates();
	BOOST_CHECK_EQUAL(numStates, numAlgebraicVariables + numDifferentialVariables);
	utils::Array<EsoDouble> stateVals(numStates);
	utils::Array<EsoDouble> compareStates(numStates);
	compareStates[0] = 12.45;
	compareStates[1] = 34.98;
	compareStates[2] = 17.6;
	
	FMIGenericEso->setStateValues(numStates, compareStates.getData());
	FMIGenericEso->getStateValues(numStates, stateVals.getData());

	for (unsigned i = 0; i<stateVals.getSize(); i++) {
    BOOST_CHECK_CLOSE(stateVals[i], compareStates[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> derivativeVals(numDifferentialVariables);
	utils::Array<EsoDouble> compareDerivs(numDifferentialVariables);
	compareDerivs[0] = 23.41;
	compareDerivs[1] = 1.09;
	FMIGenericEso->setDerivativeValues(numDifferentialVariables, compareDerivs.getData());
	FMIGenericEso->getDerivativeValues(numDifferentialVariables, derivativeVals.getData());

	for (unsigned i = 0; i<derivativeVals.getSize(); i++) {
    BOOST_CHECK_CLOSE(derivativeVals[i], compareDerivs[i], floatingPointComparisonThreshold());
	}

	const EsoIndex n_idx = 3;
	utils::Array<EsoIndex> indices(n_idx);
	utils::Array<EsoDouble> varVals(n_idx);
	utils::Array<EsoDouble> compVarVals(n_idx);
	indices[0] = 1;
	indices[1] = 3;
	indices[2] = 0;
	compVarVals[0] = 13.4;
	compVarVals[1] = -7.41;
	compVarVals[2] = 6.234;
	FMIGenericEso->setVariableValues(n_idx, compVarVals.getData(), indices.getData());
	FMIGenericEso->getVariableValues(n_idx, varVals.getData(), indices.getData());

	for (EsoIndex i = 0; i<n_idx; i++) {
    BOOST_CHECK_CLOSE(varVals[i], compVarVals[i], floatingPointComparisonThreshold());
	}

	utils::Array<EsoDouble> allVariables(numVariables);
	utils::Array<EsoDouble> compareVariables(numVariables);

	compareVariables[0] = 2.2;
	compareVariables[1] = 4.3;
	compareVariables[2] = 8.4;
	compareVariables[3] = 0;
	
	FMIGenericEso->setAllVariableValues(numVariables, compareVariables.getData());
	FMIGenericEso->getAllVariableValues(numVariables, allVariables.getData());

	vector<string> variableNames(numVariables);
	FMIGenericEso->getVariableNames(variableNames);

	BOOST_REQUIRE_EQUAL(allVariables.getSize(), numVariables);
	for (EsoIndex i = 0; i<numVariables; i++) {
    BOOST_CHECK_CLOSE(allVariables[i], compareVariables[i], floatingPointComparisonThreshold());
	}
	delete FMIGenericEso;
}


/**
* @test FMIGenericEsoTest - check if Jacobian struct information is correct
*/
BOOST_AUTO_TEST_CASE(testJacobian)
{
	printf("Boost Test testJacobian. \n");

	GenericEso *FMIGenericEso;
	GenericEsoFactory genEsoFac;
	BOOST_REQUIRE_NO_THROW(FMIGenericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI,1e-8));
	const EsoIndex numJacobianEntries = 6;
	const EsoIndex numDiffJacobianEntries = 2;

	BOOST_REQUIRE_EQUAL(numJacobianEntries, FMIGenericEso->getNumNonZeroes());
	BOOST_REQUIRE_EQUAL(numDiffJacobianEntries, FMIGenericEso->getNumDifferentialNonZeroes());

	utils::Array<EsoIndex> compareRowIndices(numJacobianEntries);
	compareRowIndices[0] = 0;
	compareRowIndices[1] = 0;
	compareRowIndices[2] = 0;
	compareRowIndices[3] = 1;
	compareRowIndices[4] = 2;
	compareRowIndices[5] = 2;


	utils::Array<EsoIndex> compareColIndices(numJacobianEntries);
	compareColIndices[0] = 0;
	compareColIndices[1] = 1;
	compareColIndices[2] = 3;
	compareColIndices[3] = 0;
	compareColIndices[4] = 0;
	compareColIndices[5] = 2;


	utils::Array<EsoIndex> rowIndices(numJacobianEntries);
	utils::Array<EsoIndex> colIndices(numJacobianEntries);
	FMIGenericEso->getJacobianStruct(numJacobianEntries, rowIndices.getData(),
		colIndices.getData());

	BOOST_CHECK_EQUAL_COLLECTIONS(rowIndices.getData(),
		rowIndices.getData() + numJacobianEntries,
		compareRowIndices.getData(),
		compareRowIndices.getData() + numJacobianEntries);
	BOOST_CHECK_EQUAL_COLLECTIONS(colIndices.getData(),
		colIndices.getData() + numJacobianEntries,
		compareColIndices.getData(),
		compareColIndices.getData() + numJacobianEntries);

	delete FMIGenericEso;

}

/**
* @test FMIGenericEsoTest - check function getAllBounds
* @todo complete or delete this test
*/
BOOST_AUTO_TEST_CASE(testBounds)
{

	printf("Boost Test testBounds. \n");

	// function does nothing 
	// however FMU would allow functionality to set bounds in accordance to the model inside FMU
	GenericEso *FMIGenericEso;
	GenericEsoFactory genEsoFac;
	BOOST_REQUIRE_NO_THROW(FMIGenericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI,1e-8));
	const EsoIndex numVariables = FMIGenericEso->getNumVariables();
	utils::Array<EsoDouble> lowerBounds(numVariables);
	utils::Array<EsoDouble> upperBounds(numVariables);
	FMIGenericEso->getAllBounds(numVariables, lowerBounds.getData(), upperBounds.getData());
	delete FMIGenericEso;
}

/**
* @test FMIGenericEsoTest - check functions get/setIndependentVariable
*/
BOOST_AUTO_TEST_CASE(testIndependentVariable)
{

	printf("Boost Test testIndependentVariable.\n");

	// independent variable is not yet implemented for the FMU inteferace
	// however FMU would allow it, must be implemented 
	EsoDouble d = 3;
	GenericEso *FMIGenericEso;
	GenericEsoFactory genEsoFac;
	BOOST_REQUIRE_NO_THROW(FMIGenericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI,1e-8));
	FMIGenericEso->setIndependentVariable(d);
	BOOST_CHECK_EQUAL(d, FMIGenericEso->getIndependentVariable());

	
	delete FMIGenericEso;
}



/**
* @test FMIGenericEsoTest - check function getJacobianMultVector
*/
BOOST_AUTO_TEST_CASE(testJacobianMultVector)
{

	printf("Boost Test testJacobianMultVector.\n");

	GenericEso *FMIGenericEso;
	GenericEsoFactory genEsoFac;
	BOOST_REQUIRE_NO_THROW(FMIGenericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI,1e-8));
	const EsoIndex numStates = FMIGenericEso->getNumStates();
	const EsoIndex numParams = FMIGenericEso->getNumParameters();
	const EsoIndex numEqns = FMIGenericEso->getNumEquations();
	utils::Array<EsoDouble> seedStates(numStates, 1.0);
	utils::Array<EsoDouble> seedParams(numParams, 1.0);
	utils::Array<EsoDouble> y(numEqns, 0.0);



	// check tangent-linear calculation
	bool transpose = false;




	FMIGenericEso->getJacobianMultVector(transpose, numStates, seedStates.getData(), numParams, seedParams.getData(), numEqns, y.getData());

	std::vector<EsoDouble> compY(numEqns, 0.0);
	compY[0] = 35;
	compY[1] = 1;
	compY[2] = -9;

	for (EsoIndex i = 0; i<numEqns; i++) {
    BOOST_CHECK_CLOSE(y[i], compY[i], floatingPointComparisonThreshold());
	}

	delete FMIGenericEso;
}



/**
* @test FMIGenericEsoTest - check function testBlockJacobian
*/
BOOST_AUTO_TEST_CASE(testBlockJacobian)
{

	printf("Boost Test testBlockJacobian. \n");

	GenericEso *FMIGenericEso;
	GenericEsoFactory genEsoFac;
	BOOST_REQUIRE_NO_THROW(FMIGenericEso = genEsoFac.createFMIGenericEso(PATH_FMI_GENERICESO_TEST_FMI,1e-8));

	int n_yy_ind = FMIGenericEso->getNumNonZeroes();
	std::vector<int> yy_ind(n_yy_ind);

	for (int i=0; i<n_yy_ind; i++) {
			yy_ind[i] = i;

	}



	std::vector<double> t1_states (FMIGenericEso->getNumEquations(),1);
	std::vector<double> t1_der_states (FMIGenericEso->getNumEquations(),1);
	std::vector<double> residuals (FMIGenericEso->getNumEquations(),99);
	std::vector<double> t1_p (FMIGenericEso->getNumParameters(),1);
	std::vector<double> t1_residuals (n_yy_ind,999);

	std::vector<EsoDouble> values(n_yy_ind,999);

	std::vector<EsoDouble> valuesCorrect(n_yy_ind);
	valuesCorrect[0] = 11;
	valuesCorrect[1] = -1;
	valuesCorrect[2] = 25;
	valuesCorrect[3] = 1;
	valuesCorrect[4] = -10;
	valuesCorrect[5] = 1;


	FMIGenericEso->getJacobianValues(n_yy_ind, values.data(),yy_ind.data());


	for (int i = 0; i < n_yy_ind; i++) {

		BOOST_CHECK_EQUAL(values[i], valuesCorrect[i]);


	}

	delete FMIGenericEso;
}


BOOST_AUTO_TEST_SUITE_END()
