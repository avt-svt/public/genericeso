/** 
* @file JadeGenericEso.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* In this testsuite the extended car model is tested                   \n
* =====================================================================\n
* 
* @author Moritz Schmitz, Tjalf Hoffmann
* @date 21.12.2011
*/

#include <vector>

#include "Array.hpp"

using namespace std;

BOOST_AUTO_TEST_SUITE(TestJadeGenericEso)

/**
* @test JadeGenericEsoTest - test initialization and deletion of an JadeGenericEso object
*
* @author Moritz Schmitz. Tjalf Hoffmann
* @date 21.12.2011
*/
BOOST_AUTO_TEST_CASE(TestInitializationJadeGenericEso)
{
  GenericEso *genericEso;
  BOOST_CHECK_NO_THROW(genericEso = genEsoFac.createJadeCarExample());
  BOOST_CHECK_NO_THROW(delete genericEso);
  
}

/**
* @test JadeGenericEsoTest - test all residual funcions
*/
BOOST_AUTO_TEST_CASE(TestGetResiduals)
{
  GenericEso *JadeGenericEso;
  BOOST_REQUIRE_NO_THROW(JadeGenericEso = genEsoFac.createJadeCarExample());
  const EsoIndex numAlgebraicEquations=1;
  const EsoIndex numDifferentialEquations=4;
  const EsoIndex numEquations=numAlgebraicEquations+numDifferentialEquations;
  
  BOOST_REQUIRE_EQUAL(numAlgebraicEquations, JadeGenericEso->getNumAlgEquations());


  utils::Array<EsoDouble> residuals(numEquations);
  std::vector<EsoDouble> compare(numEquations);

  compare[0] = -3;
  compare[1] = -0.99676;
  compare[2] = -1;
  compare[3] = -0.5;
  compare[4] = -15.5;
  BOOST_CHECK_NO_THROW(JadeGenericEso->getAllResiduals(numEquations, residuals.getData()));

  BOOST_REQUIRE_EQUAL(residuals.getSize(), numEquations);
  for(unsigned i=0;i<residuals.getSize(); i++){
    BOOST_CHECK_CLOSE(residuals[i], compare[i], THRESHOLD);
  }
  
  utils::Array<EsoDouble> differentialResiduals(numDifferentialEquations);
  BOOST_CHECK_NO_THROW(JadeGenericEso->getDifferentialResiduals(numDifferentialEquations, differentialResiduals.getData()));
  BOOST_REQUIRE_EQUAL(differentialResiduals.getSize(), numDifferentialEquations);
  for(unsigned i=0;i<differentialResiduals.getSize(); i++){
    BOOST_CHECK_CLOSE(differentialResiduals[i], compare[i], THRESHOLD);
  }

  utils::Array<EsoDouble> algebraicResiduals(numAlgebraicEquations);
  BOOST_CHECK_NO_THROW(JadeGenericEso->getAlgebraicResiduals(numAlgebraicEquations, algebraicResiduals.getData()));
  compare.erase(compare.begin(), compare.begin()+numDifferentialEquations);
  BOOST_REQUIRE_EQUAL(algebraicResiduals.getSize(), numAlgebraicEquations);
  for(unsigned i=0;i<algebraicResiduals.getSize(); i++){
    BOOST_CHECK_CLOSE(algebraicResiduals[i], compare[i], THRESHOLD);
  }

  delete JadeGenericEso;
}

/**
* @test JadeGenericEsoTest - test all variable functions
*/
BOOST_AUTO_TEST_CASE(TestVariablesESO)
{
  GenericEso *JadeGenericEso;
  BOOST_REQUIRE_NO_THROW(JadeGenericEso = genEsoFac.createJadeCarExample());
  const EsoIndex numAlgebraicVariables = 1;
  const EsoIndex numDifferentialVariables = 4;
  const EsoIndex numParameters = 1;
  const EsoIndex numVariables = numParameters + numDifferentialVariables + numAlgebraicVariables;
  const double defaultValue=0.0;

  BOOST_REQUIRE_EQUAL(numVariables, JadeGenericEso->getNumVariables());
  BOOST_REQUIRE_EQUAL(numAlgebraicVariables, JadeGenericEso->getNumAlgebraicVariables());
  BOOST_REQUIRE_EQUAL(numDifferentialVariables, JadeGenericEso->getNumDifferentialVariables());
  BOOST_REQUIRE_EQUAL(numParameters, JadeGenericEso->getNumParameters());

  utils::Array<EsoDouble> differentialVals(numDifferentialVariables);
  vector<EsoDouble> compareDiff(numDifferentialVariables);

  compareDiff[0] = 2.0;
  compareDiff[1] = 4.0;
  compareDiff[2] = 3.0;
  compareDiff[3] = 5.0;

  BOOST_CHECK_NO_THROW(JadeGenericEso->getDifferentialVariableValues(numDifferentialVariables, differentialVals.getData()));

  BOOST_REQUIRE_EQUAL(differentialVals.getSize(), numDifferentialVariables);
  for(unsigned i=0;i<differentialVals.getSize(); i++){
    BOOST_CHECK_CLOSE(differentialVals[i], compareDiff[i], THRESHOLD);
  }
  
  utils::Array<EsoDouble> parameters(numParameters);
  vector<EsoDouble> compareParams(numParameters);
  compareParams[0] = 1.0;

  BOOST_CHECK_NO_THROW(JadeGenericEso->getParameterValues(numParameters, parameters.getData()));
  
  BOOST_REQUIRE_EQUAL(parameters.getSize(), numParameters);
  for(unsigned i=0;i<parameters.getSize(); i++){
    BOOST_CHECK_CLOSE(parameters[i], compareParams[i], THRESHOLD);
  }
  
  utils::Array<EsoDouble> algebraicVals(numAlgebraicVariables);
  vector<EsoDouble> compareAlgeb(numAlgebraicVariables);
  compareAlgeb[0] = 0.5;

  BOOST_CHECK_NO_THROW(JadeGenericEso->getAlgebraicVariableValues(numAlgebraicVariables, algebraicVals.getData()));
  
  BOOST_REQUIRE_EQUAL(algebraicVals.getSize(), numAlgebraicVariables);
  for(unsigned i=0;i<algebraicVals.getSize(); i++){
    BOOST_CHECK_CLOSE(algebraicVals[i], compareAlgeb[i], THRESHOLD);
  }


  utils::Array<EsoDouble> allVariables(numVariables);
  vector<EsoDouble> compareVariables(numVariables);

  compareVariables[0] = 2.0;
  compareVariables[1] = 4.0;
  compareVariables[2] = 3.0;
  compareVariables[3] = 5.0;
  compareVariables[4] = 0.5;
  compareVariables[5] = 1.0;

  BOOST_CHECK_NO_THROW(JadeGenericEso->getAllVariableValues(numVariables, allVariables.getData()));

  BOOST_REQUIRE_EQUAL(allVariables.getSize(), numVariables);
  for(EsoIndex i=0; i<numVariables; i++){
    BOOST_CHECK_CLOSE(allVariables[i], compareVariables[i], THRESHOLD);
  }

  const EsoIndex numIndices = 4;
  utils::Array<EsoIndex> indices(numIndices);
  indices[0] = 2;
  indices[1] = 4;
  indices[2] = 1;
  indices[3] = 3;
  utils::Array<EsoDouble> variableSubset(numIndices);

  BOOST_CHECK_NO_THROW(JadeGenericEso->getVariableValues(numIndices, variableSubset.getData(), indices.getData()));

  for(EsoIndex i=0; i<numIndices; i++){
    BOOST_CHECK_CLOSE(variableSubset[i], compareVariables[indices[i]], THRESHOLD);
  }

  delete JadeGenericEso;
}

/**
* @test JadeGenericEsoTest - set variables and test all variable functions and check for correct residuals
*/
BOOST_AUTO_TEST_CASE(TestSetVariablesESO)
{
  GenericEso *JadeGenericEso;
  BOOST_REQUIRE_NO_THROW(JadeGenericEso = genEsoFac.createJadeCarExample());
  const EsoIndex numAlgebraicVariables = 1;
  const EsoIndex numDifferentialVariables = 4;
  const EsoIndex numParameters = 1;
  const EsoIndex numVariables = numParameters + numDifferentialVariables + numAlgebraicVariables;
  const EsoDouble defaultValue = 1.0;

  BOOST_REQUIRE_EQUAL(numVariables, JadeGenericEso->getNumVariables());
  BOOST_REQUIRE_EQUAL(numAlgebraicVariables, JadeGenericEso->getNumAlgebraicVariables());
  BOOST_REQUIRE_EQUAL(numDifferentialVariables, JadeGenericEso->getNumDifferentialVariables());
  BOOST_REQUIRE_EQUAL(numParameters, JadeGenericEso->getNumParameters());

  utils::Array<EsoDouble> differentialVals(numDifferentialVariables);
  utils::Array<EsoDouble> compareDiff(numDifferentialVariables);

  compareDiff[0] = 2*defaultValue;
  compareDiff[1] = 3*defaultValue;
  compareDiff[2] = 4*defaultValue;
  compareDiff[3] = 0.5*defaultValue;

  JadeGenericEso->setDifferentialVariableValues(numDifferentialVariables, compareDiff.getData());
  JadeGenericEso->getDifferentialVariableValues(numDifferentialVariables, differentialVals.getData());

  BOOST_REQUIRE_EQUAL(differentialVals.getSize(), numDifferentialVariables);
  for(unsigned i=0; i<differentialVals.getSize(); i++){
    BOOST_CHECK_CLOSE(differentialVals[i], compareDiff[i], THRESHOLD);
  }
  
  utils::Array<EsoDouble> parameters(numParameters);
  utils::Array<EsoDouble> compareParams(numParameters);
  compareParams[0] = 3.9;
  JadeGenericEso->setParameterValues(numParameters, compareParams.getData());
  JadeGenericEso->getParameterValues(numParameters, parameters.getData());
  
  BOOST_REQUIRE_EQUAL(parameters.getSize(), numParameters);
  for(unsigned i=0; i<parameters.getSize(); i++){
    BOOST_CHECK_CLOSE(parameters[i], compareParams[i], THRESHOLD);
  }
  
  utils::Array<EsoDouble> algebraicVals(numAlgebraicVariables);
  utils::Array<EsoDouble> compareAlgeb(numAlgebraicVariables);
  compareAlgeb[0] = defaultValue + 15;
  JadeGenericEso->setAlgebraicVariableValues(numAlgebraicVariables, compareAlgeb.getData());
  JadeGenericEso->getAlgebraicVariableValues(numAlgebraicVariables, algebraicVals.getData());
  
  for(unsigned i=0; i<algebraicVals.getSize(); i++){
    BOOST_CHECK_CLOSE(algebraicVals[i], compareAlgeb[i], THRESHOLD);
  }
  
  const EsoIndex numStates = JadeGenericEso->getNumStates();
  BOOST_CHECK_EQUAL(numStates, numAlgebraicVariables + numDifferentialVariables);
  utils::Array<EsoDouble> stateVals(numStates);
  utils::Array<EsoDouble> compareStates(numStates);
  compareStates[0] = 12.45;
  compareStates[1] = 34.98;
  compareStates[2] = -17.6;
  compareStates[3] = 1.17;
  compareStates[4] = 85.42;
  JadeGenericEso->setStateValues(numStates, compareStates.getData());
  JadeGenericEso->getStateValues(numStates, stateVals.getData());
  
  for(unsigned i=0; i<stateVals.getSize(); i++){
    BOOST_CHECK_CLOSE(stateVals[i], compareStates[i], THRESHOLD);
  }
  
  utils::Array<EsoDouble> derivativeVals(numDifferentialVariables);
  utils::Array<EsoDouble> compareDerivs(numDifferentialVariables);
  compareDerivs[0] = 23.41;
  compareDerivs[1] = 1.09;
  compareDerivs[2] = -23.43;
  compareDerivs[3] = 4.19;
  JadeGenericEso->setDerivativeValues(numDifferentialVariables, compareDerivs.getData());
  JadeGenericEso->getDerivativeValues(numDifferentialVariables, derivativeVals.getData());
  
  for(unsigned i=0; i<derivativeVals.getSize(); i++){
    BOOST_CHECK_CLOSE(derivativeVals[i], compareDerivs[i], THRESHOLD);
  }
  
  const EsoIndex n_idx = 4;
  utils::Array<EsoIndex> indices(n_idx);
  utils::Array<EsoDouble> varVals(n_idx);
  utils::Array<EsoDouble> compVarVals(n_idx);
  indices[0] = 0;
  indices[1] = 3;
  indices[2] = 4;
  indices[3] = 5;
  compVarVals[0] = 13.4;
  compVarVals[1] = -7.41;
  compVarVals[2] = 6.234;
  compVarVals[3] = 71.0;
  JadeGenericEso->setVariableValues(n_idx, compVarVals.getData(), indices.getData());
  JadeGenericEso->getVariableValues(n_idx, varVals.getData(), indices.getData());

  for(EsoIndex i=0; i<n_idx; i++){
    BOOST_CHECK_CLOSE(varVals[i], compVarVals[i], THRESHOLD);
  } 
  
  utils::Array<EsoDouble> allVariables(numVariables);
  utils::Array<EsoDouble> compareVariables(numVariables);

  compareVariables[0] = 2.2;
  compareVariables[1] = 4.3;
  compareVariables[2] = 8.4;
  compareVariables[3] = 6.5;
  compareVariables[3] = 9.75;
  compareVariables[4] = -3.6;
  compareVariables[5] = -5.0;

  JadeGenericEso->setAllVariableValues(numVariables, compareVariables.getData());
  JadeGenericEso->getAllVariableValues(numVariables, allVariables.getData());

  vector<string> variableNames(numVariables);
  JadeGenericEso->getVariableNames(variableNames);

  BOOST_REQUIRE_EQUAL(allVariables.getSize(), numVariables);
  for(EsoIndex i=0; i<numVariables; i++){
    BOOST_CHECK_CLOSE(allVariables[i], compareVariables[i], THRESHOLD);
  }
  delete JadeGenericEso;
}

/**
* @test JadeGenericEsoTest - check if Jacobian struct information is correct
*/
BOOST_AUTO_TEST_CASE(testJacobian)
{
  GenericEso *JadeGenericEso;
  BOOST_REQUIRE_NO_THROW(JadeGenericEso = genEsoFac.createJadeCarExample());
  const EsoIndex numJacobianEntries = 6;
  const EsoIndex numDiffJacobianEntries = 4;

  BOOST_REQUIRE_EQUAL(numJacobianEntries, JadeGenericEso->getNumNonZeroes());
  BOOST_REQUIRE_EQUAL(numDiffJacobianEntries, JadeGenericEso->getNumDifferentialNonZeroes());

  utils::Array<EsoIndex> compareRowIndices(numJacobianEntries);
  compareRowIndices[0] = 0;
  compareRowIndices[1] = 1;
  compareRowIndices[2] = 1;
  compareRowIndices[3] = 3;
  compareRowIndices[4] = 4;
  compareRowIndices[5] = 4;

  utils::Array<EsoIndex> compareColIndices(numJacobianEntries);
  compareColIndices[0] = 2;
  compareColIndices[1] = 2;
  compareColIndices[2] = 5;
  compareColIndices[3] = 4;
  compareColIndices[4] = 4;
  compareColIndices[5] = 5;

  utils::Array<EsoIndex> rowIndices(numJacobianEntries);
  utils::Array<EsoIndex> colIndices(numJacobianEntries);
  JadeGenericEso->getJacobianStruct(numJacobianEntries, rowIndices.getData(),
                                       colIndices.getData());

  BOOST_CHECK_EQUAL_COLLECTIONS(rowIndices.getData(),
                                rowIndices.getData()+numJacobianEntries,
                                compareRowIndices.getData(),
                                compareRowIndices.getData()+numJacobianEntries);
  BOOST_CHECK_EQUAL_COLLECTIONS(colIndices.getData(),
                                colIndices.getData()+numJacobianEntries,
                                compareColIndices.getData(),
                                compareColIndices.getData()+numJacobianEntries);
  delete JadeGenericEso;

}

/**
* @test JadeGenericEsoTest - check correctness of differential Jacobian struct information
*/
BOOST_AUTO_TEST_CASE(testDiffJacobianStruct)
{
  GenericEso *JadeGenericEso;
  BOOST_REQUIRE_NO_THROW(JadeGenericEso = genEsoFac.createJadeCarExample());
  const EsoIndex numDiffJacobianEntries = JadeGenericEso->getNumDifferentialNonZeroes();
  utils::Array<EsoIndex> rowIndices(numDiffJacobianEntries);
  utils::Array<EsoIndex> colIndices(numDiffJacobianEntries);
  JadeGenericEso->getDiffJacobianStruct(numDiffJacobianEntries, rowIndices.getData(),
                                           colIndices.getData());

  utils::Array<EsoIndex> compareRowIndices(numDiffJacobianEntries);
  compareRowIndices[0] = 0;
  compareRowIndices[1] = 1;
  compareRowIndices[2] = 2;
  compareRowIndices[3] = 3;

  utils::Array<EsoIndex> compareColIndices(numDiffJacobianEntries);
  compareColIndices[0] = 0;
  compareColIndices[1] = 2;
  compareColIndices[2] = 1;
  compareColIndices[3] = 3;

  BOOST_CHECK_EQUAL_COLLECTIONS(rowIndices.getData(),
                                rowIndices.getData()+numDiffJacobianEntries,
                                compareRowIndices.getData(),
                                compareRowIndices.getData()+numDiffJacobianEntries);
  BOOST_CHECK_EQUAL_COLLECTIONS(colIndices.getData(),
                                colIndices.getData()+numDiffJacobianEntries,
                                compareColIndices.getData(),
                                compareColIndices.getData()+numDiffJacobianEntries);
  delete JadeGenericEso;
}

/**
* @test JadeGenericEsoTest - check function getAllBounds
* @todo complete or delete this test
*/
BOOST_AUTO_TEST_CASE(testBounds)
{
  GenericEso *JadeGenericEso;
  BOOST_REQUIRE_NO_THROW(JadeGenericEso = genEsoFac.createJadeCarExample());
  const EsoIndex numVariables = JadeGenericEso->getNumVariables();
  utils::Array<EsoDouble> lowerBounds(numVariables);
  utils::Array<EsoDouble> upperBounds(numVariables);
  JadeGenericEso->getAllBounds(numVariables, lowerBounds.getData(), upperBounds.getData());
  delete JadeGenericEso;
}

/**
* @test JadeGenericEsoTest - check functions get/setIndependentVariable
*/
BOOST_AUTO_TEST_CASE(testIndependentVariable)
{
  EsoDouble d=3;
  GenericEso *JadeGenericEso;
  BOOST_REQUIRE_NO_THROW(JadeGenericEso = genEsoFac.createJadeCarExample());
  JadeGenericEso->setIndependentVariable(d);
  BOOST_CHECK_EQUAL(d, JadeGenericEso->getIndependentVariable());
  delete JadeGenericEso;
}

/** 
* @test JadeGenericEsoTest - check function getJacobianMultVector
*/
BOOST_AUTO_TEST_CASE(testJacobianMultVector)
{
  GenericEso *JadeGenericEso;
  BOOST_REQUIRE_NO_THROW(JadeGenericEso = genEsoFac.createJadeCarExample());
  const EsoIndex numStates = JadeGenericEso->getNumStates();
  const EsoIndex numParams = JadeGenericEso->getNumParameters();
  const EsoIndex numEqns = JadeGenericEso->getNumEquations();
  utils::Array<EsoDouble> seedStates(numStates,1.0);
  utils::Array<EsoDouble> seedParams(numStates,1.0);
  utils::Array<EsoDouble> y(numEqns,0.0);
  
  // check tangent-linear calculation
  bool transpose = false;
  utils::Array<EsoDouble> compY(numEqns,0.0);
  
  compY[0] = -1.0;
  compY[1] = -0.99784;
  compY[2] = 0.0;
  compY[3] = -1.0;
  compY[4] = -14.0;
  JadeGenericEso->getJacobianMultVector(transpose, numStates, seedStates.getData(),
                                           numParams, seedParams.getData(), numEqns, y.getData());

  for(EsoIndex i=0; i<numEqns; i++){
    BOOST_CHECK_CLOSE(y[i], compY[i], THRESHOLD);
  }
  
  // check adjoint calculation
  transpose = true;
  utils::Array<EsoDouble> compSeedStates(numStates,0.0);
  utils::Array<EsoDouble> compSeedParams(numParams,0.0);
  
  compSeedStates[0] = 0.0;
  compSeedStates[1] = 0.0;
  compSeedStates[2] = -0.99784;
  compSeedStates[3] = 0.0;
  compSeedStates[4] = 0.0;
  compSeedParams[0] = -16.0;
  
  utils::Array<EsoDouble> a1_y(numEqns,1.0);
  JadeGenericEso->getJacobianMultVector(transpose, numStates, seedStates.getData(),
                                         numParams, seedParams.getData(), numEqns, a1_y.getData());

  for(EsoIndex i=0; i<numStates; i++){
    BOOST_CHECK_CLOSE(seedStates[i], compSeedStates[i], THRESHOLD);
  }
  for(EsoIndex i=0; i<numParams; i++){
    BOOST_CHECK_CLOSE(seedParams[i], compSeedParams[i], THRESHOLD);
  }
  delete JadeGenericEso;
}

/** 
* @test JadeGenericEsoTest - check function eval_t2_a1_residuals
*/
BOOST_AUTO_TEST_CASE(testEval_t2_a1_residuals)
{
  JadeGenericEso *JadeGenericEso = genEsoFac.createJadeCarExample();
  const EsoIndex numStates = JadeGenericEso->getNumStates();
  const EsoIndex numParams = JadeGenericEso->getNumParameters();
  const EsoIndex numEqns = JadeGenericEso->getNumEquations();
  utils::Array<EsoDouble> t2_states(numStates,1.0);
  utils::Array<EsoDouble> t2_der_states(numStates,1.0);
  utils::Array<EsoDouble> t2_p(numParams,1.0);
  utils::Array<EsoDouble> a1_states(numStates,0.0); // output
  utils::Array<EsoDouble> a1_der_states(numStates,0.0); // output
  utils::Array<EsoDouble> a1_p(numParams,0.0); // output
  utils::Array<EsoDouble> t2_a1_states(numStates,0.0); // output
  utils::Array<EsoDouble> t2_a1_der_states(numStates,0.0); // output
  utils::Array<EsoDouble> t2_a1_p(numParams,0.0); // output
  utils::Array<EsoDouble> a1_yy(numEqns,1.0);
  utils::Array<EsoDouble> t2_a1_yy(numEqns,1.0);
  
  utils::Array<EsoDouble> comp_a1_states(numStates);
  utils::Array<EsoDouble> comp_a1_der_states(numStates);
  utils::Array<EsoDouble> comp_a1_p(numParams);
  utils::Array<EsoDouble> comp_t2_a1_states(numStates);
  utils::Array<EsoDouble> comp_t2_a1_der_states(numStates);
  utils::Array<EsoDouble> comp_t2_a1_p(numParams);
  
  comp_a1_states[0] = 0.0;
  comp_a1_states[1] = 0.0;
  comp_a1_states[2] = -0.99784;
  comp_a1_states[3] = 0.0;
  comp_a1_states[4] = 0.0;
  comp_a1_der_states[0] = 1.0;
  comp_a1_der_states[1] = 1.0;
  comp_a1_der_states[2] = 1.0;
  comp_a1_der_states[3] = 1.0;
  comp_a1_der_states[4] = 0.0;
  comp_a1_p[0] = -17.361763731773432181354662;
  // we have to add the values of comp_a1_z because t2_a1_yy = 1.0
  // see formula below
  comp_t2_a1_states[0] = 0.0 +      comp_a1_states[0];
  comp_t2_a1_states[1] = 0.0 +      comp_a1_states[1];
  comp_t2_a1_states[2] = 0.00072 +  comp_a1_states[2];
  comp_t2_a1_states[3] = 0.0 +      comp_a1_states[3];
  comp_t2_a1_states[4] = 0.0 +      comp_a1_states[4];
  comp_t2_a1_der_states[0] = 0.0 + comp_a1_der_states[0];
  comp_t2_a1_der_states[1] = 0.0 + comp_a1_der_states[1];
  comp_t2_a1_der_states[2] = 0.0 + comp_a1_der_states[2];
  comp_t2_a1_der_states[3] = 0.0 + comp_a1_der_states[3];
  comp_t2_a1_der_states[4] = 0.0 + comp_a1_der_states[4];
  comp_t2_a1_p[0] = -1501.8650608412072895037628158 + comp_a1_p[0];
  
  const EsoIndex n_pars = 1;
  utils::Array<EsoDouble> parValues(n_pars);
  utils::Array<EsoIndex> parIndices(n_pars);
  parValues[0] = 0.001; // fix a new value for parameter 'a'
  parIndices[0] = 5;
  // to avoid zero second-order derivatives for equation with index 4
  JadeGenericEso->setVariableValues(n_pars, parValues.getData(), parIndices.getData());
  
  // output definition: a1_z = (dF(z)/dz)^T * a1_yy;
  //                    t2_a1_z = <d2F(z)/dzdz, a1_yy, t2_z> + <dF(z)/dz, t2_a1_yy>;
  
  JadeGenericEso->evaluate2ndOrderDerivatives(t2_states.getSize(), t2_states.getData(),
                                                 t2_der_states.getSize(), t2_der_states.getData(),
                                                 t2_p.getSize(), t2_p.getData(),
                                                 a1_states.getSize(), a1_states.getData(),
                                                 a1_der_states.getSize(), a1_der_states.getData(),
                                                 a1_p.getSize(), a1_p.getData(),
                                                 t2_a1_states.getSize(), t2_a1_states.getData(),
                                                 t2_a1_der_states.getSize(), t2_a1_der_states.getData(),
                                                 t2_a1_p.getSize(), t2_a1_p.getData(),
                                                 a1_yy.getSize(), a1_yy.getData(),
                                                 t2_a1_yy.getSize(), t2_a1_yy.getData());

  for(EsoIndex i=0; i<numStates; i++){
    BOOST_CHECK_CLOSE(a1_states[i], comp_a1_states[i], THRESHOLD);
    BOOST_CHECK_CLOSE(a1_der_states[i], comp_a1_der_states[i], THRESHOLD);
    BOOST_CHECK_CLOSE(t2_a1_states[i], comp_t2_a1_states[i], THRESHOLD);
    BOOST_CHECK_CLOSE(t2_a1_der_states[i], comp_t2_a1_der_states[i], THRESHOLD);
  }

  BOOST_CHECK_CLOSE(a1_p[0], comp_a1_p[0], THRESHOLD);
  BOOST_CHECK_CLOSE(t2_a1_p[0],comp_t2_a1_p[0], 1.5*THRESHOLD); //to remove obscure error in release mode
  
  delete JadeGenericEso;
}

/**
* @brief test functions for the initial model
*
* As initial model the GenericEsoTestInitial model is used having these equations:
*
* x = Para * Cont
* y = 5*Para
* z = 0.37256
*
* tested functions are:
* getNumInitialNonZeroes
* getInitialJacobian
* setInitialValues
* getInitialStates
*/
BOOST_AUTO_TEST_CASE(TestInitialModel)
{
  GenericEsoFactory genEsoFac;
  JadeGenericEso *JadeGenericEso = genEsoFac.createJadeGenericEso("GenericEsoTest", "GenericEsoTestInitial");
  
  //test getNumInitialNonZeroes
  const EsoIndex numInitialNonZeroes = JadeGenericEso->getNumInitialNonZeroes();
  BOOST_CHECK_EQUAL(numInitialNonZeroes, 6);
  
  utils::Array<EsoIndex> rowIndices(numInitialNonZeroes), colIndices(numInitialNonZeroes);
  utils::Array<EsoDouble> values(numInitialNonZeroes);
  BOOST_CHECK_EQUAL(JadeGenericEso->getInitialJacobian(numInitialNonZeroes, rowIndices.getData(),
                                                          colIndices.getData(), values.getData()),
                    GenericEso::OK);
  BOOST_CHECK_EQUAL(rowIndices[0], 0);
  BOOST_CHECK_EQUAL(rowIndices[1], 0);
  BOOST_CHECK_EQUAL(rowIndices[2], 0);
  BOOST_CHECK_EQUAL(rowIndices[3], 1);
  BOOST_CHECK_EQUAL(rowIndices[4], 1);
  BOOST_CHECK_EQUAL(rowIndices[5], 2);
  
  BOOST_CHECK_EQUAL(colIndices[0], 1);
  BOOST_CHECK_EQUAL(colIndices[1], 6);
  BOOST_CHECK_EQUAL(colIndices[2], 7);
  BOOST_CHECK_EQUAL(colIndices[3], 2);
  BOOST_CHECK_EQUAL(colIndices[4], 6);
  BOOST_CHECK_EQUAL(colIndices[5], 3);
  
  BOOST_CHECK_CLOSE(values[0], 1.0, THRESHOLD);
  BOOST_CHECK_CLOSE(values[1], -3.7, THRESHOLD);
  BOOST_CHECK_CLOSE(values[2], -1.3, THRESHOLD);
  BOOST_CHECK_CLOSE(values[3], 1.0, THRESHOLD);
  BOOST_CHECK_CLOSE(values[4], -5.0, THRESHOLD);
  BOOST_CHECK_CLOSE(values[5], 1.0, THRESHOLD);
  
  //test setInitialValues
  //set diffVal y and consider the parameter Cont as set (use value if the model)
  //y, z and Para should be calculated
  EsoIndex numDiffVals = 1, numParameters = 1;
  utils::Array<EsoIndex> diffIndices(numDiffVals, 2), parameterIndices(numDiffVals, 7);
  utils::Array<EsoDouble> diffValues(numDiffVals, 0.75);
  BOOST_CHECK_EQUAL(JadeGenericEso->setInitialValues(numDiffVals, diffIndices.getData(),
                                                                     diffValues.getData(),
                                                        numParameters, parameterIndices.getData()),
                    GenericEso::OK);

  //test function getInitialStateValues
  EsoIndex numStates = JadeGenericEso->getNumStates();
  BOOST_REQUIRE_EQUAL(numStates, 6);
  utils::Array<EsoDouble> initialStates(numStates);
  JadeGenericEso->getInitialStateValues(numStates, initialStates.getData());
  
  BOOST_CHECK_CLOSE(initialStates[0], 0.0, THRESHOLD);
  BOOST_CHECK_CLOSE(initialStates[1], 0.555, THRESHOLD);
  BOOST_CHECK_CLOSE(initialStates[2], 0.75, THRESHOLD);
  BOOST_CHECK_CLOSE(initialStates[3], 0.37256, THRESHOLD);
  delete JadeGenericEso;
  //don't test the last two states, since these are algebraic states of the main model
  
}

BOOST_AUTO_TEST_SUITE_END()