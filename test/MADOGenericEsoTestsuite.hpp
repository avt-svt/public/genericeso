/** 
* @file MADOGenericEso.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrennstechnik, RWTH Aachen          \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* In this testsuite the extended WO batch model is tested              \n
* =====================================================================\n
* 
* @author Lars Henrichfreise
* @date 05.02.2020
*/

#include <vector>
#include <memory>
#include<stdio.h>
#include<stdlib.h>
#include "Array.hpp"

using namespace std;



BOOST_AUTO_TEST_SUITE(TestMADOGenericEso)

/**
* @test MADOGenericEsoTest - test initialization and deletion of an MADOGenericEso object
*
* @author Lars Henrichfreise
* @date 05.02.2020
*/
BOOST_AUTO_TEST_CASE(TestInitializationMADOGenericEso)
{
  GenericEso *genericEso;
  BOOST_CHECK_NO_THROW(genericEso = genEsoFac.createMADOmodelWoBatchExample());
  BOOST_CHECK_NO_THROW(delete genericEso);

}

BOOST_AUTO_TEST_CASE(TestModelMembers)
{
	GenericEso *MADOGenericEso;
	MADOGenericEso = genEsoFac.createMADOmodelWoBatchExample();
	const EsoIndex numAlgebraicEquations = 3;
	const EsoIndex numDifferentialEquations = 9;
	const EsoIndex numEquations = numAlgebraicEquations + numDifferentialEquations;
	const EsoIndex numVariables = 14;
	const EsoIndex numAllVariables = 23;
	const EsoIndex numDifferentialVariables = 9;
	const EsoIndex numParameters = 2;
	const EsoIndex numAlgebraicVariables = 3;
	const EsoIndex numStates = 12;
	const EsoIndex numNonZeros = 56;  //verify
	const EsoIndex numDiffNonZeros = 9; //verify
	const EsoIndex numConditions = 0;


	std::vector<std::string> names;
	MADOGenericEso->getVariableNames(names);
	std::vector<std::string> varnames_correct = { "x1","x2","x3","x4","x5","x6","x7","x8","x9","derX7","Fbin","Tw","TwCur","FbinCur","der_x1","der_x2","der_x3","der_x4","der_x5","der_x6","der_x7","der_x8","der_x9" };
	for (unsigned i = 0; i < numVariables; i++) {
		BOOST_CHECK_EQUAL(varnames_correct[i], names[i]);
	}

	BOOST_REQUIRE_EQUAL(numAlgebraicEquations, MADOGenericEso->getNumAlgEquations());
	BOOST_REQUIRE_EQUAL(numDifferentialEquations, MADOGenericEso->getNumDiffEquations());
	BOOST_REQUIRE_EQUAL(numEquations, MADOGenericEso->getNumEquations());
	BOOST_REQUIRE_EQUAL(numVariables, MADOGenericEso->getNumVariables());
	BOOST_REQUIRE_EQUAL(numDifferentialVariables, MADOGenericEso->getNumDifferentialVariables());
	BOOST_REQUIRE_EQUAL(numParameters, MADOGenericEso->getNumParameters());
	BOOST_REQUIRE_EQUAL(numAlgebraicVariables, MADOGenericEso->getNumAlgebraicVariables());
	BOOST_REQUIRE_EQUAL(numStates, MADOGenericEso->getNumStates());
	BOOST_REQUIRE_EQUAL(numNonZeros, MADOGenericEso->getNumNonZeroes());
	BOOST_REQUIRE_EQUAL(numDiffNonZeros, MADOGenericEso->getNumDifferentialNonZeroes());
	BOOST_REQUIRE_EQUAL(numConditions, MADOGenericEso->getNumConditions());


	delete MADOGenericEso;
}

BOOST_AUTO_TEST_CASE(TestVariablesSetterAndGetter)
{
	GenericEso *MADOGenericEso;
	MADOGenericEso = genEsoFac.createMADOmodelWoBatchExample();
	const unsigned numVariables = 14;
	double x1, x2, x3, x4, x5, x6, x7, x8, x9;
	double Fbin;
	double Tw;
	double derX7;

	double* x      = new double[numVariables];
	double* x_test = new double[numVariables];

//	double* x = (double *)calloc(numVariables, sizeof(double));
//	double* x_test = (double *)calloc(numVariables, sizeof(double));

	x1 = 1;
	x2 = 1;
	x3 = 1;
	x4 = 1;
	x5 = 1;
	x6 = 1;
	x7 = 1;
	x8 = 1;
	x9 = 1;
	derX7 = 1;
	Fbin = 1;
	Tw = 1;
	double TwCur = 0.02;
	double FbinCur = 5.784;

	x_test[0] = x1;
	x_test[1] = x2;
	x_test[2] = x3;
	x_test[3] = x4;
	x_test[4] = x5;
	x_test[5] = x6;
	x_test[6] = x7;
	x_test[7] = x8;
	x_test[8] = x9;
	x_test[9] = derX7;
	x_test[10] = Fbin;
	x_test[11] = Tw;
    x_test[12] = TwCur;
    x_test[13] = FbinCur;


 	BOOST_CHECK_NO_THROW(MADOGenericEso->setAllVariableValues(numVariables, x_test));

	BOOST_CHECK_NO_THROW(MADOGenericEso->getAllVariableValues(numVariables, x));

	for (unsigned i = 0; i < numVariables; i++) {
		BOOST_CHECK_EQUAL(x_test[i], x[i]);
	}



	const EsoIndex numParam = 2;
	double* par_test = new double[numParam];
	par_test[0] = TwCur+1;
	par_test[1] = FbinCur+1;
	BOOST_CHECK_NO_THROW(MADOGenericEso->setParameterValues(numParam, par_test));
	
	double* par = new double[numParam];
	BOOST_CHECK_NO_THROW(MADOGenericEso->getParameterValues(numParam, par));
	for (unsigned i = 0; i < numParam; i++) {
		BOOST_CHECK_EQUAL(par_test[i], par[i]);
	}
	delete[] par_test;
	delete[] par;


	const EsoIndex numAlg = 3;
	double* alg_test = new double[numAlg];
	alg_test[0] = derX7 + 1;
	alg_test[1] = Fbin + 1;
	alg_test[2] = Tw + 1;
	BOOST_CHECK_NO_THROW(MADOGenericEso->setAlgebraicVariableValues(numAlg, alg_test));

	double* alg = new double[numAlg];
	BOOST_CHECK_NO_THROW(MADOGenericEso->getAlgebraicVariableValues(numAlg, alg));
	for (unsigned i = 0; i < numAlg; i++) {
		BOOST_CHECK_EQUAL(alg_test[i], alg[i]);
	}
	delete[] alg_test;
	delete[] alg;

	const EsoIndex numDiffVar = 9;
	double* diffVar_test = new double[numDiffVar];
	diffVar_test[0] = x1+1;
	diffVar_test[1] = x2+1;
	diffVar_test[2] = x3+1;
	diffVar_test[3] = x4+1;
	diffVar_test[4] = x5+1;
	diffVar_test[5] = x6+1;
	diffVar_test[6] = x7+1;
	diffVar_test[7] = x8+1;
	diffVar_test[8] = x9+1;
	BOOST_CHECK_NO_THROW(MADOGenericEso->setDifferentialVariableValues(numDiffVar, diffVar_test));

	double* diffVar = new double[numDiffVar];
	BOOST_CHECK_NO_THROW(MADOGenericEso->getDifferentialVariableValues(numDiffVar, diffVar));
	for (unsigned i = 0; i < numDiffVar; i++) {
		BOOST_CHECK_EQUAL(diffVar_test[i], diffVar[i]);
	}
	delete[] diffVar_test;
	delete[] diffVar;

	const EsoIndex numStates = 12;
	double* states_test = new double[numStates];
	states_test[0] = x1 + 2;
	states_test[1] = x2 + 2;
	states_test[2] = x3 + 2;
	states_test[3] = x4 + 2;
	states_test[4] = x5 + 2;
	states_test[5] = x6 + 2;
	states_test[6] = x7 + 2;
	states_test[7] = x8 + 2;
	states_test[8] = x9 + 2;
	states_test[9] = derX7 + 2;
	states_test[10] = Fbin + 2;
	states_test[11] = Tw + 2;
	BOOST_CHECK_NO_THROW(MADOGenericEso->setStateValues(numStates, states_test));

	double* states = new double[numStates];
	BOOST_CHECK_NO_THROW(MADOGenericEso->getStateValues(numStates, states));
	for (unsigned i = 0; i < numStates; i++) {
		BOOST_CHECK_EQUAL(states_test[i], states[i]);
	}
	delete[] states_test;
	delete[] states;


	double* derVal_test = new double[numDiffVar];
	derVal_test[0] = 1;
	derVal_test[1] = 1;
	derVal_test[2] = 1;
	derVal_test[3] = 1;
	derVal_test[4] = 1;
	derVal_test[5] = 1;
	derVal_test[6] = 1;
	derVal_test[7] = 1;
	derVal_test[8] = 1;
	BOOST_CHECK_NO_THROW(MADOGenericEso->setDerivativeValues(numDiffVar, derVal_test));

	double* derVal = new double[numDiffVar];
	BOOST_CHECK_NO_THROW(MADOGenericEso->getDerivativeValues(numDiffVar, derVal));
	for (unsigned i = 0; i < numDiffVar; i++) {
		BOOST_CHECK_EQUAL(derVal_test[i], derVal[i]);
	}
	delete[] derVal_test;
	delete[] derVal;


	const EsoIndex n_idx = 4;
	EsoDouble* varByIndex_test = new EsoDouble[n_idx];
	EsoIndex* test_indices = new EsoIndex[n_idx];
	varByIndex_test[0] = x1 + 3;
	varByIndex_test[1] = derX7 + 3;
	varByIndex_test[2] = FbinCur + 3;
	varByIndex_test[3] = 3;
	test_indices[0] = 0;
	test_indices[1] = 9;
	test_indices[2] = 13;
	test_indices[3] = 22;
	BOOST_CHECK_NO_THROW(MADOGenericEso->setVariableValues(n_idx, varByIndex_test, test_indices));

	EsoDouble* varByIndex = new EsoDouble[n_idx];
	BOOST_CHECK_NO_THROW(MADOGenericEso->getVariableValues(n_idx, varByIndex, test_indices));
	for (unsigned i = 0; i < n_idx; i++) {
		BOOST_CHECK_EQUAL(varByIndex_test[i], varByIndex[i]);
	}
	delete[] varByIndex;
	delete[] varByIndex_test;
	delete[] test_indices;

	x_test[0] = x1+3;
	x_test[1] = x2+2;
	x_test[2] = x3+2;
	x_test[3] = x4+2;
	x_test[4] = x5+2;
	x_test[5] = x6+2;
	x_test[6] = x7+2;
	x_test[7] = x8+2;
	x_test[8] = x9+2;
	x_test[9] = derX7+3;
	x_test[10] = Fbin+2;
	x_test[11] = Tw+2;
	x_test[12] = TwCur+1;
	x_test[13] = FbinCur+3;
	BOOST_CHECK_NO_THROW(MADOGenericEso->getAllVariableValues(numVariables, x));
	for (unsigned i = 0; i < numVariables; i++) {
		BOOST_CHECK_EQUAL(x_test[i], x[i]);
	}

	delete[] x_test;
	delete[] x;
    delete MADOGenericEso;
}

BOOST_AUTO_TEST_CASE(TestIndexing)
{
	GenericEso *MADOGenericEso;
	MADOGenericEso = genEsoFac.createMADOmodelWoBatchExample();
	const EsoIndex numAlgebraicEquations = 3;
	const EsoIndex numDifferentialEquations = 9;
	const EsoIndex numEquations = numAlgebraicEquations + numDifferentialEquations;
	const EsoIndex numVariables = 14;
	const EsoIndex numAllVariables = 23;
	const EsoIndex numDifferentialVariables = 9;
	const EsoIndex numParameters = 2;
	const EsoIndex numAlgebraicVariables = 3;
	const EsoIndex numStates = 12;

	EsoIndex* parIdx = new EsoIndex[numParameters];
	EsoIndex parIdx_correct[numParameters] = {12, 13};
	BOOST_CHECK_NO_THROW(MADOGenericEso->getParameterIndex(numParameters, parIdx));
	for (unsigned i = 0; i < numParameters; i++) {
		BOOST_CHECK_EQUAL(parIdx_correct[i], parIdx[i]);
	}

	EsoIndex* diffIdx = new EsoIndex[numDifferentialVariables];
	EsoIndex diffIdx_correct[numDifferentialVariables] = {0,1,2,3,4,5,6,7,8};
	BOOST_CHECK_NO_THROW(MADOGenericEso->getDifferentialIndex(numDifferentialVariables, diffIdx));
	for (unsigned i = 0; i < numDifferentialVariables; i++) {
		BOOST_CHECK_EQUAL(diffIdx_correct[i], diffIdx[i]);
	}

	EsoIndex* algIdx = new EsoIndex[numAlgebraicVariables];
	EsoIndex algIdx_correct[numAlgebraicVariables] = { 9, 10, 11 };
	BOOST_CHECK_NO_THROW(MADOGenericEso->getAlgebraicIndex(numAlgebraicVariables, algIdx));
	for (unsigned i = 0; i < numAlgebraicVariables; i++) {
		BOOST_CHECK_EQUAL(algIdx_correct[i], algIdx[i]);
	}

	EsoIndex* statesIdx = new EsoIndex[numStates];
	EsoIndex statesIdx_correct[numStates] = { 0,1,2,3,4,5,6,7,8,9,10,11 };
	BOOST_CHECK_NO_THROW(MADOGenericEso->getStateIndex(numStates, statesIdx));
	for (unsigned i = 0; i < numStates; i++) {
		BOOST_CHECK_EQUAL(statesIdx_correct[i], statesIdx[i]);
	}

	EsoIndex* diffEqnIdx = new EsoIndex[numDifferentialEquations];
	EsoIndex diffEqnIdx_correct[numDifferentialEquations] = { 0,1,2,3,4,5,6,7,8};
	BOOST_CHECK_NO_THROW(MADOGenericEso->getDiffEquationIndex(numDifferentialEquations, diffEqnIdx));
	for (unsigned i = 0; i < numDifferentialEquations; i++) {
		BOOST_CHECK_EQUAL(diffEqnIdx_correct[i], diffEqnIdx[i]);
	}

	EsoIndex* algEqnIdx = new EsoIndex[numAlgebraicEquations];
	EsoIndex algEqnIdx_correct[numAlgebraicEquations] = { 9,10,11 };
	BOOST_CHECK_NO_THROW(MADOGenericEso->getAlgEquationIndex(numAlgebraicEquations, algEqnIdx));
	for (unsigned i = 0; i < numAlgebraicEquations; i++) {
		BOOST_CHECK_EQUAL(algEqnIdx_correct[i], algEqnIdx[i]);
	}

	BOOST_CHECK_EQUAL(MADOGenericEso->getEsoIndexOfVariable("Fbin"), 10);
	BOOST_CHECK_EQUAL(MADOGenericEso->getVariableNameOfIndex(13),"FbinCur");
	BOOST_CHECK_EQUAL(MADOGenericEso->getVariableNameOfIndex(6), "x7");

	delete[] algEqnIdx;
	delete[] diffEqnIdx;
	delete[] statesIdx;
	delete[] algIdx;
	delete[] diffIdx;
	delete[] parIdx;
	delete MADOGenericEso;

}


BOOST_AUTO_TEST_CASE(TestResiduals)
{
	GenericEso *MADOGenericEso;
	MADOGenericEso = genEsoFac.createMADOmodelWoBatchExample();
	const EsoIndex numVariables = 14;
	const EsoIndex numAlgebraicEquations = 3;
    const EsoIndex numDifferentialEquations = 9;
    const EsoIndex numEquations = numAlgebraicEquations + numDifferentialEquations;

	double x1, x2, x3, x4, x5, x6, x7, x8, x9;
	double Fbin;
	double Tw;
	double derX7;

	x1 = 1;
	x2 = 1;
	x3 = 1;
	x4 = 1;
	x5 = 1;
	x6 = 1;
	x7 = 1;
	x8 = 1;
	x9 = 1;
	derX7 = 1;
	Fbin = 1;
	Tw = 1;
	double TwCur = 0.02;
	double FbinCur = 5.784;

	double* x_test = new double[numVariables];
	x_test[0] = x1;
	x_test[1] = x2;
	x_test[2] = x3;
	x_test[3] = x4;
	x_test[4] = x5;
	x_test[5] = x6;
	x_test[6] = x7;
	x_test[7] = x8;
	x_test[8] = x9;
	x_test[9] = derX7;
	x_test[10] = Fbin;
	x_test[11] = Tw;
	x_test[12] = TwCur;
	x_test[13] = FbinCur;

	MADOGenericEso->setAllVariableValues(numVariables, x_test);

	double res1, res2, res3, res4, res5, res6, res7, res8, res9, res10, res11, res12;

	//correct residual values, based on MATLAB calculation
	res1 = -0.0010456073638891456620380184551751; //-1659900 * exp(-133334 / 5483)-1 / 1000;
	res2 = -0.000090985616725424579443895323416514; //-1659900 * exp(-133334 / 5483) - 721170000 * exp(-166666 / 5483);
	res3 = -0.0010062369493511677618429996543714; //3319800 * exp(-133334 / 5483) - 1442340000 * exp(-166666 / 5483) - 2674500000000 * exp(-222220 / 5483) - 1 / 1000;
	res4 = -0.00095796933289217170814776454587803; //721170000 * exp(-166666 / 5483) - 1337250000000 * exp(-222220 / 5483) - 1 / 1000;
	res5 = -0.00090924349432744216518824626351722;//1442340000 * exp(-166666 / 5483) - 1 / 1000;
	res6 = -0.00098995724281464812333907575764173;//4011750000000 * exp(-222220 / 5483) - 1 / 1000;
	res7 = 0.001;//1 / 1000;
	res8 = -0.24486967981282087275764884869936; //7427220225000000 * exp(-222220 / 5483) - 4187055326400 * exp(-166666 / 5483);
	res9 = 1;
	res10 = -0.71783424756321446530822086145625; // (54735202500 * exp(-133334 / 5483)) / 523 + (14270151375000 * exp(-166666 / 5483)) / 523 + (75654918750000000 * exp(-222220 / 5483)) / 523 - 1666637429931326838507 / 2305843009213693952000;
	res11 = 4.784; //598 / 125;
	res12 = -0.98;//-49 / 50;



	// tolerance for the comparison in percentage
	double eps_res = 0.00000000001;  //todo; use threshhold from genericesotest


	double* all_res = new double[12];
	BOOST_CHECK_NO_THROW(MADOGenericEso->getAllResiduals(numEquations, all_res));

	double*all_res_correct = new double[numEquations];
	all_res_correct[0] = res1;
	all_res_correct[1] = res2;
	all_res_correct[2] = res3;
	all_res_correct[3] = res4;
	all_res_correct[4] = res5;
	all_res_correct[5] = res6;
	all_res_correct[6] = res7;
	all_res_correct[7] = res8;
	all_res_correct[8] = res9;
	all_res_correct[9] = res10;
	all_res_correct[10] = res11;
	all_res_correct[11] = res12;

	for (unsigned i = 0; i < 12; i++) {
		BOOST_CHECK_CLOSE(all_res_correct[i], all_res[i], eps_res);
	}


	double* diff_res = new double[9];
	BOOST_CHECK_NO_THROW(MADOGenericEso->getDifferentialResiduals(numDifferentialEquations, diff_res));

	double*diff_res_correct = new double[numDifferentialEquations];
	diff_res_correct[0] = res1;
	diff_res_correct[1] = res2;
	diff_res_correct[2] = res3;
	diff_res_correct[3] = res4;
	diff_res_correct[4] = res5;
	diff_res_correct[5] = res6;
	diff_res_correct[6] = res7;
	diff_res_correct[7] = res8;
	diff_res_correct[8] = res9;

	for (unsigned i = 0; i < 9; i++) {
		BOOST_CHECK_CLOSE(diff_res_correct[i], diff_res[i], eps_res);
	}


	double* alg_res = new double[3];
	BOOST_CHECK_NO_THROW(MADOGenericEso->getAlgebraicResiduals(numAlgebraicEquations, alg_res));

	double*alg_res_correct = new double[numAlgebraicEquations];
	alg_res_correct[0] = res10;
	alg_res_correct[1] = res11;
	alg_res_correct[2] = res12;


	for (unsigned i = 0; i < 3; i++) {
		BOOST_CHECK_CLOSE(alg_res_correct[i], alg_res[i], eps_res);
	}


	EsoIndex numTestRes = 4; //random number for test
	double* res_by_index = new double[numTestRes];
	int res_equationIndex [4] = { 3,5,6,9 };
	BOOST_CHECK_NO_THROW(MADOGenericEso->getResiduals(numTestRes, res_by_index, res_equationIndex));

	double*res_by_index_correct = new double[numTestRes];
	res_by_index_correct[0] = res4;
	res_by_index_correct[1] = res6;
	res_by_index_correct[2] = res7;
	res_by_index_correct[3] = res10;


	for (unsigned i = 0; i < numTestRes; i++) {
		BOOST_CHECK_CLOSE(res_by_index_correct[i], res_by_index[i], eps_res);
	}





	delete[] res_by_index_correct;
	delete[] diff_res_correct;
	delete[] alg_res_correct;
	delete[] all_res_correct;
    delete[] x_test;
	delete[] all_res;
	delete[] alg_res;
	delete[] diff_res;
	delete[] res_by_index;
	delete MADOGenericEso;
}

/*
BOOST_AUTO_TEST_CASE(TestDiffJacobiansZero)
{
	GenericEso *MADOGenericEso;
	MADOGenericEso = genEsoFac.createMADOmodelWoBatchExample();
	const EsoIndex numVariables = 14;
	const EsoIndex numNonZeros = 56;
	const EsoIndex numDiffNonZeros = 9;
	const EsoIndex numAlgebraicEquations = 3;
	const EsoIndex numDifferentialEquations = 9;
	const EsoIndex numEquations = numAlgebraicEquations + numDifferentialEquations;

	double mx[23] = { 0 };

	MADOGenericEso->setAllVariableValues(numVariables, mx);

	double* diffJacValues = new double[numNonZeros];
	MADOGenericEso->getJacobianValues(numNonZeros, diffJacValues);

	delete[] diffJacValues;

}
*/


BOOST_AUTO_TEST_CASE(TestJacobians)
{
	GenericEso *MADOGenericEso;
	MADOGenericEso = genEsoFac.createMADOmodelWoBatchExample();
	const EsoIndex numVariables = 14;
	const EsoIndex numNonZeros = 56; 
	const EsoIndex numDiffNonZeros = 9; 
	const EsoIndex numAlgebraicEquations = 3;
	const EsoIndex numDifferentialEquations = 9;
	const EsoIndex numEquations = numAlgebraicEquations + numDifferentialEquations;

	double x1, x2, x3, x4, x5, x6, x7, x8, x9;
	double Fbin;
	double Tw;
	double derX7;

	x1 = 1;
	x2 = 1;
	x3 = 1;
	x4 = 1;
	x5 = 1;
	x6 = 1;
	x7 = 1;
	x8 = 1;
	x9 = 1;
	derX7 = 1;
	Fbin = 1;
	Tw = 1;
	double TwCur = 0.02;
	double FbinCur = 5.784;

	double* x_test = new double[14];
	x_test[0] = x1;
	x_test[1] = x2;
	x_test[2] = x3;
	x_test[3] = x4;
	x_test[4] = x5;
	x_test[5] = x6;
	x_test[6] = x7;
	x_test[7] = x8;
	x_test[8] = x9;
	x_test[9] = derX7;
	x_test[10] = Fbin;
	x_test[11] = Tw;
	x_test[12] = TwCur;
	x_test[13] = FbinCur;

	MADOGenericEso->setAllVariableValues(numVariables, x_test);

	double* values = new double[numNonZeros];
	MADOGenericEso->getJacobianValues(numNonZeros, values);

	//correct jacobian values for existing variables in each model equation, based on MATLAB calculation
	/* 5 */std::vector<double> values_0_correct{ -0.0010456073638891456620380184551751,  -0.000045607363889145662038018455175125,  -0.0000040454737050196654811635231942321, 0.001, -0.001 };
	/* 6 */std::vector<double> values_1_correct{ -0.000045607363889145662038018455175125, -0.0010909856167254245794438953234165, -0.00004537825283627891740587686824139, -0.0000090768672343246341080944449056386, 0, 0 };
	/* 7 */std::vector<double> values_2_correct{ 0.000091214727778291324076036910350249, 0.00000045822210573348926428317386747023,  -0.0010974516771294590859190365647216, -0.0000066951714569012511072828282388476, -0.0000029616189113469419220510656244135, 0.001, -0.001 };
	/* 6 */std::vector<double> values_3_correct{ 0.00004537825283627891740587686824139, 0.000042030667107828291852235454121966, -0.0010033475857284506255536414141194, 0.0000045365038979168008116727874163742, 0.001, -0.001 };
	/* 6 */std::vector<double> values_4_correct{ 0.000090756505672557834811753736482779, 0.000090756505672557834811753736482779, -0.001, 0.000010062787058609937253861843422813, 0.001, -0.001 };
	/* 6 */std::vector<double> values_5_correct{ 0.000010042757185351876660924242358271, 0.000010042757185351876660924242358271, -0.001, 0.0000014846688941645034457744028850969, 0.001, -0.001 };
	/* 1 */std::vector<double> values_6_correct{ 0.001 };
	/* 5 */std::vector<double> values_7_correct{ -0.26346250570720849214512862686005, -0.24486967981282087275764884869936, 0.018592825894387619387479778160692, -0.02646320181796928058774557329465, -0.24486967981282087275764884869936 };
	/* 1 */std::vector<double> values_8_correct{ 1.0 };
	/* 9 */std::vector<double> values_9_correct{ 0.0028755312127047384430280278382404, 0.0045923996218784842903870881256716, 0.0020789901349616887550020105815161, 0.0003621217257879429076429502940849, -0.00074449373782091478525243031304263, -0.034, -1.0, 0.034, 0.24345468577489400149524434446846 };
	/* 2 */std::vector<double> values_10_correct{ -1.0, 1.0 };
	/* 2 */std::vector<double> values_11_correct{ -1.0, 1.0 };


	// tolerance for the comparison in percentage
	double eps_jac = 1e-10;

	//boost check for each model equation
	unsigned counter = 0;
	for (unsigned i = counter; i < counter+5; i++) {
		BOOST_CHECK_CLOSE(values_0_correct[i], values[i], eps_jac);
	}
	counter += 5;

	for (unsigned i = counter; i < counter+6; i++) {
		BOOST_CHECK_CLOSE(values_1_correct[i - counter], values[i], eps_jac);
	}
	counter += 6;

	for (unsigned i = counter; i < counter+7; i++) {
		BOOST_CHECK_CLOSE(values_2_correct[i - counter], values[i], eps_jac);
	}
	counter += 7;

	for (unsigned i = counter; i < counter +6; i++) {
		BOOST_CHECK_CLOSE(values_3_correct[i - counter], values[i], eps_jac);
	}
	counter += 6;

	for (unsigned i = counter; i < counter +6; i++) {
		BOOST_CHECK_CLOSE(values_4_correct[i - counter], values[i], eps_jac);
	}
	counter += 6;

	for (unsigned i = counter; i < counter+6; i++) {
		BOOST_CHECK_CLOSE(values_5_correct[i - counter], values[i], eps_jac);
	}
	counter += 6;

	for (unsigned i = counter; i < counter+1; i++) {
		BOOST_CHECK_CLOSE(values_6_correct[i - counter], values[i], eps_jac);
	}
	counter += 1;

	for (unsigned i = counter; i < counter+5; i++) {
		BOOST_CHECK_CLOSE(values_7_correct[i - counter], values[i], eps_jac);
	}
	counter += 5;

	for (unsigned i = counter; i < counter+1; i++) {
		BOOST_CHECK_CLOSE(values_8_correct[i - counter], values[i], eps_jac);
	}
	counter += 1;

	for (unsigned i = counter +2; i < counter +9; i++) {
		BOOST_CHECK_CLOSE(values_9_correct[i - counter], values[i], eps_jac);
	}
	counter += 9;

	for (unsigned i = counter; i < counter +2; i++) {
		BOOST_CHECK_CLOSE(values_10_correct[i - counter], values[i], eps_jac);
	}
	counter += 2;

	for (unsigned i = counter; i < counter +2; i++) {
		BOOST_CHECK_CLOSE(values_11_correct[i - counter], values[i], eps_jac);
	}


	double* diff_values = new double[numDiffNonZeros];
	MADOGenericEso->getDiffJacobianValues(numDiffNonZeros, diff_values);

	std::vector<double> diff_value_correct{ -1,-1,-1,-1,-1,-1,-1,-1,-1 };

	// tolerance for the comparison in percentage
	double eps_diff_jac = 0.00000000001;

	for (int i = 0; i < 9; i++) {
		BOOST_CHECK_CLOSE(diff_value_correct[i], diff_values[i], eps_diff_jac);
	}


	EsoIndex* rowIndices = new EsoIndex[numNonZeros];
	EsoIndex* colIndices = new EsoIndex[numNonZeros];
	MADOGenericEso->getJacobianStruct(numNonZeros, rowIndices, colIndices);
	
	//random selection for test 
	BOOST_CHECK_EQUAL(0, rowIndices[0]);
	BOOST_CHECK_EQUAL(0, colIndices[0]);
	BOOST_CHECK_EQUAL(2, rowIndices[16]);
	BOOST_CHECK_EQUAL(7, colIndices[16]);
	BOOST_CHECK_EQUAL(1, rowIndices[5]);
	BOOST_CHECK_EQUAL(0, colIndices[5]);
	BOOST_CHECK_EQUAL(11, rowIndices[numNonZeros-1]);
	BOOST_CHECK_EQUAL(12, colIndices[numNonZeros-1]);

	EsoIndex* diffRowIndices = new EsoIndex[numDiffNonZeros];
	EsoIndex* diffColIndices = new EsoIndex[numDiffNonZeros];
	MADOGenericEso->getDiffJacobianStruct(numDiffNonZeros, diffRowIndices, diffColIndices);
	
	//selection for test 
	BOOST_CHECK_EQUAL(0, diffRowIndices[0]);
	BOOST_CHECK_EQUAL(0, diffColIndices[0]);
	BOOST_CHECK_EQUAL(8, diffRowIndices[numDiffNonZeros - 1]);
	BOOST_CHECK_EQUAL(6, diffColIndices[numDiffNonZeros - 1]);


	delete[] diffRowIndices;
	delete[] diffColIndices;
	delete[] rowIndices;
	delete[] colIndices;
	delete[] values;
	delete[] diff_values;
	delete[] x_test;
	delete MADOGenericEso;
}


/**
* @test JadeGenericEsoTest - check function getAllBounds
* @todo complete or delete this test
*/
BOOST_AUTO_TEST_CASE(testBounds)
{
	GenericEso *genericEso;
	BOOST_REQUIRE_NO_THROW(genericEso = genEsoFac.createMADOmodelWoBatchExample());
	const EsoIndex numVariables = genericEso->getNumVariables();
	utils::Array<EsoDouble> lowerBounds(numVariables);
	utils::Array<EsoDouble> upperBounds(numVariables);
	genericEso->getAllBounds(numVariables, lowerBounds.getData(), upperBounds.getData());
	delete genericEso;
}


BOOST_AUTO_TEST_SUITE_END()