/**
* @file GenericEsoTest.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* testsuite     for the Generic Eso module	                             \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 15.8.2011
*/
#include "GenericEsoTestFactories.hpp"


/**
* @def BOOST_TEST_MODULE
* @brief name of the test unit (needed to be set for BOOST_TEST)
*/
#define BOOST_TEST_MODULE GenericEso


#include "boost/test/unit_test.hpp"
#include "boost/test/tools/floating_point_comparison.hpp"


/**
* @def THRESHOLD
* @brief general threshold for BOOST_*_CLOSE macros
*/
#define THRESHOLD 1e-13

//! factory used to create all test objects
static GenericEsoTestFactory genEsoFac;

#ifdef BUILD_WITH_MADO
#include "MADOGenericEsoTestsuite.hpp"
#endif

#include "JadeGenericEsoTestsuite.hpp"
#include "EsoUtilitiesTestsuite.hpp"
