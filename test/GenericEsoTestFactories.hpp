/** 
* @file GenericEsoTestFactories.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* factory for Generic Eso test objects                                 \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 21.12.2011
*/

#pragma once
#include "GenericEsoFactory.hpp"

#ifdef BUILD_WITH_MADO
	#include "MADOGenericEsoTestConfig.hpp"
#endif


/**
* @class GenericEsoTestFactory
* @brief factory creating GenericEso test objects
*/
class GenericEsoTestFactory : public GenericEsoFactory
{
public:
  /**
  * @brief create the GenericEso test object using the extended Jade car example
  *
  * The model has been translated from the following .mo file: \n
  * \n
  * model car \n
  *  constant Real alpha = 0.36; \n
  *  constant Real k1 = 15; \n
  *  constant Real k2 = 1; \n
  *  constant Real m = 1000; \n
  *  Real s=2.0; \n
  *  Real t=4.0; \n
  *  Real v=3.0; \n
  *  Real obj=5.0; \n
  *  Real deriv=0.5; \n
  *  parameter Real a=1.0; \n
  * \n
  * equation \n
  *  der(s) = v; \n
  *  der(v) = -alpha/m*pow(v,2)+a; \n
  *  der(t) = 1.0; \n
  *  der(obj) = der; \n
  *  deriv = k2 + k1*a*(tanh(m*a)+1)/2; \n
  * end car; \n
  * @return JadeGenericEso object for the extended car example
  */
  JadeGenericEso *createJadeCarExample() const
  {
    return createJadeGenericEso("carErweitert");
  }
  
  JadeGenericEso *createJadeCarExampleNonGeneral()
  {
    return new JadeGenericEso("carErweitert");
  }
  
  /**
  * @brief create the GenericEso test object using the GenericEsoTest model (jade/Example/DyOS)
  *
  * @return JadeGenericEso object for the GenericEsoTest example
  */
  GenericEso *createJadeGenericEsoTestExample() const
  {
    return createJadeGenericEso("GenericEsoTest");
  }


#ifdef BUILD_WITH_MADO
  /**
* @brief create the GenericEso test object using the extended MADO woBatch example
* insert model eqn here (see createJadeCarExample)
*
* @return MADOGenericEso object for the woBatch example
*/
  MADOGenericEso *createMADOmodelWoBatchExample() const
  {
	  return createMADOGenericEso(PATH_MADO_GENERICESO_TEST_MODEL);
  }

  MADOGenericEso *createMADOmodelWoBatchExampleNonGeneral()
  {
	  return new MADOGenericEso(PATH_MADO_GENERICESO_TEST_MODEL);
  }
#endif
};






