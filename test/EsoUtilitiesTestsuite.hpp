/**
* @file EsoUtilities.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* In this test functionality of EsoUtilities is tested.                \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 12.07.2012
*/

#include "EsoUtilities.hpp"

BOOST_AUTO_TEST_SUITE(EsoUtilitiesTest)

BOOST_AUTO_TEST_CASE(testGetBMatrix)
{
  GenericEso::Ptr genericEso;
  BOOST_REQUIRE_NO_THROW(genericEso = GenericEso::Ptr(genEsoFac.createJadeCarExample()));
  CsTripletMatrix::Ptr bMatrix;
  BOOST_CHECK_NO_THROW(bMatrix = getBMatrix(genericEso));
  int numNonZeroes = 4;
  BOOST_REQUIRE_EQUAL(bMatrix->get_nnz(), numNonZeroes);
  
  for(int i=0; i<numNonZeroes; i++){
    BOOST_CHECK_CLOSE(bMatrix->get_matrix_ptr()->x[i], 1.0, 1e-10);
    BOOST_CHECK_EQUAL(bMatrix->get_matrix_ptr()->i[i], i);
  }
  BOOST_CHECK_EQUAL(bMatrix->get_matrix_ptr()->p[0], 0);
  BOOST_CHECK_EQUAL(bMatrix->get_matrix_ptr()->p[1], 2);
  BOOST_CHECK_EQUAL(bMatrix->get_matrix_ptr()->p[2], 1);
  BOOST_CHECK_EQUAL(bMatrix->get_matrix_ptr()->p[3], 3);
}

BOOST_AUTO_TEST_CASE(testBMatrixIsConstant)
{
  GenericEso::Ptr genericEso;
  BOOST_REQUIRE_NO_THROW(genericEso = GenericEso::Ptr(genEsoFac.createJadeCarExample()));
  
  BOOST_CHECK(BMatrixIsConstant(genericEso));
  
  
  //GenericEso::Ptr genericEsoNotConstantMMatrix(new JadeGenericEso("carErweitertTimeVariant")); //carErweitertTimeVariant
  //BOOST_CHECK(!BMatrixIsConstant(genericEsoNotConstantMMatrix));
  
}

BOOST_AUTO_TEST_CASE(testGetSetFullDerivativeValues)
{
}

BOOST_AUTO_TEST_SUITE_END()