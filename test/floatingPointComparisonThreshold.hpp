#ifndef FLOATINGPOINTCOMPARISONTHRESHOLD_HPP
#define FLOATINGPOINTCOMPARISONTHRESHOLD_HPP

/**
* @def floatingPointComparisonThreshold
* @brief general threshold for BOOST_*_CLOSE macros
* @return threshold for floating point comparisons
*/
inline double floatingPointComparisonThreshold() {
    return 1e-9;
}

#endif // FLOATINGPOINTCOMPARISONTHRESHOLD_HPP
