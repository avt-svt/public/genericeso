#include "EsoAcsammm_Header.hpp"


void init ( double * x, double * p, int &n_x, int& n_p, int& n_c) {
	x[0] = 0; // x
	x[1] = 0; // y
	x[2] = 0; // z
	p[0] = 1.3; // Para
	p[1] = 3.7; // Cont
	n_x = 3; n_p = 2; get_num_cond(n_c);
}; // end of init

#include <vector>
#include <string>
void get_var_names(std::vector<std::string> * names) {
	names->push_back("x");
	names->push_back("y");
	names->push_back("z");
}
void get_num_vars(int & nv) {
  nv = 3;
}

void get_par_names(std::vector<std::string> * names) {
	names->push_back("Para");
	names->push_back("Cont");
}
void get_num_pars(int & np) {
  np = 2;
}


void get_num_eqns(int& ne){
 ne = 3;
}

void get_num_cond(int& nc){
 nc = 0;
}
