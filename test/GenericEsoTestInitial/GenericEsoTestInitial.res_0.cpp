#include "EsoAcsammm_Header.hpp"

void res(double * yy, 
double* der_x, double * x, 
double* p,
int* condit, int* lock, int* prev,
int &n_x, int &n_p, int &n_c)
  #pragma ad indep x p
  #pragma ad dep yy
{

	int i_E=0;
	int i_C=0;
	int i__=0;
	int j__=0;
	int k__=0;
	int m__=0;
	int r_i=0; // for vector assignments
	int r_j=0; // for matrix assignments
	int i_a=0;
	int i_b=0;
	int i_loop=0;
	int i__switch=0;
	int i__start=0;
	int i__end=0;
	int i__step=0;
	int while_end=0; // for vector statements
	int i_cont=0; // for container assignments
	int j_cont=0; // for container assignments
	int k_cont=0; // for container assignments
	double var_x=0;
	double der_var_x=0;
	double var_y=0;
	double der_var_y=0;
	double var_z=0;
	double der_var_z=0;
	double par_Para=0;
	double par_Cont=0;
	int i_i=0; //loop
	int i_j=0; //loop
	int i_z=0; //loop
	var_x = x[0];
	der_var_x = der_x[0];
	var_y = x[1];
	der_var_y = der_x[1];
	var_z = x[2];
	der_var_z = der_x[2];
	par_Para = p[0];
	par_Cont = p[1];

// #pragma dcc equation begin
// scalar equation 0
yy[i_E] = var_x - (par_Para * par_Cont);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 1
yy[i_E] = var_y - (5 * par_Para);
i_E = i_E+1;
// #pragma dcc equation end

// #pragma dcc equation begin
// scalar equation 2
yy[i_E] = var_z - (0.37256);
i_E = i_E+1;
// #pragma dcc equation end

n_c=i_C;
}
