/**
* @file SparseJacobian.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* Header with declaration of class SparseJacobian                      \n
* =====================================================================\n
* @author Moritz Schmitz
* @date 15.9.2011
*/
#pragma once

#include "Array.hpp"
#include "Jacobian.hpp"
#include "jsp.hpp"
#include <vector>

//Forward declaration of used ColPack class to keep dependencies minimal
namespace ColPack{
  class BipartiteGraphPartialColoringInterface;
}

/**
* @class SparseJacobian
*
* This interface provides functions and members to manipulate
* the Jacobian matrix stored in sparse format
*/

class SparseJacobian : public Jacobian
{
public:

  SparseJacobian(const EsoIndex firstVarIndex, const EsoIndex lastVarIndex, jsp* sp_res, const EsoIndex len_sp_res);

  virtual ~SparseJacobian();

  virtual void getValuesFromBuffer(EsoDouble** buffer, EsoDouble* values, const EsoIndex lenValues) const;

  virtual void getSparsityPattern(const EsoIndex* eqIndices, const EsoIndex lenEI,
                                  const EsoIndex* varIndices, const EsoIndex lenVI,
                                  EsoIndex* rowIndices, EsoIndex* colIndices,
                                  EsoIndex& numEntries) const;

  virtual void getSparsityPattern(const EsoIndex* indices, const EsoIndex lenInd,
                                  EsoIndex* rowIndices, EsoIndex* colIndices) const;

  /**
  * @brief returns the transposed seed-matrix
  *
  * @return transposed seed-matrix
  */
  double** getTransposedSeed() const {return m_seedMatrix_t;}

  /**
  * @brief getter for number of columns in seed-matrix
  *
  * @return number of columns
  */
  EsoIndex getNumColsSeed() const {return m_numColsSeed;}

protected:

  //! Member to access ColPack functions to determine the seedmatrix by coloring algorithms
  ColPack::BipartiteGraphPartialColoringInterface* m_coloringInterface;
  //! Stores the sparsity pattern relative to range between firstVarIndex and lastVarIndex
  //! (due to ColPack function: RecoverD2Cln_CoordinateFormat).
  //! m_sparsityPattern is a pointer to an int** where the first dimension is the number of equations
  //! and the second dimension is (number of non-zeros + 1). The first entry for each equations holds
  //! the number of non-zero entries
  unsigned int*** m_sparsityPattern;
  //! Stores the transposed seed-matrix to determine the jacobian values by tangent-linear calculation
  double** m_seedMatrix_t;
  //! Number of columns of the seed-matrix
  EsoIndex m_numColsSeed;
  //! Number of equations equals number of rows of jacobian
  EsoIndex m_num_equations;
  //! Index of the variable that corresponds to the first column of the jacobain
  EsoIndex m_firstVarIndex;
};
