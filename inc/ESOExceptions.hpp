/** 
* @file ESOExceptions.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* This header contains exception implementations for any exceptions    \n
* that can be thrown from EsoWrapper modules.                          \n
* =====================================================================\n
* @author Tjalf Hoffmann 
* @date 11.7.2011
*/

#include <string>
#include <exception>

#ifndef ESO_EXCEPTIONS_HPP
#define ESO_EXCEPTIONS_HPP
#endif

/**
* @class ESOException
* 
* @brief basic exception class for any exception occurring within EsoWrapper code
*/
class ESOException:public std::exception
{

public:
  std::string m_message;

   /**
  * @brief constructor
  *
  * @param error_message_in detailed error message - default message is an empty string
  */
#ifdef WIN32
  //for any reason inheritance does not work on windows. only standard message (unknown exception)
  //is printed on screen when catching this exception
  ESOException(std::string error_message_in_=""): exception(error_message_in_.c_str())
#else
  ESOException(std::string error_message_in_="")
#endif
  {
    m_message = error_message_in_;
  }
  
  ~ESOException() throw(){}
  
  virtual const char* what() const throw()
  {
    return m_message.c_str();
  }
};

