/**
* @file EsoInput.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericESO                                                           \n
* =====================================================================\n
* This file contains the declarations of the input structs for creating\n
* GenericEso objects.                                                  \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 10.02.2012
*/

#pragma once

#include <string>
#include <HaveFmuConfig.hpp>
#include "EsoTypes.hpp"

namespace FactoryInput
{
  /**
  * @struct EsoInput
  * @brief information provided by user to create a GenericEso object
  */
  struct EsoInput
  {
    /// @brief name of the model to be loaded
    std::string model;
    /// @brief name of the initial model to be loaded (for JADE only)
    std::string initialModel;
    /// @brief type of the GenericEso to be created
    EsoType type;
#ifdef BUILD_WITH_FMU
	//// @brief relative tolerance for FMU
	double relFmuTolerance = 0;
#endif
  };
}// FactoryInput
