/** 
* @file GenericEsoFactory.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* GenericESO                                                           \n
* =====================================================================\n
* This file contains the declarations of factories for GenericEsos     \n
* =====================================================================\n
* @author Tjalf Hoffmann 
* @date 10.02.2012
*/

#pragma once

#include "HaveFmuConfig.hpp"
#include "HaveJade2Config.hpp"
#include "HaveMADOConfig.hpp"

#include "EsoInput.hpp"
#include "JadeGenericEso.hpp"
#include "InputExceptions.hpp"

#ifdef BUILD_WITH_MADO
#include "MADOGenericEso.hpp"
#endif

#ifdef BUILD_WITH_FMU
#include "FMIGenericEso.hpp"
#endif

#ifdef BUILD_JADE2
#include "JADE2GenericEso.hpp"
#endif

/**
* @class GenericEsoFactory
* @brief factory class for creating GenericEso objects
*/
class GenericEsoFactory
{
public:
  GenericEso::Ptr create(const struct FactoryInput::EsoInput &input)const;
  GenericEso2ndOrder::Ptr create2ndOrder(const struct FactoryInput::EsoInput &input)const;
  
  JadeGenericEso *createJadeGenericEso(const std::string &model,
                                             const std::string &initialModel = "")const;
#ifdef BUILD_JADE2										 
  JADE2GenericEso *createJADE2GenericEso(const std::string &model)const;
#endif   
#ifdef BUILD_WITH_MADO
  MADOGenericEso *createMADOGenericEso(const std::string &model)const;
#endif 
#ifdef BUILD_WITH_FMU
  FMIGenericEso *createFMIGenericEso(const std::string &FmiName, double relFmuTolerance)const;  
#endif
};