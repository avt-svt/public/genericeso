/**
* @file Equation.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* This file contains the equation data structure of the generic eso    \n
* according to the UML Diagramm METAESO_2.                             \n
* The class EquationHandler and the attribute m_residuals of class     \n
* Equation have been removed. The residual data as well as the data in \n
* class EquationHandler is handled by the GenericEso class itself      \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 14.6.2011
*/

#include <vector>
#include "EsoTypes.hpp"

using std::vector;

/**
* @class EquationSet
*
* @brief general class for handling a set of Eso equation
*
* @author Tjalf Hoffmann
* @date 14.6.2011
*/
class EquationSet
{
protected:
  //! indices of the equations handled by this class
  vector<EsoIndex> m_equationIndex;
public:
  EquationSet();
  void setEquationIndex(const vector<EsoIndex> &equationIndex);
  EsoIndex getNumEquations() const;
  vector<EsoIndex>& getEquationIndex();
};

/**
* @class AlgebraicEquationSet
*
* @brief class for handling algebraic equations
*
* @author Tjalf Hoffmann
* @date 14.6.2011
*/
class AlgebraicEquationSet:public EquationSet
{
public:
  AlgebraicEquationSet();
};

/**
* @class DifferentialEquationSet
*
* @brief class for handling differential equations
*
* @author Tjalf Hoffmann
* @date 14.6.2011
*/
class DifferentialEquationSet:public EquationSet
{
public:
  DifferentialEquationSet();
};
