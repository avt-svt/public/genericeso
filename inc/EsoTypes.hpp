/**
* @file EsoTypes.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* Definition of all types that are used in the ESO                     \n
* =====================================================================\n
* @author Hans Pirnay, Ralf Hannemann-Tamas
* @date 09.10.2019
*/


#ifndef GENERICESO_ESO_TYPES_HPP
#define GENERICESO_ESO_TYPES_HPP

#include <string>
#include <ostream>

typedef int EsoIndex;
typedef double EsoDouble;
typedef std::string ModelName;


/**
 * @brief The EsoType enum describes whether we use the JADE/JADE2 or FMI ESO
 */
enum class EsoType {
  JADE, ///Esotype for Jade ESO
  JADE2,///Esotype for Jade2 ESO
  FMI,   ///Esotype for FMI ESO
  MADO   ///Esotype for MADO ESO
};


/**
 * @brief to_string converts EsoType to string
 * @param[in] esoType
 * @return name of EsoType as string
 */
inline std::string to_string(EsoType esoType)
{
  std::string result;
  switch (esoType)
  {
  case EsoType::JADE:
    result= "JADE";
    break;
  case EsoType::JADE2:
    result=  "JADE2";
    break;
  case EsoType::FMI:
    result=  "FMI";
    break;
  case EsoType::MADO:
	  result = "MADO";
	  break;
  }
  return result;
}

/**
 * @brief to_int converts EsoType to int
 * @param[in] esoType
 * @return index of EsoType as int
 */
inline int to_int(EsoType esoType)
{
  int result;
  switch (esoType)
  {
  case EsoType::JADE:
    result= 0;
    break;
  case EsoType::JADE2:
    result=  1;
    break;
  case EsoType::FMI:
    result=  2;
    break;
  case EsoType::MADO:
	  result = 3;
	  break;
  }
  return result;
}

inline std::ostream& operator<<(std::ostream& out, const EsoType& esoType) {
  out << to_string(esoType);
  return out;
}


#endif
