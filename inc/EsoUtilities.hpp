/**
* @file EsoUtilities.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* This file contains utility functions for GenericEso objects          \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 2.10.2012
*/

#pragma once

#include "GenericEso.hpp"
#include "boost/shared_ptr.hpp"
#include "TripletMatrix.hpp"
#include "Array.hpp"


EsoIndex getEquationIndexOfVariable(const EsoIndex esoIndex, GenericEso::Ptr esoPtr);
EsoIndex getAlgebraicEquationIndex(const EsoIndex esoIndex, const GenericEso::Ptr esoPtr);
EsoIndex getDifferentialEquationIndex(const unsigned diffEsoIndex, const GenericEso::Ptr esoPtr);

CsTripletMatrix::Ptr getBMatrix(const GenericEso::Ptr esoPtr);

void getFullDerivativeValues(const GenericEso::Ptr eso, utils::Array<double>& ydfull);

void setFullDerivativeValues(const GenericEso::Ptr eso, const utils::Array<double>& ydfull);

bool BMatrixIsConstant(const GenericEso::Ptr esoPtr);