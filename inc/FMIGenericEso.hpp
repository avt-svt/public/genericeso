/**
* @file FMIGenericEso.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrenstechnik, RWTH Aachen           \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* Header with declaration of class FMIGenericEso                       \n
* =====================================================================\n
* @author Adrian Caspari and Johannes M.M. Faust
* @date 03.11.2017
*/
#pragma once

#include "GenericEso.hpp"
#include "Array.hpp"
#include "DyosFmiLoader.hpp"
#include "SparseJacobian.hpp"
#include <string>
#include <vector>
#include <map>
#include "UtilityFunctions.hpp"

/**
* @class FMIGenericEso
*
* @brief GenericEso using Modelica as model server
*
* The class FMIGenericEso implemtents all declared functions of the class GenericEso for the
* case where the model server is Modelica. Additional functions to provide first-order tangent and
* adjoint derivatives as well as second-order derivatives in forward over adjoint mode are
* implemented.
*/

class FMIGenericEso : public GenericEso2ndOrder {
protected:
	/**
	* @remark
	*
	* The infix '_t1' in the member variables means that this variable is used in the evaluation of
	* the first-order tangent-linear derivatives. Accordingly '_a1' means that this variable is used
	* in the evaluation of the first-order adjoint derivatives. The infix '_t2_a1' means that this
	*variable is used in the evaluation of the second-order tangent over adjoint derivatives.
	*/

	//! name of loaded model
	ModelName m_model;
	//! responsible for dynamically loading the model and encapsulates all calls to it
	FMILoader m_FMILoader;
	//! number of algebraic and differential states
	EsoIndex m_n_states;
	//! number of equations (= m_numEquations in GenericEso but needed for initialization)
	EsoIndex m_n_eq;
	//! number of switching functions
	EsoIndex m_n_cond;
	//! number of assigned variables (parameters)
	EsoIndex m_n_pars;
	//! number of entries of the state jacobian df/dstates
	EsoIndex m_n_stateJacEntries;
	//! number of entries of the parameter jacobian df/dparameters
	EsoIndex m_n_paramJacEntries;
	//! value of the independent variable
	EsoIndex m_numInitialNonZeroesFMI;
	double m_independentVar;
	EsoIndex m_n_diffStates;
	EsoIndex m_n_algStates;
	//! array of length m_n_states that holds the values of all states
	utils::Array<double> m_states;
	//! array of length m_n_states that holds the values of all initial states
	utils::Array<double> m_initialStates;
	//! array of length m_n_states that holds the time derivative of the differential variables and
	//! zero at the entries corresponding to the algebraic variables
	utils::Array<double> m_der_states;
	//! array of length m_n_pars that holds the values of all parameters
	utils::Array<double> m_parametersACS;
	//! array of length m_n_eq that holds the values of the residual = lhs - rhs
	utils::Array<double> m_res;
	//! array of length m_n_states that holds the part of the seed matrix relative to the states
	//! used for calculating the first-order forward derivatives of the dynamic model
	utils::Array<double> m_t1_states;
	//! array of length m_n_states that holds the part of the seed matrix relative to the differen-
	//! tiated states used for calculating the first-order forward derivatives of the dynamic model
	utils::Array<double> m_t1_der_states;
	//! array of length m_n_pars that holds the part of the seed matrix relative to the parameters
	//! used for calculating the first-order forward derivatives of the dynamic model
	utils::Array<double> m_t1_p;
	//! array of length m_n_eq that holds the results of the multiplication of the Jacobian
	//! and the seed matrix in first-order forward (=tangent-linear) mode
	utils::Array<double> m_t1_res;

	//! member used to calculate the second-order tangent over adjoint derivatives
	//! of the dynamic model:
	//!
	//! output definition: m_a1_z_2ndOrd = (dF(z)/dz)^T * m_a1_res;
	//!                    m_t2_a1_z = <d2F(z)/dzdz, m_a1_res, m_t2_z> + <dF(z)/dz, m_t2_a1_res>;
	//!
	//! where z = [states p der_states]^T.
	utils::Array<double> m_a1_states_2ndOrd;
	//! @copydoc JadeGenericEso::m_a1_states_2ndOrd
	utils::Array<double> m_a1_der_states_2ndOrd;
	//! @copydoc JadeGenericEso::m_a1_states_2ndOrd
	utils::Array<double> m_a1_p_2ndOrd;
	//! @copydoc JadeGenericEso::m_a1_states_2ndOrd
	utils::Array<double> m_a1_res_2ndOrd;
	//! @copydoc JadeGenericEso::m_a1_states_2ndOrd
	utils::Array<double> m_t2_states;
	//! @copydoc JadeGenericEso::m_a1_states_2ndOrd
	utils::Array<double> m_t2_der_states;
	//! @copydoc JadeGenericEso::m_a1_states_2ndOrd
	utils::Array<double> m_t2_p;
	//! @copydoc JadeGenericEso::m_a1_states_2ndOrd
	utils::Array<double> m_t2_res;
	//! @copydoc JadeGenericEso::m_a1_states_2ndOrd
	utils::Array<double> m_t2_a1_states;
	//! @copydoc JadeGenericEso::m_a1_states_2ndOrd
	utils::Array<double> m_t2_a1_der_states;
	//! @copydoc JadeGenericEso::m_a1_states_2ndOrd
	utils::Array<double> m_t2_a1_p;
	//! @copydoc JadeGenericEso::m_a1_states_2ndOrd
	utils::Array<double> m_t2_a1_res;
	//! All the information about the Jacobian matrix [d(residual)/(d states) d(residual)/(d param)]


	
	//! member used to calculate the second-order tangent over adjoint derivatives
	//! of the dynamic model:
	//!
	//! output definition: m_a1_z_2ndOrd = (dF(z)/dz)^T * m_a1_res;
	//!                    m_t2_a1_z = <d2F(z)/dzdz, m_a1_res, m_t2_z> + <dF(z)/dz, m_t2_a1_res>;
	//!
	//! where z = [states p der_states]^T.
	//! All the information about the Jacobian matrix [d(residual)/(d states) d(residual)/(d param)]
	SparseJacobian* m_sparseJacobian;
	//! All the information about the DiffJacobian matrix [d(residual)/(d derivatedStates)]
	SparseJacobian* m_sparseDiffJacobian;
	//! buffer matrix m_buffer = Jacobian * m_seed
	EsoDouble** m_buffer;
	//! buffer matrix m_bufferDiff = df/dder_x * m_seedDiff
	EsoDouble** m_bufferDiff;

	/** Switching stuff
	* @{
	*/
	//! current conditions
	utils::Array<EsoIndex> m_conditions;
	//! current locks
	utils::Array<EsoIndex> m_locks;

public:

	//! not in GenericEso.hpp
	void evaluate1stOrdForwDerivatives(unsigned lenT1States, double *t1States,
		unsigned lenT1DerStates, double *t1DerStates,
		unsigned lenT1P, double *t1P,
    unsigned lenT1Res, double *t1Residuals) override;
	//! not in GenericEso.hpp
	void eval_t1_block_residuals(int n_yy_ind, int *yy_ind, double *t1_states, double *t1_der_states,
		double *t1_p, double *residuals, double *t1_residuals);
	//! not in GenericEso.hpp
	void eval_a1_residuals(double *a1_yy, double *a1_states, double *a1_der_states, double *a1_p);

	void evaluate2ndOrderDerivatives(unsigned lenT2States, double *t2States,
		unsigned lenT2DerStates, double *t2DerStates,
		unsigned lenT2P, double *t2P,
		unsigned lenA1States, double *a1States,
		unsigned lenA1DerStates, double *a1DerStates,
		unsigned lenA1P, double *a1P,
		unsigned lenT2A1States, double *t2A1States,
		unsigned lenT2A1DerStates, double *t2A1DerStates,
		unsigned lenT2A1P, double *t2A1P,
		unsigned lenA1Yy, double *a1Yy,
    unsigned lenT2A1Yy, double *t2A1Yy) override;

	FMIGenericEso(const std::string &model, double relativeFmuTolerance);
  ~FMIGenericEso() override;
  void setIndependentVariable(const EsoDouble var) override;
  EsoDouble getIndependentVariable() const override;
  void getVariableNames(std::vector<std::string> &names) override;

  void setAllVariableValues(const EsoIndex n_var, const EsoDouble *variables) override;
  void setParameterValues(const EsoIndex n_pars, const EsoDouble *parameters) override;
  void setAlgebraicVariableValues(const EsoIndex n_alg_var, const EsoDouble *algebraicVariables) override;
  void setDifferentialVariableValues(const EsoIndex n_diff_var,
    const EsoDouble *differentialVariables) override;
  void setStateValues(const EsoIndex n_states, const EsoDouble *states) override;
  void setVariableValues(const EsoIndex n_idx, const EsoDouble *variables,
    const EsoIndex *indices) override;
  ModelName getModel() const override;
  void getAllVariableValues(const EsoIndex n_var, EsoDouble *variables) const override;
  void getParameterValues(const EsoIndex n_params, EsoDouble *parameters) override;
  void getAlgebraicVariableValues(const EsoIndex n_alg_var, EsoDouble *algebraicVariables) override;
  void getDifferentialVariableValues(const EsoIndex n_diff_var, EsoDouble *differentialVariables) override;
  void getStateValues(const EsoIndex n_states, EsoDouble *states) override;
  void getInitialStateValues(const EsoIndex n_states, EsoDouble *states) override;
  void getVariableValues(const EsoIndex n_idx, EsoDouble *variables, const EsoIndex *indices) override;
  void setDerivativeValues(const EsoIndex n_diff_var, const EsoDouble *derivatives) override;
  void getDerivativeValues(const EsoIndex n_diff_var, EsoDouble *derivatives) override;
  void getAllBounds(const EsoIndex n_var, EsoDouble *lowerBounds, EsoDouble *upperBounds) override;
  GenericEso::RetFlag getAllResiduals(const EsoIndex n_eq, EsoDouble *residuals) override;
  GenericEso::RetFlag getDifferentialResiduals(const EsoIndex n_diff_eq, EsoDouble *differentialResiduals) override;
  GenericEso::RetFlag getAlgebraicResiduals(const EsoIndex n_alg_eq, EsoDouble *algebraicResiduals) override;
  GenericEso::RetFlag  getResiduals(const EsoIndex n_eq, EsoDouble *residuals, const EsoIndex *equationIndex) override;
  GenericEso::RetFlag  getConditionResiduals(const EsoIndex n_c, EsoDouble *residuals) override;
    GenericEso::RetFlag solveAlgebraicEquations() override;
  GenericEso::RetFlag solveDifferentialEquations() override;
  //void getConditionJacobianValues(EsoIndex n_nz, EsoDouble* values);
  void evalConditions(const EsoIndex n_c, EsoIndex *conditions) override;
  void setConditions(const EsoIndex n_c, const EsoIndex* conditions) override;
  void getCurrentConditions(const EsoIndex n_c, EsoIndex* condtitions) override;
  void setLocks(const EsoIndex n_c, const EsoIndex* locks) override;
  void getLocks(const EsoIndex n_c, EsoIndex* locks) override;
	void updateAfterSwitch();

	//possibly type Jacobian would be a better output parameter
	void calculateJacobianStructure();
  GenericEso::RetFlag  getJacobianValues(const EsoIndex n_nz, EsoDouble *values) override;
  GenericEso::RetFlag  getJacobianValues(const EsoIndex n_idx, EsoDouble *values, const EsoIndex *indices) override;
  GenericEso::RetFlag  getDiffJacobianValues(const EsoIndex n_diff_nz, EsoDouble *values) override;

  GenericEso::RetFlag  getInitialJacobian(const EsoIndex n_nz, EsoIndex *rowIndices,
    EsoIndex *colIndices, EsoDouble *values) override;
  GenericEso::RetFlag setInitialValues(const EsoIndex numDiffVals,
		EsoIndex *diffIndices,
		EsoDouble * diffValues,
		const EsoIndex numParameters,
    EsoIndex *parameterIndices) override;
  GenericEso::RetFlag  getJacobianMultVector(const bool transpose, const EsoIndex n_states, EsoDouble *seedStates,
		const EsoIndex n_params, EsoDouble *seedParameters, const EsoIndex n_y,
    EsoDouble *y) override;

	//get GenericEso Type
  EsoType getType() const override;

};
