/**
* @file Jacobian.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* Header with declaration of class Jacobian                            \n
* =====================================================================\n
* @author Moritz Schmitz, Tjalf Hoffmann
* @date 5.8.2011
*/
#pragma once

#include "EsoTypes.hpp"
#include <vector>

/**
* @class Jacobian
*
* This interface provides functions and members to manipulate
* the Jacobian matrix stored in either sparse or dense format
*/

class Jacobian
{
public:
  /*
  * @brief copy constructor
  *
  * The constructed object is a deep copy of the given object
  * @param[in] jac Jacobian object to be copied
  */
  //Jacobian(const Jacobian& rhs) : m_rowIndices(rhs.m_rowIndices), m_colIndices(rhs.m_colIndices){};

  virtual ~Jacobian(){}

  /**
  * @brief getter for number of entries
  *
  * @return number of entries
  */
  virtual EsoIndex getNumEntries() const;

  /**
  * @brief obtain the values of the jacobian matrix knowing the product jacobian*seed=buffer
  *
  * The method calculates the values of the whole jacobian matrix taking as input the buffer
  * matrix, that is the product of the jacobian matrix and the seed matrix. This buffer matrix is
  * typically obtained using some AD-techniques to calculate the jacobian. The recovery is done
  * using some ColPack utilities.
  *
  * @param[in] buffer - holds the product of the jacobian matrix and the seed matrix calculated in
  *            first-order tangent-linear mode.
  * @param[out] values - holds the jacobian values requested on successful termination.
  */
  virtual void getValuesFromBuffer(EsoDouble** /*buffer*/, EsoDouble* /*values*/, const EsoIndex /*lenValues*/)const{};

  /**
  * @brief Calculates the sparsity pattern for the subsystem defined by the equations whose indices
  *        are in 'eqIndices' and by the variables whose indices are in 'varIndices'.
  * @param[in] eqIndices equation indices that define the subsystem. If the size of eqIndices is
  *            zero, the subsystem is defined by the first lenEI equations.
  * @param[in] lenEI number of entries in eqIndices
  * @param[in] varIndices variable indices that define the subsystem. If the size of varIndices is
  *             zero, the subsystem is defined by the first lenVI variables.
  * @param[in] lenVI number of entries in varIndices
  * @param[out] rowIndices row indices of sparsity pattern in coordinate format. Entry 'i'
  *             specifies that the equation of the i-th entry in 'eqIndices' is concerned.
  * @param[out] colIndices column indices of sparsity pattern in coordinate format. Entry 'j'
  *             specifies that the variable of the j-th entry in 'varIndices' is concerned.
  * @param[out] numEntries number of entries in 'rowIndices' and 'colIndices'
  * @todo implement function
  */
  virtual void getSparsityPattern(const EsoIndex* /*eqIndices*/, const EsoIndex /*lenEI*/,
                                  const EsoIndex* /*varIndices*/, const EsoIndex /*lenVI*/,
                                  EsoIndex* /*rowIndices*/, EsoIndex* /*colIndices*/,
                                  EsoIndex& /*numEntries*/)const{};

  /**
  * @brief Calculates the sparsity pattern for the subsystem defined by the non-zero Jacobian
  *        entries at indices. Therefore the entries in indices must be in the interval
  *        [0 ... numJacobianEntries-1]
  * @param[in] indices indices of the Jacobian entries in between [0 ... numJacobianEntries-1]
  *            where the coordinates are wanted
  * @param[out] rowIndices row indices of of the non-zero jacobian entries at 'indices'.
  * @param[out] colIndices col indices of of the non-zero jacobian entries at 'indices'.
  */
  virtual void getSparsityPattern(const EsoIndex* indices, const EsoIndex lenInd,
                                  EsoIndex* rowIndices, EsoIndex* colIndices) const;

  /**
  * @brief getter for the row index vector
  *
  * @return row index vector
  */
  virtual std::vector<EsoIndex>& getRowIndices();

  /**
  * @brief setter for the row index vector
  *
  * @param[in] rowIndices row index vector to be copied
  */
  virtual void setRowIndices(const std::vector<EsoIndex> &rowIndices);

  /**
  * @brief getter for the column index vector
  *
  * @return column index vector
  */
  virtual std::vector<EsoIndex>& getColIndices();

  /**
  * @brief setter for the column index vector
  *
  * @param[in] colIndices column index vector to be copied
  */
  virtual void setColIndices(const std::vector<EsoIndex> &colIndices);

protected:
  //! row indices of sparsity pattern for whole jacobian [df/dx]
  std::vector<EsoIndex> m_rowIndices;
  //! col indices of sparsity pattern for whole jacobian [df/dx]
  std::vector<EsoIndex> m_colIndices;
};
