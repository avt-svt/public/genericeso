/**
* @file ModelVariable.hpp
*
* =====================================================================\n
* (C) Lehrstuhl fuer Prozesstechnik, RWTH Aachen                      \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                          \n
* =====================================================================\n
* This file contains the data structure of the generic eso            \n
* according to the UML Diagramm METAESO_2                             \n
* =====================================================================\n
* @author Fady Assassa                                                 \n
*/
#pragma once
#include <vector>
#include <string>
#include "EsoTypes.hpp"


/**
* @class ModelVariableSet
* @brief base datastructure for all other variables
* @note This class is abstract
*/
class ModelVariableSet{
protected:
  // Attributes
  //! variable names
  std::vector<std::string> m_name;
  //!variable indices inside the Eso server
  std::vector<EsoIndex> m_esoIndices;
  //!lower bound inside the model
  std::vector<EsoDouble> m_lowerModelBound;
  //!upper bound inside the model
  std::vector<EsoDouble> m_upperModelBound;
public:
  // Methods
  //! @brief standard constructor
  ModelVariableSet(){}
  // setter
  void setEsoIndex(const std::vector<EsoIndex> &esoIndex);
  void setName(const std::vector<std::string> &name);
  void setLowerModelBound(const std::vector<EsoDouble> &lowerModelBound);
  void setUpperModelBound(const std::vector<EsoDouble> &upperModelBound);
  void setModelBounds(const std::vector<EsoDouble> &lowerModelBound, const std::vector<EsoDouble> &upperModelBound);

  /**
  * @brief get the number of variables
  * @return the size of the vector m_esoIndex
  */
  EsoIndex getNumberOfVariables(void)const{return static_cast<EsoIndex>(m_esoIndices.size());}

  /**
  * @brief getter of the attribute m_esoIndices
  * @return reference to m_esoIndices
  */
  std::vector<EsoIndex>& getEsoIndices(void){return m_esoIndices;}
  /**
  * @brief getter of the attribute m_lowerModelBound
  * @return reference to m_lowerModelBound
  */
  std::vector<EsoDouble>& getLowerModelBound(){return m_lowerModelBound;}
  /**
  * @brief getter of the attribute m_upperModelBound
  * @return reference to m_upperModelBound
  */
  std::vector<EsoDouble>& getUpperModelBound(){return m_upperModelBound;}
};

/**
* @class ESOVariableSet
* @brief another base datastructure which includes
* values and derivative values
*/
class ESOVariableSet : public ModelVariableSet{
protected:
public:
  /** @brief standard constructor */
  ESOVariableSet(){}
  //void setValue(const utils::Array<EsoDouble> &value);
};

/*!
* @class ParameterSet
* @brief represents the Parameter section in Modelica
*/
class ParameterSet : public ESOVariableSet{
protected:
  // this string should be replaced by an enum structure.
  /** controls, optimization parameters */
  std::vector<std::string> m_type;
public:
  /** @brief standard constructor */
  ParameterSet(){}
};

/**
* @class StateVariableSet
* @brief base class for the algebraic and the differential states
*/
class StateVariableSet : public ESOVariableSet{
public:
  /** @brief standard constructor */
  StateVariableSet(){}
};

/**
* @class AlgebraicStateVariableSet
* @brief class for algebraic states
*/
class AlgebraicStateVariableSet : public StateVariableSet{
public:
  /** @brief standard constructor */
  AlgebraicStateVariableSet(){};
};

/**
* @class DifferentialStateVariableSet
* @brief class for differential states
*/
class DifferentialStateVariableSet : public StateVariableSet{
protected:
public:
  /** @brief standard constructor */
  DifferentialStateVariableSet(){};
  //void setDerivativeValue(const utils::Array<EsoDouble> &derivativeValue);
};
