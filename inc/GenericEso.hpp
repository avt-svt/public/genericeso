/**
* @file GenericEso.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* This file contains  the generic eso class                            \n
* according to the UML Diagramm METAESO_2                              \n
* =====================================================================\n
* @author Fady Assassa, Tjalf Hoffmann
* @date 14.6.2011
*/
#pragma once
#include "Equation.hpp"
#include "EsoTypes.hpp"
#include "ModelVariable.hpp"
#include "Jacobian.hpp"
#include "UtilityFunctions.hpp"
//#include "boost/shared_ptr.hpp"
#include <memory>
#include <cassert>

/**
* @class GenericEso
*
* @brief class managing Low-level structures of the MetaEso
*
* This class contains the management of all Eso related data. Calls to the Eso server are forwarded to the
* specific server. Constant data is once requested from the server at initialization and is then stored as
* attributes in this class
* Since no Eso server can be connected to this class, it may be considered as a pure virtual class
*
* @author Fady Assassa, Tjalf Hoffmann
* @date 11.7.2011
*/
class GenericEso
{
protected:

  //! total number of variables
  EsoIndex m_numVariables;
  //! total number of equations (without switching)
  EsoIndex m_numEquations;
  //! total number of switching conditions
  EsoIndex m_numSwitching;
  //! number of nonzeroes of the initial Jacobian
  EsoIndex m_numInitialNonZeroes;

  //! all differential equations of the model
  DifferentialEquationSet m_differentialEquations;
  //! all algebraic equations of the model
  AlgebraicEquationSet m_algebraicEquations;

  //! all algebraic states of the model
  AlgebraicStateVariableSet m_algebraicStateVariables;
  //! all differential states of the model
  DifferentialStateVariableSet m_differentialStateVariables;
  //! all parameters of the model
  ParameterSet m_parameters;
  //! all states of the model
  StateVariableSet m_stateVariables;

  //! Jacobian struct and values
  Jacobian m_jacobian;
  //! Differential Jacobian struct and values
  Jacobian m_diffJacobian;
  //! variable names used in some functions
  std::vector<std::string> m_varNames;
public:
  typedef std::shared_ptr<GenericEso> Ptr;
  //! @brief standard constructor
  GenericEso(){}
  //! @brief destructor
  virtual ~GenericEso();
  enum RetFlag{
    OK, //successful result
    FAIL //failure happened
  };

  class Error : public std::exception {
  public:
	  Error(const std::string& message) : message(message) {}
    virtual  ~Error() override;
	  virtual const char* what() const noexcept override {
		  return message.c_str();
	  }
  private:
	  std::string message;
  };

  virtual EsoIndex getNumVariables() const;
  virtual EsoIndex getNumDifferentialVariables() const;
  virtual EsoIndex getNumParameters() const;
  virtual EsoIndex getNumAlgebraicVariables() const;
  virtual EsoIndex getNumStates() const;
  virtual EsoIndex getNumEquations() const;
  virtual EsoIndex getNumDiffEquations() const;
  virtual EsoIndex getNumAlgEquations() const;
  virtual EsoIndex getNumNonZeroes() const;
  virtual EsoIndex getNumInitialNonZeroes() const;
  virtual EsoIndex getNumDifferentialNonZeroes() const;
  virtual EsoIndex getNumConditions() const;
  virtual EsoType getType() const = 0;

  /**
  * @brief set the independent variable (time) to a new value
  *
  * @param[in] var new value of the independent variable
  */
  virtual void setIndependentVariable(const EsoDouble var) = 0;

  /**
  * @brief get the name of loaded model
  *
  * @return name of the loaded model
  */

  virtual ModelName getModel() const = 0;

  /**
  * @brief get the current value of the independent variable(time)
  *
  * @return current value of the independent variable
  */
  virtual EsoDouble getIndependentVariable() const = 0;

  /**
  * @brief get the names of all variables of the model
  *
  * @param[out] names vector receiving the variable names - must be of size getNumVariables
  */
  virtual void getVariableNames(std::vector<std::string> &names) = 0;

  /**
  * @brief set the values of all variables
  *
  * @param[in] variables array containing the values of the variables to be set -
                     must be of size getNumVariables
  */
  virtual void setAllVariableValues(const EsoIndex n_var, const EsoDouble *variables) = 0;

  /**
  * @brief set the value of the parameters
  *
  * @param[in] parameter array containing the values of the variables to be set -
                             must be of size getNumParameters
  */
  virtual void setParameterValues(const EsoIndex n_pars, const EsoDouble *parameters) = 0;

  /**
  * @brief set the value of the algebraic variables
  *
  * @param[in] algebraicVariables array containing the values of the  variables to be set -
                              must be of size getNumAlgebraicVariables
  */
  virtual void setAlgebraicVariableValues(const EsoIndex n_alg_var, const EsoDouble *algebraicVariables) = 0;

  /**
  * @brief set the value of the differential variables
  *
  * @param[in] differentialVariables array containing the values of the variables to be set -
                                 must be of size getNumDifferentialVariables
  */
  virtual void setDifferentialVariableValues(const EsoIndex n_diff_var, const EsoDouble *differentialVariables) = 0;

  /**
  * @brief set the values of all states
  *
  * @param[in] states array containing the values of the states to be set - must be of size getNumStates
  */
  virtual void setStateValues(const EsoIndex n_states, const EsoDouble *states) = 0;

  /**
  * @brief set the values of a subset of variables
  *
  * @param[in] variables array containing the values of the variables to be set -
  *                  must be at least of the size of parameter indices
  * @param[in] indices vector containing the indices for the variables to be set
  */
  virtual void setVariableValues(const EsoIndex n_idx, const EsoDouble *variables, const EsoIndex *indices) = 0;

  /**
  * @brief getter for the values of all variables
  *
  * @param[out] variables array receiving the values of the variables -
  *                  must be of size getNumVariables
  */
  virtual void getAllVariableValues(const EsoIndex n_var, EsoDouble *variables) const = 0;

  /**
  * @brief getter for the values of the parameters
  *
  * @param[out] parameter array receiving the values of the parameters -
  *                          must be of size getNumParameters
  */
  virtual void getParameterValues(const EsoIndex n_params, EsoDouble *parameters) = 0;

  /**
  * @brief getter for the values of the algebraic variables
  *
  * @param[out] algebraicVariables array receiving the values of the algebraic variables -
  *                           must be of size getNumAlgebraicVariables
  */
  virtual void getAlgebraicVariableValues(const EsoIndex n_alg_var, EsoDouble *algebraicVariables) = 0;

  /**
  * @brief getter for the values of the differential variables
  *
  * @param[out] differentialVariables array receiving the values of the differential variables -
  *                              must be of size getNumDifferentialVariables
  */
  virtual void getDifferentialVariableValues(const EsoIndex n_diff_var, EsoDouble *differentialVariables) = 0;

  /**
  * @brief get the values of all states
  *
  * @param[out] states array receiving the values of the states to be set - must be of size getNumStates
  */
  virtual void getStateValues(const EsoIndex n_states, EsoDouble *states) = 0;
  
  /**
  * @brief get the values of all states - use initial equations to reset states
  *
  * @param[out] states array receiving the values of the states to be set - must be of size getNumStates
  */
  virtual void getInitialStateValues(const EsoIndex n_states, EsoDouble *states) = 0;

  /**
  * @brief getter for the values of a subset of all variables
  *
  * @param[out] variables array receiving the values of the variables -
  *                  must be at least of size of parameter indices
  * @param[in] indices vector containing the indices of the variables to be received
  */
  virtual void getVariableValues(const EsoIndex n_idx, EsoDouble *variables, const EsoIndex *indices) = 0;

  /**
  * @brief set the values of all derivatives
  *
  * Only the derivatives of the differential variables are set.
  * @param[in] derivatives array containing the values of the derivatives to be set -
  *                    must be of size getNumDifferentialVariables
  */
  virtual void setDerivativeValues(const EsoIndex n_diff_var, const EsoDouble *derivatives) = 0;

  /**
  * @brief getter for the values of all derivatives
  *
  * Only the derivatives of the differential variables are returned
  * @param[out] derivatives array receiving the values of the derivatives to be set -
  *                    must be of size getNumDifferentialVariables
  */
  virtual void getDerivativeValues(const EsoIndex n_diff_var, EsoDouble *derivatives) = 0;

  /**
  * @brief getter for all variable bounds
  *
  * @param[out] lowerBounds vector receiving the lower bounds of all variables -
  *                    must be of size getNumVariables
  * @param[out] upperBounds vector receiving the upper bounds of all variables -
  *                    must be of size getNumVariables
  */
  virtual void getAllBounds(const EsoIndex n_var, EsoDouble *lowerBounds, EsoDouble *upperBounds) = 0;

  /**
  * @brief getter for the residuals of all equations
  *
  * @param[out] residuals array receiving the residuals - must be of size getNumEquations
  */
  virtual GenericEso::RetFlag getAllResiduals(const EsoIndex n_eq, EsoDouble *residuals) = 0;

  /**
  * @brief getter for the residuals of all differential equations
  *
  * @param[out] differentialResiduals array receiving the residuals -
  *                              must be of size getNumDifferentialEquations
  */
  virtual GenericEso::RetFlag getDifferentialResiduals(const EsoIndex n_diff_eq, EsoDouble *differentialResiduals) = 0;

  /**
  * @brief getter for the residuals of all algebraic equations
  *
  * @param[out] algebraicResiduals array receiving the residuals - must be of size getNumAlgebraicEquations
  */
  virtual GenericEso::RetFlag getAlgebraicResiduals(const EsoIndex n_alg_eq, EsoDouble *algebraicResiduals) = 0;

  /**
  * @brief getter for the residuals of a subset of equations
  *
  * @param[out] residuals array receiving the residuals - must be at least of the size of equationIndex
  * @param[in] equationIndex vector containing the indices of the equations to be returned
  */
  virtual GenericEso::RetFlag getResiduals(const EsoIndex n_eq, EsoDouble *residuals, const EsoIndex *equationIndex) = 0;

  /**
  * @brief get the Jacobian from the initial ESO
  *
  * @param[in] n_nz number of initial nonzeroes
  * @param[out] rowIndices row indices of initial Jacobian (must be of size n_nz)
  * @param[out] colIndices column indices of initial Jacobian (must be of size n_nz)
  * @param[out] values row values of initial Jacobian (must be of size n_nz)
  * @return flag indicating whether everything went right
  */
  virtual GenericEso::RetFlag getInitialJacobian(const EsoIndex n_nz, EsoIndex *rowIndices,
                                                 EsoIndex *colIndices, EsoDouble *values) = 0;
  
  /**
  * @brief initialize differential variables with user defined values
  *
  * override initial values of the differential variables in the model's initial equations
  * if a variable is initialized to a constant value in the initial sections there 
  * will be no problem doing so. Variables initialized by a model parameter can only be overridden,
  * if the parameter is not used as a optimization variable and will remain unset.
  * The 
  * @param numDiffVals number of differential variables being set
  * @param diffIndices eso indices of the differential variables being set (length of numDiffVals)
  * @param diffValues new initial values of the differential variables
  * @param numParameters number of used parameters
  * @param parameterIndices indices of used parameters
  * @return GenericEso::OK if setting of values is not problematic, GenericEso::FAIL if dependencies
  *         of parameters lead to possible inconsistencies
  */
  virtual GenericEso::RetFlag setInitialValues(const EsoIndex numDiffVals,
                                                     EsoIndex *diffIndices,
                                                     EsoDouble * diffValues,
                                               const EsoIndex numParameters,
                                                     EsoIndex *parameterIndices) = 0;
  
  virtual void getJacobianStruct(const EsoIndex n_nz, EsoIndex *rowIndices, EsoIndex *colIndices);

  /**
  * @brief getter for all Jacobian values
  *
  * @param[out] values array receiving the Jacobian values in COO sparse format
  * @sa getJacobianStruct
  */
  virtual GenericEso::RetFlag getJacobianValues(const EsoIndex n_nz, EsoDouble *values) = 0;

  /**
  * @brief getter for a subset of Jacobian values
  *
  * @param[out] values array receiving the Jacobian values
  * @param[in] indices vector containing the eso indices of the subset -
                   each etnry must be in range [0, numNonZeroes-1]
  */
  virtual GenericEso::RetFlag getJacobianValues(const EsoIndex n_idx, EsoDouble *values, const EsoIndex *indices) = 0;

  virtual void getDiffJacobianStruct(const EsoIndex n_diff_nz, EsoIndex *rowIndices, EsoIndex *colIndices);

  /**
  * @brief getter for all differential Jacobian data of the model
  *
  * @param[out] values array receiving the differential Jacobian values in COO sparse format
  * @sa getDiffJacobianStruct
  */
  virtual GenericEso::RetFlag getDiffJacobianValues(const EsoIndex n_diff_nz, EsoDouble *values) = 0;

  /**
  * @brief calculation of a jacobian-vector product
  *
  * If transpose equals false, the forward jacobian vector product (tangent-linear TL)
  * y = d(res)/dstates * seedStates + d(res)/dparams * seedParameters is calculated.
  * If transpose equals true, the backward jacobian vector product (adjoint mode AD)
  * seedStates = [d(res)/dstates]^T * y, and
  * seedParameters = [d(res)/dparams]^T * y is calculated.
  *
  * @param[in] transpose true for backward jacobian vector product false and for forward jacobian
  *            vector product
  * @param[in] n_states length of array seedStates
  * @param[in/out] seedStates array multiplying d(res)/dstates in TL mode or receiving
  *                [d(res)/dstates]^T * y in AD mode
  * @param[in] n_params length of array seedParameters
  * @param[in/out] seedParameters array multiplying d(res)/dparams in TL mode or receiving
  *                [d(res)/dparams]^T * y in AD mode
  * @param[in] n_y length of array y
  * @param[out/in] y array storing the result in TL mode or vector multiplying
  *                [d(res)/dstates]^T and [d(res)/dparams]^T in AD mode
  * @todo to implement
  */
  virtual GenericEso::RetFlag getJacobianMultVector(const bool transpose, const EsoIndex n_states, EsoDouble *seedStates,
                             const EsoIndex n_params, EsoDouble *seedParameters, const EsoIndex n_y,
                             EsoDouble *y)
  {
    //transpose, n_states, seedStates, n_params, seedParameters, n_y, y;// delete this line after implementation.
    throw(std::exception());
  }

  /**
   * \defgroup Switching function stuff
   * @{
   */

  /**
   * @brief Evaluate the residual of the switching functions
   *
   * Switching functions are used for dynamic systems with discontinuous right-hand sides.
   * This function returns the residual.
   * A dummy implementation is given that just makes sure that n_c is zero.
   *
   * @param[in] number of switching functions (for testing)
   * @param[out] residual of the switching function
   */
  virtual GenericEso::RetFlag getConditionResiduals(const EsoIndex n_c, EsoDouble* /* residuals */) {
    assert(n_c==0);
    return GenericEso::OK;
  }

  virtual void setConditions(const EsoIndex n_c, const EsoIndex* /* conditions */) {
    assert(n_c==0);
  }

  /**
   * @brief get the conditions which are currently set
   */
  virtual void getCurrentConditions(const EsoIndex n_c, EsoIndex* /* condtitions */) {
    assert(n_c==0);
  }

  /**
   * @brief Set the locks for the switching functions.
   *
   * The result is that all following computations will be done using the current m_conditions
   * instead of the actual ones for computations.
   *
   * A dummy implementeation is provided for non-switching systems.
   */
  virtual void setLocks(const EsoIndex n_c, const EsoIndex* /* locks */) {
    assert(n_c==0);
  }

  virtual void getLocks(const EsoIndex n_c, EsoIndex* /* locks */) {
    assert(n_c==0);
  }

  virtual void evalConditions(const EsoIndex n_c, EsoIndex* /* conditions */) {
    assert(n_c==0);
  }
  /**@}*/



  virtual void getParameterIndex(const EsoIndex n_para, EsoIndex *parameterIndex);
  virtual void getDifferentialIndex(const EsoIndex n_diff_var, EsoIndex *differentialIndex);
  virtual void getAlgebraicIndex(const EsoIndex n_alg_var, EsoIndex *algebraicIndex);
  virtual void getStateIndex(const EsoIndex n_states, EsoIndex *stateIndex);
  virtual void getDiffEquationIndex(const EsoIndex n_diff_eq, EsoIndex *diffEqIndex);
  virtual void getAlgEquationIndex(const EsoIndex n_alg_eq, EsoIndex *algEqIndex);

  //utility functions
  virtual EsoIndex getEsoIndexOfVariable(const std::string &varName);
  virtual std::string getVariableNameOfIndex(const EsoIndex esoIndex);
  virtual EsoIndex getStateIndexOfVariable(const EsoIndex esoIndex);


  virtual GenericEso::RetFlag solveAlgebraicEquations() {
	  return GenericEso::OK;
  }

  virtual GenericEso::RetFlag solveDifferentialEquations() {
	  return GenericEso::OK;
  }

};

class GenericEso2ndOrder : public GenericEso
{
public:
  typedef std::shared_ptr<GenericEso2ndOrder> Ptr;
  virtual void evaluate2ndOrderDerivatives(unsigned lenT2States, double *t2States,
                                           unsigned lenT2DerStates, double *t2DerStates,
                                           unsigned lenT2P, double *t2P,
                                           unsigned lenA1States, double *a1States,
                                           unsigned lenA1DerStates, double *a1DerStates,
                                           unsigned lenA1P, double *a1P,
                                           unsigned lenT2A1States, double *t2A1States,
                                           unsigned lenT2A1DerStates, double *t2A1DerStates,
                                           unsigned lenT2A1P, double *t2A1P,
                                           unsigned lenA1Yy, double *a1Yy,
                                           unsigned lenT2A1Yy, double *t2A1Yy) = 0;

  virtual void evaluate1stOrdForwDerivatives(unsigned lenT1States, double *t1States,
                                             unsigned lenT1DerStates, double *t1DerStates,
                                             unsigned lenT1P, double *t1P,
                                             unsigned lenT1Res, double *t1Residuals) = 0;
};


//utility functions
std::ostream& operator<<(std::ostream& out, GenericEso &gEso);
