/**
* @file MADOGenericEso.testsuite
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Systemverfahrennstechnik, RWTH Aachen          \n
* =====================================================================\n
* Generic ESO - Part of DyOS                                           \n
* =====================================================================\n
* In this testsuite the extended WO batch model is tested              \n
* =====================================================================\n
*
* @author Lars Henrichfreise
* @date 05.02.2020
*/




#ifndef MADO_GENERIC_ESO
#define MADO_GENERIC_ESO

#include "GenericEso.hpp"
#include "DyosMADOLoader.hpp"
#include <sstream>




class MADOGenericEso : public GenericEso2ndOrder {

protected:

  MADO* m_myMADO = nullptr;
  ModelName model_name;
  //double* m_x; // differential + algebraic + parameters + derivatives
  std::vector<double> m_x;

  EsoIndex m_numStates;
  EsoIndex m_numDiffVariables;
  EsoIndex m_numAlgVariables;
  EsoIndex m_numParameters;
  EsoIndex m_numDiffEquations;
  EsoIndex m_numAlgEquations;
  EsoIndex m_numNonZeros;
  EsoIndex m_numDiffNonZeros;
  EsoIndex m_numAllVariables;

  std::vector<std::string> m_variableNames;

  std::vector<unsigned int> m_indexDiffEqn;
  std::vector<unsigned int> m_indexAlgEqn;
 

  bool m_jacobianStructKnown = false;
  bool m_jacobianDiffStructKnown = false;

  std::vector<EsoIndex> m_rowIndicesJacStruct;
  std::vector<EsoIndex> m_colIndicesJacStruct;

  std::vector<EsoIndex> m_rowIndicesDiffJacStruct;
  std::vector<EsoIndex> m_colIndicesDiffJacStruct;


  //double** m_J_dense; //Jacobian with derivatives in respect to all (including derivatives of differential variables) variables (with zero entries)
  //double* m_J_derDiff=NULL; //Jacobian with derivatives in respect to all derivatives of differential states (without zero entries)
  //double* m_J=NULL; //Jacobian with derivatives in respect to all variables (including derivatives of differential variables) (without zero entries)
  std::vector<double> m_J_derDiff;
  std::vector<double> m_J;

  std::vector<unsigned int> m_rind;
  std::vector<unsigned int> m_cind;
  std::vector<unsigned int> m_rind_diff;
  std::vector<unsigned int> m_cind_diff;
  
  
//  unsigned int* m_rind_J = NULL;
//  unsigned int* m_cind_J = NULL;


  MADOLoader madoLoader;


public:

	MADOGenericEso(const std::string& libname = "modelWoBatch"); 

  ~MADOGenericEso() override;

  /**
    * @brief get the total number of variables
    *
    * @return total number of variables
    */
  EsoIndex getNumVariables() const override;

  /**
    * @brief get the number of differential variables
    *
    * @return number of differential variables
    */
  EsoIndex getNumDifferentialVariables() const override;

  /**
    * @brief get the number of parameter variables
    *
    * @return number of parameter variables
    */
  virtual EsoIndex getNumParameters() const override;

  /**
    * @brief get the number of algebraic variables
    *
    * @return number of algebraic variables
    */
  EsoIndex getNumAlgebraicVariables() const override;

  /**
    * @brief get the number of state variables
    *
    * @return number of state variables
    */
  EsoIndex getNumStates() const override;

  /**
    * @brief get the number of differential equations
    *
    * @return number of differential equations
    */
  EsoIndex getNumEquations() const override;

  /**
    * @brief get the total number of equations
    *
    * @return total number of equations
    */
  EsoIndex getNumDiffEquations() const override;

  /**
    * @brief get the number of algebraic equations
    *
    * @return number of algebraic equations
    */
  EsoIndex getNumAlgEquations() const override;

  /**
    * @brief get the current value of the independent variable(time)
    *
    * @return current value of the independent variable
    */
  EsoIndex getNumNonZeroes() const override;

  /**
    * @brief get the current value of the independent variable(time)
    *
    * @return current value of the independent variable
    */
  EsoIndex getNumDifferentialNonZeroes() const override;

  /**
    * @brief get the current value of the independent variable(time)
    *
    * @return current value of the independent variable
    */
  EsoIndex getNumConditions() const override;

  /**
  * @brief get the names of all variables of the model
  *
  * @param[out] names vector receiving the variable names - must be of size getNumVariables
  */
  void getVariableNames(std::vector<std::string> &names) override;
  /**
    * @brief set the values of all variables
    *
    * @param[in] variables array containing the values of the variables to be set -
  //				   must be of size getNumVariables
    */

   void setAllVariableValues(const EsoIndex n_var, const EsoDouble *variables) override;

  /**
    * @brief set the value of the parameters
    *
    * @param[in] parameter array containing the values of the variables to be set -
  //						   must be of size getNumParameters
    */

   void setParameterValues(const EsoIndex n_pars, const EsoDouble *parameters) override;

  /**
    * @brief set the value of the algebraic variables
    *
    * @param[in] algebraicVariables array containing the values of the  variables to be set -
  //							must be of size getNumAlgebraicVariables
    */
   void setAlgebraicVariableValues(const EsoIndex n_alg_var, const EsoDouble *algebraicVariables) override;

  /**
    * @brief set the value of the differential variables
    *
    * @param[in] differentialVariables array containing the values of the variables to be set -
  //							   must be of size getNumDifferentialVariables
    */
   void setDifferentialVariableValues(const EsoIndex n_diff_var, const EsoDouble *differentialVariables) override;

  /**
    * @brief set the values of all states
    *
    * @param[in] states array containing the values of the states to be set -
    *               must be of size getNumStates
    */
  void setStateValues(const EsoIndex n_states, const EsoDouble *states) override;

  /**
    * @brief set the values of a subset of variables
    *
    * @param[in] variables array containing the values of the variables to be set -
    *                  must be at least of the size of parameter indices
    * @param[in] indices vector containing the indices for the variables to be set
    */
   void setVariableValues(const EsoIndex n_idx, const EsoDouble *variables, const EsoIndex *indices) override;

  /**
    * @brief getter for the values of all variables
    *
    * @param[out] variables array receiving the values of the variables -
    *                  must be of size getNumVariables
    */
  void getAllVariableValues(const EsoIndex n_var, EsoDouble *variables) const override;

  /**
    * @brief getter for the values of the parameters
    *
    * @param[out] parameter array receiving the values of the parameters -
    *                          must be of size getNumParameters
    */
  void getParameterValues(const EsoIndex n_params, EsoDouble *parameters) override;

  /** @brief getter for the values of the algebraic variables
    *
    * @param[out] algebraicVariables array receiving the values of the algebraic variables -
    *                           must be of size getNumAlgebraicVariables
    */
  void getAlgebraicVariableValues(const EsoIndex n_alg_var, EsoDouble *algebraicVariables) override;

  /**
    * @brief getter for the values of the differential variables
    *
    * @param[out] differentialVariables array receiving the values of the differential variables -
    *                              must be of size getNumDifferentialVariables
    */
  void getDifferentialVariableValues(const EsoIndex n_diff_var, EsoDouble *differentialVariables) override;

  /**
    * @brief get the values of all states
    *
    * @param[out] states array receiving the values of the states to be set - must be of size getNumStates
    */
  void getStateValues(const EsoIndex n_states, EsoDouble *states) override;

  /**
    * @brief getter for the values of a subset of all variables
    *
    * @param[out] variables array receiving the values of the variables -
    *                  must be at least of size of parameter indices
    * @param[in] indices vector containing the indices of the variables to be received
    */
  void getVariableValues(const EsoIndex n_idx, EsoDouble *variables, const EsoIndex *indices) override;

  /**
    * @brief set the values of all derivatives
    *
    * Only the derivatives of the differential variables are set.
    * @param[in] derivatives array containing the values of the derivatives to be set -
    *                    must be of size getNumDifferentialVariables
    */
  void setDerivativeValues(const EsoIndex n_diff_var, const EsoDouble *derivatives) override;

  /**
    * @brief getter for the values of all derivatives
    *
    * Only the derivatives of the differential variables are returned
    * @param[out] derivatives array receiving the values of the derivatives to be set -
    *                    must be of size getNumDifferentialVariables
    */
  void getDerivativeValues(const EsoIndex n_diff_var, EsoDouble *derivatives) override;

  /**
    * @brief getter for the residuals of all equations
    *
    * @param[out] residuals array receiving the residuals - must be of size getNumEquations
    */
  GenericEso::RetFlag getAllResiduals(const EsoIndex n_eq, EsoDouble *residuals) override;

  /**
    * @brief getter for the residuals of all differential equations
    *
    * @param[out] differentialResiduals array receiving the residuals -
    *                              must be of size getNumDifferentialEquations
    */
  GenericEso::RetFlag getDifferentialResiduals(const EsoIndex n_diff_eq, EsoDouble *differentialResiduals) override;

  /**
    * @brief getter for the residuals of all algebraic equations
    *
    * @param[out] algebraicResiduals array receiving the residuals - must be of size getNumAlgebraicEquations
    */
  GenericEso::RetFlag getAlgebraicResiduals(const EsoIndex n_alg_eq, EsoDouble *algebraicResiduals) override;

  /**
    * @brief getter for the residuals of a subset of equations
    *
    * @param[out] residuals array receiving the residuals - must be at least of the size of equationIndex
    * @param[in] equationIndex vector containing the indices of the equations to be returned
    */
  GenericEso::RetFlag getResiduals(const EsoIndex n_eq, EsoDouble *residuals, const EsoIndex *equationIndex) override;

  virtual void getJacobianStruct(const EsoIndex n_nz, EsoIndex *rowIndices, EsoIndex *colIndices) override;

  /**
    * @brief getter for all Jacobian values
    *
    * @param[out] values array receiving the Jacobian values in COO sparse format
    * @sa getJacobianStruct
    */
  virtual GenericEso::RetFlag getJacobianValues(const EsoIndex n_nz, EsoDouble *values) override;

  /**
    * @brief getter for a subset of Jacobian values
    *
    * @param[out] values array receiving the Jacobian values
    * @param[in] indices vector containing the eso indices of the subset -
  //				 each etnry must be in range [0, numNonZeroes-1]
    */
  virtual GenericEso::RetFlag getJacobianValues(const EsoIndex n_idx, EsoDouble *values,
                                                const EsoIndex *indices) override;

  virtual void getDiffJacobianStruct(const EsoIndex n_diff_nz, EsoIndex *rowIndices, EsoIndex *colIndices) override;

  /**
    * @brief getter for all differential Jacobian data of the model
    *
    * @param[out] values array receiving the differential Jacobian values in COO sparse format
    * @sa getDiffJacobianStruct
    */
  virtual GenericEso::RetFlag getDiffJacobianValues(const EsoIndex n_diff_nz, EsoDouble *values) override;

  virtual void getParameterIndex(const EsoIndex n_para, EsoIndex *parameterIndex) override;

  virtual void getDifferentialIndex(const EsoIndex n_diff_var, EsoIndex *differentialIndex) override;

  virtual void getAlgebraicIndex(const EsoIndex n_alg_var, EsoIndex *algebraicIndex) override;

  virtual void getStateIndex(const EsoIndex n_states, EsoIndex *stateIndex) override;

  virtual void getDiffEquationIndex(const EsoIndex n_diff_eq, EsoIndex *diffEqIndex) override;

  virtual void getAlgEquationIndex(const EsoIndex n_alg_eq, EsoIndex *algEqIndex) override;

  ////utility functions
  virtual EsoIndex getEsoIndexOfVariable(const std::string &varName) override;
  
  virtual std::string getVariableNameOfIndex(const EsoIndex esoIndex) override;

  virtual EsoIndex getStateIndexOfVariable(const EsoIndex esoIndex) override;

  virtual EsoType getType() const override;

  virtual ModelName getModel() const override;







  /**
   * @brief get the current value of the independent variable(time)
   *
   * @return current value of the independent variable
   */
  EsoIndex getNumInitialNonZeroes() const override;

  /**
  * @brief set the independent variable (time) to a new value
  *
  * @param[in] var new value of the independent variable
  */
  void setIndependentVariable(const EsoDouble var) override;

  /**
	* @brief get the current value of the independent variable(time)
	*
	* @return current value of the independent variable
	*/

  EsoDouble getIndependentVariable() const override;

  /**
  * @brief getter for all variable bounds
  *
  * @param[out] lowerBounds vector receiving the lower bounds of all variables -
  *                    must be of size getNumVariables
  * @param[out] upperBounds vector receiving the upper bounds of all variables -
  *                    must be of size getNumVariables
  */
  void getAllBounds(const EsoIndex n_var, EsoDouble *lowerBounds, EsoDouble *upperBounds) override;

  /**
   * @brief get the values of all states - use initial equations to reset states
   *
   * @param[out] states array receiving the values of the states to be set - must be of size getNumStates
   */
  void getInitialStateValues(const EsoIndex n_states, EsoDouble *states) override;

  /**
  * @brief get the Jacobian from the initial ESO
  *
  * @param[in] n_nz number of initial nonzeroes
  * @param[out] rowIndices row indices of initial Jacobian (must be of size n_nz)
  * @param[out] colIndices column indices of initial Jacobian (must be of size n_nz)
  * @param[out] values row values of initial Jacobian (must be of size n_nz)
  * @return flag indicating whether everything went right
  */
  GenericEso::RetFlag getInitialJacobian(const EsoIndex n_nz, EsoIndex *rowIndices,
	  EsoIndex *colIndices, EsoDouble *values) override;
  /**
	* @brief initialize differential variables with user defined values
	*
	* override initial values of the differential variables in the model's initial equations
	* if a variable is initialized to a constant value in the initial sections there
	* will be no problem doing so. Variables initialized by a model parameter can only be overridden,
	* if the parameter is not used as a optimization variable and will remain unset.
	* The
	* @param numDiffVals number of differential variables being set
	* @param diffIndices eso indices of the differential variables being set (length of numDiffVals)
	* @param diffValues new initial values of the differential variables
	* @param numParameters number of used parameters
	* @param parameterIndices indices of used parameters
	* @return GenericEso::OK if setting of values is not problematic, GenericEso::FAIL if dependencies
	*         of parameters lead to possible inconsistencies
	*/
  GenericEso::RetFlag setInitialValues(const EsoIndex numDiffVals, EsoIndex *diffIndices,
	  EsoDouble * diffValues, const EsoIndex numParameters,
	  EsoIndex *parameterIndices) override;

  virtual GenericEso::RetFlag getJacobianMultVector(const bool transpose, const EsoIndex n_states, EsoDouble *seedStates,
	  const EsoIndex n_params, EsoDouble *seedParameters, const EsoIndex n_y,
	  EsoDouble *y) override;

  /**
   * @brief Calculates second-order tangent-linear over adjoint derivatives of the DA-System.
   *
   * This functions calls the code generated by dcc calculating second-order tangent-linear over
   * adjoint derivatives. The actual values of the states, the parameters and the derivated states are
   * taken from the members m_states, m_parametersACS and m_der_states.
   * For explanation: z = [states p der_states]^T,
   *                  all arrays with suffix '_states' or '_der_states' are of size (number of states)
   *                  all arrays with suffix '_p' are of size (number of parameters)
   *                  all arrays with suffix '_yy' or '_residuals' are of size (number of equations)
   *                  <.,.> represents the inner dot product with appropriate dimensions.
   *
   * output definition: a1_z = (dF(z)/dz)^T * a1Yy;
   *                    t2_a1_z = <d2F(z)/dzdz, a1Yy, t2_z> + <dF(z)/dz, t2_a1Yy>;
   *
   * @param[in] lenT2States length of t2States array. Must be equal to m_n_states
   * @param[in] t2States array that defines the first m_n_states entries of t2_z
   * @param[in] lenT2DerStates length of t2DerStates arry. Must be equal to m_n_states
   * @param[in] t2DerStates array that defines the last m_n_states entries of t2_z
   * @param[in] lenT2P length of t2P array. Must be equal to m_n_pars
   * @param[in] t2P array that defines the entries from index m_n_states til index
   *            (m_n_states+m_n_pars-1) of t2_z
   * @param[in] lenA1States length of a1States array. Must be equal to m_n_states
   * @param[out] a1States contains on output the product of the transposed Jacobian, relative to the
   *             states, with a1Yy
   * @param[in] lenA1DerStates length of a1DerStates array. Must be equal to m_n_states
   * @param[out] a1DerStates contains on output the product of the transposed Jacobian, relative to
   *             the derivated states, with a1Yy
   * @param[in] lenA1P length of a1P array. Must be equal to m_n_pars
   * @param[out] a1P contains on output the product of the transposed Jacobian, relative to the
   *             parameters, with a1Yy
   * @param[in] lenT2A1States length of t2A1States array. Must be equal to m_n_states
   * @param[out] t2A1States array that defines the first m_n_states entries of output array
   *             t2_a1_z
   * @param[in] lenT2A1DerStates length of t2A1DerStates array. Must be equal to m_n_states
   * @param[out] t2A1DerStates array that defines the last m_n_states entries of output array
   *             t2_a1_z
   * @param[in] lenT2A1P length of t2A1P array. Must be equal to m_n_pars
   * @param[out] t2A1P array that defines the entries from index m_n_states til index
   *            (m_n_states+m_n_pars-1) of output array t2_a1_z
   * @param[in] lenA1Yy length of a1Yy array. Must be equal to m_n_eq
   * @param[in] a1Yy seed vector multiplying the Jacobian matrix to calculate a1_z and the Hessian
   *            matrix to calculate t2_a1_z.
   * @param[in] lenT2A1Yy length of t2A1Yy array. Must be equal to m_n_eq
   * @param[in] t2A1Yy seed vector to define the jacobian part of t2_a1_z
   */
  void evaluate2ndOrderDerivatives(unsigned lenT2States, double *t2States,
	  unsigned lenT2DerStates, double *t2DerStates,
	  unsigned lenT2P, double *t2P,
	  unsigned lenA1States, double *a1States,
	  unsigned lenA1DerStates, double *a1DerStates,
	  unsigned lenA1P, double *a1P,
	  unsigned lenT2A1States, double *t2A1States,
	  unsigned lenT2A1DerStates, double *t2A1DerStates,
	  unsigned lenT2A1P, double *t2A1P,
	  unsigned lenA1Yy, double *a1Yy,
      unsigned lenT2A1Yy, double *t2A1Yy) override;


/**
 * @brief Calculates first-order tangent-linear derivatives of the DA-System.
 *
 * This functions calls the code generated by dcc calculating first-order forward derivatives.
 * The actual values of the states, the parameters and the derivated states are taken from the
 * members m_states, m_parametersACS and m_der_states.
 * For explanation: z = [states p der_states]^T
 *                  all arrays with suffix '_states' or '_der_states' are of size (number of states)
 *                  all arrays with suffix '_p' are of size (number of parameters)
 *                  all arrays with suffix '_yy' or '_residuals' are of size (number of equations)
 *
 * output definition: t1_residuals = dF(z)/dz * t1_z
 *
 * @param[in] lenT1States length of t2States array. Must be equal to m_n_states
 * @param[in] t1States seed vector multiplying the Jacobian part relative to the states
 * @param[in] lenT1DerStates length of t2States array. Must be equal to m_n_states
 * @param[in] t1DerStates seed vector multiplying the Jacobian part relative to the derivated
 *            states
 * @param[in] lenT1P length of t2States array. Must be equal to m_n_pars
 * @param[in] t1P seed vector multiplying the Jacobian part relative to the parameters
 * @param[in] lenT1Res length of t2States array. Must be equal to m_n_eq
 * @param[out] t1Residuals product of the jacobian matrix and the seed vectors
 */
  void evaluate1stOrdForwDerivatives(unsigned lenT1States, double *t1States,
	  unsigned lenT1DerStates, double *t1DerStates,
	  unsigned lenT1P, double *t1P,
      unsigned lenT1Res, double *t1Residuals) override;
};
#endif //MADO_GENERIC_ESO
