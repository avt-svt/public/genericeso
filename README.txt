This file describes the definition of the generic eso attributes and methods.

Assume we have a system of equations of the form:

	M*der(x) - f(x,y,p) = 0
	g(x,y,p) = 0

where x are the differential variables, y the algebraic variables and p the parameters (or controls).

m_numVariables:					Specifies dim(x) + dim(y) + dim(p)					Method: getNumVariables()
m_numEquations:					Specifies dim(f) + dim(g)							Method: getNumEquations()
m_numSwitching: 				Number of if-then-else conditions inside a model	Method: 

m_differentialEquations:		Specifies dim(f)									Method: getNumDiffEquations()
m_algebraicEquations:			Specifies dim(g)									Method: getNumAlgEquations()
								Specifies dim(f) + dim(g)							Method: getNumEquations()

m_algebraicStateVariables:		Specifies dim(y)									Method: getNumAlgebraicVariables()
m_differentialStateVariables:	Specifies dim(x)									Method: getNumDifferentialVariables()
m_parameters:					Specifies dim(p)									Method: getNumParameters()
m_stateVariables:				Specifies dim(x) + dim(y)							Method: getNumStates()

m_variableNames:				Vectors of strings with names of x, y, p


m_x: 							Contains [x, y, p, der_x] = [variables, der_x]


METHODS:

getJacobianValues():			Returns Jacobian values of res w.r.t. x, y, p
getDiffJacobianValues():		Returns Jacobian values of res w.r.t. der_x

setAllVariableValues():			Set values for x, y, p in m_x
setVariableValues():			Set values for some x, y, p in m_x specified by an index set indices
setDerivativeValues():			Set values for der_x in m_x

getVariableValues():			Get values for some x, y, p in m_x specified by an index set indices
getAllVariableValues():			Get values for x, y, p in m_x